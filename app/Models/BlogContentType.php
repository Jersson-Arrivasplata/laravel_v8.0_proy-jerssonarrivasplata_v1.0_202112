<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogContentType extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'name','path','description','image','type','highlight','background',
    ];

    protected $table = 'blog_content_type';

    protected $dates = ['deleted_at'];
}
