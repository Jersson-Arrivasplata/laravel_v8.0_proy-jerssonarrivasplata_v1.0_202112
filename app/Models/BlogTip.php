<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogTip extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'tip',
        'legend',
        'title',
        'description',
        'comment',
        'language',
        'type',
        'used',
        'last_used_at'
    ];

    protected $table = 'blog_tips';

    protected $dates = ['deleted_at'];
}
