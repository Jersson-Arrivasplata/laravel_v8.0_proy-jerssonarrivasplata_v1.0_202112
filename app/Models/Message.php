<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'name','email','celphone','message', 'language'
    ];

    protected $table = 'messages';

    protected $dates = ['deleted_at'];

}
