<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogSocialMedia extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'idRow', 'type', 'description', 'url'
    ];

    protected $table = 'blog_social_media';

    protected $dates = ['deleted_at'];

}
