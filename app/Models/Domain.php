<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Domain extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'name',
        'type'
    ];

    protected $table = 'domains';

    protected $dates = ['deleted_at'];
}
