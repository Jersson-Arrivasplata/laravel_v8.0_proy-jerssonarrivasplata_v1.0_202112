<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteMapAlternates extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'id_sitemaps',
        'lang',
        'url'
    ];

    protected $table = 'sitemaps_alternates';

    protected $dates = ['deleted_at'];
}
