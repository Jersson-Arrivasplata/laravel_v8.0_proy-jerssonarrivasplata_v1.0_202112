<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogContentGenerate extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'id_blog_content',
        'id_blog_content_type_group',
        'position',
        'idRow',
        'selectSocialMedia',
        'selectSocialMediaWidth',
        'selectSocialMediaHeight',
        'imgRange',
        'contentWidthRange',
        'contentHeightRange',
        'imgFontSizeRange',
        'contentTitleFontSizeRange',
        'contentBottomFontSizeRange',
        'url_linkedin',
        'description'
    ];

    protected $table = 'blog_content_generates';

    protected $dates = ['deleted_at'];

    public function contents()
    {
        return $this->hasMany('App\Models\BlogContent', 'id_blog_content_generate');
    }
}
