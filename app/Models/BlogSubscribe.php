<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogSubscribe extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'email'
    ];

    protected $table = 'blog_subscribe';

    protected $dates = ['deleted_at'];

}
