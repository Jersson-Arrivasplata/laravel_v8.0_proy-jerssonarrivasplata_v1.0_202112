<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogPage extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        "body",
        "title",
        "url",
        "meta-charset",
        "meta-name-keywords",
        "meta-name-viewport",
        "meta-name-author",
        "meta-name-description",
        "meta-name-twitter-description",
        "meta-name-twitter-title",
        "meta-name-twitter-url",
        "meta-name-twitter-card",
        "meta-name-msapplication-config",
        "meta-name-msapplication-TileImage",
        "meta-name-msapplication-TileColor",
        "meta-name-theme-color",
        "meta-name-apple-mobile-web-app-capable",
        "meta-property-og-site-name",
        "meta-property-og-url",
        "meta-property-og-title",
        "meta-property-og-description",
        "meta-property-og-image",
        "meta-http-equiv-cleartype",
        "meta-http-equiv-x-ua-compatible",
        "meta-http-equiv-expires",
        "meta-http-equiv-last-modified",
        "meta-http-equiv-cache-control",
        "meta-http-equiv-pragma"
    ];

    protected $table = 'blog_pages';

    protected $dates = ['deleted_at'];

}
