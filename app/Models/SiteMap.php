<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteMap extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'url',
        'description',
        'priority',
        'type'
    ];

    protected $table = 'sitemaps';

    protected $dates = ['deleted_at'];
}
