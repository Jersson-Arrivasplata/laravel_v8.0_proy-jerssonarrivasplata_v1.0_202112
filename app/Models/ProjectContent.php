<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectContent extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'type','name','links','description','image','url','path','folder','implementation'
    ];

    protected $table = 'project_content';

    protected $dates = ['deleted_at'];

}
