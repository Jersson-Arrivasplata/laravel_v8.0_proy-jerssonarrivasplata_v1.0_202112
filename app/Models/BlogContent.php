<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogContent extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'idRow',
        'id_blog_content_generate',
        'id_blog_content_type',
        'id_blog_content_type_group',
        'position',
        'height',
        'checkPreviewBox',
        'checkImageBox',
        'contentfontSizeRange',
        'contentMargintop',
        'contentPaddingLeft',
        'contentPaddingRight',
        'checkContentBorderLeft',
        'checkContentBorderRight',
        'checkContentDirectionRight',
        'imgTitle',
        'imgSubTitle',
        'contentTitle',
        'contentSubTitle',
        'bgColor',
        'typeLanguage',
        'typeLanguageBgColor',
        'typeLanguageTitle',
        'typeLanguageSubTitle',
        'codes',
        'before',
        'activated'
    ];

    protected $attributes = [
        'activated' => false,
    ];

    protected $table = 'blog_content';

    protected $dates = ['deleted_at'];

    public function contents()
    {
        return $this->belongsTo('App\Models\BlogContentType', 'id_blog_content_type', 'type');
    }

    public function socials()
    {
        return $this->hasMany('App\Models\BlogSocialMedia', 'idRow', 'idRow');
    }
}
