<?php

namespace App\View\Components;

use Illuminate\View\Component;

class HtmlHead extends Component
{
    public $link;
    public $linkImage;
    public $color;
    public $googleTagManagerId;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($link, $linkImage, $color, $googleTagManagerId)
    {
        //
        $this->link = $link;
        $this->linkImage = $linkImage;
        $this->color = $color;
        $this->googleTagManagerId = $googleTagManagerId;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.html-head');
    }
}
