<?php

namespace App\View\Components;

use Illuminate\View\Component;

class LinkSocialMedia extends Component
{
    public $link;
    public $linkImage;
    public $text;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($link, $linkImage, $text)
    {
        //
        $this->link = $link;
        $this->linkImage = $linkImage;
        $this->text = $text;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.link-social-media');
    }
}
