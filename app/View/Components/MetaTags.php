<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MetaTags extends Component
{
    public $title;
    public $description;
    public $link;
    public $ogType;
    public $twitterContentCard;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $description,$link,$ogType,$twitterContentCard)
    {
        //
        $this->title = $title;
        $this->description = $description;
        $this->link = $link;
        $this->ogType = $ogType;
        $this->twitterContentCard = $twitterContentCard;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.meta-tags');
    }
}
