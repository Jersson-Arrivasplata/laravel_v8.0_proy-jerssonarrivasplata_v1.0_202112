<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Subscriber extends Component
{
    public $link;
    public $title;
    public $subtitle;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($link, $title, $subtitle)
    {
        //
        $this->link = $link;
        $this->title = $title;
        $this->subtitle = $subtitle;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.subscriber');
    }
}
