<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CardImage extends Component
{
    public $title;
    public $subtitle;
    public $link;
    public $linkImage;
    public $counter;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $subtitle, $link, $linkImage, $counter)
    {
        //
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->link = $link;
        $this->linkImage = $linkImage;
        $this->counter = $counter;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.card-image');
    }
}
