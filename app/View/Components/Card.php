<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Card extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $title;
    public $subtitle;
    public $description;
    public $link;
    public $linkImage;
    public $counter;

    public function __construct($title, $subtitle, $description, $link, $linkImage, $counter)
    {
        //
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->description = $description;
        $this->link = $link;
        $this->linkImage = $linkImage;
        $this->counter = $counter;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.card');
    }
}
