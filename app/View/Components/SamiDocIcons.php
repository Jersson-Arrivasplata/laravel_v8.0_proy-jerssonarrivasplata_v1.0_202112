<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SamiDocIcons extends Component
{

    public $id;
    public $content;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id, $content)
    {
        //
        $this->id = $id;
        $this->content = $content;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.sami-doc-icons');
    }
}
