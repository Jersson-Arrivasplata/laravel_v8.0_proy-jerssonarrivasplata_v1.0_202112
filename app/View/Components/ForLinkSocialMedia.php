<?php

namespace App\View\Components;

use App\Classes\Utils;
use Illuminate\View\Component;

class ForLinkSocialMedia extends Component
{
    public $content;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        //
        $this->content = $content;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $return = "";
        foreach (json_decode($this->content) as $data) {
            if ($data->type == 'github') {
                $return .= '<x-link-social-media
                link="' . $data->url . '"
                link-image="' . Utils::getUrlBase() . '/project/' . $data->urlImage . '"
                text="' . $data->name . '"
                >
                </x-link-social-media>';
            }
        }
        return $return;
    }
}
