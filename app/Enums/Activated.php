<?php
// Crear clase Status
namespace App\Enums;

final class Activated
{
    const Active = true;
    const Inactive = false;
    const All = 'all';

    /**
     * Devuelve todos los estados posibles como un array.
     *
     * @return array
     */
    public static function getAllStatuses()
    {
        return [
            self::Active,
            self::Inactive,
            self::All,
        ];
    }

    /**
     * Verifica si el valor dado es un estado válido.
     *
     * @param string $value
     * @return bool
     */
    public static function isValidStatus($value)
    {
        return in_array($value, self::getAllStatuses(), true);
    }
}
