<?php
// Crear clase Status
namespace App\Enums;

final class TypeAI
{
    const OpenAI = 0;
    const SAI = 1;

    /**
     * Devuelve todos los estados posibles como un array.
     *
     * @return array
     */
    public static function getAllStatuses()
    {
        return [
            self::OpenAI,
            self::SAI
        ];
    }

    /**
     * Verifica si el valor dado es un estado válido.
     *
     * @param string $value
     * @return bool
     */
    public static function isValidStatus($value)
    {
        return in_array($value, self::getAllStatuses(), true);
    }
}
