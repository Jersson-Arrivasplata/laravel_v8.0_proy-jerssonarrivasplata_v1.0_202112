<?php

namespace App\Services;

use App\Models\BlogContent;
use App\Models\BlogSocialMedia;

class BlogSocialMediaService
{

    public function __construct()
    {
    }

    public function store($array)
    {
        $data = BlogSocialMedia::create([
            'idRow' => $array['idRow'],
            'type' => $array['type'],
            'description' => $array['description'],
            'url' => $array['url']
        ]);

        return $data;
    }

    public function  update($array, $idRow)
    {
        $data = BlogSocialMedia::where('idRow', $idRow)->update([
            'type' => $array['type'],
            'description' => $array['description'],
            'url' => $array['url']
        ]);

        return $data;
    }

    public function getAllByIdRow($idRow)
    {
        $blogSocialMedia = BlogSocialMedia::where('idRow', $idRow)->get(['id', 'idRow', 'type', 'description', 'url']);

        return $blogSocialMedia;
    }

    public function getByIdRow($idRow)
    {
        $data = BlogSocialMedia::where('idRow', $idRow)->first(['id', 'idRow', 'type', 'description', 'url']);

        return $data;
    }

    public function removeByIdRow($id)
    {
        $response = BlogSocialMedia::where('id', $id)->delete();

        return $response;
    }
}
