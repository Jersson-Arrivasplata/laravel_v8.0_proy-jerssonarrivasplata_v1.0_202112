<?php

namespace App\Services;

use App\Classes\Utils;
use App\Enums\Activated;
use App\Models\BlogContent;
use App\Models\BlogContentType;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class BlogContentService
{

    public function __construct()
    {
    }

    public function add($array)
    {
        $blogContent = BlogContent::create([
            'id_blog_content_generate' => $array['blogContentGenerateId'],
            'id_blog_content_type' => $array['id_blog_content_type'],
            'position' => $array['position'],
            'checkPreviewBox' => $array['checkPreviewBox'],
            'checkImageBox' => $array['checkImageBox'],
            'contentfontSizeRange' => $array['contentfontSizeRange'],
            'contentMargintop' => $array['contentMargintop'],
            'contentPaddingLeft' => $array['contentPaddingLeft'],
            'contentPaddingRight' => null,
            'checkContentBorderLeft' => null,
            'checkContentBorderRight' => null,
            'checkContentDirectionRight' => null,
            'imgTitle' => $array['imgTitle'],
            'imgSubTitle' => $array['imgSubTitle'],
            'contentTitle' => $array['contentTitle'],
            'contentSubTitle' => $array['contentSubTitle'],
            'bgColor' => $array['bgColor'],
            'typeLanguage' => $array['typeLanguage'],
            'typeLanguageBgColor' => $array['typeLanguageBgColor'],
            'typeLanguageTitle' => $array['typeLanguageTitle'],
            'typeLanguageSubTitle' => $array['typeLanguageSubTitle'],
            'codes' => $array['codes']
        ]);

        return $blogContent;
    }

    public function store($array)
    {
        $data = BlogContent::create([
            'idRow' => Utils::generateString(11),
            'id_blog_content_type_group' => $array['idBlogContentTypeGroup'],
            'id_blog_content_type' => $array['idBlogContentType'],
            'contentTitle' => $array['contentTitle'],
            'contentSubTitle' => $array['contentSubTitle'],
            'codes' => $array['codes'],
            'position' => $array['position'],
            'activated' => $array['activated'],
            'height' => $array['height'],
        ]);

        return $data;
    }

    public function update($array, $id)
    {
        $data = BlogContent::where('id', $id)->update([
            'id_blog_content_type_group' => $array['idBlogContentTypeGroup'],
            'id_blog_content_type' => $array['idBlogContentType'],
            'contentTitle' => $array['contentTitle'],
            'contentSubTitle' => $array['contentSubTitle'],
            'codes' => $array['codes'],
            'position' => $array['position'],
            'activated' => $array['activated'],
            'height' => $array['height'],
        ]);

        return $data;
    }

    public function getAll($path = "")
    {
        // Obtener todos los BlogContent y agruparlos por idRow en la aplicación.
        // Cargar BlogContent con relaciones 'contents' filtradas por campos específicos
        $blogContents = BlogContent::with([
            'contents' => function ($query) {
                $query->select('id', 'name', 'description', 'image', 'type', 'highlight', 'background');
            },
            'socials' => function ($query) {
                $query->select('id', 'idRow', 'type', 'description', 'url');
            }
        ])->get(['id', 'idRow', 'id_blog_content_type_group', 'id_blog_content_type', 'position', 'height', 'contentTitle', 'contentSubTitle', 'lineHeight', 'contentfontSizeRange', 'checkPreviewBox', 'codes', 'before', 'activated']); // Utilizar get() en lugar de all() para aplicar 'with'

        // Agrupar los resultados por idRow utilizando colecciones de Laravel.
        $groupedBlogContents = $blogContents->groupBy('idRow');

        // Ordenar los elementos de cada grupo por 'position' de menor a mayor
        $sortedGroupedBlogContents = $groupedBlogContents->map(function ($group) {
            return $group->sortBy('position');
        });

        // Convertir la colección agrupada en un array para la paginación
        $groupedArray = $sortedGroupedBlogContents->toArray();

        // Obtener la página actual desde la petición, por defecto es 1
        $currentPage = request()->get('page', 1);

        // Número de ítems por página
        $perPage = 10;

        // Calcular los ítems para la página actual
        $currentItems = array_slice($groupedArray, ($currentPage - 1) * $perPage, $perPage);

        // Crear un objeto paginador
        $paginatedItems = new LengthAwarePaginator($currentItems, count($groupedArray), $perPage, $currentPage, [
            'path' => request()->url(),
            'query' => request()->query(),
        ]);
        return $paginatedItems;
    }

    public function getAllByType($path = "", $activated){
        $type = BlogContentType::where('path', $path)->value('type');


        $query = BlogContent::where('id_blog_content_type_group', $type);

        if ($activated !== Activated::All) {
            $query = $query->where('activated', $activated);
        }

        $blogContents = $query
            ->with([
                'contents' => function ($query) {
                    $query->select('id', 'name', 'description', 'image', 'type', 'highlight', 'background');
                },
                'socials' => function ($query) {
                    $query->select('id', 'idRow', 'type', 'description', 'url');
                }
            ])
            ->get(['id', 'idRow', 'id_blog_content_type_group', 'id_blog_content_type', 'position', 'height', 'contentTitle', 'contentSubTitle', 'lineHeight', 'contentfontSizeRange', 'checkPreviewBox', 'codes', 'before', 'activated']);

        // Agrupar los resultados por idRow utilizando colecciones de Laravel.
        $groupedBlogContents = $blogContents->groupBy('idRow');

        // Ordenar los elementos de cada grupo por 'position' de menor a mayor
        $sortedGroupedBlogContents = $groupedBlogContents->map(function ($group) {
            return $group->sortBy('position');
        });

        // Convertir la colección agrupada en un array para la paginación
        $groupedArray = $sortedGroupedBlogContents->toArray();

        // Obtener la página actual desde la petición, por defecto es 1
        $currentPage = request()->get('page', 1);

        // Número de ítems por página
        $perPage = 10;

        // Calcular los ítems para la página actual
        $currentItems = array_slice($groupedArray, ($currentPage - 1) * $perPage, $perPage);

        // Crear un objeto paginador
        $paginatedItems = new LengthAwarePaginator($currentItems, count($groupedArray), $perPage, $currentPage, [
            'path' => request()->url(),
            'query' => request()->query(),
        ]);
        return $paginatedItems;

    }

    public function getById($id)
    {
        /*$idRow = BlogContent::where('id', $id)->value('idRow'); */
        $data = BlogContent::with(['contents' => function ($query) {
            $query->select('id', 'name', 'description', 'image', 'type', 'highlight', 'background');
        }])->where('id', $id)->first(['id', 'idRow', 'id_blog_content_type_group', 'id_blog_content_type', 'position', 'height', 'contentTitle', 'contentSubTitle', 'lineHeight', 'contentfontSizeRange', 'checkPreviewBox', 'codes', 'before', 'activated']);

        return $data;
    }

    public function remove($id)
    {

        $response = BlogContent::where('id', $id)->delete();
        return $response;
    }

    public function addNewBlank($id)
    {
        $blogContent = BlogContent::where('id', $id)->first(['idRow', 'id_blog_content_type_group', 'id_blog_content_type', 'contentTitle', 'contentSubTitle', 'codes']);
        $data = BlogContent::create([
            'idRow' => $blogContent->idRow,
            'id_blog_content_type_group' => $blogContent->id_blog_content_type_group,
            'id_blog_content_type' => $blogContent->id_blog_content_type,
            'contentTitle' => $blogContent->contentTitle,
            'contentSubTitle' => $blogContent->contentSubTitle,
            'codes' => $blogContent->codes,
            'position' => 99999,
            'activated' => false,
        ]);

        return $data;
    }
}
