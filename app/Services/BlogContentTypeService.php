<?php

namespace App\Services;

use App\Models\BlogContent;
use App\Models\BlogContentType;

class BlogContentTypeService
{

    public function __construct()
    {
    }

    public function getByPath($path)
    {
        $data = BlogContentType::where('path', $path)->first();

        return $data;
    }

    public function getAllByTypeInBlogContent()
    {
        $uniqueBlogContentTypeGroups = BlogContent::distinct()->pluck('id_blog_content_type_group');

        $blogContentTypes = BlogContentType::whereIn('type', $uniqueBlogContentTypeGroups->toArray())
            ->orderBy('type', 'asc')
            ->get();

        return $blogContentTypes;
    }
}
