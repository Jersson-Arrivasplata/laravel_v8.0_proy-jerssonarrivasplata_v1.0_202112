<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class OpenAIService
{
    protected $apiKey;
    protected $baseUrl = 'https://api.openai.com/v1';

    public function __construct()
    {
        $this->apiKey = env('OPENAI_API_KEY');
    }

    public function generateText($prompt, $maxTokens)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $this->apiKey,
            'Content-Type' => 'application/json',
        ])->post("{$this->baseUrl}/completions", [
            'model' => 'gpt-3.5-turbo-instruct',
            'prompt' => $prompt,
            'max_tokens' => $maxTokens,
            'temperature' => 0.7,
            'top_p' => 1,
            'n' => 1,
        ]);

        if ($response->successful()) {
            return $response->json()['choices'][0]['text'];
        }

        return 'Error al generar texto con OpenAI.';
    }
}
