<?php

namespace App\Services;

use App\Models\BlogContentGenerate;
use Illuminate\Support\Facades\DB;

class BlogContentGenerateService
{

    public function __construct()
    {
    }

    public function add($array)
    {
        $blogContent = BlogContentGenerate::create([
            'id_blog_content_type_group' => $array['id_blog_content_type_group'],
            'position' => $array['id_blog_content_type_group'],
            'idRow' => $array['idRow'],
            'selectSocialMedia' => $array['selectSocialMedia'],
            'selectSocialMediaWidth' => $array['selectSocialMediaWidth'],
            'selectSocialMediaHeight' => $array['selectSocialMediaHeight'],
            'imgRange' => $array['imgRange'],
            'contentWidthRange' => $array['contentWidthRange'],
            'contentHeightRange' => $array['contentHeightRange'],
            'imgFontSizeRange' => $array['imgFontSizeRange'],
            'contentTitleFontSizeRange' => $array['contentTitleFontSizeRange'],
            'contentBottomFontSizeRange' => $array['contentBottomFontSizeRange'],
            'url_linkedin' => $array['url_linkedin'],
            'description' => $array['description'],
        ]);

        return $blogContent;
    }
}
