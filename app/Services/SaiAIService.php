<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class SaiAIService
{
    protected $apiKey;
    // Correct the placeholder from {data} to {apikey}
    protected $baseUrl = 'https://sai-library.saiapplications.com/api/templates/{apikey}/execute';

    public function __construct()
    {
        $this->apiKey = env('SAI_API_KEY');
    }

    public function generatePromptByLanguageAndTechnology($tech, $language)
    {
        $identify = $this->getIdentify($language);

        // Correctly replaces {apikey} with the identify value
        $response = Http::withHeaders([
            'X-Api-Key' => $this->apiKey
        ])->post(str_replace('{apikey}', $identify, $this->baseUrl), [
            'inputs' => [
                'language' => $language,
                'tech' => $tech
            ]
        ]);

        if ($response->successful()) {
            return $response->body();
        }
        return 'Error al generar texto con OpenAI.';
    }

    public function getIdentify($language)
    {
        $identify = "";
        if ($language == 'es') {
            $identify = "668c1707334f6817e38a7afe";
        } else if ($language == 'en') {
            $identify = "668c1c21334f6817e38a7bd0";
        }
        return $identify;
    }
}
