<?php

namespace App\Services;

use App\Models\BlogContentType;
use App\Models\BlogTip;
use Illuminate\Support\Facades\DB;

class TipService
{

    protected $blobContentService;

    public function __construct(BlogContentService $blobContentService)
    {
        $this->blobContentService = $blobContentService;
    }


    ///api/getOpenAITip?language=es&tech=JavaScript
    public function add($array)
    {
        try {
            // Iniciar una transacción
            DB::beginTransaction();
            // Crear un nuevo registro en la base de datos usando el modelo BlogTip
            $blogTip = new BlogTip([
                'tip' => $array['tip'],
                'legend' => $array['legend'],
                'title' => $array['title'],
                'description' => $array['description'],
                'comment' => $array['comment'],
                'language' => $array['language'],
                'type' => $array['type'],
                'used' => 0, // o cualquier otro valor predeterminado adecuado
                // o cualquier otro valor predeterminado adecuado
                // Asegúrate de ajustar los campos según tu modelo y necesidades
            ]);
            $blogTip->save(); // Guardar el registro en la base de datos


            $blogContentType = BlogContentType::whereRaw('LOWER(name) = LOWER(?)', [$array['type']])
                ->first(['description', 'type']);

            $array = array(
                'idBlogContentTypeGroup' => $blogContentType->type,
                'idBlogContentType' => $blogContentType->type,
                'contentTitle' => $blogContentType->description,
                'contentSubTitle' => $array['title'],
                'codes' => $array['description'],
                'position' => 1,
                'activated' => false,
                'height' => 320,
            );

            $response = $this->blobContentService->store($array);
            DB::commit();
        } catch (\Exception $e) {
            // Si algo falla, revierte la transacción
            DB::rollback();
            return $e;
            // Maneja el error, por ejemplo, registrando el error o enviando una respuesta de error
        }

        return $blogTip;
    }
}
