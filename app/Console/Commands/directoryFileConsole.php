<?php

namespace App\Console\Commands;

use App\Classes\directoryFileClass;
use Illuminate\Console\Command;

class directoryFileConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'directory:generate-json-file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generara un Json con la base de todo el directorio ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       
        $dir=dirname( __FILE__ );
        $RD=new directoryFileClass();
        $RD->directory($dir.'/'."../../../");
        $this->info("Json de directorio generado correctamente!!!");
       /* 
        */
        //php artisan directory:generate-json-file
    }
}
