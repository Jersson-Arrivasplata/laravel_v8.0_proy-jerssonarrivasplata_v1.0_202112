<?php

namespace App\Classes;

use DOMDocument;

class MetaTags
{

    const INIT_METATAGS = [
        "charset" => "",
        "keywords" => "",
        "viewport" => "",
        "author" => "",
        "description" => "",
        "twitter:description" => "",
        "twitter:title" => "",
        "twitter:url" => "",
        "twitter:card" => "",
        "msapplication-config" => "",
        "msapplication-TileImage" => "",
        "msapplication-TileColor" => "",
        "theme-color" => "",
        "apple-mobile-web-app-capable" => "",
        "og:site_name" => "",
        "og:url" => "",
        "og:title" => "",
        "og:description" => "",
        "og:image" => "",
        "cleartype" => "",
        "X-UA-Compatible" => "",
        "Expires" => "",
        "Last-Modified" => "",
        "Cache-Control" => "",
        "Pragma" => ""
    ];

   const INIT_TABLE = [
        "body" => "",
        "title" => "",
        "url" => "",
        "meta-charset" => "",
        "meta-name-keywords" => "",
        "meta-name-viewport" => "",
        "meta-name-author" => "",
        "meta-name-description" => "",
        "meta-name-twitter-description" => "",
        "meta-name-twitter-title" => "",
        "meta-name-twitter-url" => "",
        "meta-name-twitter-card" => "",
        "meta-name-msapplication-config" => "",
        "meta-name-msapplication-TileImage" => "",
        "meta-name-msapplication-TileColor" => "",
        "meta-name-theme-color" => "",
        "meta-name-apple-mobile-web-app-capable" => "",
        "meta-property-og-site-name" => "",
        "meta-property-og-url" => "",
        "meta-property-og-title" => "",
        "meta-property-og-description" => "",
        "meta-property-og-image" => "",
        "meta-http-equiv-cleartype" => "",
        "meta-http-equiv-x-ua-compatible" => "",
        "meta-http-equiv-expires" => "",
        "meta-http-equiv-last-modified" => "",
        "meta-http-equiv-cache-control" => "",
        "meta-http-equiv-pragma" => ""
    ];

    const METATAGS_TO_TABLE = [
        "charset" => "meta-charset",
        "keywords" => "meta-name-keywords",
        "viewport" => "meta-name-viewport",
        "author" => "meta-name-author",
        "description" => "meta-name-description",
        "twitter:description" => "meta-name-twitter-description",
        "twitter:title" => "meta-name-twitter-title",
        "twitter:url" => "meta-name-twitter-url",
        "twitter:card" => "meta-name-twitter-card",
        "msapplication-config" => "meta-name-msapplication-config",
        "msapplication-TileImage" => "meta-name-msapplication-TileImage",
        "msapplication-TileColor" => "meta-name-msapplication-TileColor",
        "theme-color" => "meta-name-theme-color",
        "apple-mobile-web-app-capable" => "meta-name-apple-mobile-web-app-capable",
        "og:site_name" => "meta-property-og-site-name",
        "og:url" => "meta-property-og-url",
        "og:title" => "meta-property-og-title",
        "og:description" => "meta-property-og-description",
        "og:image" => "meta-property-og-image",
        "cleartype" => "meta-http-equiv-cleartype",
        "X-UA-Compatible" => "meta-http-equiv-x-ua-compatible",
        "Expires" => "meta-http-equiv-expires",
        "Last-Modified" => "meta-http-equiv-last-modified",
        "Cache-Control" => "meta-http-equiv-cache-control",
        "Pragma" => "meta-http-equiv-pragma"
    ];

    public function __construct()
    {
    }

    public static function get_meta_tags($url)
    {
        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );

        $response = file_get_contents($url, false, stream_context_create($arrContextOptions));

        // Asumiendo que las anteriores etiquetas se encuentran en www.example.com
        //   $tags = get_meta_tags($request->url);

        libxml_use_internal_errors(true); //Prevents Warnings, remove if desired
        $dom = new DOMDocument();
        $dom->loadHTML($response);



        //
        $meta_array = MetaTags::INIT_METATAGS;

        $title = MetaTags::get_title($dom);

        $tags = MetaTags::get_all_tags($dom);

        $body = MetaTags::get_all_body($dom);
        //$body="";

        $meta_array = MetaTags::get_array_meta_tags($tags);

        return MetaTags::get_meta_to_table($title, $url, $body, $meta_array);
    }

    public static function get_title($dom)
    {
        $title = "";
        foreach ($dom->getElementsByTagName("title")->item(0)->childNodes as $child) {
            $title .= $dom->saveHTML($child);
        }
        return $title;
    }

    public static function get_all_body($dom)
    {
        $body = substr($dom->getElementsByTagName("body")->item(0)->textContent, 0, 1000);
        return $body;
    }

    public static function get_all_tags($dom)
    {
        foreach ($dom->getElementsByTagName("meta") as $element) {
            $tag = [];
            foreach ($element->attributes as $node) {
                $tag[$node->name] = $node->value;
            }
            $tags[] = $tag;
        }
        return $tags;
    }



    public static function get_array_meta_tags($tags, $meta_array = MetaTags::INIT_TABLE, $array_tags = MetaTags::METATAGS_TO_TABLE)
    {

        foreach ($tags as $tag) {
            $keys = array_keys($tag);
            foreach ($keys as $key) {
                if ($key == "charset") {
                    $meta_array[$key] = $tag[$key];
                } else if ($key == "name") {
                    if (key_exists($tag["name"], $array_tags)) {
                        $meta_array[$tag["name"]] = $tag["content"];
                    }
                } else if ($key == "property") {
                    if (key_exists($tag["property"], $array_tags)) {
                        $meta_array[$tag["property"]] = $tag["content"];
                    }
                } else if ($key == "http-equiv") {
                    if (key_exists($tag["http-equiv"], $array_tags)) {
                        $meta_array[$tag["http-equiv"]] = $tag["content"];
                    }
                }
            }
        }
        return $meta_array;
    }


    public static function get_meta_to_table($title, $url,$body, $meta_array)
    {
        $array = [
            "body" => $body,
            "title" => $title,
            "url" => $url,
            "meta-charset" => isset($meta_array["charset"]) ? $meta_array["charset"] : '',
            "meta-name-keywords" =>  isset($meta_array["keywords"]) ? $meta_array["keywords"] : '',
            "meta-name-viewport" =>  isset($meta_array["viewport"]) ? $meta_array["viewport"] : '',
            "meta-name-author" =>  isset($meta_array["author"]) ? $meta_array["author"] : '',
            "meta-name-description" =>  isset($meta_array["description"]) ? $meta_array["description"] : '',
            "meta-name-twitter-description" =>  isset($meta_array["twitter:description"]) ? $meta_array["twitter:description"] : '',
            "meta-name-twitter-title" =>  isset($meta_array["twitter:title"]) ? $meta_array["twitter:title"] : '',
            "meta-name-twitter-url" =>  isset($meta_array["twitter:url"]) ? $meta_array["twitter:url"] : '',
            "meta-name-twitter-card" =>  isset($meta_array["twitter:card"]) ? $meta_array["twitter:card"] : '',
            "meta-name-msapplication-config" =>  isset($meta_array["msapplication-config"]) ? $meta_array["msapplication-config"] : '',
            "meta-name-msapplication-TileImage" =>  isset($meta_array["msapplication-TileImage"]) ? $meta_array["msapplication-TileImage"] : '',
            "meta-name-msapplication-TileColor" =>  isset($meta_array["msapplication-TileColor"]) ? $meta_array["msapplication-TileColor"] : '',
            "meta-name-theme-color" =>  isset($meta_array["theme-color"]) ? $meta_array["theme-color"] : '',
            "meta-name-apple-mobile-web-app-capable" =>  isset($meta_array["apple-mobile-web-app-capable"]) ? $meta_array["apple-mobile-web-app-capable"] : '',
            "meta-property-og-site-name" =>  isset($meta_array["og:site_name"]) ? $meta_array["og:site_name"] : '',
            "meta-property-og-url" =>  isset($meta_array["og:url"]) ? $meta_array["og:url"] : '',
            "meta-property-og-title" =>  isset($meta_array["og:title"]) ? $meta_array["og:title"] : '',
            "meta-property-og-description" =>  isset($meta_array["og:description"]) ? $meta_array["og:description"] : '',
            "meta-property-og-image" =>  isset($meta_array["og:image"]) ? $meta_array["og:image"] : '',
            "meta-http-equiv-cleartype" =>  isset($meta_array["cleartype"]) ? $meta_array["cleartype"] : '',
            "meta-http-equiv-x-ua-compatible" =>  isset($meta_array["X-UA-Compatible"]) ? $meta_array["X-UA-Compatible"] : '',
            "meta-http-equiv-expires" =>  isset($meta_array["Expires"]) ? $meta_array["Expires"] : '',
            "meta-http-equiv-last-modified" =>  isset($meta_array["Last-Modified"]) ? $meta_array["Last-Modified"] : '',
            "meta-http-equiv-cache-control" =>  isset($meta_array["Cache-Control"]) ? $meta_array["Cache-Control"] : '',
            "meta-http-equiv-pragma" =>  isset($meta_array["Pragma"]) ? $meta_array["Pragma"] : '',
        ];
        return $array;
    }
}
