<?php

namespace App\Classes;

use App\Models\BlogContent;
use App\Models\BlogContentType;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;
class GlobalClass
{

    public function __construct()
    {
    }
    public static function pathFileBlogContent($url)
    {
        return config('base.website').'/' . substr(BlogContent::where('url', $url)->value('path') , 0, -1). '#' . $url;
    }

    public static function getDomain($url)
    {
        $disallowed = array('http://', 'https://');
        foreach ($disallowed as $d) {
            if (strpos($url, $d) === 0) {
                return str_replace($d, '', $url);
            }
        }
        return $url;
    }

    public static function getProtocol($url)
    {
        $protocol = stripos($url, 'https') === 0 ? 'https://' : 'http://';
        return $protocol;
    }

    public static function getDomainFolder($position){
        $domain = config('base.domains')[$position];
        View::addNamespace($domain, config('view.paths')[0] . "/pages/{$domain}/");
        return $domain;
    }

    public static function getCreatorUser(){
        return config('base.info.first_name') .' G. '.  config('base.info.last_name') .' '. config('base.info.mother_last_name');
    }

}
