<?php

namespace App\Classes;

class directoryFileClass
{

    public function __construct()
    {
    }

    private $texto = '';
    private $textoAll;
    private $fileArray = array();
    public function directory($dir)
    {

        $ArchivoJSON = fopen($dir . "resources/json/directory2.json", "w") or die("No se puede abrir/crear el archivo!");

        $this->directory_children($dir);

        fwrite($ArchivoJSON, $this->texto);
        fclose($ArchivoJSON);

        echo ($this->textoAll);
    }
    public function directory_children($dir)
    {

        $cleanPath = realpath($dir) . DIRECTORY_SEPARATOR;
        $scanDir = scandir($cleanPath);


        $this->texto .= '[';
        foreach ($scanDir as $key => $file) {

            if ($file == "." || $file == "..") continue;

            if (is_file($cleanPath . $file)) {
                $this->texto .= '{';
                $this->texto .= '"realpath":"' . realpath($file) . '",';
                $this->texto .= '"path":"' .$cleanPath.$file . '",';

                $this->texto .= '"title":"' . $file . '"';
                if (count($scanDir) == ($key + 1)) {
                    $this->texto .= '}';
                } else {
                    $this->texto .= '},';
                }
            }


            if (is_dir($cleanPath . $file)) {

                $this->texto .= '{"isFolder": true, "title":"' . $file . '","children":';
                if (is_readable($cleanPath . $file)) {
                    $this->directory_children($cleanPath . $file);
                }
                if (count($scanDir) == ($key + 1)) {
                    $this->texto .= '}';
                } else {
                    $this->texto .= '},';
                }
            }
        }
        $this->texto .= ']';
    }
}
