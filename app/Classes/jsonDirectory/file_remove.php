<?php

    class RMF{
        private $nom_file_tipo;
        private $nom_file_dir;
        public function __CONSTRUCT(){
            try
            {
                if(isset($_POST['nom_file_tipo'])){
                    $this->nom_file_tipo=$_POST['nom_file_tipo'];
                }else{
                    $this->nom_file_tipo="";
                }
                if(isset($_POST['nom_file_dir'])){
                    $this->nom_file_dir=$_POST['nom_file_dir'];
                }else{
                    $this->nom_file_dir="";
                }
            }
            catch(Exception $e)
            {
                die($e->getMessage());
                exit;
            }
        }
        
        function directory($dir){
            $dir=$dir.'\/'."..\/..\/";
            $cleanPath=realpath($dir).DIRECTORY_SEPARATOR;
            $cleanPath=realpath($cleanPath.$this->nom_file_dir).DIRECTORY_SEPARATOR;
            $cleanPath=substr($cleanPath,0,strlen($cleanPath)-1);
    
            if($this->nom_file_tipo=='file'){
                if(file_exists($cleanPath)){
                    unlink($cleanPath);
                    print_r('{"0":"1"}');
                }
            }else{
                $this->delete_files($cleanPath);
            }
        }
        
        
        function delete_files($cleanPath){
            if( is_dir($cleanPath)){
                $files = glob( $cleanPath . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
                //$files[0]=substr($files[0],0,strlen($files[0])-1);
                try{
                foreach( $files as $file )
                {
                    $this->delete_files( $file );      
                }
                    if( is_dir($cleanPath)){
                        rmdir($cleanPath );
                        print_r('{"0":"1"}');
                    }
                }catch(Exception $e){
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                } 
            }
        }
        
    }
$dir=dirname( __FILE__ );
$RMF=new RMF();
$RMF->directory($dir);