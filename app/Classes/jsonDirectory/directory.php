<?php

    class RD{
        public function directory($dir){
            $this->directory_children($dir);
        }
        public function directory_children($dir){
            //echo $dir;
            $cleanPath=realpath($dir).DIRECTORY_SEPARATOR;
            $scanDir=scandir($cleanPath);
            echo("<ul>");
            foreach ($scanDir as $key => $file) {
                # code...
                if($file=="."||$file=="..") continue;
                echo("<li>");
                    echo($file);
                    //echo($cleanPath.$file);
                    if(is_dir($cleanPath.$file)&&is_readable($cleanPath.$file)){
                        $this->directory_children($cleanPath.$file);
                    }
                echo("</li>");
                //echo("<li>".$file."</li>");
            }
            echo("</ul>");

        }
    }
    $RD=new RD();
    $RD->directory('./');//C:\xampp\htdocs\KRM_BLOG