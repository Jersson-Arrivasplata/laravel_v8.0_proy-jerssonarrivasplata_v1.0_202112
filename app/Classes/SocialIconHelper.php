<?php
namespace App\Classes;

class SocialIconHelper
{
    public static function getIconName($type)
    {
        switch ($type) {
            case 0:
                return 'linkedin.svg';
            case 1:
                return 'facebook.svg';
            case 2:
                return 'instagram.svg';
            case 3:
                return 'whatsapp.svg';
            default:
                return 'default.svg'; // Asume un ícono por defecto si no coincide ninguno
        }
    }
}
