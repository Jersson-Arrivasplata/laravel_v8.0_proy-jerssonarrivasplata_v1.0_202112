<?php

namespace App\Classes;
use Illuminate\Support\Str;

class directoryJsonFileClass
{

    public function __construct()
    {
    }
    public $content = "";

    public function svgFolder(){
        return '
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-folder" viewBox="0 0 16 16">
            <path d="M.54 3.87.5 3a2 2 0 0 1 2-2h3.672a2 2 0 0 1 1.414.586l.828.828A2 2 0 0 0 9.828 3h3.982a2 2 0 0 1 1.992 2.181l-.637 7A2 2 0 0 1 13.174 14H2.826a2 2 0 0 1-1.991-1.819l-.637-7a1.99 1.99 0 0 1 .342-1.31zM2.19 4a1 1 0 0 0-.996 1.09l.637 7a1 1 0 0 0 .995.91h10.348a1 1 0 0 0 .995-.91l.637-7A1 1 0 0 0 13.81 4H2.19zm4.69-1.707A1 1 0 0 0 6.172 2H2.5a1 1 0 0 0-1 .981l.006.139C1.72 3.042 1.95 3 2.19 3h5.396l-.707-.707z"/>
        </svg>
        ';
    }
    public function tree_directory($json){
        //echo $dir;
        //$dir=dirname( __FILE__ );
       // $cleanPath=realpath($dir.'/'."../../../").DIRECTORY_SEPARATOR;
       $this->content .= '<ul>';
        foreach ($json as $key => $value) {
            # code...
            $rand = rand();
            if(array_key_exists('title',$value)){
                if(array_key_exists('isFolder',$value)){
                    $this->content .='<li class="fa fa-folder-open" >
                        <a data-toggle="collapse" href="#'.$rand.Str::slug($value['title'], '-').'" role="button" aria-expanded="false" aria-controls="collapseExample">'
                        .$this->svgFolder().' '.$value['title'].
                        ' </a>
                    </li>';
                }else{
                    $this->content .='<li><a data-path="'.$value['path'].'" href="#'.$rand.Str::slug($value['title'], '-').'" data-toggle="modal" data-target="#fileModal">'.$value['title'].'</a></li>';
                }
            }

            if(array_key_exists('isFolder',$value)){
                $this->content .='<li class="collapse" id="'.$rand.Str::slug($value['title'], '-').'">';
                 $this->tree_directory($value['children']);
                $this->content .='</li>';
            }
        }
        $this->content .='</ul>';
    }
}
