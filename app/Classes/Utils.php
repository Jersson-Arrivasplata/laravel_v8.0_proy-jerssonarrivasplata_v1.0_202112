<?php

namespace App\Classes;

use App\Models\BlogContent;
use App\Models\ProjectContent;

class Utils
{

    public function __construct()
    {
    }

    public static function getCounterBlogContentGenerate($type)
    {
        $blogContents = BlogContent::with(['contents' => function ($query) {
            $query->select('id', 'name', 'description', 'image', 'type', 'highlight', 'background');
        }])->where('id_blog_content_type_group', $type)
        ->get(['id', 'idRow', 'id_blog_content_type', 'position',  'height', 'contentTitle', 'contentSubTitle', 'lineHeight', 'contentfontSizeRange', 'checkPreviewBox', 'codes', 'before']); // Utilizar get() en lugar de all() para aplicar 'with'

        // Agrupar los resultados por idRow utilizando colecciones de Laravel.
        $groupedBlogContents = $blogContents->groupBy('idRow');

        // Convertir la colección agrupada en un array para la paginación
        $groupedArray = $groupedBlogContents->toArray();

        //tamaño de $groupedArray
        return count($groupedArray);
    }

    public static function getCounterProjectContent($type)
    {
        return ProjectContent::orderBy('created_at', 'desc')
            ->where('type', $type)
            ->count();
    }

    public static function getArrayOfContentGenerates($content)
    {
        $array = [];
        foreach ($content as $key => $data) {
            if (empty($array[$data['idRow']])) {
                $array[$data['idRow']] = [];
            }
            array_push($array[$data['idRow']], $data);
        }
        return $array;
    }

    public static function getUrlBase($type = '')
    {
        return GlobalClass::getProtocol(env('APP_URL')) . (($type) ? $type . '.' : '') . GlobalClass::getDomain(env('APP_URL'));
    }

    /* public static function getSegmentToText($text = '', $position=1)
    {
        return Request::segment($position)==$text;
    }*/
    public static function getBooleanString($bool = false)
    {
        return $bool == 1 ? true : false;
    }

    public static function generateString($strength = 16)
    {
        $input = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $input_length = strlen($input);
        $random_string = '';
        for ($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }
}
