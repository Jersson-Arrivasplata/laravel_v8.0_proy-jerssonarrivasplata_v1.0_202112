<?php

namespace App\Helper;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class PromptSAIHelper
{

    //getOpenAITip?language=es&tech=JavaScript
    // Leer parámetros de la solicitud con valores predeterminados
    public static function generatePromptByLanguageAndTechnology($tech, $language)
    {
        $identify = "";
        if($language == 'es'){
            $identify = "668c1707334f6817e38a7afe";
        }else if($language == 'en'){
            $identify = "668c1c21334f6817e38a7bd0";
        }

        $client = new Client();
        $headers = [
            'X-Api-Key' => env('SAI_API_KEY'),
            'Content-Type' => 'application/json'
        ];
        $body = '{
            "inputs": {
                "language": "' . $language . '",
                "tech": "' . $tech . '"
            }
        }';
        $request = new Request('POST', 'https://sai-library.saiapplications.com/api/templates/'.$identify.'/execute', $headers, $body);
        $res = $client->sendAsync($request)->wait();
        return $res->getBody();
    }
}
