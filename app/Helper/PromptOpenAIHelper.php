<?php

namespace App\Helper;

class PromptOpenAIHelper {

    public static function promptByLang($tech, $language)
    {
        if ($language == "es" || $language == "pe") {
            return "
            Genera un tip diario de {$tech} con la siguiente estructura:

            - Un título que resuma el tip.
            - Un fragmento de código {$tech} con comentarios explicando el código.

            Asegúrate de que los comentarios sean parte del código y expliquen claramente cada paso. La explicación debe proporcionar contexto e ideas adicionales. Aquí tienes un formato de ejemplo:

            ## Tip Diario de {$tech}

            **Título:** Entendiendo los Closures en {$tech}

            ```{$tech}
            // Los closures son un concepto fundamental en {$tech}.
            // Un closure es una función que recuerda el entorno
            // en el que fue creada, incluso después de ser ejecutada
            // fuera de ese entorno.

            Usa este formato para crear un nuevo tip cada día.
            ";
        } else {
            return "
            Generate a daily {$tech} tip with the following structure:

            - A title summarizing the tip.
            - {$tech} code snippet with comments explaining the code.

            Ensure the comments are part of the code and clearly explain each step. The explanation should provide additional context and insights. Here is an example format:

            ## Daily {$tech} Tip

            **Title:** Understanding Closures in {$tech}

            ```{$tech}
            // Closures are a fundamental concept in {$tech}.
            // A closure is a function that remembers the environment
            // in which it was created, even after it is executed outside
            // that environment.

            Use this format to create one new tip each day.
            ";
        }
    }

    public static function extractTip($inputText, $tech, $language)
    {
        $data = [];

        if ($language == "es" || $language == "pe") {
            // Utilizamos expresiones regulares para extraer el contenido
            preg_match('/## Tip Diario de  ' . preg_quote($tech, '/') . '/', $inputText, $dailyTip);
            preg_match('/\*\*Título:\*\* (.+)/', $inputText, $title);
            preg_match('/```' . preg_quote($tech, '/') . '(.*?)```/s', $inputText, $matches);
            // Ajustar la expresión regular para capturar correctamente el comentario después del bloque de código
            preg_match('/```' . preg_quote($tech, '/') . '.*?```(.*?)$/s', $inputText, $comment);

            // Creamos un arreglo con el contenido extraído
            $data = [
                'DailyTip' => isset($dailyTip[0]) ? $dailyTip[0] : '',
                'Title' => isset($title[1]) ? $title[1] : '',
                'Code' => isset($matches[1]) ? preg_replace('/^\s*/m', '', preg_replace('/^\R/', '', $matches[1])) : '',
                'Comment' => isset($comment[1]) ? trim($comment[1]) : ''
            ];
        } else {
            // Utilizamos expresiones regulares para extraer el contenido
            preg_match('/## Daily ' . preg_quote($tech, '/') . ' Tip/', $inputText, $dailyTip);
            preg_match('/\*\*Title:\*\* (.+)/', $inputText, $title);
            preg_match('/```' . preg_quote($tech, '/') . '(.*?)```/s', $inputText, $matches);
            // Ajustar la expresión regular para capturar correctamente el comentario después del bloque de código
            preg_match('/```' . preg_quote($tech, '/') . '.*?```(.*?)$/s', $inputText, $comment);

            // Creamos un arreglo con el contenido extraído
            $data = [
                'DailyTip' => isset($dailyTip[0]) ? $dailyTip[0] : '',
                'Title' => isset($title[1]) ? $title[1] : '',
                'Code' => isset($matches[1]) ? preg_replace('/^\s*/m', '', preg_replace('/^\R/', '', $matches[1])) : '',
                'Comment' => isset($comment[1]) ? trim($comment[1]) : ''
            ];
        }
        return $data;
    }
}
