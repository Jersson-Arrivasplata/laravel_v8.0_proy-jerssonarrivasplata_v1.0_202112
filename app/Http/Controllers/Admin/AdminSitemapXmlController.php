<?php


namespace App\Http\Controllers\Admin;

use App\Classes\GlobalClass;
use App\Http\Controllers\Controller;
use App\Models\SiteMap;
use App\Models\SiteMapAlternates;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AdminSitemapXmlController extends Controller
{
    public function index()
    {
        $content = SiteMap::orderBy('created_at', 'desc')->paginate(15);
        return view(GlobalClass::getDomainFolder(2) . "::admin.admin-sitemap.index", ['content' => $content]);
    }



    public static function get_site_map($type){
        $content = SiteMap::where('type', $type)->get();
        for ($i=0; $i < count($content); $i++) { 
            $content[$i]['alternates'] = SiteMapAlternates::where('id_sitemaps', $content[$i]['id'])->get();
        }
        return response()->view(GlobalClass::getDomainFolder(2) . "::admin.admin-sitemap.show", ['content' => $content])
            ->header('Content-Type', 'text/xml');
    }

    public function create()
    {
        return view(GlobalClass::getDomainFolder(2) . "::admin.admin-sitemap.create");
    }

    public function store(Request $request)
    {

        SiteMap::create($request->toArray());
        return back()->with('success', '¡Mensaje creado con éxito!');
    }

   /* public function show(Requedst $request)
    {
        //
        $content = SiteMap::where('type', $request->type)->get();
        return response()->view(GlobalClass::getDomainFolder(2) . "::admin.admin-sitemap.index", ['content' => $content])
            ->header('Content-Type', 'text/xml');
    }*/
}
