<?php

namespace App\Http\Controllers\Admin;

use App\Classes\GlobalClass;
use App\Http\Controllers\Controller;
use App\Services\BlogSocialMediaService;
use Illuminate\Http\Request;

class BlogSocialMediaController extends Controller
{

    protected $blobSocialMediaService;

    public function __construct(BlogSocialMediaService $blobSocialMediaService)
    {
        $this->blobSocialMediaService = $blobSocialMediaService;
    }

    public function store(Request $request)
    {
        // Acceder directamente a los datos del formulario
        $array = $request->only(['type', 'description', 'url', 'idRow']);

        $response = $this->blobSocialMediaService->store($array);

        return $this->show($array['idRow']);
    }

    public function show($idRow)
    {
        $response = $this->blobSocialMediaService->getAllByIdRow($idRow);

        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-social-media.show", ['contents' => $response, 'idRow' => $idRow]);
    }

    public function destroy(Request $request, $idRow)
    {
        $response = $this->blobSocialMediaService->removeByIdRow($request->input('id'));

        return $this->show($idRow);
    }
}
