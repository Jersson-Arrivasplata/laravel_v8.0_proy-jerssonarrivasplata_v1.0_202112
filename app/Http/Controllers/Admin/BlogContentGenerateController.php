<?php

namespace App\Http\Controllers\Admin;

use App\Classes\GlobalClass;
use App\Classes\Utils;
use App\Http\Controllers\Controller;
use App\Models\BlogContent;
use App\Models\BlogContentGenerate;
use App\Models\BlogContentType;
use App\Services\BlogContentGenerateService;
use App\Services\BlogContentService;
use Illuminate\Http\Request;

class BlogContentGenerateController extends Controller
{
    protected $blobContentService;
    protected $blobContentGenerateService;

    public function __construct(BlogContentService $blobContentService, BlogContentGenerateService $blobContentGenerateService)
    {
        $this->blobContentService = $blobContentService;
        $this->blobContentGenerateService = $blobContentGenerateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content = BlogContentGenerate::orderBy('created_at','desc')->paginate(15);
        return view(GlobalClass::getDomainFolder(2) ."::dev.blog-content-generate.index", ['content' => $content, 'name' => ""]);
    }


    public function indexType($name)
    {

        $type = BlogContentType::where('name', $name)->value('type');
        $content = BlogContentGenerate::where('id_blog_content_type_group', $type)
            ->orderBy('position', 'asc')
            ->paginate(15);

        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-content-generate.index-type", ['content' => $content, 'type' => 0, 'name' => $name]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        if ($request->type == 0) {
            return redirect()->route('admin.blog-content-generate.index');
        }
        //view('admin.blog-content.create-content');
        $content = BlogContentGenerate::where('id_blog_content_type', $request->type)
            ->orderBy('position', 'asc')
            ->paginate(15);
        //return view('admin.blog-content-generate.index', ['content' => $content, 'type' => $request->type]);
        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-content-generate.index", ['content' => $content, 'type' => $request->type]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //return view('admin.blog-content-generate.create');
        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-content-generate.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    public function store(Request $request)
    {
        $array = json_decode($request->getContent(), true);

        $array['position'] = $this->getBlogContentGeneratePosition($array['id_blog_content_type_group']);
        $array['url_linkedin'] = null;
        $array['description'] = null;

        $blobContentGenerate = $this->blobContentGenerateService->add($array);

        $array['blogContentGenerateId'] =  $blobContentGenerate->id;


        $response = $this->blobContentService->add($array);

        return  response()->json($response);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //return view('admin.blog-content-generate.show');
        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-content-generate.show");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //BlogContentGenerate::where('id', $id_blog_content_generate)
        $blogContent = BlogContent::find($id);
        $blogContentGenerate = BlogContentGenerate::where('id', $blogContent->id_blog_content_generate)->first();

        $content = [
            'id' => $blogContent->id,
            'id_blog_content_generate' => $blogContent->id_blog_content_generate,
            'id_blog_content_type' => $blogContent->id_blog_content_type,
            'position' => $blogContent->position,
            'checkPreviewBox' => $blogContent->checkPreviewBox,
            'checkImageBox' => $blogContent->checkImageBox,
            'contentfontSizeRange' => $blogContent->contentfontSizeRange,
            'contentMargintop' => $blogContent->contentMargintop,
            'contentPaddingLeft' => $blogContent->contentPaddingLeft,
            'contentPaddingRight' => $blogContent->contentPaddingRight,
            'checkContentBorderLeft' => $blogContent->checkContentBorderLeft,
            'checkContentBorderRight' => $blogContent->checkContentBorderRight,
            'checkContentDirectionRight' => $blogContent->checkContentDirectionRight,
            'imgTitle' => $blogContent->imgTitle,
            'imgSubTitle' => $blogContent->imgSubTitle,
            'contentTitle' => $blogContent->contentTitle,
            'contentSubTitle' => $blogContent->contentSubTitle,
            'bgColor' => $blogContent->bgColor,
            'typeLanguage' => $blogContent->typeLanguage,
            'typeLanguageBgColor' => $blogContent->typeLanguageBgColor,
            'typeLanguageTitle' => $blogContent->typeLanguageTitle,
            'typeLanguageSubTitle' => $blogContent->typeLanguageSubTitle,
            'codes' => $blogContent->codes,
            'id_blog_content_type_group' => $blogContentGenerate->id_blog_content_type_group,
            'idRow' => $blogContentGenerate->idRow,
            'selectSocialMedia' => $blogContentGenerate->selectSocialMedia,
            'selectSocialMediaWidth' => $blogContentGenerate->selectSocialMediaWidth,
            'selectSocialMediaHeight' => $blogContentGenerate->selectSocialMediaHeight,
            'imgRange' => $blogContentGenerate->imgRange,
            'contentWidthRange' => $blogContentGenerate->contentWidthRange,
            'contentHeightRange' => $blogContentGenerate->contentHeightRange,
            'imgFontSizeRange' => $blogContentGenerate->imgFontSizeRange,
            'contentTitleFontSizeRange' => $blogContentGenerate->contentTitleFontSizeRange,
            'contentBottomFontSizeRange' => $blogContentGenerate->contentBottomFontSizeRange,
            'url_linkedin' => $blogContentGenerate->url_linkedin,
            'description' => $blogContentGenerate->description
        ];
        //return view('admin.blog-content-generate.edit', ['content' => $content]);
        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-content-generate.edit", ['content' => $content]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function getBlogContentPosition($id)
    {
        $array_blog_content = BlogContent::select('position')
            ->where('id', $id)
            ->orderBy('position', 'desc')
            ->first();
        $position = 1;
        if ($array_blog_content->position > 0) {
            $position = ($array_blog_content->position + 1);
        }
        return $position;
    }

    public function update(Request $request, $id)
    {
        //
        $array = json_decode($request->getContent(), true);
        unset($array["_method"]);
        unset($array["_token"]);

        $id_blog_content_generate = BlogContent::where('id', $id)->value('id_blog_content_generate');


        BlogContentGenerate::where('id', $id_blog_content_generate)
            ->update([
                'selectSocialMedia' => $array['selectSocialMedia'],
                'selectSocialMediaWidth' => $array['selectSocialMediaWidth'],
                'selectSocialMediaHeight' => $array['selectSocialMediaHeight'],
                'imgRange' => $array['imgRange'],
                'contentWidthRange' => $array['contentWidthRange'],
                'contentHeightRange' => $array['contentHeightRange'],
                'imgFontSizeRange' => $array['imgFontSizeRange'],
                'contentTitleFontSizeRange' => $array['contentTitleFontSizeRange'],
                'contentBottomFontSizeRange' => $array['contentBottomFontSizeRange'],
                'url_linkedin' =>  $array['url_linkedin'],
                'description' =>  $array['description'],
            ]);


        $response = BlogContent::where('id', $id)
            ->update([
                'id_blog_content_type' => $array['id_blog_content_type'],
                'checkPreviewBox' => $array['checkPreviewBox'],
                'checkImageBox' => $array['checkImageBox'],
                'contentfontSizeRange' => $array['contentfontSizeRange'],
                'contentMargintop' => $array['contentMargintop'],
                'contentPaddingLeft' => $array['contentPaddingLeft'],
                'imgTitle' => $array['imgTitle'],
                'imgSubTitle' => $array['imgSubTitle'],
                'contentTitle' => $array['contentTitle'],
                'contentSubTitle' => $array['contentSubTitle'],
                'bgColor' => $array['bgColor'],
                'typeLanguage' => $array['typeLanguage'],
                'typeLanguageBgColor' => $array['typeLanguageBgColor'],
                'typeLanguageTitle' => $array['typeLanguageTitle'],
                'typeLanguageSubTitle' => $array['typeLanguageSubTitle'],
                'codes' => $array['codes'],
                'position' => $array['position'] ? $array['position'] :  $this->getBlogContentPosition($id)
            ]);

        //$response = DB::table('blog_content_generates')->where('id', $id)->update($array);

        //$response = BlogContentGenerate::where('id',$id)
        //->update($request->all());
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id_blog_content_generate = BlogContent::where('id', $id)->value('id_blog_content_generate');
        BlogContent::where('id', $id)->update([
            'position' => 0
        ]);
        $response = BlogContent::destroy($id);
        if (!(BlogContent::where('id_blog_content_generate', $id_blog_content_generate)->count() > 0)) {
            BlogContentGenerate::destroy($id_blog_content_generate);
        }

        return response()->json($response);
        //return back()->with('success','¡Fue eliminado correctamente');
    }

    public function getBlogContentGeneratePosition($id_blog_content_type_group)
    {
        $array_blog_content_generate = BlogContentGenerate::select('position')
            ->where('id_blog_content_type_group', $id_blog_content_type_group)
            ->orderBy('position', 'desc')
            ->first();
        $position = 1;
        if ($array_blog_content_generate) {
         $position = ($array_blog_content_generate->position + 1);
        }
        return $position;
    }

    public function clone(Request $request, $id)
    {
        $input = json_decode($request->getContent(), true);

        $id_blog_content_generate = BlogContent::where('id', $id)->value('id_blog_content_generate');
        $blogContentGenerate = BlogContentGenerate::find($id_blog_content_generate);
        $blogContent = BlogContent::find($id);

        $new_blogContentGenerate = $blogContentGenerate->replicate();
        $new_blogContentGenerate->created_at = now();

        $new_blogContent = $blogContent->replicate();
        $new_blogContent->created_at = now();


        if ($input['replicate']) {
            $new_blogContentGenerate->position =  $this->getBlogContentGeneratePosition($blogContentGenerate['id_blog_content_type_group']);
            $new_blogContent->position = 1;
            $new_blogContentGenerate->idRow = Utils::generateString(5);
            $new_blogContentGenerate->save();

            $new_blogContent->id_blog_content_generate = $new_blogContentGenerate->id;
        } else {
            $new_blogContent->position = BlogContent::where('id_blog_content_generate', $id_blog_content_generate)->max('position') + 1;
        }

        $new_blogContent->save();
        return response()->json($new_blogContent);
    }
}
