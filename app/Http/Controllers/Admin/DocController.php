<?php

namespace App\Http\Controllers\Admin;

use App\Classes\directoryJsonFileClass;
use App\Classes\GlobalClass;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class DocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jsonString = file_get_contents(base_path('resources/json/directory.json'));

        $json = json_decode($jsonString, true);

        $RD = new directoryjsonFileClass();
        $RD->tree_directory($json);
        $content = $RD->content;
        //return view('admin.docs.index', compact('content'));
        return view(GlobalClass::getDomainFolder(2) . "::dev.docs.index", compact('content'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //return view('admin.docs.create');
        return view(GlobalClass::getDomainFolder(2) . "::dev.docs.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return back()->with('success','¡Mensaje creado con éxito!');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


        //return view('admin.docs.show');
        return view(GlobalClass::getDomainFolder(2) . "::dev.docs.show");

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


       // return view('admin.docs.edit');
        return view(GlobalClass::getDomainFolder(2) . "::dev.docs.edit");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        return back()->with('success','¡El registro se actualizo con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return back()->with('success','¡Fue eliminado correctamente');
    }
}
