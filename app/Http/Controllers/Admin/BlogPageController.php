<?php

namespace App\Http\Controllers\Admin;

use App\Classes\GlobalClass;
use App\Classes\MetaTags;
use App\Http\Controllers\Controller;
use App\Models\BlogPage;
use DOMDocument;
use Illuminate\Http\Request;

use function PHPSTORM_META\map;

class BlogPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content = BlogPage::orderBy('created_at', 'desc')->get();
        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-page.index", ['content' => $content]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-page.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $array = MetaTags::get_meta_tags($request->url);
        //dd($array);

        BlogPage::create($array);

        return back()->with('success', '¡Agregado con éxito!');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $content = BlogPage::find($id);

        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-page.show", ['content' => $content]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $content = BlogPage::find($id);

        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-page.edit", ['content' => $content]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        BlogPage::where('id', $id)->update($request->toArray());
        return back()->with('success', '¡El registro se actualizo con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        BlogPage::destroy($id);
        return back()->with('success', '¡Fue eliminado correctamente');
    }

    public function allContentTypes()
    {
        $response = BlogPage::all();
        return response()->json($response);
    }
}
