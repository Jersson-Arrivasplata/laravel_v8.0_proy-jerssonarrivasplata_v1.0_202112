<?php

namespace App\Http\Controllers\Admin;

use App\Classes\GlobalClass;
use App\Enums\Activated;
use App\Http\Controllers\Controller;
use App\Services\BlogContentService;
use Illuminate\Http\Request;

class BlogContentHighlightController extends Controller
{

    protected $blobContentService;

    public function __construct(BlogContentService $blobContentService)
    {
        $this->blobContentService = $blobContentService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $path = $request->input('path'); // Capture the 'name' parameter from the request
        $page = $request->input('page', 1); // Captura el parámetro 'page' de la solicitud, con un valor predeterminado de 1 si no se proporciona


        if($path) {
            $response =  $this->blobContentService->getAllByType($path, Activated::All);
        }else{
            $response = $this->blobContentService->getAll();
        }

        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-content-highlight.index", [
            'contents' => $response,
            'path'=>$path,
            'page' => $page // Pasa el 'page' a la vista
        ]);
    }

    public function create()
    {
        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-content-highlight.create");
    }

    public function store(Request $request)
    {
        $array = json_decode($request->getContent(), true);

        $response = $this->blobContentService->store($array);

        return  response()->json($response);
    }

    public function show($id)
    {
        $response = $this->blobContentService->getById($id);

        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-content-highlight.show", ['c' => $response]);
    }

    public function update(Request $request, $id)
    {
        //
        $array = json_decode($request->getContent(), true);

        $response = $this->blobContentService->update($array, $id);

        return  response()->json($response);
    }
    public function destroy($id)
    {
        $response = $this->blobContentService->remove($id);

        return response()->json($response);
    }

    public function addNewBlank($id)
    {
        $response = $this->blobContentService->addNewBlank($id);

        return response()->json($response);
    }
}
