<?php

namespace App\Http\Controllers\Admin;

use App\Classes\GlobalClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use App\Models\BlogSubscribe;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogSubscribers = BlogSubscribe::all();
        //return View::make('admin.home.index', compact('blogSubscribers'));
        return view(GlobalClass::getDomainFolder(2) ."::dev.home.index", compact('blogSubscribers'));

    }
}
