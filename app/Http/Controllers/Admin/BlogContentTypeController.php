<?php

namespace App\Http\Controllers\Admin;

use App\Classes\GlobalClass;
use App\Http\Controllers\Controller;
use App\Models\BlogContentType;
use App\Models\Message;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class BlogContentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content = BlogContentType::orderBy('created_at','desc')->paginate(15);
        //return view('admin.blog-content-type.index', ['content' => $content]);
        return view(GlobalClass::getDomainFolder(2) ."::dev.blog-content-type.index", ['content' => $content]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $type = (BlogContentType::orderBy('type', 'DESC')->latest('type')->value('type') + 1);

        //return view('admin.blog-content-type.create',compact('type'));
        return view(GlobalClass::getDomainFolder(2). "::dev.blog-content-type.create", compact('type'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileName = "";
        $path = public_path().'/img/blog/content-type/';
        $files = $request->file('file');
        foreach($files as $file){
            $fileName = $file->getClientOriginalName();
            $file->move($path, $fileName);
        }

        $array = [
            'name' => $request->name,
            'description' => $request->description,
            'type'  => $request->type,
            'image'  => $fileName,
            'highlight'  => $request->highlight,
            'background'  => $request->background
        ];
        BlogContentType::create($array);

        return back()->with('success','¡Agregado con éxito!');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $content = BlogContentType::find($id);

        //return view('admin.blog-content-type.show', ['content' => $content]);
        return view(GlobalClass::getDomainFolder(2) ."::dev.blog-content-type.show", ['content' => $content]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $content = BlogContentType::find($id);

        //return view('admin.blog-content-type.edit', ['content' => $content]);
        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-content-type.edit", ['content' => $content]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $fileName = "";
        $path = public_path().'/img/blog/content-type/';
        $files = $request->file('file');
        foreach($files as $file){
            $fileName = $file->getClientOriginalName();
            $file->move($path, $fileName);
        }

        $array = [
            'name' => $request->name,
            'description' => $request->description,
            'type'  => $request->type,
            'image'  => $fileName,
            'highlight'  => $request->highlight,
            'background'  => $request->background
        ];

        BlogContentType::where('id', $id)->update($array);
        return back()->with('success','¡El registro se actualizo con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        BlogContentType::destroy($id);
        return back()->with('success','¡Fue eliminado correctamente');
    }

    public function allContentTypes(){
        $response = BlogContentType::all();
        return response()->json($response);
    }
}
