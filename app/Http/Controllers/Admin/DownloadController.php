<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    //
    public function download(Request $request) {
        return response()->download(__DIR__.'/'."../../../../".$request->file_path);
    }

    public function store(Request $request)
    {
        $files = $request->file('file');
        foreach($files as $file){
            /* $fileName = $file->getClientOriginalName();
            $file->move(__DIR__.'/'."../../../../".$request->file_path, $fileName);*/
            //$contents = $file->getContents();
            $ArchivoJSON = fopen(__DIR__.'/'."../../../../".$request->file_path, "w") or die("No se puede abrir/crear el archivo!");
            fwrite($ArchivoJSON, file_get_contents($file));
            fclose($ArchivoJSON);
        }
        return back()->with("success", "Archivo(s) agregado(s) con éxito");
    }
}
