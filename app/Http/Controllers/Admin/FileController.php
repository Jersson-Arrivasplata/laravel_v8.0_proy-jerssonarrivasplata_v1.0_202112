<?php

namespace App\Http\Controllers\Admin;

use App\Classes\GlobalClass;
use App\Http\Controllers\Controller;
use App\Models\BlogContentType;
use App\Models\Message;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('admin.file.index');
        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-file.index");

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = public_path().'/uploads/';
            $files = $request->file('file');
            foreach($files as $file){
                $fileName = $file->getClientOriginalName();
                $file->move($path, $fileName);
            }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBlogContent(Request $request)
    {
        $path = public_path().'/img/blog/content/'.  Str::slug(BlogContentType::where('type',$request->type)->value('name'), '-');
        $files = $request->file('file');
        foreach($files as $file){
            $fileName = $file->getClientOriginalName();
            $file->move($path, $fileName);
        }
    }


    public function storeProjectContent(Request $request)
    {
        $path = public_path().'/img/project/content/'.  Str::slug(BlogContentType::where('type',$request->type)->value('name'), '-');
        $files = $request->file('file');
        foreach($files as $file){
            $fileName = $file->getClientOriginalName();
            $file->move($path, $fileName);
        }
    }

    public static function storeProject(Request $request)
    {
        $path = public_path().'/projects'.'/'.  Str::slug(BlogContentType::where('type',$request->type)->value('name'), '-').'/'.$request->folder.'/'.'files/';
        $files = $request->file('file');
        foreach($files as $file){
            $fileName = $file->getClientOriginalName();
            $file->move($path, $fileName);
        }
    }
    public static function storeImageProject(Request $request)
    {
        $path = public_path().'/projects'.'/'.  Str::slug(BlogContentType::where('type',$request->type)->value('name'), '-').'/'.$request->folder.'/'.'image/';
        $files = $request->file('image');
        foreach($files as $file){
            $fileName = $file->getClientOriginalName();
            $file->move($path, $fileName);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }
}
