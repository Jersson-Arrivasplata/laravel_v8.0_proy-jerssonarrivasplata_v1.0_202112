<?php

namespace App\Http\Controllers\Admin;

use App\Classes\GlobalClass;
use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content = Message::orderBy('created_at','desc')->paginate(15);
        //return view('admin.message.index', ['content' => $content]);
        return view(GlobalClass::getDomainFolder(2) ."::user.message.index", ['content' => $content]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       // return view('admin.message.create');
        return view(GlobalClass::getDomainFolder(2) . "::user.message.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $array = [
            'name' => $request->name,
            'email' => $request->email,
            'celphone' => $request->celphone,
            'message'  => $request->message
        ];

        Message::create($array);
        return back()->with('success','¡Mensaje creado con éxito!');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $content = Message::find($id);

        //return view('admin.message.show', ['content' => $content]);
        return view(GlobalClass::getDomainFolder(2) . "::user.message.show", ['content' => $content]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $content = Message::find($id);

        //return view('admin.message.edit', ['content' => $content]);
        return view(GlobalClass::getDomainFolder(2)."::user.message.edit", ['content' => $content]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $array = [
            'name' => $request->name,
            'email' => $request->email,
            'celphone' => $request->celphone,
            'message'  => $request->message
        ];

        Message::where('id', $id)->update($array);
        return back()->with('success','¡El registro se actualizo con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Message::destroy($id);
        return back()->with('success','¡Fue eliminado correctamente');
    }
}
