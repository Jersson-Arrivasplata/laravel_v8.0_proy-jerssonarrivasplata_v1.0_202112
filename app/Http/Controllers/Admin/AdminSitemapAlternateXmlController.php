<?php


namespace App\Http\Controllers\Admin;

use App\Classes\GlobalClass;
use App\Http\Controllers\Controller;
use App\Models\SiteMap;
use App\Models\SiteMapAlternates;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AdminSitemapAlternateXmlController extends Controller
{
    public function show($id)
    {
        
        $content = SiteMapAlternates::where('id_sitemaps', $id)->orderBy('created_at', 'desc')->paginate(15);
      
        return view(GlobalClass::getDomainFolder(2) . "::admin.admin-sitemap-alternate.show", ['content' => $content, 'id' => $id]);
    }

    public function destroy($id)
    {
        //
        SiteMapAlternates::destroy($id);
        return back()->with('success','¡Fue eliminado correctamente');
    }

    public function store(Request $request)
    {

        SiteMapAlternates::create($request->toArray());
        return back()->with('success', '¡Mensaje creado con éxito!');
    }
}
