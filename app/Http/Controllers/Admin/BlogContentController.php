<?php

namespace App\Http\Controllers\Admin;

use App\Classes\GlobalClass;
use App\Http\Controllers\Controller;
use App\Models\BlogContent;
use App\Models\BlogContentType;
use App\Models\Message;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BlogContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content = BlogContent::orderBy('created_at', 'desc')->paginate(15);
        return view(GlobalClass::getDomainFolder(2) ."::dev.blog-content.index", ['content' => $content]);

        //return view('admin.blog-content.index', ['content' => $content]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //return view('admin.blog-content.create');

        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-content.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $fileName = "";
        $path = public_path().'/img/blog/content/'.  Str::slug(BlogContentType::where('type',$request->type)->value('name'), '-');
        $files = $request->file('file');
        foreach($files as $file){
            $fileName = $file->getClientOriginalName();
            $file->move($path, $fileName);
        }*/

        $array = [
            'type' => $request->type,
            'documention_link' => $request->documention_link,
            'name' => $request->name,
            'content'  => $request->content,
            'resume'  => $request->resume,
            'path'  => 'blog/' . BlogContentType::where('type', $request->type)->value('name') . '/',
            'url'  => Str::slug($request->name, '-'),

            //'image'  =>  'content/'.  Str::slug(BlogContentType::where('type',$request->type)->value('name'), '-').'/'.$fileName,
            'button_content'  => $request->button_content
        ];

        BlogContent::create($array);
        return back()->with('success', '¡Agregado con éxito!');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $content = BlogContent::find($id);

        //return view('admin.blog-content.show', ['content' => $content]);
        return view(GlobalClass::getDomainFolder(2) . "::dev.blog-content.show", ['content' => $content]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $content = BlogContent::find($id);

       // return view('admin.blog-content.edit', ['content' => $content]);

        return view(GlobalClass::getDomainFolder(2) ."::dev.blog-content.edit", ['content' => $content]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        /* $fileName = "";
        $path = public_path().'/img/blog/content/'.  Str::slug(BlogContentType::where('type',$request->type)->value('name'), '-');
        $files = $request->file('file');
        foreach($files as $file){
            $fileName = $file->getClientOriginalName();
            $file->move($path, $fileName);
        }*/

        $array = [
            'type' => $request->type,
            'documention_link' => $request->documention_link,
            'name' => $request->name,
            'content'  => $request->content,
            'resume'  => $request->resume,
            'path'  => 'blog/' . BlogContentType::where('type', $request->type)->value('name') . '/',
            'url'  => Str::slug($request->name, '-'),
            //'image'  => 'content/'.  Str::slug(BlogContentType::where('type',$request->type)->value('name'), '-').'/'.$fileName,
            'button_content'  => $request->button_content
        ];

        BlogContent::where('id', $id)->update($array);
        return back()->with('success', '¡El registro se actualizo con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        BlogContent::destroy($id);
        return back()->with('success', '¡Fue eliminado correctamente');
    }

    public function createContent()
    {
        return view('admin.blog-content.create-content');
    }
}
