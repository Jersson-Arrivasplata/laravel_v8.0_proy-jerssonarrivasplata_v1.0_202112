<?php

namespace App\Http\Controllers\Api;

use App\Enums\TypeAI;
use App\Helper\PromptOpenAIHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\OpenAIService;
use App\Services\SaiAIService;
use App\Services\TipService;

class TipController extends Controller
{
    protected $openAI;
    protected $saiAI;
    protected $tipService;

    public function __construct(OpenAIService $openAI, SaiAIService $saiAI,  TipService $tipService)
    {
        $this->openAI = $openAI;
        $this->saiAI = $saiAI;
        $this->tipService = $tipService;
    }

    public function getOpenAITip(Request $request)
    {
        $language = strtolower($request->input('language', 'es')); // Idioma predeterminado: español
        $tech = $request->input('tech', 'JavaScript'); // Tecnología predeterminada: JavaScript
        $language = htmlspecialchars($language, ENT_QUOTES, 'UTF-8');
        $tech = htmlspecialchars($tech, ENT_QUOTES, 'UTF-8');

        $data = ['language' => $language, 'tech' => $tech];

        $tip = $this->switchTypeOfToken(TypeAI::SAI, $data);

        $tipArray = PromptOpenAIHelper::extractTip($tip, $tech, $language);

        $array = [
            'tip' => $tip,
            'legend' => $tipArray['DailyTip'],
            'title' => $tipArray['Title'],
            'description' => $tipArray['Code'],
            'comment' => $tipArray['Comment'],
            'language' => $language,
            'type' => $tech,
        ];

        $blog = $this->tipService->add($array);

        return response()->json($blog);
    }

    public function switchTypeOfToken($typeAI, $data)
    {
        $language = $data['language'];
        $tech = $data['tech'];
        $tip = '';

        switch ($typeAI) {
            case TypeAI::OpenAI:
                $prompt = PromptOpenAIHelper::promptByLang($tech, $language);
                $tip = $this->openAI->generateText($prompt, 1000);
                break;
            case TypeAI::SAI:
                $tip = $this->saiAI->generatePromptByLanguageAndTechnology($tech, $language);
                break;
        }

        return $tip;
    }
}
