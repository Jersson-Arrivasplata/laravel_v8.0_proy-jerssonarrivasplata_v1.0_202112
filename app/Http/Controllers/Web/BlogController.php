<?php

namespace App\Http\Controllers\Web;

use App\Classes\GlobalClass;
use App\Enums\Activated;
use App\Http\Controllers\Controller;
use App\Models\BlogSubscribe;
use App\Services\BlogContentService;
use App\Services\BlogContentTypeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class BlogController extends Controller
{
    protected $blobContentService;
    protected $blobContentTypeService;

    public function __construct(BlogContentService $blobContentService, BlogContentTypeService $blobContentTypeService)
    {
        $this->blobContentService = $blobContentService;
        $this->blobContentTypeService = $blobContentTypeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogContentType = $this->blobContentTypeService->getAllByTypeInBlogContent();

        return view(GlobalClass::getDomainFolder(0) . "::lang.". request()->route()->parameter('language').".blog", compact('blogContentType'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contentType($translate, $path)
    {

        $type = $this->blobContentTypeService->getByPath($path)['type'];

        $contents = $this->blobContentService->getAllByType($path, Activated::Active);

        return View::make(GlobalClass::getDomainFolder(5). "::lang.". request()->route()->parameter('language').".content", compact('contents', 'type'));
    }

    public function subscribe(Request $request)
    {

        $array = [
            'email' => $request->email
        ];

        BlogSubscribe::create($array);
        return back()->with('success', '¡Gracias por suscribirte!');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
