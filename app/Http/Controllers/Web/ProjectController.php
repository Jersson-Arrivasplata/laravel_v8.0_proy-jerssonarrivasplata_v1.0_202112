<?php

namespace App\Http\Controllers\Web;

use App\Classes\GlobalClass;
use App\Http\Controllers\Controller;
use App\Models\BlogContentType;
use App\Models\BlogSubscribe;
use App\Models\ProjectContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogContentType = BlogContentType::all();

        return View::make(GlobalClass::getDomainFolder(5). "::lang.". request()->route()->parameter('language').".project.index", compact('blogContentType'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function subscribe(Request $request)
    {

        $array = [
            'email' => $request->email
        ];

        BlogSubscribe::create($array);
        return back()->with('success','¡Gracias por suscribirte!');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public  function show($translate, $name, $url)
    {
        //
        $response = ProjectContent::where('url', $url)->get();

        return View::make(GlobalClass::getDomainFolder(5). "::lang.". request()->route()->parameter('language').".components.index", compact('response','name','url'));
    }

    public static function contentType($translate, $name)
    {
        $type = BlogContentType::where('name', $name)->value('type');

        $content = ProjectContent::orderBy('created_at', 'desc')->where('type',$type)->get();

        return View::make(GlobalClass::getDomainFolder(5). "::lang.". request()->route()->parameter('language').".project.content", compact('content','type'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
