<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;

class CheckLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $language = $request->route()->parameter('language');
        if ($language) {
            if (App::getLocale() != $language) {
                App::setLocale($language);
            }
        } else {
            App::setLocale('es');
        }
        return $next($request);
    }
}
