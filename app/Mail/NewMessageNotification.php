<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewMessageNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $details;
    public $title;

    /**
     * Create a new message instance.
     *
     * @param  array  $details
     * @return void
     */
    public function __construct($details, $title)
    {
        $this->details = $details;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = 'Nuevo mensaje de contacto desde ' . $this->title;
        return $this->subject($title)
            ->view('emails.message')
            ->with([
                'details' => $this->details,
                'title' =>  $title,
            ]);
    }
}
