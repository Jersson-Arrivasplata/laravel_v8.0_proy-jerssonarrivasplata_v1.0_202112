//const dropzone = new Dropzone("div.my-dropzone", { url: "/file/post" });

const dropzone = new Dropzone("#admin-blog-content", {
    autoProcessQueue: false,
    uploadMultiple: true,
    maxFilezise: 10,
    maxFiles: 2,
    clickable: "#dropzone-click-target",
    addRemoveLinks: true,
    autoQueue: true,
    dictRemoveFile: "Remover archivo",
    url: $('#admin-blog-content').attr('action'),
    autoDiscover: false,
    sending: function(data, xhr, formData) {
        formData.append('_token', "{{ csrf_token() }}");

    },

    removeFilePromise: function removeFilePromise() {
        return new Promise(function(resolve, reject) {
            var rand = Math.floor(Math.random() * 3);
            console.log(rand);
            if (rand == 0) reject('¡No se pudo remover adecuadamente!');
            if (rand > 0) resolve();
        });
    },
    previewTemplate: `
    <div class="dz-preview dz-file-preview jersson">
    <div class="dz-image"> <img data-dz-thumbnail /> </div>
    <div class="dz-details">
        <div class="dz-size"> <span data-dz-size></span> </div>
        <div class="dz-filename"><span data-dz-name></span></div>
    </div>
    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
    <div class="dz-error-message"><span data-dz-errormessage></span></div>
    <div class="dz-success-mark"> <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1"
            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
            xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
            <title>Check</title>
            <defs></defs>
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                <path d="M23.5,31.8431458 L17.5852419,25.9283877
                        C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508
                        10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541
                        20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202
                        26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887
                        43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022
                        L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1
                        C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2"
                    stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF"
                    sketch:type="MSShapeGroup">
                </path>
            </g>
        </svg></div>
    <div class="dz-error-mark"> <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1"
            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
            xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
            <title>Error</title>
            <defs></defs>
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158"
                    fill="#FFFFFF" fill-opacity="0.816519475">
                    <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887
                            38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022
                            L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729
                            15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564
                            L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113
                            15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978
                            L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271
                            38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436
                            L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1
                            C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2"
                        sketch:type="MSShapeGroup">
                    </path>
                </g>
            </g>
        </svg>
    </div>
</div>
    `,

    previewsContainer: '#previewsContainer',
    acceptedFiles: ".png,.jpg,.jpeg",
    init: function init() {
        var submitBtn = document.querySelector("#submit");
        myDropzone = this;
        submitBtn.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            myDropzone.processQueue();
        });
        this.on("addedfile", function(file) {
            /* if (file.size > 1048576) {
                 var removeButton = Dropzone.createElement("<div class='column'><span class='btn btn-danger btn-xs pull-right cancel'><i class='fa fa-times'></i> Remove File</span></div>");
             } else {
                 var removeButton = Dropzone.createElement("<div class='column'><span data-dz-remove class='btn btn-danger btn-xs pull-right delete'><i class='fa fa-trash-o'></i> Delete</span></div>");
             }
             file.previewElement.appendChild(removeButton);*/
            // Capture the Dropzone instance as closure.
            var self = this; // Listen to the remove button click event

            /*removeButton.addEventListener("click", function(e) {
                if (file.size > 1048576) {
                    self.removeFile(file);
                } else if (window.confirm('Are you sure you want to delete ?')) {
                  $.post("ajax/delete-file.php?nominationId=" + file.nominationId + "&id=" + file.id + "&media=" + file.media, function() {
                        self.removeFile(file);
                    });
                  }
            });*/
            // CUSTOM THUMBNAIL FOR FLES OTHER THAN IMAGE TYPE

            if (file.type == "application/pdf" || file.type == "pdf") {
                file.previewElement.querySelector("[data-dz-thumbnail]").src = "images/pdf-icon.png";
            } else if (file.type == "text/plain" || file.type == "txt") {
                file.previewElement.querySelector("[data-dz-thumbnail]").src = "images/txt-icon.png";
            } else if (file.type == "application/msword" || file.type == "docx" || file.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
                file.previewElement.querySelector("[data-dz-thumbnail]").src = "images/docx-icon.png";
            } else if (file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type == "xlsx") {
                file.previewElement.querySelector("[data-dz-thumbnail]").src = "images/xlsx-icon.png";
            }
        });
        this.on("complete", function(response) {
            if (response.status == "success") {
                $('#alerts').append(`
                <div class="alert alert-success" role="alert"  id="alertSuccess">
                    ¡Realizado con éxito!
                    <button type="button" class="btn-close" onclick="removeAlert()" aria-label="Close">X</button>
                </div>
                `);
            }
            myDropzone.removeFile(file);

            // location.replace(file['xhr']['response'])
            // window.document = file['xhr']['response']
        });
        this.on("success", myDropzone.processQueue.bind(myDropzone));
        this.on("totaluploadprogress", function(progress) {
            console.log("progress ", progress);
            $('.roller').width(progress + '%');
        });
        this.on("queuecomplete", function(progress) {
            $('.meter').delay(999).slideUp(999);
        });
    }
});

let init = {
    modalDescription: $('#descriptionModal'),
    modalDescriptionContent: '',
    modalImage: '',
    modalName: ''
}


removeAlert = () => {
    $('#alerts').empty();
};

//CKEDITOR INICIAR

var ckeditor = CKEDITOR.replace('editor');
// The "change" event is fired whenever a change is made in the editor.
ckeditor.on('change', function(evt) {
    // getData() returns CKEditor's HTML content.
    // console.log('Total bytes: ' + evt.editor.getData().length);
    $('#editor').val(evt.editor.getData());
});
var ckeditorTEXT = CKEDITOR.replace('editorHtml', {
    skin: 'moono-lisa',
    enterMode: CKEDITOR.ENTER_BR,
    shiftEnterMode: CKEDITOR.ENTER_P,
    extraAllowedContent: 'div',
    height: 300,
    toolbar: [
        { name: 'document', groups: ['mode', 'document', 'doctools'] },
        { name: 'clipboard', groups: ['clipboard', 'undo'] },
        { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
        { name: 'forms', groups: ['forms'] },
        { name: 'colors', groups: ['colors'] },
        { name: 'tools', groups: ['tools'] },
        { name: 'others', groups: ['others'] },
        { name: 'about', groups: ['about'] },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor'] },
        { name: 'styles', items: ['Format', 'Font', 'FontSize'] },
        { name: 'scripts', items: ['Subscript', 'Superscript'] },
        { name: 'justify', groups: ['blocks', 'align'], items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
        { name: 'paragraph', groups: ['list', 'indent'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'] },
        { name: 'links', items: ['Link', 'Unlink'] },
        { name: 'insert', items: ['Image'] },
        { name: 'spell', items: ['jQuerySpellChecker'] },
        { name: 'table', items: ['Table'] }
    ],
});

ckeditorTEXT.on('instanceReady', function() {
    // Output self-closing tags the HTML4 way, like <br>.
    this.dataProcessor.writer.selfClosingEnd = '>';

    // Use line breaks for block elements, tables, and lists.
    var dtd = CKEDITOR.dtd;
    for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
            indent: true,
            breakBeforeOpen: true,
            breakAfterOpen: true,
            breakBeforeClose: true,
            breakAfterClose: true
        });
    }
    // Start in source mode.
    this.setMode('source');
});
