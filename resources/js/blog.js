let init = {
    modalDescription: $('#descriptionModal'),
    modalDescriptionContent: '',
    modalImage: '',
    modalName: ''
}

showModalDescription = (id) => {
    init.modalDescriptionContent = $('.blog-index__btn-content_' + id).data('content');
    init.modalImage = $('.blog-index__btn-content_' + id).data('image');
    init.modalName = $('.blog-index__btn-content_' + id).data('name');

    init.modalDescription.modal('show');
}
closeModalDescription = () => {
    init.modalDescription.modal('hide');
}
init.modalDescription.on('show.bs.modal', (event) => {
    init.modalDescription.find('.modal-body').empty();
    // init.modalDescription.find('.modal-body').append('<h1 class="text-center">' + init.modalName + '</h1>');
    init.modalDescription.find('.modal-body').html(init.modalDescriptionContent);
    //init.modalDescription.find('.modal-body').append('<img src="../img/blog/' + init.modalImage + '" style="  margin-top:1em;   width: 100%;"/>');
});

loader = () => {
    const loaderClass = document.getElementsByClassName('loader')[0].classList;
    const classVerify = 'd-none';
    if (loaderClass.contains(classVerify)) loaderClass.remove(classVerify);
}