(() => {
    menu = () => {
        const headerClass = document.getElementsByTagName('header')[0].classList;
        const classVerify = 'header-close';
        headerClass.contains(classVerify) ? headerClass.remove(classVerify) : headerClass.add(classVerify);
    };

    loaderWithHash = () => {
        if (location.pathname !== '/') {
            loader();
        }

    };
    loaderWithOutHash = () => {
        loader();

    };
    loader = () => {
        const loaderClass = document.getElementsByClassName('loader')[0].classList;
        const classVerify = 'd-none';
        if (loaderClass.contains(classVerify)) loaderClass.remove(classVerify);
    }
})();