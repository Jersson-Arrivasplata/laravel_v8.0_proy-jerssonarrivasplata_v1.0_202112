let init = {
    modalDescription: $('#descriptionModal'),
    modalDescriptionContent: ''
}

showModalDescription = (id) => {
    init.modalDescriptionContent = $('.admin-message-index__btn-show-modal-description_' + id).data('content');
    init.modalDescription.modal('show');
}
init.modalDescription.on('show.bs.modal', (event) => {
    init.modalDescription.find('.modal-body').append(init.modalDescriptionContent);
})