<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
</head>
<body>
    <h1>¡Tienes un nuevo Mensaje!</h1>
    <p><strong>Nombre:</strong> {{ $details['name'] }}</p>
    <p><strong>Correo:</strong> {{ $details['email'] }}</p>
    <p><strong>N&uacute;mero de Contacto:</strong> {{ $details['celphone'] }}</p>
    <p><strong>Lenguaje:</strong> {{ $details['language'] }}</p>
    <p><strong>Mensaje:</strong> {{ $details['message'] }}</p>
</body>
</html>
