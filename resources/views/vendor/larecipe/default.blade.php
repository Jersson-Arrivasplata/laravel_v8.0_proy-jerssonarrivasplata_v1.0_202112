<!doctype html>
<html>
    <head>
        <!-- META Tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title>{{ isset($title) ? $title . ' | ' : null }}{{ config('app.name') }}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--Inicio Limpiar Cache-->
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <!--Fin Limpiar Cache-->
        <link rel="icon" type="image/png"  href="{{ Utils::getUrlBase('doc') }}/img/doc.jerssonarrivasplata.com.png">
        <meta name="msapplication-TileColor" content="#cc1e2d">
        <meta name="msapplication-TileImage" content="{{ Utils::getUrlBase('doc') }}/img/doc.jerssonarrivasplata.com.png">
        <meta name="theme-color" content="#cc1e2d">

        <!-- SEO -->
        <meta name="author" content="{{ config('larecipe.seo.author') }}">
        <meta name="description" content="Documentaci&oacute;n de{{ isset($title) ?' '. $title : null }} desarrollado por {{ GlobalClass::getCreatorUser() }} -
            Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
            con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
            an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">
        <meta name="keywords" content="{{ config('larecipe.seo.keywords') }}">
        <meta name="twitter:card" value="summary">
        @if (isset($canonical) && $canonical)
            <link rel="canonical" href="{{ url($canonical) }}" />
        @endif
        @if($openGraph = config('larecipe.seo.og'))
            @foreach($openGraph as $key => $value)
                @if($value)
                    <meta property="og:{{ $key }}" content="{{ $value }}" />
                @endif
            @endforeach
        @endif

        <meta property="og:title" content="{{ isset($title) ? $title . ' | ' : null }}{{ config('app.name') }}" />
        <meta property="og:description" content="Documentaci&oacute;n de{{ isset($title) ?' '. $title : null }} desarrollado por {{ GlobalClass::getCreatorUser() }} -
            Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
            con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
            an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">


        <meta property="og:url" content="{{ url()->full() }}" />
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="65">
        <meta property="og:image:height" content="65">

        <!--http://doc.jerssonarrivasplata2.com/docs/1.0/angular/angular.guideline-->
        <!-- CSS -->
        <link rel="stylesheet" href="{{ larecipe_assets('css/app.css') }}">

        @if (config('larecipe.ui.fav'))
            <!-- Favicon -->
            <link rel="apple-touch-icon" href="{{ asset(config('larecipe.ui.fav')) }}">
            <link rel="shortcut icon" type="image/png" href="{{ asset(config('larecipe.ui.fav')) }}"/>
        @endif

        <!-- FontAwesome -->
        <link rel="stylesheet" href="{{ larecipe_assets('css/font-awesome.css') }}">
        @if (config('larecipe.ui.fa_v4_shims', true))
            <link rel="stylesheet" href="{{ larecipe_assets('css/font-awesome-v4-shims.css') }}">
        @endif

        <!-- Dynamic Colors -->
        @include('larecipe::style')

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @foreach(LaRecipe::allStyles() as $name => $path)
            @if (preg_match('/^https?:\/\//', $path))
                <link rel="stylesheet" href="{{ $path }}">
            @else
                <link rel="stylesheet" href="{{ route('larecipe.styles', $name) }}">
            @endif
        @endforeach
        <meta name="twitter:card" content="summary">
        <meta name="twitter:url" content="{{ url()->full() }}">
        <meta name="twitter:title" content="{{ isset($title) ? $title . ' | ' : null }}{{ config('app.name') }}">
        <meta name="twitter:description" content="Documentaci&oacute;n de{{ isset($title) ?' '. $title : null }} desarrollado por {{ GlobalClass::getCreatorUser() }} -
            Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
            con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
            an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">
        <meta name="twitter:site" content="@jerssonarrivas">
        <meta name="twitter:creator" content="@jerssonarrivas">



        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-WJ9HTCVVV6"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-WJ9HTCVVV6');
        </script>
    </head>
    <body>
        <div id="app" v-cloak>
            @include('larecipe::partials.nav')

            @include('larecipe::plugins.search')

            @yield('content')

            <larecipe-back-to-top></larecipe-back-to-top>
        </div>


        <script>
            window.config = @json([]);
        </script>

        <script type="text/javascript">
            if(localStorage.getItem('larecipeSidebar') == null) {
                localStorage.setItem('larecipeSidebar', !! {{ config('larecipe.ui.show_side_bar') ?: 0 }});
            }
        </script>

        <script src="{{ larecipe_assets('js/app.js') }}"></script>

        <script>
            window.LaRecipe = new CreateLarecipe(config)
        </script>

        <!-- Google Analytics -->
        @if(config('larecipe.settings.ga_id'))
            <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('larecipe.settings.ga_id') }}"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', "{{ config('larecipe.settings.ga_id') }}");
            </script>
        @endif
        <!-- /Google Analytics -->

        @foreach (LaRecipe::allScripts() as $name => $path)
            @if (preg_match('/^https?:\/\//', $path))
                <script src="{{ $path }}"></script>
            @else
                <script src="{{ route('larecipe.scripts', $name) }}"></script>
            @endif
        @endforeach

        <script>
            LaRecipe.run()
        </script>
    </body>
</html>
