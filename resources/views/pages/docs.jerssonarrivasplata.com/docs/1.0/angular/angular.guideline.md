# Lineamientos de Angular

---
<h2>Lista de Lineamientos</h2>

1. Lineamientos para nombrar un Proyecto <a href="#section-angular-guideline-1">(Presione aquí para Ir)</a>
2. Lineamientos del uso de Enum <a href="#section-angular-guideline-2">(Presione aquí para Ir)</a>
3. Lineamientos del uso de Interfaces <a  href="#section-angular-guideline-3">(Presione aquí para Ir)</a>
4. Lineamientos del uso de Types <a  href="#section-angular-guideline-4">(Presione aquí para Ir)</a>
5. Lineamientos del uso de Environments <a  href="#section-angular-guideline-5">(Presione aquí para Ir)</a>
6. Lineamientos de Nombre de Estilos (Scss, Css u Otros) <a  href="#section-angular-guideline-6">(Presione aquí para Ir)</a>
7. Lineamientos del uso de TypeScript (Componentes, Clases, Modelos u Otros)<a  href="#section-angular-guideline-7">(Presione aquí para Ir)</a>


<a name="section-angular-guideline-1"></a>
 
## Lineamiento para nombrar proyecto en angular

<a href="#section-angular-guideline-1-step-1" name="section-angular-guideline-1-step-1">
  ### <span># </span>Paso 1. Tabla de Lineamiento 
</a>

1. Lineamientos de Nombre de Repositorios

| Framework | Tipo | Versión  | Nombre del Proyecto  | Tipo  | Año  | Mes | Nombre del Repositorio                         |
| --------- | ---- | -------- | -------------------- | ----- | ---- | --- | ---------------------------------------------- |
| ANGULAR   | PROY | VX       | XXXXXXX              | S     | YY   | MM  | ANGULAR-PROY-VX-XXXXXXX-SYYMM                  |
| ANGULAR   | PROY | V1       | EJEMPLO              | S     | 22   | 01  | ANGULAR-PROY-V1-EJEMPLO-S2201                  |

<a href="#section-angular-guideline-1-step-2" name="section-angular-guideline-1-step-2">
  ### <span># </span>Paso 2. Descripción de la Tabla de Lineamiento
</a>

1. Columna #04 (Framework): Nombre del Framework del desarrollo.
2. Columna #01 (Tipo): Tipo de proyecto que hace referencia al proyecto - (Ejemplo: **PROY** -> Proyecto).
3. Columna #02 (Versión - **VX**): Versión del proyecto - (Ejemplo: **V1**).
4. Columna #03 (Nombre): Nombre que se le asignará al proyecto.
5. Columna #05 (Tipo): Indica que S de Starter - Inicio del Proyecto y continua la Fecha - (Ejemplo: **S**).
6. Columna #06 (Año - **YY**): Año en la que se creo el proyecto - (Ejemplo: **22**).
7. Columna #07 (Mes - **MM**): Mesa en la que se creo el proyecto - (Ejemplo: **01**).
8. Columna #08 (Nombre del Repositorio): Nombre final que resulta al unir todos los campos anteriores - (Ejemplo: **PROY-V1-EJEMPLO-ANGULAR-S2201**).

<a name="section-angular-guideline-2"></a>

## Lineamientos del uso de Enum en Proyecto Angular

<a href="#section-angular-guideline-2-step-1" name="section-angular-guideline-2-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos del uso de Enum 
</a>

1. Tabla de Lineamiento de Archivos Enum

| Convención de Nombre             | Descripción                                                                          | Resultado                  |
| -------------------------------- | ------------------------------------------------------------------------------------ | -------------------------- |
| Lower Kebab Case                 | Cada palabra en minuscula se separa con un guión y se concatena con (**.enum.ts.**)  | lower-kebab-case.enum.ts   |

2. Tabla de Lineamiento de Nombre de Clases Enum

| Convención de Nombre             | Descripción                                                                                                  | Resultado                  |
| -------------------------------- | ------------------------------------------------------------------------------------------------------------ | -------------------------- |
| Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas y se concatena con (**Enum**) al ultimo.  | UpperCamelCaseEnum         |

3. Tabla de Lineamiento de Clave - Valor de un Enum

| Convención de Nombre             | Descripción                                                             | Resultado                  |
| -------------------------------- | ----------------------------------------------------------------------- | -------------------------- |
| Upper Snake Case                 | Cada letra en mayuscula separada por un guión bajo.    	               | UPPER_SNAKE_CASE           |

<a href="#section-angular-guideline-2-step-2" name="section-angular-guideline-2-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Archivos Enum
</a>

Ejemplo: Archivo Enum que contiene los errores de un registro

```js
document-type.enum.ts

export enum ErrorMessageRegisterEnum {
  ERROR_COMPLETE_FIELD = 'Debes completar este campo.'
}

```

1. **document-type.enum.ts**: Revisar la Tabla de Lineamiento de Archivos Enum
2. **ErrorMessageRegisterEnum**: Revisar la Tabla de Lineamiento de Nombre de Clases Enum
3. **ERROR_COMPLETE_FIELD**: Revisar la Tabla de Lineamiento de Clave - Valor de un Enum



<a name="section-angular-guideline-3"></a>

## Lineamientos del uso de Interfaces en Proyecto Angular


<a href="#section-angular-guideline-3-step-1" name="section-angular-guideline-3-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos del uso de Interfaces 
</a>

1. Tabla de Lineamiento de Archivos Interface

| Convención de Nombre             | Descripción                                                                               | Resultado                       |
| -------------------------------- | ----------------------------------------------------------------------------------------- | ------------------------------- |
| Lower Kebab Case                 | Cada palabra en minuscula se separa con un guión y se concatena con (**.interface.ts.**)  | lower-kebab-case.interface.ts   |

2. Tabla de Lineamiento de Nombre de Clase Interface

| Convención de Nombre             | Descripción                                                                                                 | Resultado                  |
| -------------------------------- | ----------------------------------------------------------------------------------------------------------- | -------------------------- |
| Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas y se concatena con (**I**) al inicio. 	 | IUpperCamelCase            |

3. Tabla de Lineamiento de métodos y propiedades de una Interface

| Tipo               | Convención de Nombre             | Descripción                                                                                        | Resultado                  |
| ------------------ | -------------------------------- | ------------------------------------------------------------------------------------------------   | -------------------------- |
| Propiedad o Método | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas e inicia con minúscula.         | upperCamelCase             |
| Interface | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas  y se concatena con (**I**) al inicio.	 | IUpperCamelCase            |
| Enum      | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas y se concatena con (**Enum**) al ultimo. | UpperCamelCaseEnum         |
| Type      | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas  y se concatena con (**T**) al inicio.	 | TUpperCamelCase            |


<a href="#section-angular-guideline-3-step-2" name="section-angular-guideline-3-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Archivos Interface
</a>

Ejemplo: Archivo Interface que contiene las propiedades de un perfil de usuario 

```js
user.interface.ts

export interface IUser {
  name: string;
  lastName: string;
  birthDate: Date;
  age: void;
}

```

1. **user.interface.ts**: Revisar la Tabla de Lineamiento de Archivos Interface
2. **IUser**: Revisar la Tabla de Lineamiento de Nombre de Clase Interface
3. **name || lastName || birthDate || age**: Revisar la Tabla de Lineamiento de métodos y propiedades de una Interface

<a name="section-angular-guideline-4"></a>

## Lineamientos del uso de Types en Proyecto Angular


<a href="#section-angular-guideline-4-step-1" name="section-angular-guideline-4-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos del uso de Types 
</a>

1. Tabla de Lineamiento de Archivos Types

| Convención de Nombre             | Descripción                                                                          | Resultado                  |
| -------------------------------- | ------------------------------------------------------------------------------------ | -------------------------- |
| Lower Kebab Case                 | Cada palabra en minuscula se separa con un guión y se concatena con (**.type.ts.**)  | lower-kebab-case.type.ts   |

2. Tabla de Lineamiento de Nombre de Clase Type

| Convención de Nombre             | Descripción                                                                                                 | Resultado                  |
| -------------------------------- | ----------------------------------------------------------------------------------------------------------- | -------------------------- |
| Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas  y se concatena con (**T**) al inicio.	 | TUpperCamelCase            |


3. Tabla de Lineamiento de métodos y propiedades de Clase Type

| Tipo               | Convención de Nombre             | Descripción                                                                                        | Resultado                  |
| ------------------ | -------------------------------- | ------------------------------------------------------------------------------------------------   | -------------------------- |
| Propiedad o Método | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas e inicia con minúscula.         | upperCamelCase             |
| Interface | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas  y se concatena con (**I**) al inicio.	 | IUpperCamelCase            |
| Enum      | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas y se concatena con (**Enum**) al ultimo. | UpperCamelCaseEnum         |
| Type      | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas  y se concatena con (**T**) al inicio.	 | TUpperCamelCase            |

<a href="#section-angular-guideline-4-step-2" name="section-angular-guideline-4-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Archivos Types
</a>

Ejemplo: Archivo Type que contiene las propiedades de un perfil de usuario 

```js
user.type.ts

export type TUser = {
  name: string;
  lastName: string;
  birthDate: Date;
  age: void;
}

```
Ejemplo: Archivo Type que contiene las propiedades de una interfaz de perfil de usuario
```js
user.type.ts

export type TUser = IUser;

```

1. **user.type.ts**: Revisar la Tabla de Lineamiento de Archivos Types
2. **TUser**: Revisar la Tabla de Lineamiento de Nombre de Clase Type
3. **name || lastName || birthDate || age || IUser**: Revisar la Tabla de Lineamiento de métodos y propiedades de Clase Type

<a name="section-angular-guideline-5"></a>

## Lineamientos del uso de Environments en Proyecto Angular


<a href="#section-angular-guideline-5-step-1" name="section-angular-guideline-5-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos del uso de Environments 
</a>

1. Tabla de Lineamiento de Archivos Environment

| Convención de Nombre       | Descripción                                                        | Resultado        |
| -------------------------- | ------------------------------------------------------------------ | ---------------- |
| Lower Case                 | Cada palabra en minuscula y se concatena con un tipo (**.xx.ts**)  | environment.ts   |

2. Tabla de Lineamiento de Tipos de Environment

| Convención de Nombre             | Tipos                                                                       | Resultado                  |
| -------------------------------- | --------------------------------------------------------------------------- | -------------------------- |
| Lower Kebab Case                 | **.ts** o  **.local** o  **.dev.ts** o  **.cert.ts** o  **.prod.ts**        | environment.dev.ts         |


3. Tabla de Lineamiento de métodos y propiedades de la constante Environment

| Tipo               | Convención de Nombre             | Descripción                                                                                    | Resultado                  |
| ------------------ | -------------------------------- | --------------------------------------------------------------------------------------------   | -------------------------- |
| Propiedad o Método | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas e inicia con minúscula.     | upperCamelCase             |

<a href="#section-angular-guideline-5-step-2" name="section-angular-guideline-5-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Environment
</a>

Ejemplo: Archivo Environment que contiene las propiedades de un ambiente de desarrollo

```js
environment.dev.ts

export const environment = {
  production: false,
  apiUrl: 'https://www.xxx.com/api/',
  baseUrl: 'https://www.xxx.com/',
  type: 'dev'
};

```

1. **environment.dev.ts**: Revisar la Tabla de Lineamiento de Archivos Environment
2. **.dev.ts**: Revisar la Tabla de Lineamiento de Tipos de Environment
3. **production || apiUrl || baseUrl || type**: Revisar la Tabla de Lineamiento de métodos y propiedades de la constante Environment


<a name="section-angular-guideline-6"></a>

## Lineamientos de Nombre de Estilos (Scss, Css u Otros) en Proyecto Angular


<a href="#section-angular-guideline-6-step-1" name="section-angular-guideline-6-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos de Nombre de Estilos (Scss, Css u Otros)
</a>

1. Tabla de Lineamiento de Nombre de Clase en Div Padre en Componente html

| Convención de Nombre    | Descripción                                        | Resultado          |
| ----------------------- | -------------------------------------------------- | ------------------ |
| Lower kebab Case        | Cada palabra en minuscula se separa con un guióny. | lower-kebab-case   |

2. Tabla de Lineamiento de Nombre de Clase en Divs Hijos en Componente html

| Convención de Nombre   | Tipos                                                                                                                   | Resultado                    |
| ---------------------- | ----------------------------------------------------------------------------------------------------------------------- | ---------------------------- |
| Lower kebab Case       | Cada palabra en minuscula se separa con un guión y se concatena al inicio con la clase padre (**lower-kebab-case___**)  | lower-kebab-case___lower-kebab-case-component   |

<a href="#section-angular-guideline-6-step-2" name="section-angular-guideline-6-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Nombre de Estilos (Scss, Css u Otros)
</a>

Ejemplo: Archivo example-styles.component.html

```html
example-styles.component.html

<div class="example-styles">
    <div class="example-styles___border-left">
    </div>
    <div class="example-styles___border-right">
    </div>
</div>

```

1. **example-styles**: Revisar la Tabla de Lineamiento de Nombre de Clase en Div Padre en Componente html
2. <p><span style="font-weight:bolder">example-styles___border-left || example-styles___border-right:</span> Revisar la Tabla de Lineamiento de Nombre de Clase en Divs Hijos en Componente html</p>


Ejemplo: Componente de Estilos Scss y su practica con nombres estandarizados

```scss
example-styles.component.scss

$wrap: 'example-styles';
.#{$wrap} {
    display: inline-block;
    & .#{$wrap}___border-left {
        width: 60%;
    }

    & .#{$wrap}___border-right {
        margin: 0px;
        width: 40%;
    }
}

```

Ejemplo: Componente de Estilos Css y su practica con nombres estandarizados

```css
example-styles.component.css

.example-styles {
    display: inline-block;
}

.example-styles___border-left {
    width: 60%;
}

.example-styles___border-right {
    margin: 0px;
    width: 40%;
}

```


<a name="section-angular-guideline-7"></a>

## Lineamientos del uso de TypeScript (Componentes, Clases, Modelos u Otros)


<a href="#section-angular-guideline-7-step-1" name="section-angular-guideline-7-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos en TypeScript
</a>

1. Tabla de Lineamiento de Nombre de Variables y Métodos en TypeScript

| Convención de Nombre             | Descripción                                                                                        | Resultado          |
| -------------------------------- | -------------------------------------------------------------------------------------------------- | ------------------ |
| Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas e inicia con minúscula.         | upperCamelCase     |

<a href="#section-angular-guideline-7-step-2" name="section-angular-guideline-7-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Nombre de Variables y Métodos en TypeScript
</a>

Ejemplo: Archivo example.component.ts

```js
example.component.ts

isEnable: boolean = false;

validateFormControl() { }

```

1. **isEnable || validateFormControl() { }**: Revisar la Tabla de Lineamiento de Nombre de Variables y Métodos en TypeScript

