# Formularios de Angular - @angular/forms

---
Formulario reactivo en angular y formulario reactivo controlados por plantillas procesan y administran los datos del formulario de manera diferente. 

<a name="section-angular-forms-1"></a>

## Formulario Reactivo En Angular

1. Proporcionan **acceso directo** y explícito al **modelo de objetos de formularios subyacente**.
2. Son más robustos: son más escalables, reutilizables y comprobables.
3. Si los **formularios son una parte clave de la aplicación** o si ya está utilizando patrones reactivos para crear la aplicación, use formularios reactivos.
4. Proporcionan **acceso directo a la API de formulario subyacente**.
5. Utilizan el **flujo de datos sincrónico** entre la vista y el modelo de datos.
6. Las pruebas **no requieren** una comprensión profunda de la detección de cambios para probar correctamente las actualizaciones y la validación de los formularios.
7. La directiva crea y administra una instacia para un elemento del formulario determinado (NgModel FormControl)
8. Cada vez que se activa un cambio en el modelo de datos, **la instancia devuelve un nuevo modelo de datos** en lugar de actualizar el modelo de datos existente.
9. Tiene la **capacidad de realizar un seguimiento de los cambios únicos** en el modelo de datos a través del observable del control.
10. Proporcionan un **enfoque basado en modelos** para manejar entradas de formularios cuyos valores cambian con el tiempo.
11. Cada cambio en el estado del formulario devuelve un nuevo estado, que mantiene la integridad del modelo entre cambios. 
12. Se construyen **alrededor de flujos observables**, donde las entradas y los valores de los formularios se proporcionan como flujos de valores de entrada, a los que se puede acceder de forma sincrónica
13. Cualquier consumidor de los flujos tiene acceso para manipular esos datos de forma segura.


<a name="section-angular-forms-2"></a>

## Formulario Controlados Por Plantillas

1. Se basan en las directivas de la plantilla para crear y manipular el modelo de objetos subyacente.
2. Son útiles para agregar un formulario simple a una aplicación, como un formulario de registro de lista de correo electrónico.
3. Fáciles de agregar a una aplicación, **pero no se escalan tan bien** como las formas reactivas.
4. Para requisitos de formulario y lógica muy básicos que se pueden administrar únicamente en la plantilla **(Mejor Opcion)**
5. Abstraen la API de formulario subyacente
6. Usan el flujo de datos asincrónico entre la vista y el modelo de datos.
7. Abstracción de formularios basados en plantillas también afecta a las pruebas
8. Las pruebas dependen en gran medida **de la ejecución manual de detección de cambios** para ejecutarse correctamente y requieren más configuración.
9. La manipulación de los datos lo realiza solo la plantilla no hay directiva de intermediario ni instancia.
10. Se basan en la mutabilidad con enlace de datos bidireccional para actualizar el modelo de datos en el componente a medida que se realizan cambios en la plantilla.
11. Se basan en directivas incrustadas en la plantilla, junto con datos mutables para realizar un seguimiento de los cambios de forma asincrónica.

## Formulario Reactivo vs Formulario Controlados Por Plantillas


|                                          | Reactivo                                        | Basado en Plantillas                |
| ---------------------------------------- | ----------------------------------------------- | ----------------------------------- |
| Configuración del modelo de formulario   | Explícito, creado en la clase de componente	 | Implícito, creado por directivas    |
| Modelo de datos                          | Estructurado e inmutable	                     | No estructurado y mutable           |
| Flujo de datos                           | Síncrono                                        | Asíncrono                           |
| Validación de formularios                | Funciones                                       | Directivas                          |


La diferencia se demuestra en los ejemplos anteriores que utilizan el elemento de entrada de color favorito.


**Ejemplo de formularios reactivos:** la instancia de FormControl siempre devuelve un nuevo valor cuando se actualiza el valor del control.

```js
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-reactive-favorite-course',
  template: `
    Curso Favorito: <input type="text" [formControl]="favoriteCourseControl">
  `
})
export class FavoriteCourseComponent {
  favoriteCourseControl = new FormControl('');
}

```


**Ejemplo de formularios controlados por plantilla:** la propiedad de curso favorito siempre se modifica a su nuevo valor.

```js
import { Component } from '@angular/core';
@Component({
  selector: 'app-template-favorite-course',
  template: `
    Curso Favorito: <input type="text" [(ngModel)]="favoriteCourse">
  `
})
export class FavoriteCourseComponent {
  favoriteCourse = '';
}

```

## Clases de Formularios Comunes

| Nombre                     | Descripción                                                                                                   |
| -------------------------- | ------------------------------------------------------------------------------------------------------------- |
| FormControl                | Realiza un **seguimiento del valor y el estado de validación** de un control de **formulario individual**.	 |
| FormGroup                  | Realiza un **seguimiento** de los mismos valores y estado **para una colección de controles de formulario**.	 |
| FormArray                  | Realiza un **seguimiento** de los mismos valores y estado **para una matriz de controles de formulario**.	 |
| ControlValueAccessor       | Crea un **puente entre las instancias angulares y los elementos DOM integrados FormControl**.          	     |
| FormBuilder                | El servicio es un **proveedor inyectable que se proporciona con el módulo de formularios reactivos.**         |

Nota:
1. FormControl: Obtiene acceso inmediato para escuchar, actualizar y validar el estado de la entrada del formulario.
2. FormGroup: Define un formulario con un conjunto fijo de controles que se pueden administrar juntos.
3. FormGroup: Se pueden anidar grupos de formularios para crear formularios más complejos.




<a name="section-angular-forms-3"></a>

## Lineamientos del uso de TypeScript


<a href="#section-angular-forms-3-step-1" name="section-angular-forms-3-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos en TypeScript
</a>

1. Tabla de Lineamiento de Nombre de Variables y Métodos en TypeScript

| Tipo           | Convención de Nombre             | Descripción                                                                                                                        | Resultado          |
| ---------------| -------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------- | ------------------ |
| @angular/forms | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas e inicia con minúscula  y se concatena con (**Form**) al final. | upperCamelCase     |

<a href="#section-angular-forms-3-step-2" name="section-angular-forms-3-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Nombre de Variables y Métodos en TypeScript
</a>

Ejemplo: Archivo example.component.ts y uso de @angular/forms

```js
@angular/forms

profileForm = new FormGroup({});

```

1. **profileForm**: Revisar la Tabla de Lineamiento de Nombre de Variables y Métodos en TypeScript



<a name="section-angular-forms-4"></a>

## Actualización de Formulario Reactivo


<a href="#section-angular-forms-4-step-1" name="section-angular-forms-4-step-1">
  ### <span># </span>Paso 1. Formas de Actualizar Modelo Reactivo
</a>

| Nombre                     | Descripción                                                                                                                       |
| -------------------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| setValue()                 | El método se adhiere estrictamente a la estructura del grupo de formularios y reemplaza todo el valor del modelo de formulario.	 |
| patchValue()               | El método para reemplazar las propiedades definidas en el objeto que hayan cambiado en el modelo de formulario.	                 |
