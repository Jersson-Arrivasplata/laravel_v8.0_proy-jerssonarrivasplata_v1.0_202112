# Comandos de Angular

---

<a name="section-angular-step-2-commands"></a>

## Comandos Para Trabajar en Angular

<a href="#section-angular-guideline-7-step-2" name="section-angular-guideline-7-step-2">
  ### <span># </span>Paso 1. Comando Para Crear Un Componente en Angular
</a>

 Utilice el comando CLI para generar un componente en el proyecto.
 El nombre del componente a crear debe ser de convencion **Upper Case.**

```bash
ng generate component UserProfile

```
Resultado:
```bash
CREATE src/app/user-profile/user-profile.component.css
CREATE src/app/user-profile/user-profile.component.html
CREATE src/app/user-profile/user-profile.component.spec.ts
CREATE src/app/user-profile/user-profile.component.ts
```
