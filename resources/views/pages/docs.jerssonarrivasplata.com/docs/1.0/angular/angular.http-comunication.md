# Lineamientos de Comunicación Http

---
<h2>Lista de Lineamientos</h2>

1. Lineamientos del uso de Interfaces <a  href="#section-angular-http-comunication-1">(Presione aquí para Ir)</a>


## Lineamientos del uso de Interfaces en Proyecto Angular


<a href="#section-angular-http-comunication-step-1" name="section-angular-http-comunication-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos del uso de Interfaces para comunicación Http
</a>


1. Tabla de Lineamiento de Archivos Interface

| Tipo               | Convención de Nombre             | Descripción                                                                               | Resultado                       |
| ------------------ | -------------------------------- | ----------------------------------------------------------------------------------------- | ------------------------------- |
| Interface Request  | Upper Camel Case o Camel Case    | Cada palabra en minuscula se separa con un guión y se concatena con (**.request.interface.ts.**)  | lower-kebab-case.request.interface.ts   |
| Interface Response | Upper Camel Case o Camel Case    | Cada palabra en minuscula se separa con un guión y se concatena con (**.response.interface.ts.**)  | lower-kebab-case.response.interface.ts   |

2. Tabla de Lineamiento de Nombre de Clase Interface

| Tipo               | Convención de Nombre             | Descripción                                                                                        | Resultado                  |
| ------------------ | -------------------------------- | ------------------------------------------------------------------------------------------------   | -------------------------- |
| Interface Request  | Upper Camel Case o Camel Case | La primera letra de cada palabra concatenada se pone en mayúsculas y se concatena con (**I**) al inicio y (**Request**) al final.	 | ICountryRequest  |
| Interface Response | Upper Camel Case o Camel Case | La primera letra de cada palabra concatenada se pone en mayúsculas y se concatena con (**I**) al inicio y (**Response**) al final.	 | ICountryResponse  |


<a href="#section-angular-http-comunication-step-2" name="section-angular-http-comunication-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Archivos Interface
</a>

Ejemplo: Archivo Interface que contiene las propiedades de un 

```js
country.request.interface.ts

export interface ICountryRequest {
  code:string;
}

```

```js
country.response.interface.ts

export interface ICountryResponse {
  name:string;
  code:string;
}

```
1. **country.request.interface.ts**: Revisar la Tabla de Lineamiento de Archivos Interface
2. **ICountryRequest | ICountryResponse**: Revisar la Tabla de Lineamiento de Nombre de Clase Interface
3. **code**: Revisar la Tabla de Lineamiento de métodos y propiedades de una Interface <a  href="angular.guideline#section-angular-guideline-3-step-1">(Presione aquí para Ir)</a>


## Implementación de Interfaces en consulta http

Se realiza la implementación haciendo uso de HttpClient con @angular/common/http


```js

country.response.interface.ts

findCountryByCode(data: ICountryRequest): Observable<ICountryResponse> {
    return this.http.get<ICountryResponse>(${url});
}

```
