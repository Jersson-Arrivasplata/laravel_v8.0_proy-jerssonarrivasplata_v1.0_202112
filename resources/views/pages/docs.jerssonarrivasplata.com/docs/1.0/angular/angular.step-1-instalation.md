# Pasos Para Iniciar la Instalación

---

<a name="section-angular-step-1-instalation"></a>

## Generación de un Proyecto en Angular

### Paso 1. Comando Para Crear Proyecto Angular
Se recomienda revisar los lineamientos para nombrar un Proyecto en Angular <a href="angular.guideline#section-angular-guideline-1">(Presione aquí para Ir)</a>

```bash
> ng new ANGULAR-PROY-V1-IMPLEMENTATION-ANGULAR-FORMS-S2201

```
### Paso 2. Comando Para Aceptar Reducción de Paquetes de Manera Estricta 

Se ingresa Yes si se requiere reducir estrictamente los paquetes de angular.

```bash
? Do you want to enforce stricter type checking and stricter bundle budgets in the workspace?
  This setting helps improve maintainability and catch bugs ahead of time.
  For more information, see https://angular.io/strict Yes

```
### Paso 3. Comando Para Agregar enrutamiento en Angular

Se ingresa Yes si se requiere crear un enrutamiento automatico al crear el proyecto en angular.

```bash
? Would you like to add Angular routing? Yes

```
### Paso 4. Comando Para Aceptar Formato de Estilos

Se selecciona el tipo de estilo que se utilizara al crear un proyecto en angular y se espera por la instalación.

```bash
? Which stylesheet format would you like to use? (Use arrow keys)
❯ CSS 
  SCSS   [ https://sass-lang.com/documentation/syntax#scss                ] 
  Sass   [ https://sass-lang.com/documentation/syntax#the-indented-syntax ]
  Less   [ http://lesscss.org                                             ]
  Stylus [ https://stylus-lang.com                                        ] 

? Which stylesheet format would you like to use? CSS

```



