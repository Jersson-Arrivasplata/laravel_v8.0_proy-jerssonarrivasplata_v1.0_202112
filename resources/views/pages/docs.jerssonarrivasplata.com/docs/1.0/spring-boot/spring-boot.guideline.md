# Lineamientos de Spring Boot

---
<h2>Lista de Lineamientos</h2>

1. Lineamientos para nombrar un Proyecto <a href="#section-spring-boot-guideline-1">(Presione aquí para Ir)</a>

<a name="section-spring-boot-guideline-1"></a>
 
## Lineamiento para nombrar proyecto en Spring Boot

<a href="#section-spring-boot-guideline-1-step-1" name="section-spring-boot-guideline-1-step-1">
  ### <span># </span>Paso 1. Tabla de Lineamiento 
</a>

1. Lineamientos de Nombre de Repositorios

| Framework | Tipo | Versión  | Nombre del Proyecto  | Tipo  | Año  | Mes | Nombre del Repositorio                         |
| --------- | ---- | -------- | -------------------- | ----- | ---- | --- | ---------------------------------------------- |
| SPRING    | PROY | VX       | XXXXXXX              | S     | YY   | MM  | SPRING-PROY-VX-XXXXXXX-SYYMM                  |
| SPRING    | PROY | V1       | EJEMPLO              | S     | 22   | 01  | SPRING-PROY-V1-EJEMPLO-S2201                  |

<a href="#section-spring-boot-guideline-1-step-2" name="section-spring-boot-guideline-1-step-2">
  ### <span># </span>Paso 2. Descripción de la Tabla de Lineamiento
</a>

1. Columna #04 (Framework): Nombre del Framework del desarrollo.
2. Columna #01 (Tipo): Tipo de proyecto que hace referencia al proyecto - (Ejemplo: **PROY** -> Proyecto).
3. Columna #02 (Versión - **VX**): Versión del proyecto - (Ejemplo: **V1**).
4. Columna #03 (Nombre): Nombre que se le asignará al proyecto.
5. Columna #05 (Tipo): Indica que S de Starter - Inicio del Proyecto y continua la Fecha - (Ejemplo: **S**).
6. Columna #06 (Año - **YY**): Año en la que se creo el proyecto - (Ejemplo: **22**).
7. Columna #07 (Mes - **MM**): Mesa en la que se creo el proyecto - (Ejemplo: **01**).
8. Columna #08 (Nombre del Repositorio): Nombre final que resulta al unir todos los campos anteriores - (Ejemplo: **PROY-V1-EJEMPLO-SPRING-S2201**).
