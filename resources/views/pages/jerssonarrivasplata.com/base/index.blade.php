<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="{{ Utils::getUrlBase() }}/img/jerssonarrivasplata.com.png">

    <!--Inicio Fonts-->
    @include('common.fonts')
    <!--Fin Fonts-->

    <!--Inicio seo-->
    @include('common.metatags-seo')
    <!--Fin seo-->

    <!--Inicio Limpiar Cache-->
    @include('common.metatags-cache')
    <!--Fin Limpiar Cache-->

    <meta name="msapplication-TileColor" content="#0652dd">
    <meta name="msapplication-TileImage" content="{{ Utils::getUrlBase() }}/img/jerssonarrivasplata.com.png">
    <meta name="theme-color" content="#0652dd">

    <!--Inicio Metatags Open Graph-->
    @include('common.metatags-open-graph')
    <meta property="og:image" content="{{ Utils::getUrlBase() }}/img/jerssonarrivasplata.com.png">
    <meta property="og:image:secure_url" content="{{ Utils::getUrlBase() }}/img/jerssonarrivasplata.com.png">
    <!--Fin Metatags Open Graph-->

    <!--Inicio Metatags Twitter-->
    @include('common.metatags-twitter')
    <meta name="twitter:image" content="{{ Utils::getUrlBase() }}/img/jerssonarrivasplata.com.png">
    <!--Fin Metatags Twitter-->

    @section('metatags')
        <!-- some master css here -->
    @show

    <link href="{{ Utils::getUrlBase() }}/css/globals.css?v=<?php echo rand(); ?>" rel="stylesheet">



    <script type="module" src="{{ Utils::getUrlBase() }}/library/sami/build/sami.esm.js?v=<?php echo rand(); ?>"></script>
    <script nomodule src="{{ Utils::getUrlBase() }}/library/sami/build/sami.js?v=<?php echo rand(); ?>"></script>

    @section('script-header')
        <!-- some master css here -->
    @show
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-VM4CBHYEBR"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-VM4CBHYEBR');
    </script>
</head>

<body>
    <!--Inicio Sdk Facebook-->
    @include('common.facebook')
    <!--Fin Sdk Facebook-->

    <div class="loader d-none">
        <span>{</span><span>}</span>
    </div>

    <div>
        @include(GlobalClass::getDomainFolder(0) . '::lang.'.request()->route()->parameter('language').'.common.header')
        @yield('content')
        @include(GlobalClass::getDomainFolder(0) . '::lang.'.request()->route()->parameter('language').'.common.footer')

    </div>
    <script src="{{ Utils::getUrlBase() }}/js/app.js?v=<?php echo rand(); ?>" type="text/javascript"></script>
    <script src="{{ Utils::getUrlBase() }}/js/globals.js?v=<?php echo rand(); ?>" type="text/javascript"></script>
    @section('script-bottom')
        <!-- some js here -->
    @show
    <sami-loader type="pulse">
        <sami-span>{</sami-span>
        <sami-span>}</sami-span>
    </sami-loader>

    <script type="text/javascript">
        const loader = () => {
            customElements.whenDefined('sami-loader').then(() => {
                const load = document.querySelector('sami-loader');
                load.componentOnReady().then(() => {
                    load.show();
                });
            })
        }
    </script>
</body>

</html>
