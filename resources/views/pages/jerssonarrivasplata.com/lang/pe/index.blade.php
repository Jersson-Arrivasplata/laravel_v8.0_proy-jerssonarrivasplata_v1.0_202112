@extends(GlobalClass::getDomainFolder(0).'::base.index')
@section('metatags')
    @parent
    <!-- some master css here -->
    <title>{{ GlobalClass::getCreatorUser() }} | Software Developer</title>

    <meta property="og:type" content="website">

    <meta name="description" content="Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">

    <meta property="og:description"
    content="Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">

    <meta property="og:title" content="{{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta property="og:url" content="{{ Utils::getUrlBase() }}">


    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="{{ Utils::getUrlBase() }}">
    <meta name="twitter:title" content="{{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta name="twitter:description"
    content="Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">

@stop
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase() }}/css/index.css?v=<?php echo(rand()); ?>" rel="stylesheet">

@stop

@section('content')
    @parent
    <!-- Main content goes here -->
    <main>
        @include(GlobalClass::getDomainFolder(0) . '::lang.'.request()->route()->parameter('language').'.common.header-menu')
        <section>
            <ul></ul>
            <ul>
                <li>Yo soy {{ GlobalClass::getCreatorUser() }}</li>
                <li>L&iacute;der Java Developer en Stefanini LATAM, dirije desarrollos en Java y microservicios, asegurando
                    calidad. Consultor T&eacute;cnico Semi Senior en SOAINT, destaca en Promsace con librer&iacute;as en Stencil y Angular. Experto en
                    Front-End (Angular, Stencil-Js) y Back-End (Spring Boot, Java 8+), contribuye al Proyecto Soaint con lineamientos de
                    Git y automatizaci&oacute;n en Angular +14. Con experiencia en BCP y Vass, lidera proyectos clave bajo &aacute;gil Scrum. Para culminar
                    brinda cursos de Front-End en la Cibertec.</li>
            </ul>
        </section>
        <section id="sobremi">
            <p>SOBRE M&Iacute;</p>
            <div>
                <div>
                    <p>
                        Bachiller en la carrera de Ingenier&iacute;a de Software de la Universidad Tecnol&oacute;gica del Per&uacute; de Lima-Per&uacute;.
                        <br />
                        Soy una persona muy proactiva que busco aprender y aplicar mis conocimientos para ayudar a otras
                        personas o en el trabajo en el cual me desempe&ntilde;o.
                        <br />
                        Me gusta escuchar musica, ver pel&iacute;culas, salir de viaje, sacar a pasear a mi mascota y pasar
                        tiempo con mi familia.
                    </p>
                    <p class="bold">Mi Perfil: </p>
                    <ul class="pl-2">
                        <li>Linkedin: <a target="_blank"
                                href="{{ config('base.social_networks.linkedin') }}">{{Str::after(config('base.social_networks.linkedin'), config('base.others.https'))}}</a>
                        </li>
                        <li>Facebook: <a target="_blank"
                                href="{{ config('base.social_networks.facebook') }}">{{Str::after(config('base.social_networks.facebook'), config('base.others.https'))}}</a>
                        </li>
                        <li >Instagram: <a target="_blank"
                                href="{{ config('base.social_networks.instagram') }}">{{Str::after(config('base.social_networks.instagram'), config('base.others.https'))}}</a>
                        </li>
                        <li>Github: <a target="_blank"
                                href="{{ config('base.repositories.github.jersson-arrivasplata') }}">{{Str::after(config('base.repositories.github.jersson-arrivasplata'), config('base.others.https'))}}</a></li>
                        <li>WebSite: <a target="_blank" href="{{ config('base.website') }}">{{Str::after(config('base.website'), config('base.others.https'))}}</a>
                        </li>
                        <li>N&uacute;mero de tel&eacute;fono: <a target="_blank" href="tel:({{config('base.info.celphone.code')}}){{config('base.info.celphone.number')}}">+{{ config('base.info.celphone.code') }} {{config('base.info.celphone.number')}} (M&oacute;vil)</a></li>
                        {{--<li>Direcci&oacute;n: {{config('base.info.address')}}</li>--}}
                        <li>Email: <a target="_blank" href="mailto:{{ config('base.info.email') }}">{{ config('base.info.email') }}</a></li>
                        <li>Cumplea&ntilde;os: 11 de julio</li>
                    </ul>

                </div>

                <div>
                    <p>
                        <picture>

                            <img src="{{ Utils::getUrlBase() }}/img/profile.png" alt="Imagen {{ GlobalClass::getCreatorUser() }}">
                        </picture>
                    </p>
                </div>
                </ul>
        </section>
        <section id="miexperiencia">
            <p>MI EXPERIENCIA</p>
            <article class="main-article-index">
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/cibertec.jfif" alt="Logo de empresa CIBERTEC">
                    </picture>
                    <p class="">
                        Certificado de Trabajo
                    </p>
                    <a class=""target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-CIBERTEC.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-CIBERTEC.png"
                            alt="Certificado de trabajo de CIBERTEC por parte de {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p class="">
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-CIBERTEC.png">
                            (click para descargar)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Profesor de Desarrollo de Software</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>CIBERTEC | Jornada parcial</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>jul 2023 – actualidad
                    </p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>12 meses</p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Lima, Per&uacute;</p>
                    <ul>
                        <li>Dictado de Cursos o M&oacute;dulos especializados</li>
                        <li>
                            <ul>
                                <li>Curso de Fundamentos de Java (Java 8+, Stream, Functional y otros) </li>
                                <li>Curso de Aplicaciones Web (JavaEE y JakartaEE)</li>
                                <li>Curso de Front-End (Angular +12, React y JavaScript)</li>
                                <li>Curso de Back-End (SpringBoot)</li>
                            </ul>
                        </li>
                        <li>Funciones:</li>
                        <li>
                            <ul>
                                <li>Dictado de Cursos especializados.</li>
                                <li>Dictado de M&oacute;dulos especializados.</li>
                            </ul>
                        </li>
                        <li>Herramientas:</li>
                        <li> Spring Boot, Java 8+, Streams, git, IntelliJ IDEA (IDE), React, PostgreSQL, Angular, Javascript, Html5, CSS, CSS3, SCSS, Angular 8+, Postman, NodeJs, TypeScript, Tslint, Unit Test y otros.</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/stefanini.jfif" alt="Logo de empresa Stefanini LATAM">
                    </picture>
                    <p class="invisible">
                        Certificado de Trabajo
                    </p>
                    <a class="invisible" target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-STEFANINI-LATAM.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-STEFANINI-LATAM.png"
                            alt="Certificado de trabajo de Stefanini LATAM por parte de {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p class="invisible">
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-STEFANINI-LATAM.png">
                            (click para descargar)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Java Developer</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>Stefanini It Solutions Peru S.A.C. | Stefanini LATAM | Jornada completa</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>ago 2023 – actualidad
                    </p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>11 meses</p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Lima, Per&uacute;</p>
                    <ul>
                        <li>Proyecto de Mi Movistar App (<a target="_blank" href="https://www.movistar.com.pe/app-mi-movistar">https://www.movistar.com.pe/app-mi-movistar</a>)</li>
                        <li>
                            <ul>
                                <li>Soporte de plataformas WebAdmin que consume la aplicaci&oacute;n Mi Movistar App</li>
                                <li>Soporte de microservicios que consume la aplicaci&oacute;n Mi Movistar App</li>
                                <li>Soporte a la aplicaci&oacute;n en ionic que consume la aplicaci&oacute;n Mi Movistar App </li>
                            </ul>
                        </li>
                        <li>Funciones:</li>
                        <li>
                            <ul>
                                <li>Desarrollo de Aplicaciones: Dise&ntilde;o y desarrollo de aplicaciones Java y microservicios, garantizando su robustez y escalabilidad.</li>
                                <li>Integraci&oacute;n de APIs: Responsable de integrar nuestras aplicaciones con servicios externos mediante el consumo de APIs, asegurando una integraci&oacute;n eficiente y sin problemas.</li>
                                <li>Pruebas Exhaustivas: Realizaci&oacute;n de pruebas unitarias e integraci&oacute;n para asegurar la calidad del c&oacute;digo y la funcionalidad de nuestras aplicaciones.</li>
                                <li>Colaboraci&oacute;n &aacute;gil: Trabajo estrecho con el equipo de desarrollo, aplicando metodolog&iacute;as &aacute;giles como SAFe y Scrum en todas las etapas del ciclo de vida del software.</li>
                                <li>Gesti&oacute;n de Proyectos: Liderazgo en la gesti&oacute;n de equipos, garantizando una entrega oportuna y de calidad utilizando herramientas como Jira y Spring Planning.</li>
                                <li>Mejora Continua: Destacado liderazgo en la gesti&oacute;n y mejora continua de la plataforma web admin y la aplicaci&oacute;n m&oacute;vil Mi Movistar, proponiendo, gestionando e implementando m&oacute;dulos para mejorar la experiencia del usuario y la eficiencia de las aplicaciones.</li>
                                <li>Plataformas en la Nube: Experiencia en el aprovechamiento de plataformas en la nube como Azure para potenciar nuestros proyectos.</li>
                                <li>Coordinaci&oacute;n Multidisciplinaria: Coordinaci&oacute;n con equipos de front-end y mobile para garantizar una experiencia unificada para el usuario final.</li>
                                <li>Tecnolog&iacute;as Adicionales: Amplio conocimiento en tecnolog&iacute;as como Docker, Kubernetes, bases de datos NoSQL (incluyendo Cosmos y MongoDB) y bases de datos relacionales como PostgreSQL.</li>
                                <li>Herramientas y Pr&aacute;cticas DevOps: Utilizaci&oacute;n de herramientas como SonarQube y pr&aacute;cticas DevOps para asegurar la integraci&oacute;n y entrega continua de nuestras aplicaciones.</li>
                            </ul>
                        </li>
                        <li>Herramientas:</li>
                        <li>Spring Boot, Java 8+, Streams,New Relic, git, IntelliJ IDEA (IDE), PostgreSQL, Marco &aacute;gil Scrum, Angular, Javascript, Html5, CSS, CSS3, SCSS, Angular 8+, Postman, NodeJs, TypeScript, Tslint, Unit Test, Gitlab(Repositorio), Marco &aacute;gil Scrum, git y otros.</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/soaint.jfif" alt="Logo de empresa SOAINT">
                    </picture>
                    <p>
                        Certificado de Trabajo
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-SOAINT.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-SOAINT.png"
                            alt="Certificado de trabajo de SOAINT por parte de {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-SOAINT.png">
                            (click para descargar)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Consultor T&eacute;cnico Semi Senior</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>Soaint Peru S.A.C. | SOAINT | Jornada completa</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>dic 2022 – ago 2023
                    </p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>9 meses</p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Lima, Per&uacute;</p>
                    <ul>
                        <li>Proyecto con el Estado Promsace</li>
                        <li>
                            <ul>
                                <li>Microservicios para Proyecto Promsace</li>
                                <li>Librer&iacute;a y Proyecto en Front para Proyecto Promsace</li>
                                <li>Creaci&oacute;n de Librer&iacute;a en StencilJs para Proyecto Promsace</li>
                            </ul>
                        </li>
                        <li>Funciones:</li>
                        <li>
                            <ul>
                                <li>Desarrollo de Librer&iacute;a en Stencil e implementaci&oacute;n en el Proyecto de Promsace (Angular).</li>
                                <li>Desarrollo de Librer&iacute;as en Angular implementaci&oacute;n en el Proyecto de Promsace (Angular).</li>
                                <li>Desarrollo de Proyecto NodeJS para mockeo de Json en el Proyecto de Promsace (Angular).</li>
                                <li>Desarrollo de microservicios en SpringBoot (srv-config-server, srv-gateway, pgo-parametro-comun, srv-auth-keycloak y otros).</li>
                                <li>Asesor&iacute;a en el uso de buenas practicas en el desarrollo de Angular, TypeScript y seguimiento de los Lineamientos de Git.</li>
                                <li>Asesor&iacute;a en el uso del proyecto de esquematizaci&oacute;n realizada.</li>
                                <li>Asesor&iacute;a en el desarrollo de Microfrontends con Angular.</li>
                                <li>Asesor&iacute;a en el desarrollo de librer&iacute;as con Angular.</li>
                                <li>Asesor&iacute;a en el desarrollo de librer&iacute;as con StencilJs.</li>
                            </ul>
                        </li>
                        <li>Herramientas:</li>
                        <li> Spring Boot, Java 8+, Streams, git, IntelliJ IDEA (IDE), PostgreSQL, Marco &aacute;gil Scrum, Angular, Javascript, Html5, CSS, CSS3, SCSS, Angular 8+, Postman, NodeJs, TypeScript, Tslint, Unit Test, Gitlab(Repositorio), Marco &aacute;gil Scrum, git y otros.</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/bcp.jfif" alt="Logo de empresa BCP">
                    </picture>
                    <p>
                        Certificado de Trabajo
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BCP.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BCP.png"
                            alt="Certificado de trabajo de BCP por parte de {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BCP.png">
                            (click para descargar)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">FRONTEND WEB SOFTWARE ENGINEER</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>BANCO DE CREDITO
                        DEL PERU S.A. | BCP | Jornada completa</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>ago 2021 – sept 2022
                    </p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>1 a&ntilde;o y 1 mes</p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Lima, Per&uacute;</p>
                    <ul>
                        <li>Mesa de Transferencia al Exterior</li>
                        <li>
                            <ul>
                                <li>Microfronts de Transferencia al Exterior</li>
                                <li>Proyecto Web admin Web Convergente</li>
                                <li>Microfronts de Transferencia Overview al Exterior</li>
                                <li>Microfronts de Transferencia de Terceros entre otros</li>
                                <li>Soporte a Librer&iacute;a de Widgets de Transferencia al Exterior entre otras librerias.</li>
                            </ul>
                        </li>
                        <li>Funciones:</li>
                        <li>
                            <ul>
                                <li>An&aacute;lisis y debug de errores para cambios en las reglas de negocio y resoluci&oacute;n de incidencias.</li>
                                <li>An&aacute;lisis de servicios endpoints con postman para implementar en los microfronts de Transferencia.</li>
                                <li>Manejo de Jira para la gesti&oacute;n de requisitos y casos de prueba. </li>
                                <li>Generaci&oacute;n de propuestas de Soluci&oacute;n para correcci&oacute;n defectos encontrados en desarrollo y/o producci&oacute;n.</li>
                                <li>Manejo de ceremonias en marco &aacute;gil (scrum) dentro del equipo de trabajo.</li>
                                <li>Tomar decisiones a nivel de squad, como la gesti&oacute;n de las incidencias que el squad genere.</li>
                                <li>Despliegue Continuo de Microfronts en Ambiente de Desarrollo, Certificaci&oacute;n y Producci&oacute;n.</li>
                                <li>Cultura de Trabajo en el desarrollo de software DevSecOps.</li>
                                <li>Desarrollo bajo el marco de Trabajo de FEM aplicado en el desarrollo de software del BCP.</li>
                                <li>Prueba de vulnerabilidades y buenas practicas en el desarrollo de software utilizando herramientas Sonarque y Fortify.</li>
                            </ul>
                        </li>
                        <li>Herramientas:</li>
                        <li>Angular, Javascript, Html5, CSS, CSS3, SCSS, Angular 8+, Postman, TypeScript, Tslint, Unit Test, Sonarque, Fortify, entre otros.</li>
                    </ul>
                </div>
            </article>
            <article>
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/vass.jfif" alt="Logo de empresa VASS">
                    </picture>
                    <p>
                        Certificado de Trabajo
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-VASS.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-VASS.png"
                            alt="Certificado de trabajo de VASS por parte de {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-VASS.png">
                            (click para descargar)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Analista Programador</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>Vass Consultoria
                        de Sistemas Peru S.A.C. | vass Latam | Jornada completa</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>feb 2021 – jul 2021
                    </p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>4 meses y 23 d&iacute;as
                    </p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Lima, Per&uacute;</p>
                    <p>Mesa Oferta Comercial - Advengers la cual pertenece al TREN STARK de Telef&oacute;nica</p>
                    <ul>
                        <li>Se trabajo en los siguientes proyectos:</li>
                        <li>
                            <ul>
                                <li>Proyecto Web Convergente</li>
                                <li>Proyecto Web admin Web Convergente</li>
                                <li>Proyecto Web de Ventas Fija</li>
                                <li>Proyecto de Aplicaci&oacute;n de Ventas Fija</li>
                            </ul>
                        </li>
                        <li>Funciones:</li>
                        <li>
                            <ul>
                                <li>An&aacute;lisis y debug de errores para cambios en las reglas de negocio y resoluci&oacute;n de
                                    incidencias.</li>
                                <li>An&aacute;lisis de servicios endpoints con postman para implementar en la WebConvergente de
                                    Telefonica.</li>
                                <li>Manejo de Jira para la gesti&oacute;n de requisitos y casos de prueba.</li>
                                <li>Generaci&oacute;n de propuestas de Soluci&oacute;n para correcci&oacute;n defectos encontrados en
                                    desarrollo y/o producci&oacute;n.</li>
                                <li>Manejo de ceremonias en marco &aacute;gil (scrum) dentro del equipo de trabajo.</li>
                                <li>Tomar decisiones a nivel de squad, como la gesti&oacute;n de las incidencias que el squad
                                    genere.</li>
                            </ul>
                        </li>
                        <li>Herramientas:</li>
                        <li> Android Studio Java - Kotlin, Javascript, Jquery, Html5, PostgreSQL (Base de datos), CSS,
                            CSS3, SCSS, Oauth 2.0 Facebook, Google, Angular 8+, AppCenter (despliegue de aplicaciones),
                            Postman, TypeScript, entre otros.</li>
                    </ul>
                </div>
            </article>
            <article>
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/bigbyte.png" alt="Logo de empresa BIGBYTE">
                    </picture>
                    <p>
                        Certificado de Trabajo
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.jfif">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.jfif"
                            alt="Certificado de trabajo de BIGBYTE por parte de {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.jfif">
                            (click para descargar)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Analista de Sistemas</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>BIGBYTE S.A.C. |
                        BIGBYTE PER&Uacute; | Profesional independiente</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>may 2020 – dic 2020
                    </p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>8 meses</p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Lima, Per&uacute;</p>
                    <p>Mantenimiento de los m&oacute;dulos del portal oficial de turismo del estado peruano peru.travel,
                        mantenimiento de un minisitio del estado peruano perutherichestcountry.peru.travel y se trabaj&oacute;
                        en el desarrollo de 2 aplicaciones nativas de PISCO para IOS Y Android, as&iacute; como su
                        administrador para las aplicaciones de Pisco.</p>
                    <ul>
                        <li>Funciones:</li>
                        <li>
                            <ul>
                                <li>Mantenimiento de los m&oacute;dulos del portal oficial de turismo del estado peruano
                                    peru.travel (<a target="_blank"
                                        href="https://www.peru.travel">https://www.peru.travel</a>) – administrador en
                                    C#.</li>
                                <li>Mantenimiento de un minisitio del estado peruano perutherichestcountry.peru.travel (
                                    <a target="_blank"
                                        href="https://perutherichestcountry.peru.travel">https://perutherichestcountry.peru.travel</a>
                                    ) – administrador en C#.
                                </li>
                                <li>Desarrollo y despliegue en la Play Store de una aplicaci&oacute;n nativa en Android de
                                    Pisco para PromPeru - <a target="_blank"
                                        href="https://play.google.com/store/apps/details?id=com.promperu.pisco">https://play.google.com/store/apps/details?id=com.promperu.pisco</a>
                                </li>
                                <li>Desarrollo y despliegue en la App Store de una aplicaci&oacute;n nativa en IOS de Pisco
                                    para PromPeru.</li>
                                <li><a target="_blank"
                                        href="https://apps.apple.com/pe/app/pisco/id1539213077">https://apps.apple.com/pe/app/pisco/id1539213077</a>
                                </li>
                            </ul>
                        </li>

                        <li>Herramientas:</li>
                        <li>iOS - Swift, Android Studio Java, React, Javascript, Jquery, .Net C#, Html5, Sql Server
                            (Procedimientos almacenados), CSS, Oauth 2.0 Facebook, Google, otros.</li>
                    </ul>
                    <!--<p>
                                                    <a target="_blank" href="https://www.peru.travel/">
                                                        <img src="img/experiencia/perutravel.PNG"/>
                                                    </a>
                                                </p>-->
                </div>
            </article>
            <article>
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/devinnovaperu.png" alt="Logo de empresa DevInnova Per&uacute;">
                    </picture>
                    <p>
                        Certificado de Trabajo
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-DEVINNOVAPERU.jfif">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-DEVINNOVAPERU.jfif"
                            alt="Certificado de trabajo de DEVINNOVA PERU por parte de {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-DEVINNOVAPERU.jfif">
                            (click para descargar)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Analista de Sistemas | CEO & FOUNDER</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>DevInnova Peru
                        S.A.C | DevInnova Per&uacute; | Jornada completa</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>ene 2020 – dic 2020
                    </p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>12 meses</p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Lima, Per&uacute;</p>
                    <p>Desarrollo y despliegue del Sistema para Snap Store que permite gestionar los negocios de los
                        emprendedores peruanos a bajos costos.</p>
                    <ul>
                        <li>Funciones:</li>
                        <li>
                            <ul>
                                <li>Desarrollo y despliegue de plataforma web para Snap Store la cual ayuda a
                                    administrar las tiendas virtuales. www.snapstore.app - www.minegocio.snapstore.app
                                </li>
                                <li>Desarrollo y despliegue de una Landing page para Snap Store. www.snapstore.pe</li>

                            </ul>
                        </li>

                        <li>Herramientas:</li>
                        <li>Jquery, Framework Laravel - PHP - Angular, Mysql, HTML, CSS, servidor VPS Bluet Host (<a
                                target="_blank" href="https://www.digitalocean.com/">https://www.digitalocean.com/</a>)
                        </li>
                    </ul>
                </div>
            </article>
            <article>
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/afcasolution.png" alt="Logo de empresa Afca Solution">
                    </picture>
                    <p>
                        Certificado de Trabajo
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-AFCASOLUTION.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-AFCASOLUTION.png"
                            alt="Certificado de trabajo de AFCA SOLUTION por parte de {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-AFCASOLUTION.png">
                            (click para descargar)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Analista de Sistemas y Desarrollador</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>Afca Solution
                        S.AC | Afca Solution | Jornada completa</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>may 2019 – dic 2019
                    </p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>8 meses</p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Lima, Per&uacute;</p>
                    <p>Desarrollo de aplicaci&oacute;n para gamarra para administrar y publicar una tienda online exclusivo
                        para gamarra App y el desarrollo y publicaci&oacute;n de un sistema que gestiona las encomiendas de las
                        personas, as&iacute; como el desarrollo de una aplicaci&oacute;n para los conductores que permite administrar
                        las encomiendas.</p>
                    <ul>
                        <li>Funciones:</li>
                        <li>
                            <ul>
                                <li>Mantenimiento de la Aplicaci&oacute;n Stilo App que se especializa en llevar a tu casa
                                    cualquier servicio de belleza que desees. Manicuristas, estilistas, maquilladoras,
                                    barberos, cosmiatras, masajistas, pod&oacute;logos. <a target="_blank"
                                        href="https://play.google.com/store/apps/details?id=io.codigito.mylookexpress&hl=es_419&gl=US">https://play.google.com/store/apps/details?id=io.codigito.mylookexpress&hl=es_419&gl=US</a>
                                </li>
                                <li>Desarrollo y despliegue de aplicaci&oacute;n y sistema para las tiendas de Gamarra busca
                                    que el cliente pueda llegar a cada tienda de Gamarra seg&uacute;n el tipo de categor&iacute;a a la
                                    cual pertenece. Gamarra - Emporio Comercial de textiles. ( <a target="_blank"
                                        href="https://gamarraapp.com/">https://gamarraapp.com/</a> )</li>
                                <li>Desarrollo y despliegue de Sistema para encomiendas y aplicaci&oacute;n para conductores
                                    que ubica la posici&oacute;n del pedido en un trayecto determinado y sistema desarrollado
                                    para F&aacute;cil Env&iacute;o reduce los procesos de entrega hacia las distintas agencias de
                                    env&iacute;os a provincia y a cualquier punto de Lima Metropolitana. ( <a target="_blank"
                                        href="https://www.transporte.facilenvio.com/login">https://www.transporte.facilenvio.com/login</a>
                                    )</li>
                                <li>Desarrollo y despliegue en la App Store de una aplicaci&oacute;n nativa en IOS de Pisco
                                    para PromPeru.</li>
                            </ul>
                        </li>

                        <li>Herramientas:</li>
                        <li>IONIC 4 – Angular, Android Studio (Java), JavaScript, TypeScript, Html5, SCSS, Laravel –
                            PHP, Mysql, servidor cloud Digital Ocean (<a target="_blank"
                                href="https://www.digitalocean.com/">https://www.digitalocean.com/</a> ) para
                            despliegue, NodeJS – Sequelize ORM, Angular.</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <picture>
                    <img src="{{ Utils::getUrlBase() }}/img/jerssonarrivasplata.png" width="100px" alt="Logo de {{ GlobalClass::getCreatorUser() }}">
                </picture>
                <div>
                    <p class="h3 bold">Freelance</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>Freelance
                        Profesional independiente</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>sept 2018 – dic 2018
                    </p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>4 meses</p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Lima, Per&uacute;</p>
                    {{--<p class=""><span class="bold">Ubicaci&oacute;n: </span>Jir&oacute;n Zorritos 1134 -
                        Cercado de Lima</p>--}}
                    <p>Desarrollo de un sistema de ventas que administre el alquiler de productos para eventos y
                        migraci&oacute;n de sistema del INSTITUTO NACIONAL PEDAG&Oacute;GICO DE MONTERRICO.</p>
                    <ul>
                        <li>Funciones:</li>
                        <li>
                            <ul>
                                <li>Desarrollo de un sistema de ventas que administre el alquiler de productos para
                                    eventos, as&iacute; como el front-end de alquiler de productos.</li>
                                <li>Migraci&oacute;n de sistema del INSTITUTO NACIONAL PEDAG&Oacute;GICO DE MONTERRICO de un servidor
                                    de bajas prestaciones a otro servidor con mejores prestaciones (sistema est&aacute;
                                    desarrollado en visual studio 2010 y utiliza CRYSTAL REPORT para sus reportes).</li>
                            </ul>
                        </li>

                        <li>Herramientas:</li>
                        <li>Jquery, PHP,Mysql, Html5 y SCSS, Manejo de Servidor BlueHost (<a target="_blank"
                                href="https://www.bluehost.com/">https://www.bluehost.com/</a>).</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <picture>
                    <img src="{{ Utils::getUrlBase() }}/img/lamanoamiga.png" alt="Logo de empresa Centro de Conciliaci&oacute;n 'La mano amiga'">
                </picture>
                <div>
                    <p class="h3 bold">Analista de sistemas y Desarrollador</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>Centro de
                        Conciliaci&oacute;n "La mano amiga" | Profesional independiente</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>dic 2017 – 2018</p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>1 a&ntilde;o</p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Lima, Per&uacute;</p>
                    <p>Desarrollo de sitio web administrativo en la que se implement&oacute; modulo que reduzca las mermas del
                        centro de conciliaci&oacute;n.</p>
                    <ul>
                        <li>Funciones:</li>
                        <li>
                            <ul>
                                <li>Desarrollo y despliegue de un sistema administrativo que permita el manejo de sus
                                    conciliaciones (invitaciones de conciliaci&oacute;n – actas de conciliaci&oacute;n) entre otros
                                    m&oacute;dulos para la administraci&oacute;n de publicaciones.</li>
                            </ul>
                        </li>

                        <li>Herramientas:</li>
                        <li>Jquery, Laravel – PHP, Mysql, Html5, SCSS, API.</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <picture>
                    <img src="{{ Utils::getUrlBase() }}/img/jerssonarrivasplata.png" width="100px" alt="Logo de {{ GlobalClass::getCreatorUser() }}">
                </picture>
                <div>
                    <p class="h3 bold">Freelance y Desarrollador en Excel – Visual Basic</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>Freelance
                        Profesional independiente</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>may 2017 – nov 2017
                    </p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>7 meses</p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Av. Espa&ntilde;a 1505 - Cercado
                        de Lima</p>
                    <p>Desarrollo e implementaci&oacute;n de sistema de alimentos para empresa productora, exportadora de
                        pollos, cerdos, ganado.</p>
                    <ul>
                        <li>Herramientas:</li>
                        <li>Excel y Visual Basic</li>
                    </ul>
                </div>
            </article>
            <article>
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/bigbyte.png" alt="Logo de empresa BIGBYTE PERU">
                    </picture>
                    <p>
                        Certificado de Trabajo
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.png"
                            alt="Certificado de trabajo de BIGBYTE por parte de {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.png">
                            (click para descargar)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Analista de sistema</p>
                    <p class=""><span class="bold">Nombre de la empresa: </span>BIGBYTE S.A.C. |
                        BIGBYTE PER&Uacute; | Jornada completa</p>
                    <p class=""><span class="bold">Fechas de empleo: </span>jul 2016 – abr 2017
                    </p>
                    <p class=""><span class="bold">Duraci&oacute;n del empleo: </span>10 meses</p>
                    <p class=""><span class="bold">Ubicaci&oacute;n: </span>Lima 41 San Borja, Lima,
                        Peru</p>
                    <p>Desarrollo de un sistema de gesti&oacute;n para transporte de la empresa CARMELO y mantenimiento de los
                        m&oacute;dulos del portal de turismo del estado peruano VIBRA TOURS y desarrollo de la plataforma de
                        educaci&oacute;n para la PUCP la FAKU.</p>
                    <ul>
                        <li>Funciones:</li>
                        <li>
                            <ul>

                                <li>Desarrollo de un sistema de gesti&oacute;n para transporte de la empresa CARMELO –
                                    administrador en C#.</li>
                                <li>Mantenimiento de los m&oacute;dulos del portal de turismo del estado peruano VIBRA TOURS –
                                    administrador en C#.</li>
                                <li>Desarrollo de la plataforma de educaci&oacute;n para la Universidad Pontificia Cat&oacute;lica del
                                    Per&uacute; – la Faku.</li>
                            </ul>
                        </li>

                        <li>Herramientas:</li>
                        <li>Javascript, Jquery, .Net C#, Html5, Sql Server (Procedimientos almacenados), CSS, Pruebas en
                            Servidor SmarterAsp ( <a target="_blank"
                                href="https://www.smarterasp.net/index">https://www.smarterasp.net/index</a> ).</li>
                    </ul>
                </div>
            </article>
        </section>
    </main>


@stop
