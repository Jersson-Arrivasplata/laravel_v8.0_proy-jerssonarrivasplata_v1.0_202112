@extends(GlobalClass::getDomainFolder(0).'::base.index')
@section('metatags')
    @parent
        <!-- some master css here -->
    <title>Educaci&oacute;n de {{ GlobalClass::getCreatorUser() }} | Software Developer</title>


    <meta property="og:type" content="article">

    <meta name="description" content="P&aacute;gina de estudios de {{ GlobalClass::getCreatorUser() }} -
    Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">

    <meta property="og:description"
    content="P&aacute;gina de estudios de {{ GlobalClass::getCreatorUser() }} -
    Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">
    <meta property="og:title" content="Educaci&oacute;n de {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta property="og:url" content="{{ Utils::getUrlBase() }}/educacion">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="{{ Utils::getUrlBase() }}/educacion">
    <meta name="twitter:title" content="Educaci&oacute;n de {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta name="twitter:description"
    content="P&aacute;gina de estudios de {{ GlobalClass::getCreatorUser() }} -
    Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">
@stop
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase() }}/css/index-education.css?v=<?php echo rand(); ?>" rel="stylesheet">
@stop

@section('content')
    @parent
    <!-- Main content goes here -->
    <main>
        @include(GlobalClass::getDomainFolder(0) . '::lang.'.request()->route()->parameter('language').'.common.header-menu')

        <section>
            <p>Mi Educaci&oacute;n Universitaria</p>
            <ul>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/utp.jfif" alt="Logo de la UTP">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Tecnol&oacute;gica del Per&uacute;</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>Bachiller en la
                            carrera de Ingenier&iacute;a de software</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2015 -
                            2021</p>
                        <p class=" pt-4" style="margin-top: 20PX;">
                            <a target="_blank"
                                href="{{ Utils::getUrlBase() }}/img/educacion/BACHILLER-INGENIERIA-SOFTWARE.jpg">
                                    <img style="MAX-WIDTH: 145px;"
                                        src="{{ Utils::getUrlBase() }}/img/educacion/BACHILLER-INGENIERIA-SOFTWARE.jpg"
                                        alt="BACHILLER DE INGENIERIA DE SOFWTARE JERSSON ARRIVASPLATA ROJAS">
                            </a>
                        </p>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/utp.jfif" alt="Logo de la UTP">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Tecnol&oacute;gica del Per&uacute;</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>Estudiante de
                            5to ciclo | Ingenier&iacute;a Mecatr&oacute;nica</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2022 -
                            2025</p>
                    </div>
                </li>
            </ul>
        </section>
        <section>
            <p>Mi Educaci&oacute;n Lengua Extranjera</p>
            <ul>

                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/britanico.png"
                            alt="Logo de la Asociacion Cultural Peruano Britanica (Britanico)">
                    </picture>
                    <div>
                        <p class="h3 bold">Asociacion Cultural Peruano Britanica (Britanico)</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>Constancia de
                            Fase B&aacute;sica | Disciplina acad&eacute;mica | Ense&ntilde;anza de ingl&eacute;s como lengua extranjera</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2014 –
                            2015</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/CONSTANCIA-DE-ESTUDIOS-DE-INGLES-BASICO-BRITANICO.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/CONSTANCIA-DE-ESTUDIOS-DE-INGLES-BASICO-BRITANICO.jfif"
                                        alt="Asociacion Cultural Peruano Britanica (Britanico) - Constancia de Fase B&aacute;sica culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </section>
        <section>
            <p>Mis Asistencias a Conferencias y M&eacute;ritos obtenidos</p>
            <ul>

                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/microsoft.jfif"
                            alt="Logo del DEVDAYS LATAM 2016 - MICROSOFT">
                    </picture>
                    <div>
                        <p class="h3 bold">DEVDAYS LATAM 2016 - MICROSOFT | MICROSOFT</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>Asistencia
                            DEVDAYS LATAM 2016 - MICROSOFT</p>
                        <p class=""><span class="bold">Fechas en la que realizo: </span>2016</p>
                        <p>Constancia de asistencia a la conferencia que se realizo en Per&uacute; de microsoft con el top de las
                            ultimas tendencias que llegar&iacute;an al mercado mundial en desarrollo, testing, despliegue,
                            monitoreo y ultimas tendencias de IA.</p>
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <ul>
                                <li>
                                    <a target="_blank"
                                        href="{{ Utils::getUrlBase() }}/img/educacion/CONSTANCIA-ASISTENCIA-DEVDAYS-MICROSOFT-2016.PNG">
                                        <img src="{{ Utils::getUrlBase() }}/img/educacion/CONSTANCIA-ASISTENCIA-DEVDAYS-MICROSOFT-2016.PNG"
                                            alt="Constancia de asistencia a conferencia DEVDAYS LATAM 2016 - MICROSOFT por parte de {{ GlobalClass::getCreatorUser() }}">
                                    </a>
                                </li>
                            </ul>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/URP.jpg" alt="Logo del URP">
                    </picture>
                    <div>
                        <p class="h3 bold">DIPLOMA DE HONOR UNIVERSIDAD RICARDO PALMA | URP</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>Proyecto Easy
                            Ventas</p>
                        <p class=""><span class="bold">Fechas en la que realizo: </span>2015</p>
                        <p>Diploma de honor de concurso de proyectos de sistema en la Universidad Ricardo Palma con el
                            "Proyecto Easy Ventas".</p>
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <ul>
                                <li>
                                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/educacion/DIPLOMA-DE-HONOR-URP.png">
                                        <img src="{{ Utils::getUrlBase() }}/img/educacion/DIPLOMA-DE-HONOR-URP.png"
                                            alt="Diploma de honor de concurso de proyectos de sistema en la Universidad Ricardo Palma con el Proyecto Easy Ventas por parte de {{ GlobalClass::getCreatorUser() }}">
                                    </a>
                                </li>
                            </ul>
                        </ul>
                    </div>
                </li>
                   <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/URP.jpg" alt="Logo del URP">
                    </picture>
                    <div>
                        <p class="h3 bold">HACKATHON UTP - COMERCIO | HACKATHON</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span> Constancia de participaci&oacute;n Hackathon UTP - El Comercio</p>
                        <p class=""><span class="bold">Fechas en la que realizo: </span>2021</p>
                        <p>Constancia de participaci&oacute;n en la Hackathon UTP - El comercio realizada del 5 al 9 de noviembre del 2021.</p>
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <ul>
                                <li>
                                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/educacion/HACKATON-UTP-COMERCIO-2021.jpg">
                                        <img src="{{ Utils::getUrlBase() }}/img/educacion/HACKATON-UTP-COMERCIO-2021.jpg"
                                            alt="Constancia de participaci&oacute;n en la Hackathon UTP - El comercio realizada del 5 al 9 de noviembre del 2021">
                                    </a>
                                </li>
                            </ul>
                        </ul>
                    </div>
                </li>
            </ul>
        </section>

        <section>
            <p>Mis Diplomados</p>
            <ul>

                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/ccl.jfif"
                            alt="Logo del Centro de Capacitaci&oacute;n Empresarial | C&aacute;mara de Comercio de Lima | CCL">
                    </picture>
                    <div>
                        <p class="h3 bold">Centro de Capacitaci&oacute;n Empresarial | C&aacute;mara de Comercio de Lima | CCL</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>DIPLOMADO EN
                            MARKETING DIGITAL ESTRAT&Eacute;GICO</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2017 –
                            2017</p>
                        <p>Me permiti&oacute; obtener conocimientos en la planificaci&oacute;n, gesti&oacute;n e implementaci&oacute;n de estrategias y
                            acciones de marketing digital orientadas a generar posicionamiento, reconocimiento de marca.
                            Tambi&eacute;n obtuve conocimientos para promocionar e impulsar campa&ntilde;as de comunicaci&oacute;n en el mundo
                            virtual, a fin de llegar de modo directo y no invasivo a sus potenciales consumidores.</p>
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <ul>
                                <li>
                                    <a target="_blank"
                                        href="{{ Utils::getUrlBase() }}/img/educacion/CCL-MARKETING-DIGITAL-ESTRATEGICO-NOTAS.jfif">
                                        <img src="{{ Utils::getUrlBase() }}/img/educacion/CCL-MARKETING-DIGITAL-ESTRATEGICO-NOTAS.jfif"
                                            alt="Certificado de CCL MARKETING DIGITAL ESTRATEGICO NOTAS culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank"
                                        href="{{ Utils::getUrlBase() }}/img/educacion/CCL-MARKETING-DIGITAL-ESTRATEGICO-DIPLOMADO.jfif">
                                        <img src="{{ Utils::getUrlBase() }}/img/educacion/CCL-MARKETING-DIGITAL-ESTRATEGICO-DIPLOMADO.jfif"
                                            alt="Certificado de CCL DIPLOMADO EN MARKETING DIGITAL ESTRAT&Eacute;GICO culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                    </a>
                                </li>
                            </ul>
                        </ul>
                    </div>
                </li>
            </ul>
        </section>
        <section>
            <p>Mis Certificados de Estudios</p>
            <ul>
                <li>
                    <picture>
                        <img style="max-width: 120px;" src="{{ Utils::getUrlBase() }}/img/educacion/logo-udemy.svg"
                            alt="Logo de Udemy | Udemy Bussiness">
                    </picture>
                    <div>
                        <p class="h3 bold">Udemy | ReactiveX</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>REACTIVEX -
                            RXJS: DE CERO HASTA LOS DETALLES</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2022 –
                            2022</p>
                        <ul>
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/REACTIVEX-RXJS-DE-CERO-HASTA-LOS-DETALLES.jpg">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/REACTIVEX-RXJS-DE-CERO-HASTA-LOS-DETALLES.jpg"
                                        alt="El anterior certificado garantiza que {{ GlobalClass::getCreatorUser() }} ha completado con &eacute;xito el curso ReactiveX - RxJs: De cero hasta los detalles a fecha de 19/02/2022, habiendo sido impartido por Fernando Herrera en Udemy. El certificado indica que se ha completado la totalidad del curso, seg&uacute;n lo validado por el estudiante. La duraci&oacute;n del curso representa el total de horas de v&iacute;deo del curso en el momento de finalizaci&oacute;n m&aacute;s reciente.">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img style="max-width: 120px;" src="{{ Utils::getUrlBase() }}/img/educacion/logo-udemy.svg"
                            alt="Logo de Udemy | Udemy Bussiness">
                    </picture>
                    <div>
                        <p class="h3 bold">Udemy | JavaScript - ECMAScript 6</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>JAVASCRIPT:
                            ECMASCRIPT 6 Y TODOS SUS DETALLES</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2022 –
                            2022</p>
                        <ul>
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/JAVASCRIPT-ECMASCRIPT6-TODOS-SUS-DETALLES.jpg">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/JAVASCRIPT-ECMASCRIPT6-TODOS-SUS-DETALLES.jpg"
                                        alt="El anterior certificado garantiza que {{ GlobalClass::getCreatorUser() }} ha completado con &eacute;xito el curso JavaScript: ECMAScript 6 y todos sus detalles a fecha de 16/02/2022, habiendo sido impartido por Fernando Herrera en Udemy. El certificado indica que se ha completado la totalidad del curso, seg&uacute;n lo validado por el estudiante. La duraci&oacute;n del curso representa el total de horas de v&iacute;deo del curso en el momento de finalizaci&oacute;n m&aacute;s reciente.">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img style="max-width: 120px;" src="{{ Utils::getUrlBase() }}/img/educacion/logo-udemy.svg"
                            alt="Logo de Udemy | Udemy Bussiness">
                    </picture>
                    <div>
                        <p class="h3 bold">Udemy | Angular</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>ANGULAR CON
                            DEVOPS, TDD, PRUEBAS UNITARIAS, PIPELINES, AZURE</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2022 –
                            2022</p>
                        <ul>
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/ANGULAR-CON-DEVOPS-TDD-PRUEBAS-UNITARIAS-PIPELINES-AZURE.jpg">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/ANGULAR-CON-DEVOPS-TDD-PRUEBAS-UNITARIAS-PIPELINES-AZURE.jpg"
                                        alt="El anterior certificado garantiza que {{ GlobalClass::getCreatorUser() }} ha completado con &eacute;xito el curso Angular con DevOps, TDD, Pruebas Unitarias, Pipelines, Azure a fecha de 15/02/2022, habiendo sido impartido por Jhonatan Plata en Udemy. El certificado indica que se ha completado la totalidad del curso, seg&uacute;n lo validado por el estudiante. La duraci&oacute;n del curso representa el total de horas de v&iacute;deo del curso en el momento de finalizaci&oacute;n m&aacute;s reciente.">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img style="max-width: 120px;" src="{{ Utils::getUrlBase() }}/img/educacion/logo-udemy.svg"
                            alt="Logo de Udemy | Udemy Bussiness">
                    </picture>
                    <div>
                        <p class="h3 bold">Udemy | Angular</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>ANGULAR:
                            PRUEBAS UNITARIAS CON JASMINE Y KARMA</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2022 –
                            2022</p>
                        <ul>
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/ANGULAR-PRUEBAS-UNITARIAS-JASMINE-KARMA.jpg">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/ANGULAR-PRUEBAS-UNITARIAS-JASMINE-KARMA.jpg"
                                        alt="El anterior certificado garantiza que {{ GlobalClass::getCreatorUser() }} ha completado con &eacute;xito el curso Angular: Pruebas unitarias con Jasmine y Karma a fecha de 15/02/2022, habiendo sido impartido por Efisio Pitzalis Morillas en Udemy. El certificado indica que se ha completado la totalidad del curso, seg&uacute;n lo validado por el estudiante. La duraci&oacute;n del curso representa el total de horas de v&iacute;deo del curso en el momento de finalizaci&oacute;n m&aacute;s reciente.">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img style="max-width: 120px;" src="{{ Utils::getUrlBase() }}/img/educacion/logo-udemy.svg"
                            alt="Logo de Udemy | Udemy Bussiness">
                    </picture>
                    <div>
                        <p class="h3 bold">Udemy | Java</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>PROGRAMACI&Oacute;N
                            FUNCIONAL EN JAVA CON LAMBDAS Y STREAMS</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2022 –
                            2022</p>
                        <ul>
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/PROGRAMACION-FUNCIONAL-JAVA-LAMBDAS-STREAMS.jpg">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/PROGRAMACION-FUNCIONAL-JAVA-LAMBDAS-STREAMS.jpg"
                                        alt="Garantiza que {{ GlobalClass::getCreatorUser() }} ha completado con &eacute;xito el curso Programaci&oacute;n funcional en Java con Lambdas y Streams a fecha de 07/02/2022, habiendo sido impartido por Domingo Sebastian en Udemy. El certificado indica que se ha completado la totalidad del curso, seg&uacute;n lo validado por el estudiante. La duraci&oacute;n del curso representa el total de horas de v&iacute;deo del curso en el momento de finalizaci&oacute;n m&aacute;s reciente.">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/coursera.png" alt="Logo de Coursera | Google Cloud">
                    </picture>
                    <div>
                        <p class="h3 bold">Coursera | Google Cloud</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>GOOGLE CLOUD
                            PLATFORM FUNDAMENTOS: CORE INFRAESTRUCTURA</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2020 –
                            2020</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/GOOGLE-CLOUD-PLATFORM-FUNDAMENTOS-CORE-INFRAESTRUCTURA.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/GOOGLE-CLOUD-PLATFORM-FUNDAMENTOS-CORE-INFRAESTRUCTURA.jfif"
                                        alt="Certificado de GOOGLE CLOUD PLATFORM FUNDAMENTOS: CORE INFRAESTRUCTURA culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/coursera.png" alt="Logo de Coursera | Google Cloud">
                    </picture>
                    <div>
                        <p class="h3 bold">Coursera | Google Cloud</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n:
                            </span>INFRAESTRUCTURA CLOUD Y FIABILIDAD: DISE&Ntilde;O Y PROCESOS</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2020 –
                            2020</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/INFRAESTRUCTUR-CLOUD-Y-FIABILIDAD-DISENO-Y-PROCESOS.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/INFRAESTRUCTUR-CLOUD-Y-FIABILIDAD-DISENO-Y-PROCESOS.jfif"
                                        alt="Certificado de INFRAESTRUCTURA CLOUD Y FIABILIDAD: DISE&Ntilde;O Y PROCESOS culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/coursera.png" alt="Logo de Coursera | Google Cloud">
                    </picture>
                    <div>
                        <p class="h3 bold">Coursera | Google Cloud</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>FUNDAMENTOS
                            DE LA INFRAESTRUCTURA CLOUD: CORE SERVICES</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2020 –
                            2020</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/FUNDAMENTOS-DE-LA-INFRAESTRUCTURA-CLOUD-CORE-SERVICES.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/FUNDAMENTOS-DE-LA-INFRAESTRUCTURA-CLOUD-CORE-SERVICES.jfif"
                                        alt="Certificado de FUNDAMENTOS DE LA INFRAESTRUCTURA CLOUD: CORE SERVICES culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n:
                            </span>WORKSHOP:HTML5 - BOOTSTRAP</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2017 –
                            2017</p>
                        <p>El curso me permiti&oacute; conocer el c&oacute;digo HTML del que est&aacute;n hechas las p&aacute;ginas web, el c&oacute;digo CSS
                            para aplicar el formato que se desea, as&iacute; como, aplicar editores de c&oacute;digo que permiten un mejor
                            control y administraci&oacute;n.</p>
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/WORKSHOP-HTML5-BOOTSTRAP.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/WORKSHOP-HTML5-BOOTSTRAP.jfif"
                                        alt="Certificado de WORKSHOP:HTML5 - BOOTSTRAP culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>MS. WINDOWS
                            2012 SERVER -SOPORTE Y CONFIGURACI&Oacute;N</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2017 –
                            2017</p>
                        <p>El curso me proporciono las t&eacute;cnicas y conocimientos necesarios para poder implementar servicios
                            de red en una infraestructura de redes de dominio; basada en Windows Server 2012. Asimismo, los
                            conocimientos necesarios para poder instalarlos, configurarlos y administrarlos.</p>
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/MS.-WINDOWS-2012-SERVER-SOPORTE-Y-CONFIGURACION.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/MS.-WINDOWS-2012-SERVER-SOPORTE-Y-CONFIGURACION.jfif"
                                        alt="Certificado de MS. WINDOWS 2012 SERVER -SOPORTE Y CONFIGURACI&Oacute;N culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>MS. WINDOWS
                            2021 SERVER - ADMINISTRACI&Oacute;N</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2017 –
                            2017</p>
                        <p>El curso me permiti&oacute; aprender a establecer los criterios para instalar un servidor de base de
                            datos SQL SERVER, administrar archivos de una base de datos SQL Server, crear una base de datos,
                            dise&ntilde;ar e implementar la integridad de datos, establecer formas de respaldo a la Base de datos,
                            realizar la distribuci&oacute;n de datos a trav&eacute;s de la replicaci&oacute;n e implementar alertas y
                            programaci&oacute;n de tareas autom&aacute;ticas.</p>
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/MS.-WINDOWS-2021-SERVER-ADMINISTRACION.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/MS.-WINDOWS-2021-SERVER-ADMINISTRACION.jfif"
                                        alt="Certificado de MS. WINDOWS 2021 SERVER - ADMINISTRACI&Oacute;N culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>DESARROLLO DE
                            SOLUCIONES WEB ASP.NET AJAX MVC CON C#</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2017 –
                            2017</p>
                        <p>El curso me proporciono los conocimientos de implementaci&oacute;n de un sistema web en C# aplicando el
                            patr&oacute;n MVC con WebForms y formularios de mantenimiento. As&iacute; como, realizar consultas a base de
                            datos SQL SEVER.</p>
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/DESARROLLO-DE-SOLUCIONES-WEB-ASP.NET-AJAX-MVC-CON-C.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/DESARROLLO-DE-SOLUCIONES-WEB-ASP.NET-AJAX-MVC-CON-C.jfif"
                                        alt="Certificado de DESARROLLO DE SOLUCIONES WEB ASP.NET AJAX MVC CON C# culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n:
                            </span>ESPECIALISTA
                            EN EXCEL</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2017
                            –
                            2017</p>
                        <p>En esta &aacute;rea de especializaci&oacute;n obtuve los conocimientos y t&eacute;cnicas para planear y controlar las
                            actividades de los procesos de obtenci&oacute;n de la informaci&oacute;n como recurso fundamental para mejorar
                            la toma de decisiones.</p>
                        <p>
                            Comprende los M&oacute;dulos: MS.EXCEL B&Aacute;SICO 2016 | MS.EXCEL INTERMEDIO 2016 | MS.EXCEL AVANZADO 2016
                            | TABLAS DIN&Aacute;MICAS Y GR&Aacute;FICOS CON EXCEL 2016
                        </p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank" href="{{ Utils::getUrlBase() }}/img/educacion/ESPECIALISTA-EN-EXCEL.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/ESPECIALISTA-EN-EXCEL.jfif"
                                        alt="Certificado de ESPECIALISTA EN EXCEL culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>WORSHOP:
                            ETHICAL HACKING -B&Aacute;SICO</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2016
                            –
                            2016</p>
                        <p>El curso me permiti&oacute; aprender a conocer LAS 05 fases de la metodolog&iacute;a de Ethical Hacking: <BR />
                            1ER RECONOCIMIENTO | 2DO ESCANEO DE PUERTOS Y VULNERABILIDADES | 3ER GANANDO ACCESO | 4TO
                            MANTENIENDO ACCESO | 5TO BORRANDO HUELLAS
                        </p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/WORSHOP-ETHICAL-HACKING-BASICO.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/WORSHOP-ETHICAL-HACKING-BASICO.jfif"
                                        alt="Certificado de WORSHOP: ETHICAL HACKING -B&Aacute;SICO culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>MS.SQL
                            SERVER
                            2012 - ADMINISTRACI&Oacute;N</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2015
                            –
                            2016</p>
                        <p>El curso me permiti&oacute; establecer los criterios para instalar un servidor de base de datos SQL
                            SERVER, administrar archivos de una base de datos SQL Server, crear una base de datos, dise&ntilde;ar e
                            implementar la integridad de datos, establecer formas de respaldo a la Base de datos, realizar
                            la distribuci&oacute;n de datos a trav&eacute;s de la replicaci&oacute;n e implementar alertas y programaci&oacute;n de
                            tareas autom&aacute;ticas.</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/MS.SQL-SERVER-2012-ADMINISTRACION.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/MS.SQL-SERVER-2012-ADMINISTRACION.jfif"
                                        alt="Certificado de MS.SQL SERVER 2012 - ADMINISTRACI&Oacute;N culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/universidad-complutence-de-madrid.png"
                            alt="Logo de la Universidad Complutense de Madrid">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Complutense de Madrid | Google Act&iacute;vate</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>CURSO DE
                            PROGRAMACI&Oacute;N DE APPS M&Oacute;VILES</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2015
                            –
                            2015</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/CURSO-DE-PROGRAMACION-DE-APPS-MOVILES.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/CURSO-DE-PROGRAMACION-DE-APPS-MOVILES.jfif"
                                        alt="Certificado de CURSO DE PROGRAMACI&Oacute;N DE APPS M&Oacute;VILES culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>MS.SQL
                            SERVER
                            2021 - IMPLEMENTACI&Oacute;N</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2015
                            –
                            2015</p>
                        <p>El curso me permiti&oacute; aprender las herramientas integradas de SQL Server 2012 para la
                            implementaci&oacute;n de bases de datos relacionales, as&iacute; como su integraci&oacute;n con la plataforma .NET
                            para el manejo de procedimientos almacenados. Adem&aacute;s, como reconocer las t&eacute;cnicas recomendadas
                            para la implementaci&oacute;n de bases de datos, los objetos y las diferentes sentencias que ofrece SQL
                            2012 con respecto a versiones anteriores.</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/MS.SQL-SERVER-2021-IMPLEMENTACION.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/MS.SQL-SERVER-2021-IMPLEMENTACION.jfif"
                                        alt="Certificado de MS.SQL SERVER 2021 - IMPLEMENTACI&Oacute;N culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>JAVA
                            DEVELOPER</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2015
                            – 2015</p>
                        <p>El curso me permiti&oacute; comprender los fundamentos del lenguaje de programaci&oacute;n Java. Desarrolle
                            aplicaciones de escritorio con conexi&oacute;n a base de dato ORACLE y creaci&oacute;n de reportes.
                            Por ultimo implementar un sistema web utilizando tecnolog&iacute;as como JQuery, Ajax y Servlets.</p>
                        <p>
                            Comprende los M&oacute;dulos: JAVA FUNDAMENTOS | JAVA CLIENTE/SERVIDOR | JAVA APLICACIONES WEB
                        </p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank" href="{{ Utils::getUrlBase() }}/img/educacion/JAVA-DEVELOPER.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/JAVA-DEVELOPER.jfif"
                                        alt="Certificado de JAVA DEVELOPER culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/eoi.jfif"
                            alt="Logo de la Escuela de Organizaci&oacute;n Industrial">
                    </picture>
                    <div>
                        <p class="h3 bold">Escuela de Organizaci&oacute;n Industrial | Google Act&iacute;vate</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>CURSO DE
                            CLOUD COMPUTING</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2015
                            – 2015</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/CURSO-DE-CLOUD-COMPUTING.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/CURSO-DE-CLOUD-COMPUTING.jfif"
                                        alt="Certificado de CURSO DE CLOUD COMPUTING culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n:
                            </span>MODELAMIENTO DE DATOS</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2015
                            – 2015</p>
                        <p>El curso me permiti&oacute; estudiar los conceptos del Proceso Unificado de desarrollo de Software
                            (UML), as&iacute; como su aplicaci&oacute;n mediante el uso de una herramienta case Rational Rose o Visio)
                            para el modelamiento de objetos.</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank" href="{{ Utils::getUrlBase() }}/img/educacion/MODELAMIENTO-DE-DATOS.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/MODELAMIENTO-DE-DATOS.jfif"
                                        alt="Certificado de MODELAMIENTO DE DATOS culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n: </span>PHP NIVEL I
                            - PROGRAMACI&Oacute;N WEB</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2015
                            – 2015</p>
                        <p>El curso me permiti&oacute; aprender el lenguaje de programaci&oacute;n PHP y elaborar una p&aacute;gina web.</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/PHP-NIVEL-I-PROGRAMACION-WEB.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/PHP-NIVEL-I-PROGRAMACION-WEB.jfif"
                                        alt="Certificado de PHP NIVEL I - PROGRAMACI&Oacute;N WEB culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a | Facultad de Ingenier&iacute;a Industrial y
                            de Sistemas | SISTEMAS UNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n:
                            </span>INSTALACIONES Y CONFIGURACI&Oacute;N DE SO</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2014
                            – 2014</p>
                        <p>El curso me proporciono conocimientos para realizar la instalaci&oacute;n y configuraci&oacute;n de un sistema
                            operativo seg&uacute;n las necesidades del cliente, as&iacute; como, diagn&oacute;stico y soluci&oacute;n de problemas, a
                            nivel de software.</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/INSTALACIONES-Y-CONFIGURACION-DE-SO.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/INSTALACIONES-Y-CONFIGURACION-DE-SO.jfif"
                                        alt="Certificado de INSTALACIONES Y CONFIGURACI&Oacute;N DE SO culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Logo de la UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Nacional de Ingenier&iacute;a Centro de Computo | Facultad de
                            Ingenier&iacute;a Mec&aacute;nica - FIM | INFOUNI</p>
                        <p class=""><span class="bold">Nombre de la titulaci&oacute;n:
                            </span>ESPECIALISTA EN ENSAMBLAJE DE PC'S</p>
                        <p class=""><span class="bold">Fechas de graduaci&oacute;n prevista: </span>2011
                            – 2011</p>
                        <p>El curso me proporciono conocimientos para realiza el ensamblaje, diagn&oacute;stico y soluci&oacute;n de
                            problemas, a nivel de hardware y software, de un equipo de c&oacute;mputo. As&iacute; mismo, me permite poder
                            dar asesor&iacute;a a empresas que requieren equipos de &oacute;ptimo rendimiento y de acuerdo con sus
                            necesidades.</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/ESPECIALISTA-EN-ENSAMBLAJE-DE-PCS.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/ESPECIALISTA-EN-ENSAMBLAJE-DE-PCS.jfif"
                                        alt="Certificado de ESPECIALISTA EN ENSAMBLAJE DE PC'S culminado por parte de {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </section>
        <section>
            <p>Mis Otras Aptitudes</p>
            <ul>
                <li>An&aacute;lisis de datos</li>
                <li>An&aacute;lisis de negocio</li>
                <li>Capacidad de an&aacute;lisis</li>
                <li>An&aacute;lisis web</li>
                <li>Marketing digital</li>
                <li>Ingenier&iacute;a</li>
                <li>An&aacute;lisis</li>
                <li>An&aacute;lisis de sistemas</li>
                <li>Desarrollo de software</li>
                <li>Tecnolog&iacute;a de la informaci&oacute;n</li>
                <li>Ingenier&iacute;a inform&aacute;tica</li>
                <li>Desarrollo web</li>
                <li>Software</li>
                <li>Startup</li>
                <li>Marketing</li>
                <li>Scrum</li>
                <li>Desarrollo de Android</li>
                <li>Herramientas y tecnolog&iacute;as</li>
                <li>Swift (lenguaje de programaci&oacute;n)</li>
                <li>JavaScript</li>
                <li>jQuery</li>
                <li>HTML5</li>
                <li>Hojas de estilos en cascada (CSS)</li>
                <li>SCSS</li>
                <li>Laravel</li>
                <li>Kotlin</li>
                <li>React.js</li>
                <li>Bootstrap</li>
                <li>Angular</li>
                <li>Angular Material</li>
                <li>Node.js</li>
                <li>C#</li>
                <li>DotNetNuke</li>
                <li>MySQL</li>
                <li>PostgreSQL</li>
                <li>Ionic Framework</li>
                <li>Ionic</li>
                <li>API de Postman</li>
                <li>OAuth</li>
                <li>JIRA</li>
                <li>Lenguaje unificado de modelado (UML)</li>
                <li>Microsoft SQL Server</li>
                <li>Git</li>
                <li>Android Studio</li>
                <li>TypeScript</li>
                <li>Gesti&oacute;n de proyectos en cascada</li>
                <li>Haskell</li>
                <li>Express.js</li>
                <li>Rup</li>
                <li>API de Google Maps</li>
                <li>Marketing de crecimiento</li>
            </ul>
        </section>
    </main>
@stop
