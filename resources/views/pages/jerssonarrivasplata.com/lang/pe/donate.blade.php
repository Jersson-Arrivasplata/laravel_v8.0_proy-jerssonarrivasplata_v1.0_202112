@extends(GlobalClass::getDomainFolder(0).'::base.index')
@section('metatags')
    @parent
        <!-- some master css here -->
    <title>Donar a {{ GlobalClass::getCreatorUser() }} | Software Developer</title>


    <meta property="og:type" content="article">

    <meta name="description" content="P&aacute;gina de donaci&oacute;n de {{ GlobalClass::getCreatorUser() }} -
    Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">

    <meta property="og:description"
    content="P&aacute;gina de donaci&oacute;n de {{ GlobalClass::getCreatorUser() }} -
    Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">
    <meta property="og:title" content="Donar a {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta property="og:url" content="{{ Utils::getUrlBase() }}/donacion">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="{{ Utils::getUrlBase() }}/donacion">
    <meta name="twitter:title" content="Donar a {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta name="twitter:description"
     content="P&aacute;gina de donaci&oacute;n de {{ GlobalClass::getCreatorUser() }} -
    Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">
@stop
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase() }}/css/index-project.css?v=<?php echo(rand()); ?>" rel="stylesheet">
@stop

@section('content')
    @parent
    <!-- Main content goes here -->
    <main>
        @include(GlobalClass::getDomainFolder(0) . '::lang.'.request()->route()->parameter('language').'.common.header-menu')
        <section ><!--style="padding-top: 0px;"-->
            <p>DONACI&Oacute;N</p>
        </section>
        <article>
            <p class="h3">Si alguno de mis proyectos te ayudo y quieres seguir apoyandome para continuar con este
                proyecto personal. <br /><label class="bold">¡Te agradeceria dejarme una donaci&oacute;n!</label></p>
            <ul>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/bitcoin.png" />
                    <p>Bitcoin: {{ config('base.virtual_coins.bitcoin.address') }}</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/ethereum.png" />
                    <p>Ethereum: {{ config('base.virtual_coins.ethereum') }}</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/fio.png" />
                    <p>FIO: {{ config('base.virtual_coins.fio') }}</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                </li>
            </ul>
        </article>
        <article>
            <ul>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/yape.png" />
                    <p>YAPE: {{ config('base.info.celphone.number') }}</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Tipo: Soles</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/plin.png" />
                    <p>PLIN: {{ config('base.info.celphone.number') }}</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Tipo: Soles</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/tunki.png" />
                    <p>TUNKI: {{ config('base.info.celphone.number') }}</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Tipo: Soles</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/lukita-bbva.png" />
                    <p>LUKITA BBVA: {{ config('base.info.celphone.number') }}</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Tipo: Soles</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/bcp.png" />
                    <p>N° de cuenta: 19104250410043</p>
                    <p>CCI: 00219110425041004350</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Tipo: Soles</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/interbank.png" />
                    <p>N° de cuenta: 898 3187319104</p>
                    <p>CCI: 00389801318731910446</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Tipo: D&oacute;lares</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/paypal.png" />
                    <p>jersson.arrivasplata@gmail.com</p>
                    <p>@jerssonArrivasplata</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                </li>
            </ul>
        </article>
    </main>


@stop
