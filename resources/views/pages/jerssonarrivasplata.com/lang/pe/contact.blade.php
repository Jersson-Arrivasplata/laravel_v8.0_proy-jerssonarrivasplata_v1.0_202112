@extends(GlobalClass::getDomainFolder(0).'::base.index')
@section('metatags')
    @parent
    <!-- some master css here -->
    <title>Contactar a {{ GlobalClass::getCreatorUser() }} | Software Developer</title>
    <meta property="og:type" content="article">

    <meta name="description" content="P&aacute;gina de contacto de {{ GlobalClass::getCreatorUser() }} -
    Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">

    <meta property="og:description"
    content="P&aacute;gina de contacto de {{ GlobalClass::getCreatorUser() }} -
    Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">
    <meta property="og:title" content="Contactar a {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta property="og:url" content="{{ Utils::getUrlBase() }}/contacto">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="{{ Utils::getUrlBase() }}/contacto">
    <meta name="twitter:title" content="Contactar a {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta name="twitter:description"
   content="P&aacute;gina de contacto de {{ GlobalClass::getCreatorUser() }} -
    Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles,
    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,
    an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.">

@stop
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase() }}/css/index-contact.css?v=<?php echo rand(); ?>" rel="stylesheet">

    {{-- <script async src="https://pay.google.com/gp/p/js/pay.js" onload="onGooglePaylLoaded()"></script>
    <script type="text/javascript">
        function onGooglePaylLoaded() {
            const googlePayClient = new google.payments.api.PaymentsClient({
                environment: 'TEXT' //'PRODUCTION'
            });

            const clientConfiguration = {
                apiVersion: 2,
                apiVersionMinor: 0,
                allowedPaymentMethods: [cardPaymentMethod]
            };

            googlePayClient.isReadyToPay(clientConfiguration)
                .then(function(response) {
                    if (response.result) {
                        // add a Google Pay button
                        googlePayClient.createButton({
                            // defaults to black if default or omitted
                            buttonColor: 'default',
                            // defaults to long if omitted
                            buttonType: 'long',
                            onClick: onGooglePaymentsButtonClicked
                        });

                    }
                }).catch(function(err) {
                    //oCULTAR BOTON DE GOOGLE PAT
                });

        }
    </script> --}}
@stop

@section('content')
    @parent
    <!-- Main content goes here -->
    <main>
        @include(GlobalClass::getDomainFolder(0) . '::lang.'.request()->route()->parameter('language').'.common.header-menu')
        <section>
            <p>CONT&Aacute;CTAME</p>
            <ul class="pl-2">
                <li>Linkedin: <a target="_blank"
                        href="{{ config('base.social_networks.linkedin') }}">{{ Str::after(config('base.social_networks.linkedin'), config('base.others.https')) }}</a>
                </li>
                <li>Facebook: <a target="_blank"
                        href="{{ config('base.social_networks.facebook') }}">{{ Str::after(config('base.social_networks.facebook'), config('base.others.https')) }}</a>
                </li>
                <li>Instagram: <a target="_blank"
                        href="{{ config('base.social_networks.instagram') }}">{{ Str::after(config('base.social_networks.instagram'), config('base.others.https')) }}</a>
                </li>
                <li>WebSite: <a target="_blank"
                        href="{{ config('base.website') }}">{{ Str::after(config('base.website'), config('base.others.https')) }}</a>
                </li>
                <li>N&uacute;mero de tel&eacute;fono: <a target="_blank"
                        href="tel:({{ config('base.info.celphone.code') }}){{ config('base.info.celphone.number') }}">+{{ config('base.info.celphone.code') }} {{ config('base.info.celphone.number') }}
                        (M&oacute;vil)</a></li>
                <li>Correo: <a target="_blank" href="mailto:{{ config('base.info.email') }}">{{ config('base.info.email') }}</a></li>
                {{--<li>Direcci&oacute;n: {{ config('base.info.address') }}</li>--}}
            </ul>
            <form method="POST" action="{{ route('contact.message', ['language' => request()->route()->parameter('language')]) }}">
                @csrf
                <h1>ENVIAR MENSAJE</h1>

                @if (Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        {!! session('success') !!}
                    </div>
                @endif
                <input name="origin" type="text"  value="{{Utils::getUrlBase()}}" hidden/>

                <div>
                    <label>Ingresa tu nombre</label>
                    <input name="name" type="text" maxlength="100" placeholder="Ingresar Nombre" required />
                </div>
                <div>
                    <label>Ingresa tu celular</label>
                    <input name="celphone" type="number" maxlength="50" placeholder="Ingresar celular" required />
                </div>
                <div>
                    <label>Ingresa tu correo</label>
                    <input name="email" type="email" maxlength="190" placeholder="Ingresar correo" required />
                </div>
                <div>
                    <label>Ingresa tu mensaje</label>
                    <textarea name="message" maxlength="1000000" placeholder="Ingresar mensaje" required></textarea>
                </div>
                <div>
                    <input type="submit" value="Enviar" />
                </div>
            </form>
        </section>
    </main>


@stop
