  <header>
      <nav>
          <div>
              <a href="{{ Utils::getUrlBase() }}">
                  <img src="{{ Utils::getUrlBase() }}/img/jerssonarrivasplata.com.png"
                      alt="Brand of {{ GlobalClass::getCreatorUser() }}" />
              </a>
              <a href="#">Menu</a>
          </div>
          <ul>
              <li><a target="_self"  href="{{ Utils::getUrlBase() }}/{{request()->route()->parameter('language')}}#aboutme">About me</a>
              </li>
              <li><a target="_self"
                      href="{{ Utils::getUrlBase() }}/{{request()->route()->parameter('language')}}#experience">Experience</a></li>
              <li><a target="_self" onclick="loader()" href="{{ Utils::getUrlBase() }}/{{request()->route()->parameter('language')}}/education">Education</a>
              </li>
              <li><a target="_self" onclick="loader()" href="{{ Utils::getUrlBase() }}/{{request()->route()->parameter('language')}}/donation">Donation</a>
              </li>
              <li><a target="_self" onclick="loader()" href="{{ Utils::getUrlBase() }}/{{request()->route()->parameter('language')}}/blog">Blog & Projects</a></li>
              <li><a target="_self" onclick="loader()" href="{{ Utils::getUrlBase() }}/{{request()->route()->parameter('language')}}/contact">Contact</a></li>
              <li>
                  <label class="dropdown">
                      <div class="button">
                          <div>
                              <span>en</span>
                          </div>
                      </div>
                      <input type="checkbox">
                      <ul>

                          <li>
                              <a target="_self" href="/pe">
                                  pe
                              </a>
                          </li>

                      </ul>
                  </label>

              </li>
              <li><button onclick="menu()">Close</button></li>
          </ul>
      </nav>
  </header>
  <!--<i style="font-size: 1rem;" class="mdi mdi-menu-down"></i>-->
