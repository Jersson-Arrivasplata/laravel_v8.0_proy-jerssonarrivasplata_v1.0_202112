@extends(GlobalClass::getDomainFolder(0) . '::base.index')
@section('metatags')
    @parent
    <!-- some master css here -->
    <title>{{ GlobalClass::getCreatorUser() }} | Software Developer</title>

    <meta property="og:type" content="website">

    <meta name="description"
        content="Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">

    <meta property="og:description"
        content="Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">

    <meta property="og:title" content="{{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta property="og:url" content="{{ Utils::getUrlBase() }}">


    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="{{ Utils::getUrlBase() }}">
    <meta name="twitter:title" content="{{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta name="twitter:description"
        content="Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">

@stop
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase() }}/css/index.css?v=<?php echo rand(); ?>" rel="stylesheet">
    <style type="text/css">


      @media (max-width: 768px) {
        main>section:nth-child(3) {
         height: 70rem!important;
        }
       }

      @media (max-width: 425px) {
         main>section:nth-child(3) {
           height: 80rem!important;
          }
        }
    </style>
@stop

@section('content')
    @parent
    <!-- Main content goes here -->
    <main>
        @include(GlobalClass::getDomainFolder(0) .
                '::lang.' .
                request()->route()->parameter('language') .
                '.common.header-menu')
        <section>
            <ul></ul>
            <ul>
                <li>I'm {{ GlobalClass::getCreatorUser() }}</li>
                <li>
                    Senior Java Developer at Stefanini LATAM, leading Java and microservices developments,
                    ensuring quality. Technical Consultant at SOAINT, specializing in Promsace with Stencil and Angular libraries. Expert in
                    Front-End (Angular, Stencil-Js) and Back-End (Spring Boot, Java 8+), contributing to the Soaint Project with Git guidelines and
                    automation in Angular +14. Experience with BCP and Vass, leading key projects under Agile Scrum. Additionally, provides Front-End
                    courses at Cibertec.
                </li>
            </ul>
        </section>
        <section id="aboutme">
            <p>About me</p>
            <div>
                <div>
                    <p>
                        Bachelor of Software Engineering from the Technological University of Peru, Lima-Peru.
                        <br />
                        I am a very proactive person who seeks to learn and apply my knowledge to help other people or in the work in which I perform.
                        <br />
                        I like listening to music, watching movies, going on trips, taking my pet for a walk, and spending time with my family.
                    </p>
                    <p class="bold">Mi Perfil: </p>
                    <ul class="pl-2">
                        <li>Linkedin: <a target="_blank"
                                href="{{ config('base.social_networks.linkedin') }}">{{ Str::after(config('base.social_networks.linkedin'), config('base.others.https')) }}</a>
                        </li>
                        <li>Facebook: <a target="_blank"
                                href="{{ config('base.social_networks.facebook') }}">{{ Str::after(config('base.social_networks.facebook'), config('base.others.https')) }}</a>
                        </li>
                        <li>Instagram: <a target="_blank"
                                href="{{ config('base.social_networks.instagram') }}">{{ Str::after(config('base.social_networks.instagram'), config('base.others.https')) }}</a>
                        </li>
                        <li>Github: <a target="_blank"
                                href="{{ config('base.repositories.github.jersson-arrivasplata') }}">{{ Str::after(config('base.repositories.github.jersson-arrivasplata'), config('base.others.https')) }}</a>
                        </li>
                        <li>WebSite: <a target="_blank"
                                href="{{ config('base.website') }}">{{ Str::after(config('base.website'), config('base.others.https')) }}</a>
                        </li>
                        <li>Celphone number: <a target="_blank"
                                href="tel:({{ config('base.info.celphone.code') }}){{ config('base.info.celphone.number') }}">+{{ config('base.info.celphone.code') }} {{ config('base.info.celphone.number') }}
                                (Mobile)</a></li>
                        {{-- <li>Direcci&oacute;n: {{config('base.info.address')}}</li> --}}
                        <li>Email: <a target="_blank"
                                href="mailto:{{ config('base.info.email') }}">{{ config('base.info.email') }}</a></li>
                        <li>Birthday: July 11</li>
                    </ul>

                </div>

                <div>
                    <p>
                        <picture>

                            <img src="{{ Utils::getUrlBase() }}/img/profile.png"
                                alt="Image {{ GlobalClass::getCreatorUser() }}">
                        </picture>
                    </p>
                </div>
            </div>
        </section>
        <section id="experience">
            <p>My Experience</p>
            <article class="main-article-index">
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/cibertec.jfif" alt="CIBERTEC company logo">
                    </picture>
                    <p class="">
                        Employment Certificate
                    </p>
                    <a class="" target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-CIBERTEC.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-CIBERTEC.png"
                            alt="CIBERTEC employment certificate by {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p class="">
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-CIBERTEC.png">
                            (click to download)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Software Development Instructor</p>
                    <p class=""><span class="bold">Company Name: </span>CIBERTEC | Part-time</p>
                    <p class=""><span class="bold">Employment Dates: </span>Jul 2023 – Present</p>
                    <p class=""><span class="bold">Employment Duration: </span>12 months</p>
                    <p class=""><span class="bold">Location: </span>Lima, Peru</p>
                    <ul>
                        <li>Teaching specialized courses or modules:</li>
                        <li>
                            <ul>
                                <li>Foundations of Java Course (Java 8+, Stream, Functional, and others)</li>
                                <li>Web Applications Course (JavaEE and JakartaEE)</li>
                                <li>Front-End Course (Angular +12, React, and JavaScript)</li>
                                <li>Back-End Course (SpringBoot)</li>
                            </ul>
                        </li>
                        <li>Functions:</li>
                        <li>
                            <ul>
                                <li>Delivery of specialized courses.</li>
                                <li>Delivery of specialized modules.</li>
                            </ul>
                        </li>
                        <li>Tools:</li>
                        <li> Spring Boot, Java 8+, Streams, git, IntelliJ IDEA (IDE), React, PostgreSQL, Angular, JavaScript, Html5, CSS, CSS3, SCSS, Angular 8+, Postman, NodeJs, TypeScript, Tslint, Unit Test, and others.</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/stefanini.jfif" alt="Stefanini LATAM company logo">
                    </picture>
                    <p class="invisible">
                        Employment Certificate
                    </p>
                    <a class="invisible" target="_blank" href="{{ Utils::getUrlBase() }}/img/experience/CERTIFICADO-STEFANINI-LATAM.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experience/CERTIFICADO-STEFANINI-LATAM.png"
                            alt="Stefanini LATAM employment certificate by {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p class="invisible">
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experience/CERTIFICADO-STEFANINI-LATAM.png">
                            (click to download)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Java Developer</p>
                    <p class=""><span class="bold">Company Name: </span>Stefanini It Solutions Peru S.A.C. | Stefanini LATAM | Full-time</p>
                    <p class=""><span class="bold">Employment Dates: </span>Aug 2023 – Present</p>
                    <p class=""><span class="bold">Employment Duration: </span>11 months</p>
                    <p class=""><span class="bold">Location: </span>Lima, Peru</p>
                    <ul>
                        <li>Mi Movistar App Project (<a target="_blank" href="https://www.movistar.com.pe/app-mi-movistar">https://www.movistar.com.pe/app-mi-movistar</a>)</li>
                        <li>
                            <ul>
                                <li>Support for WebAdmin platforms consumed by Mi Movistar App</li>
                                <li>Support for microservices consumed by Mi Movistar App</li>
                                <li>Support for the ionic application consumed by Mi Movistar App </li>
                            </ul>
                        </li>
                        <li>Functions:</li>
                        <li>
                            <ul>
                                <li>Application Development: Design and development of Java applications and microservices, ensuring robustness and scalability.</li>
                                <li>API Integration: Responsible for integrating our applications with external services through API consumption, ensuring efficient and seamless integration.</li>
                                <li>Comprehensive Testing: Performing unit and integration tests to ensure code quality and application functionality.</li>
                                <li>Agile Collaboration: Close collaboration with the development team, applying agile methodologies such as SAFe and Scrum throughout the software lifecycle.</li>
                                <li>Project Management: Leadership in team management, ensuring timely and quality delivery using tools like Jira and Sprint Planning.</li>
                                <li>Continuous Improvement: Outstanding leadership in the continuous improvement of the web admin platform and the Mi Movistar mobile application, proposing, managing, and implementing modules to enhance user experience and application efficiency.</li>
                                <li>Cloud Platforms: Experience leveraging cloud platforms like Azure to enhance our projects.</li>
                                <li>Multidisciplinary Coordination: Coordination with front-end and mobile teams to ensure a unified user experience.</li>
                                <li>Additional Technologies: Extensive knowledge in technologies such as Docker, Kubernetes, NoSQL databases (including Cosmos and MongoDB), and relational databases like PostgreSQL.</li>
                                <li>DevOps Tools and Practices: Use of tools like SonarQube and DevOps practices to ensure continuous integration and delivery of our applications.</li>
                            </ul>
                        </li>
                        <li>Tools:</li>
                        <li>Spring Boot, Java 8+, Streams, New Relic, git, IntelliJ IDEA (IDE), PostgreSQL, Agile Framework Scrum, Angular, JavaScript, Html5, CSS, CSS3, SCSS, Angular 8+, Postman, NodeJs, TypeScript, Tslint, Unit Test, Gitlab (Repository), Agile Framework Scrum, git, and others.</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/soaint.jfif" alt="SOAINT company logo">
                    </picture>
                    <p>
                        Employment Certificate
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-SOAINT.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-SOAINT.png"
                            alt="SOAINT employment certificate by {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-SOAINT.png">
                            (click to download)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Senior Technical Consultant</p>
                    <p class=""><span class="bold">Company Name: </span>Soaint Peru S.A.C. | SOAINT | Full-time</p>
                    <p class=""><span class="bold">Employment Dates: </span>Dec 2022 – Aug 2023</p>
                    <p class=""><span class="bold">Employment Duration: </span>9 months</p>
                    <p class=""><span class="bold">Location: </span>Lima, Peru</p>
                    <ul>
                        <li>Project with State Promsace</li>
                        <li>
                            <ul>
                                <li>Microservices for Promsace Project</li>
                                <li>Library and Front Project for Promsace Project</li>
                                <li>Creation of StencilJs Library for Promsace Project</li>
                            </ul>
                        </li>
                        <li>Functions:</li>
                        <li>
                            <ul>
                                <li>Development of Stencil library and implementation in Promsace Project (Angular).</li>
                                <li>Development of Angular libraries and implementation in Promsace Project (Angular).</li>
                                <li>Development of NodeJS project for JSON mocking in Promsace Project (Angular).</li>
                                <li>Development of microservices in SpringBoot (srv-config-server, srv-gateway, pgo-parametro-comun, srv-auth-keycloak, and others).</li>
                                <li>Advising on best practices in Angular, TypeScript development and Git guidelines adherence.</li>
                                <li>Advising on project schematization usage.</li>
                                <li>Advising on Microfrontends development with Angular.</li>
                                <li>Advising on libraries development with Angular.</li>
                                <li>Advising on libraries development with StencilJs.</li>
                            </ul>
                        </li>
                        <li>Tools:</li>
                        <li> Spring Boot, Java 8+, Streams, git, IntelliJ IDEA (IDE), PostgreSQL, Agile Framework Scrum, Angular, JavaScript, Html5, CSS, CSS3, SCSS, Angular 8+, Postman, NodeJs, TypeScript, Tslint, Unit Test, Gitlab (Repository), Agile Framework Scrum, git, and others.</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/bcp.jfif" alt="Brand of company BCP">
                    </picture>
                    <p>
                        Job certificate
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BCP.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BCP.png"
                            alt="Job certificate of BCP by {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BCP.png">
                            (click to download)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">FRONTEND WEB SOFTWARE ENGINEER</p>
                    <p class=""><span class="bold">Company name: </span>CREDIT BANK OF PERU S.A. | BCP | Full time</p>
                    <p class=""><span class="bold">Date: </span>agto 2021 – sept 2022
                    </p>
                    <p class=""><span class="bold">Duration: </span>1 year and 1 month</p>
                    <p class=""><span class="bold">Location: </span>Lima, Peru</p>
                    <ul>
                        <li>Table of Transfer Abroad</li>
                        <li>
                            <ul>
                                <li>Micro App of Transfer Abroad</li>
                                <li>Micro App of Transfer Overview Abroad</li>
                                <li>Micro App of Transfer Third and others.</li>
                                <li>Maintainability a Library of Widgets about Transfer Abroad and others.</li>
                            </ul>
                        </li>
                        <li>Functions:</li>
                        <li>
                            <ul>
                                <li>Analysis and debugging of errors for changes in business rules and resolutions of incidents.</li>
                                <li>Analysis of endpoint services with postman to implement in Transfer MicroApps.</li>
                                <li>Manejo de Jira para la gesti&oacute;n de requisitos y casos de prueba. </li>
                                <li>Management of ceremonies in Agile framework (scrum) with teamwork</li>
                                <li>Continuous Deployment of MicroApps in Development, Certification and Productions Environment.</li>
                                <li>Work Culture about DevSecOps software development.</li>
                                <li>Development with FEM Framework applied in the development of BCP Software.</li>
                                <li>Testing of vulnerabilities and best practices in software development using Sonarque and Fortify.</li>
                            </ul>
                        </li>
                        <li>Tools:</li>
                        <li>Angular, JavaScript, Html5, CSS, CSS3, SCSS, Angular 8+, Postman, TypeScript, Tslint, Unit Test, Sonarque, Fortify and others.</li>
                    </ul>
                </div>
            </article>
            <article>
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/vass.jfif" alt="Brand of company VASS">
                    </picture>
                    <p>
                        Job certificate
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-VASS.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-VASS.png"
                            alt="Job certificate VASS by {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-VASS.png">
                            (click to download)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Programmer Analyst</p>
                    <p class=""><span class="bold">Company name: </span>Vass Systems Consulting Peru S.A.C. | Vass Latam | full time</p>
                    <p class=""><span class="bold">Date: </span>feb 2021 – jul 2021
                    </p>
                    <p class=""><span class="bold">Duration: </span>4 months and 23 days
                    </p>
                    <p class=""><span class="bold">Location: </span>Lima, Peru</p>
                    <p>Commercial Offer Table - Advengers is a part of TRAIN STARK in Telefonica.</p>
                    <ul>
                        <li>Functions:</li>
                        <li>
                            <ul>
                                <li>Analysis and debugging of errors for changes in business rules and resolutions of incidents.</li>
                                <li>Analysis of endpoint services with postman to implement in WebConvergente of Telefonica. </li>
                                <li>Analysis of endpoint services with postman to implement in Application of Venta de la Fija of Telefonica. </li>
                                <li>Analysis of endpoint services with postman to implement in Web Admin de Web Convergente of Telefonica.</li>
                                <li>Analysis of projects in Spring Boot that permit to support Web convergente.</li>
                                <li>Created a documentation about database of web convergente – dictionary data </li>
                                <li>Management Jira for requirements and test cases.</li>
                                <li>Management of ceremonies in Agile framework (scrum) with teamwork.</li>
                                <li>Continuous Deployment in Development Environment.</li>
                            </ul>
                        </li>
                        <li>Tools:</li>
                        <li>Android Studio Java - Kotlin, JavaScript, jQuery, Html5, PostgreSQL (Data base), CSS, CSS3, SCSS, Oauth 2.0 Facebook, Google, Angular 8+, AppCenter (deployment of applications), Postman, TypeScript, and others</li>
                    </ul>
                </div>
            </article>
            <article>
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/bigbyte.png" alt="Brand of company BIGBYTE">
                    </picture>
                    <p>
                        Job certificate
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.jfif">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.jfif"
                            alt="Job certificate BIGBYTE by {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.jfif">
                            (click to download)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Development Analyst</p>
                    <p class=""><span class="bold">Company name: </span>BIGBYTE S.A.C. |
                        BIGBYTE PERU | Independent Professional</p>
                    <p class=""><span class="bold">Date: </span>may 2020 – dec 2020
                    </p>
                    <p class=""><span class="bold">Duration: </span>8 months</p>
                    <p class=""><span class="bold">Location: </span>Lima, Peru</p>
                    <p>Maintainability of the modules of the official tourist portal peruvian state peru.travel, maintainability of a minisite of the peruvian state perutherichestcountry.peru.travel and development of two native applications that PISCO IOS and PISCO ANDROID with system administrative.</p>
                    <ul>
                        <li>Functions:</li>
                        <li>
                            <ul>
                                <li>Maintainability of the modules of the official tourist portal peruvian state peru.travel (<a target="_blank"
                                        href="https://www.peru.travel">https://www.peru.travel</a>) – administrator in C#.</li>
                                <li>Maintainability of the minisite of peruvian state perutherichestcountry.peru.travel (
                                    <a target="_blank"
                                        href="https://perutherichestcountry.peru.travel">https://perutherichestcountry.peru.travel</a>
                                    ) – administrator in C#.
                                </li>
                                <li>Development and deployment in PlayStore an application native in android PiscoApp from PromPeru - <a target="_blank"
                                        href="https://play.google.com/store/apps/details?id=com.promperu.pisco">https://play.google.com/store/apps/details?id=com.promperu.pisco</a>
                                </li>
                                <li>Development and deployment in App Store an application native in IOS PiscoApp from PromPeru</li>
                                <li><a target="_blank"
                                        href="https://apps.apple.com/pe/app/pisco/id1539213077">https://apps.apple.com/pe/app/pisco/id1539213077</a>
                                </li>
                            </ul>
                        </li>

                        <li>Tools:</li>
                        <li>iOS - Swift, Android Studio Java, React, JavaScript, jQuery, .Net C#, Html5, SQL Server (Stored Procedures), CSS, Oauth 2.0 Facebook, Google.</li>
                    </ul>
                    <!--<p>
                                                        <a target="_blank" href="https://www.peru.travel/">
                                                            <img src="img/experiencia/perutravel.PNG"/>
                                                        </a>
                                                    </p>-->
                </div>
            </article>
            <article>
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/devinnovaperu.png"
                            alt="Brand of company DevInnova Peru">
                    </picture>
                    <p>
                        Job certificate
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-DEVINNOVAPERU.jfif">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-DEVINNOVAPERU.jfif"
                            alt="Job certificate DEVINNOVA PERU by {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank"
                            href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-DEVINNOVAPERU.jfif">
                            (click to download)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">CEO & FOUNDER - FULL STACK</p>
                    <p class=""><span class="bold">Company name: </span>DevInnova Peru
                        S.A.C | DevInnova Peru | Full time</p>
                    <p class=""><span class="bold">Date: </span>jan 2020 – dec 2020
                    </p>
                    <p class=""><span class="bold">Duration: </span>12 months</p>
                    <p class=""><span class="bold">Location: </span>Lima, Peru</p>
                    <p>Development and deployment of System Snap Store that permit to management any business about entrepreneurs Peruvians.</p>
                    <ul>
                        <li>Functions:</li>
                        <li>
                            <ul>
                                <li>Development and deployment of platform web for Snap Store that permit to management any business about entrepreneurs Peruvian with virtual stores
                                </li>
                            </ul>
                        </li>

                        <li>Tools:</li>
                        <li>jQuery, Framework Laravel - PHP - Angular, MySQL, HTML, CSS, server VPS Bluet Host(<a
                                target="_blank" href="https://www.digitalocean.com/">https://www.digitalocean.com/</a>)
                        </li>
                    </ul>
                </div>
            </article>
            <article>
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/afcasolution.png" alt="Brand of company Afca Solution">
                    </picture>
                    <p>
                        Job certificate
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-AFCASOLUTION.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-AFCASOLUTION.png"
                            alt="Job certificate AFCA SOLUTION by {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-AFCASOLUTION.png">
                            (click to download)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Development and System Analyst</p>
                    <p class=""><span class="bold">Company name: </span>Afca Solution
                        S.AC | Afca Solution | Full time</p>
                    <p class=""><span class="bold">Date: </span>may 2019 – dec 2019
                    </p>
                    <p class=""><span class="bold">Duration: </span>8 months</p>
                    <p class=""><span class="bold">Location: </span>Lima, Peru</p>
                    <p>- Development application Gamarra App for administrative store and publish a store exclusive for Gamarra App</p>
                    <p>- Development application to send different product to people and driver application that manages the products</p>
                    <ul>
                        <li>Functions:</li>
                        <li>
                            <ul>
                                <li>Maintainability of application StiloApp (Ionic) that specializes in manicurists, hair stylists, makeup artists barbers, cosmiatrists, massage therapists, podiatrists, and others. . <a target="_blank"
                                        href="https://play.google.com/store/apps/details?id=io.codigito.mylookexpress&hl=es_419&gl=US">https://play.google.com/store/apps/details?id=io.codigito.mylookexpress&hl=es_419&gl=US</a>
                                </li>
                                <li>Development and deployment of application and system for Gamarra App allow search any store in application for category. Gamarra – Commercial Textile Emporium. ( <a target="_blank"
                                        href="https://gamarraapp.com/">https://gamarraapp.com/</a> )</li>
                                <li>Development and deployment a system to send different product to people for parcels and application to drivers that permit tracking position of product for company Fácil Envío. ( <a
                                        target="_blank"
                                        href="https://www.transporte.facilenvio.com/login">https://www.transporte.facilenvio.com/login</a>
                                    )</li>
                            </ul>
                        </li>

                        <li>Tools:</li>
                        <li>IONIC 4 – Angular, Android Studio (Java), JavaScript, TypeScript, Html5, SCSS, Laravel – PHP, MySQL, server cloud Digital Ocea (<a target="_blank"
                                href="https://www.digitalocean.com/">https://www.digitalocean.com/</a> ) for deployment, NodeJS – Sequelize ORM, Angular.</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <picture>
                    <img src="{{ Utils::getUrlBase() }}/img/jerssonarrivasplata.png" width="100px"
                        alt="Logo {{ GlobalClass::getCreatorUser() }}">
                </picture>
                <div>
                    <p class="h3 bold">Freelance</p>
                    <p class=""><span class="bold">Company name: </span>Freelance
                        Independent Professional</p>
                    <p class=""><span class="bold">Date: </span>sept 2018 – dec 2018
                    </p>
                    <p class=""><span class="bold">Duration: </span>4 months</p>
                    <p class=""><span class="bold">Location: </span>Lima, Peru</p>
                    {{-- <p class=""><span class="bold">Location: </span>Jir&oacute;n Zorritos 1134 -
                        Cercado de Lima</p> --}}
                    <p>Development a system sale administrative about rental of products for event and a migration system of National Institute Pedagogical of Monterrico.</p>
                    <ul>
                        <li>Functions:</li>
                        <li>
                            <ul>
                                <li>Development a system of sales that administrative any products for events and rental of products (Front-end and Back-end).</li>
                                <li>Migration system of National Institute Pedagogical of Monterrico in a server for low performance to another server to better performance. (System to development in visual studio 2010 and used Crystal Report for reports)</li>
                            </ul>
                        </li>

                        <li>Tools:</li>
                        <li>Jquery, PHP,Mysql, Html5 y SCSS, Server Management Bluehost (<a target="_blank"
                                href="https://www.bluehost.com/">https://www.bluehost.com/</a>).</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <picture>
                    <img src="{{ Utils::getUrlBase() }}/img/lamanoamiga.png"
                        alt="Brand of company Conciliation Center 'La mano amiga'">
                </picture>
                <div>
                    <p class="h3 bold">Systems Analyst</p>
                    <p class=""><span class="bold">Company name: </span>Conciliation Center "La mano amiga" | Independent Professional</p>
                    <p class=""><span class="bold">Date: </span>dec 2017 – 2018</p>
                    <p class=""><span class="bold">Duration: </span>1 year</p>
                    <p class=""><span class="bold">Location: </span>Lima, Peru</p>
                    <p>Development of administrative web site in modules about reduce decreases in center of conciliation.</p>
                    <ul>
                        <li>Functions:</li>
                        <li>
                            <ul>
                                <li>Development of administrative web site in modules about reduce decreases in center of conciliation.</li>
                                <li>Development and deployment a site web for lawyer and conciliation Ketty Rojas Mayta - https://www.kettyrojas.pe/</li>
                            </ul>
                        </li>

                        <li>Tools:</li>
                        <li>Jquery, Laravel – PHP, Mysql, Html5, SCSS, API.</li>
                    </ul>
                </div>
            </article>
            <article class="main-article-index">
                <picture>
                    <img src="{{ Utils::getUrlBase() }}/img/jerssonarrivasplata.png" width="100px"
                        alt="Logo {{ GlobalClass::getCreatorUser() }}">
                </picture>
                <div>
                    <p class="h3 bold">Freelance and Developer in Excel – Visual Basic</p>
                    <p class=""><span class="bold">Company name: </span>Freelance
                        Independent Professional</p>
                    <p class=""><span class="bold">Date: </span>may 2017 – nov 2017
                    </p>
                    <p class=""><span class="bold">Duration: </span>7 months</p>
                    <p class=""><span class="bold">Location: </span>Av. Espa&ntilde;a 1505 - Cercado
                        de Lima</p>
                    <p>Development and Implementation of food system for production company in exportation chicken, exportation porkies and exportation livestock.</p>
                    <ul>
                        <li>Tools:</li>
                        <li>Excel and Visual Basic</li>
                    </ul>
                </div>
            </article>
            <article>
                <div>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/bigbyte.png" alt="Brand of company BIGBYTE PERU">
                    </picture>
                    <p>
                        Job certificate
                    </p>
                    <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.png">
                        <img src="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.png"
                            alt="Job certificate BIGBYTE by {{ GlobalClass::getCreatorUser() }}">
                    </a>
                    <p>
                        <a target="_blank" href="{{ Utils::getUrlBase() }}/img/experiencia/CERTIFICADO-BIGBYTE.png">
                            (click to download)
                        </a>
                    </p>
                </div>
                <div>
                    <p class="h3 bold">Junior Systems Analyst</p>
                    <p class=""><span class="bold">Company name: </span>BIGBYTE S.A.C. | BIGBYTE PERU | Full time
                    </p>
                    <p class=""><span class="bold">Date: </span>jul 2016 – apr 2017
                    </p>
                    <p class=""><span class="bold">Duration: </span>10 months</p>
                    <p class=""><span class="bold">Location: </span>Lima 41 San Borja, Lima,
                        Peru</p>
                    <p>- Development a management system for CARMELO company transport and maintainability of modules in tourist portal peruvian VIBRA TOURS.</p>
                    <p>- Development a platform of education for The Pontifical Catholic University (PUCP) la FAKU.</p>
                    <ul>
                        <li>Functions:</li>
                        <li>
                            <ul>

                                <li>Development a management system for transport company CARMELO – administrative in C#.</li>
                                <li>Maintainability of modules in tourist portal of state peruvian VIBRA TOURS – administrative in C#.</li>
                                <li>Development a platform of education for The Pontifical Catholic University (PUCP) la FAKU.</li>
                            </ul>
                        </li>

                        <li>Tools:</li>
                        <li>JavaScript, jQuery, .Net C#, Html5, SQL Server (Stored Procedures), CSS, Server Testing in
                            Smarter Asp ( <a target="_blank"
                                href="https://www.smarterasp.net/index">https://www.smarterasp.net/index</a> ).</li>
                    </ul>
                </div>
            </article>
        </section>
    </main>


@stop
