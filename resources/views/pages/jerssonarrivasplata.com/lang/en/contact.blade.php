@extends(GlobalClass::getDomainFolder(0).'::base.index')
@section('metatags')
    @parent
    <!-- some master css here -->
    <title>Contact to {{ GlobalClass::getCreatorUser() }} | Software Developer</title>
    <meta property="og:type" content="article">

    <meta name="description" content="Page contact by {{ GlobalClass::getCreatorUser() }} -
    Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">

    <meta property="og:description"
    content="Page contact by {{ GlobalClass::getCreatorUser() }} -
    Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">
    <meta property="og:title" content="Contactar a {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta property="og:url" content="{{ Utils::getUrlBase() }}/contacto">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="{{ Utils::getUrlBase() }}/contacto">
    <meta name="twitter:title" content="Contactar a {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta name="twitter:description"
   content="Page contact by {{ GlobalClass::getCreatorUser() }} -
   Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
   transport sectos, bank, education, touristm and others.">

@stop
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase() }}/css/index-contact.css?v=<?php echo rand(); ?>" rel="stylesheet">

    {{-- <script async src="https://pay.google.com/gp/p/js/pay.js" onload="onGooglePaylLoaded()"></script>
    <script type="text/javascript">
        function onGooglePaylLoaded() {
            const googlePayClient = new google.payments.api.PaymentsClient({
                environment: 'TEXT' //'PRODUCTION'
            });

            const clientConfiguration = {
                apiVersion: 2,
                apiVersionMinor: 0,
                allowedPaymentMethods: [cardPaymentMethod]
            };

            googlePayClient.isReadyToPay(clientConfiguration)
                .then(function(response) {
                    if (response.result) {
                        // add a Google Pay button
                        googlePayClient.createButton({
                            // defaults to black if default or omitted
                            buttonColor: 'default',
                            // defaults to long if omitted
                            buttonType: 'long',
                            onClick: onGooglePaymentsButtonClicked
                        });

                    }
                }).catch(function(err) {
                    //oCULTAR BOTON DE GOOGLE PAT
                });

        }
    </script> --}}
@stop

@section('content')
    @parent
    <!-- Main content goes here -->
    <main>
        @include(GlobalClass::getDomainFolder(0) . '::lang.'.request()->route()->parameter('language').'.common.header-menu')
        <section>
            <p>CONTACT ME</p>
            <ul class="pl-2">
                <li>Linkedin: <a target="_blank"
                        href="{{ config('base.social_networks.linkedin') }}">{{ Str::after(config('base.social_networks.linkedin'), config('base.others.https')) }}</a>
                </li>
                <li>Facebook: <a target="_blank"
                        href="{{ config('base.social_networks.facebook') }}">{{ Str::after(config('base.social_networks.facebook'), config('base.others.https')) }}</a>
                </li>
                <li>Instagram: <a target="_blank"
                        href="{{ config('base.social_networks.instagram') }}">{{ Str::after(config('base.social_networks.instagram'), config('base.others.https')) }}</a>
                </li>
                <li>WebSite: <a target="_blank"
                        href="{{ config('base.website') }}">{{ Str::after(config('base.website'), config('base.others.https')) }}</a>
                </li>
                <li>Celphone number: <a target="_blank"
                        href="tel:({{ config('base.info.celphone.code') }}){{ config('base.info.celphone.number') }}">+{{ config('base.info.celphone.code') }} {{ config('base.info.celphone.number') }}</a></li>
                <li>Email: <a target="_blank" href="mailto:{{ config('base.info.email') }}">{{ config('base.info.email') }}</a></li>
                {{--<li>Direcci&oacute;n: {{ config('base.info.address') }}</li>--}}
            </ul>
            <form method="POST" action="{{ route('contact.message', ['language' => request()->route()->parameter('language')]) }}">
                @csrf
                <h1>SEND MESSAGE</h1>

                @if (Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        {!! session('success') !!}
                    </div>
                @endif

                <div>
                    <label>Enter your name</label>
                    <input name="name" type="text" maxlength="100" placeholder="Enter your name" required />
                </div>
                <div>
                    <label>Enter your celphone</label>
                    <input name="celphone" type="number" maxlength="50" placeholder="Enter your celphone" required />
                </div>
                <div>
                    <label>Enter your email</label>
                    <input name="email" type="email" maxlength="190" placeholder="Enter your email" required />
                </div>
                <div>
                    <label>Enter your message</label>
                    <textarea name="message" maxlength="1000000" placeholder="Enter your message" required></textarea>
                </div>
                <div>
                    <input type="submit" value="Send" />
                </div>
            </form>
        </section>
    </main>


@stop
