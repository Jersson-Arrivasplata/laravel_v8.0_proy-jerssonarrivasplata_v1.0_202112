@extends(GlobalClass::getDomainFolder(0) . '::base.index')
@section('metatags')
    @parent
    <!-- some master css here -->
    <title>Education of {{ GlobalClass::getCreatorUser() }} | Software Developer</title>


    <meta property="og:type" content="article">

    <meta name="description"
        content="Page education by {{ GlobalClass::getCreatorUser() }} -
        Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">

    <meta property="og:description"
        content="Page education by {{ GlobalClass::getCreatorUser() }} -
        Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">
    <meta property="og:title" content="Educaci&oacute;n de {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta property="og:url" content="{{ Utils::getUrlBase() }}/educacion">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="{{ Utils::getUrlBase() }}/educacion">
    <meta name="twitter:title" content="Educaci&oacute;n de {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta name="twitter:description"
        content="Page education by {{ GlobalClass::getCreatorUser() }} -
        Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">
@stop
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase() }}/css/index-education.css?v=<?php echo rand(); ?>" rel="stylesheet">
@stop

@section('content')
    @parent
    <!-- Main content goes here -->
    <main>
        @include(GlobalClass::getDomainFolder(0) .
                '::lang.' .
                request()->route()->parameter('language') .
                '.common.header-menu')

        <section>
            <p>My University Education</p>
            <ul>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/utp.jfif" alt="Brand of UTP">
                    </picture>
                    <div>
                        <p class="h3 bold">Technological University of Peru</p>
                        <p class=""><span class="bold">Degree name: </span>Bachelor's Degree in Software Engineering
                        </p>
                        <p class=""><span class="bold">Graduation Date: </span>2015 -
                            2021</p>
                        <p class=" pt-4" style="margin-top: 20PX;">
                            <a target="_blank"
                                href="{{ Utils::getUrlBase() }}/img/educacion/BACHILLER-INGENIERIA-SOFTWARE.jpg">
                                <img style="MAX-WIDTH: 145px;"
                                    src="{{ Utils::getUrlBase() }}/img/educacion/BACHILLER-INGENIERIA-SOFTWARE.jpg"
                                    alt="Bachelor's Degree in Software Engineering OF  JERSSON ARRIVASPLATA ROJAS">
                            </a>
                        </p>
                    </div>
                </li>
                <!--<li>
                            <picture>
                                <img src="{{ Utils::getUrlBase() }}/img/educacion/utp.jfif" alt="Brand of UTP">
                            </picture>
                            <div>
                                <p class="h3 bold">Universidad Tecnol&oacute;gica del Per&uacute;</p>
                                <p class=""><span class="bold">Degree name: </span>Estudiante de
                                    5to ciclo | Ingenier&iacute;a Mecatr&oacute;nica</p>
                                <p class=""><span class="bold">Graduation Date: </span>2022 -
                                    2025</p>
                            </div>
                        </li>-->
            </ul>
        </section>
        <section>
            <p>My Language Educations</p>
            <ul>

                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/britanico.png"
                            alt="Brand of British Peruvian Cultural Association (British)">
                    </picture>
                    <div>
                        <p class="h3 bold">British Peruvian Cultural Association (British)</p>
                        <p class=""><span class="bold">Degree name: </span>Basic Constancy | Academic discipline |
                            Teaching English as a Foreign Language</p>
                        <p class=""><span class="bold">Graduation Date: </span>2014 –
                            2015</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/CONSTANCIA-DE-ESTUDIOS-DE-INGLES-BASICO-BRITANICO.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/CONSTANCIA-DE-ESTUDIOS-DE-INGLES-BASICO-BRITANICO.jfif"
                                        alt="British Peruvian Cultural Association (British) - Proof of Basic Phase culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </section>
        <section>
            <p>Conferences and Merits</p>
            <ul>

                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/microsoft.jfif"
                            alt="Brand of DEVDAYS LATAM 2016 - MICROSOFT">
                    </picture>
                    <div>
                        <p class="h3 bold">DEVDAYS LATAM 2016 - MICROSOFT | MICROSOFT</p>
                        <p class=""><span class="bold">Degree name: </span>Attendance DEVDAYS LATAM 2016 - MICROSOFT
                        </p>
                        <p class=""><span class="bold">Date: </span>2016</p>
                       <!-- <p>Constancia de asistencia a la conferencia que se realizo en Per&uacute; de microsoft con el top
                            de las
                            ultimas tendencias que llegar&iacute;an al mercado mundial en desarrollo, testing, despliegue,
                            monitoreo y ultimas tendencias de IA.</p>-->
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <ul>
                                <li>
                                    <a target="_blank"
                                        href="{{ Utils::getUrlBase() }}/img/educacion/CONSTANCIA-ASISTENCIA-DEVDAYS-MICROSOFT-2016.PNG">
                                        <img src="{{ Utils::getUrlBase() }}/img/educacion/CONSTANCIA-ASISTENCIA-DEVDAYS-MICROSOFT-2016.PNG"
                                            alt="Constance of attendance DEVDAYS LATAM 2016 - MICROSOFT culminated by {{ GlobalClass::getCreatorUser() }}">
                                    </a>
                                </li>
                            </ul>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/URP.jpg" alt="Logo del URP">
                    </picture>
                    <div>
                        <p class="h3 bold">DIPLOMA DE HONOR UNIVERSIDAD RICARDO PALMA | URP</p>
                        <p class=""><span class="bold">Degree name: </span>Easy Projects Sales</p>
                        <p class=""><span class="bold">Date: </span>2015</p>
                       <!-- <p>Diploma de honor de concurso de proyectos de sistema en la Universidad Ricardo Palma con el
                            "Proyecto Easy Ventas".</p>-->
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <ul>
                                <li>
                                    <a target="_blank"
                                        href="{{ Utils::getUrlBase() }}/img/educacion/DIPLOMA-DE-HONOR-URP.png">
                                        <img src="{{ Utils::getUrlBase() }}/img/educacion/DIPLOMA-DE-HONOR-URP.png"
                                            alt="Diploma de honor de concurso de proyectos de sistema en la Universidad Ricardo Palma con el Proyecto Easy Ventas culminated by {{ GlobalClass::getCreatorUser() }}">
                                    </a>
                                </li>
                            </ul>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/URP.jpg" alt="Logo del URP">
                    </picture>
                    <div>
                        <p class="h3 bold">HACKATHON UTP - COMERCIO | HACKATHON</p>
                        <p class=""><span class="bold">Degree name: </span>Constancy participation by Hackathon UTP
                            - Comercio</p>
                        <p class=""><span class="bold">Date: </span>2021</p>
                        <!--<p>Constancia de participaci&oacute;n en la Hackathon UTP - Comercio realizada del 5 al 9 de
                            noviembre del 2021.</p>-->
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <ul>
                                <li>
                                    <a target="_blank"
                                        href="{{ Utils::getUrlBase() }}/img/educacion/HACKATON-UTP-COMERCIO-2021.jpg">
                                        <img src="{{ Utils::getUrlBase() }}/img/educacion/HACKATON-UTP-COMERCIO-2021.jpg"
                                            alt="Constancia de participaci&oacute;n en la Hackathon UTP - El comercio realizada del 5 al 9 de noviembre del 2021">
                                    </a>
                                </li>
                            </ul>
                        </ul>
                    </div>
                </li>
            </ul>
        </section>

        <section>
            <p>My Graduates</p>
            <ul>

                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/ccl.jfif"
                            alt="Logo del Centro de Capacitaci&oacute;n Empresarial | C&aacute;mara de Comercio de Lima | CCL">
                    </picture>
                    <div>
                        <p class="h3 bold">Business Training Center | Lima Camera of Commerce | CCL</p>
                        <p class=""><span class="bold">Degree name: </span>DIPLOMA IN STRATEGIC DIGITAL MARKETING</p>
                        <p class=""><span class="bold">Graduation Date: </span>2017 –
                            2017</p>
                        <!--<p>Me permiti&oacute; obtener conocimientos en la planificaci&oacute;n, gesti&oacute;n e
                            implementaci&oacute;n de estrategias y
                            acciones de marketing digital orientadas a generar posicionamiento, reconocimiento de marca.
                            Tambi&eacute;n obtuve conocimientos para promocionar e impulsar campa&ntilde;as de
                            comunicaci&oacute;n en el mundo
                            virtual, a fin de llegar de modo directo y no invasivo a sus potenciales consumidores.</p>-->
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <ul>
                                <li>
                                    <a target="_blank"
                                        href="{{ Utils::getUrlBase() }}/img/educacion/CCL-MARKETING-DIGITAL-ESTRATEGICO-NOTAS.jfif">
                                        <img src="{{ Utils::getUrlBase() }}/img/educacion/CCL-MARKETING-DIGITAL-ESTRATEGICO-NOTAS.jfif"
                                            alt="Certificate of CCL MARKETING DIGITAL ESTRATEGICO NOTAS culminated by {{ GlobalClass::getCreatorUser() }}">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank"
                                        href="{{ Utils::getUrlBase() }}/img/educacion/CCL-MARKETING-DIGITAL-ESTRATEGICO-DIPLOMADO.jfif">
                                        <img src="{{ Utils::getUrlBase() }}/img/educacion/CCL-MARKETING-DIGITAL-ESTRATEGICO-DIPLOMADO.jfif"
                                            alt="Certificate of CCL DIPLOMADO EN MARKETING DIGITAL ESTRAT&Eacute;GICO culminated by {{ GlobalClass::getCreatorUser() }}">
                                    </a>
                                </li>
                            </ul>
                        </ul>
                    </div>
                </li>
            </ul>
        </section>
        <section>
            <p>My Certificates of Studies</p>
            <ul>
                <li>
                    <picture>
                        <img style="max-width: 120px;" src="{{ Utils::getUrlBase() }}/img/educacion/logo-udemy.svg"
                            alt="Logo de Udemy | Udemy Bussiness">
                    </picture>
                    <div>
                        <p class="h3 bold">Udemy | ReactiveX</p>
                        <p class=""><span class="bold">Degree name: </span>REACTIVEX -
                            RXJS: FROM ZERO TO THE DETAILS</p>
                        <p class=""><span class="bold">Graduation Date: </span>2022 –
                            2022</p>
                        <ul>
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/REACTIVEX-RXJS-DE-CERO-HASTA-LOS-DETALLES.jpg">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/REACTIVEX-RXJS-DE-CERO-HASTA-LOS-DETALLES.jpg"
                                        alt="El anterior certificado garantiza que {{ GlobalClass::getCreatorUser() }} ha completado con &eacute;xito el curso ReactiveX - RxJs: De cero hasta los detalles a fecha de 19/02/2022, habiendo sido impartido por Fernando Herrera en Udemy. El certificado indica que se ha completado la totalidad del curso, seg&uacute;n lo validado por el estudiante. La duraci&oacute;n del curso representa el total de horas de v&iacute;deo del curso en el momento de finalizaci&oacute;n m&aacute;s reciente.">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img style="max-width: 120px;" src="{{ Utils::getUrlBase() }}/img/educacion/logo-udemy.svg"
                            alt="Logo de Udemy | Udemy Bussiness">
                    </picture>
                    <div>
                        <p class="h3 bold">Udemy | JavaScript - ECMAScript 6</p>
                        <p class=""><span class="bold">Degree name: </span>JAVASCRIPT: ECMASCRIPT 6 AND ALL
                            DETAILS</p>
                        <p class=""><span class="bold">Graduation Date: </span>2022 –
                            2022</p>
                        <ul>
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/JAVASCRIPT-ECMASCRIPT6-TODOS-SUS-DETALLES.jpg">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/JAVASCRIPT-ECMASCRIPT6-TODOS-SUS-DETALLES.jpg"
                                        alt="El anterior certificado garantiza que {{ GlobalClass::getCreatorUser() }} ha completado con &eacute;xito el curso JavaScript: ECMAScript 6 y todos sus detalles a fecha de 16/02/2022, habiendo sido impartido por Fernando Herrera en Udemy. El certificado indica que se ha completado la totalidad del curso, seg&uacute;n lo validado por el estudiante. La duraci&oacute;n del curso representa el total de horas de v&iacute;deo del curso en el momento de finalizaci&oacute;n m&aacute;s reciente.">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img style="max-width: 120px;" src="{{ Utils::getUrlBase() }}/img/educacion/logo-udemy.svg"
                            alt="Logo de Udemy | Udemy Bussiness">
                    </picture>
                    <div>
                        <p class="h3 bold">Udemy | Angular</p>
                        <p class=""><span class="bold">Degree name: </span>ANGULAR WITH DEVOPS, TDD, UNIT TESTS,
                            PIPELINES, AZURE</p>
                        <p class=""><span class="bold">Graduation Date: </span>2022 –
                            2022</p>
                        <ul>
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/ANGULAR-CON-DEVOPS-TDD-PRUEBAS-UNITARIAS-PIPELINES-AZURE.jpg">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/ANGULAR-CON-DEVOPS-TDD-PRUEBAS-UNITARIAS-PIPELINES-AZURE.jpg"
                                        alt="El anterior certificado garantiza que {{ GlobalClass::getCreatorUser() }} ha completado con &eacute;xito el curso Angular con DevOps, TDD, Pruebas Unitarias, Pipelines, Azure a fecha de 15/02/2022, habiendo sido impartido por Jhonatan Plata en Udemy. El certificado indica que se ha completado la totalidad del curso, seg&uacute;n lo validado por el estudiante. La duraci&oacute;n del curso representa el total de horas de v&iacute;deo del curso en el momento de finalizaci&oacute;n m&aacute;s reciente.">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img style="max-width: 120px;" src="{{ Utils::getUrlBase() }}/img/educacion/logo-udemy.svg"
                            alt="Logo de Udemy | Udemy Bussiness">
                    </picture>
                    <div>
                        <p class="h3 bold">Udemy | Angular</p>
                        <p class=""><span class="bold">Degree name: </span>ANGULAR: UNIT TESTS WITH JASMINE AND
                            KARMA</p>
                        <p class=""><span class="bold">Graduation Date: </span>2022 –
                            2022</p>
                        <ul>
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/ANGULAR-PRUEBAS-UNITARIAS-JASMINE-KARMA.jpg">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/ANGULAR-PRUEBAS-UNITARIAS-JASMINE-KARMA.jpg"
                                        alt="El anterior certificado garantiza que {{ GlobalClass::getCreatorUser() }} ha completado con &eacute;xito el curso Angular: Pruebas unitarias con Jasmine y Karma a fecha de 15/02/2022, habiendo sido impartido por Efisio Pitzalis Morillas en Udemy. El certificado indica que se ha completado la totalidad del curso, seg&uacute;n lo validado por el estudiante. La duraci&oacute;n del curso representa el total de horas de v&iacute;deo del curso en el momento de finalizaci&oacute;n m&aacute;s reciente.">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img style="max-width: 120px;" src="{{ Utils::getUrlBase() }}/img/educacion/logo-udemy.svg"
                            alt="Logo de Udemy | Udemy Bussiness">
                    </picture>
                    <div>
                        <p class="h3 bold">Udemy | Java</p>
                        <p class=""><span class="bold">Degree name: </span>FUNCTIONAL PROGRAMMING IN JAVA WITH LAMBDAS AND STREAMS</p>
                        <p class=""><span class="bold">Graduation Date: </span>2022 –
                            2022</p>
                        <ul>
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/PROGRAMACION-FUNCIONAL-JAVA-LAMBDAS-STREAMS.jpg">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/PROGRAMACION-FUNCIONAL-JAVA-LAMBDAS-STREAMS.jpg"
                                        alt="Garantiza que {{ GlobalClass::getCreatorUser() }} ha completado con &eacute;xito el curso Programaci&oacute;n funcional en Java con Lambdas y Streams a fecha de 07/02/2022, habiendo sido impartido por Domingo Sebastian en Udemy. El certificado indica que se ha completado la totalidad del curso, seg&uacute;n lo validado por el estudiante. La duraci&oacute;n del curso representa el total de horas de v&iacute;deo del curso en el momento de finalizaci&oacute;n m&aacute;s reciente.">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/coursera.png"
                            alt="Logo de Coursera | Google Cloud">
                    </picture>
                    <div>
                        <p class="h3 bold">Coursera | Google Cloud</p>
                        <p class=""><span class="bold">Degree name: </span>GOOGLE CLOUD PLATFORM BASICS: CORE INFRASTRUCTURE</p>
                        <p class=""><span class="bold">Graduation Date: </span>2020 –
                            2020</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/GOOGLE-CLOUD-PLATFORM-FUNDAMENTOS-CORE-INFRAESTRUCTURA.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/GOOGLE-CLOUD-PLATFORM-FUNDAMENTOS-CORE-INFRAESTRUCTURA.jfif"
                                        alt="Certificate of GOOGLE CLOUD PLATFORM BASICS: CORE INFRASTRUCTURE culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/coursera.png"
                            alt="Logo de Coursera | Google Cloud">
                    </picture>
                    <div>
                        <p class="h3 bold">Coursera | Google Cloud</p>
                        <p class=""><span class="bold">Degree name:
                            </span>CLOUD INFRASTRUCTURE AND RELIABILITY: DESIGN AND PROCESSES</p>
                        <p class=""><span class="bold">Graduation Date: </span>2020 –
                            2020</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/INFRAESTRUCTUR-CLOUD-Y-FIABILIDAD-DISENO-Y-PROCESOS.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/INFRAESTRUCTUR-CLOUD-Y-FIABILIDAD-DISENO-Y-PROCESOS.jfif"
                                        alt="Certificate of CLOUD INFRASTRUCTURE AND RELIABILITY: DESIGN AND PROCESSES culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/coursera.png"
                            alt="Logo de Coursera | Google Cloud">
                    </picture>
                    <div>
                        <p class="h3 bold">Coursera | Google Cloud</p>
                        <p class=""><span class="bold">Degree name: </span>CLOUD INFRASTRUCTURE FUNDAMENTALS: CORE SERVICES</p>
                        <p class=""><span class="bold">Graduation Date: </span>2020 –
                            2020</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/FUNDAMENTOS-DE-LA-INFRAESTRUCTURA-CLOUD-CORE-SERVICES.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/FUNDAMENTOS-DE-LA-INFRAESTRUCTURA-CLOUD-CORE-SERVICES.jfif"
                                        alt="Certificate of CLOUD INFRASTRUCTURE FUNDAMENTALS: CORE SERVICES culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name:
                            </span>WORKSHOP:HTML5 - BOOTSTRAP</p>
                        <p class=""><span class="bold">Graduation Date: </span>2017 –
                            2017</p>
                       <!--<p>El curso me permiti&oacute; conocer el c&oacute;digo HTML del que est&aacute;n hechas las
                            p&aacute;ginas web, el c&oacute;digo CSS
                            para aplicar el formato que se desea, as&iacute; como, aplicar editores de c&oacute;digo que
                            permiten un mejor
                            control y administraci&oacute;n.</p>-->
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/WORKSHOP-HTML5-BOOTSTRAP.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/WORKSHOP-HTML5-BOOTSTRAP.jfif"
                                        alt="Certificate of WORKSHOP:HTML5 - BOOTSTRAP culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name: </span>MS. WINDOWS 2012 SERVER -SUPPORT AND CONFIGURATION</p>
                        <p class=""><span class="bold">Graduation Date: </span>2017 –
                            2017</p>
                        <!--<p>El curso me proporciono las t&eacute;cnicas y conocimientos necesarios para poder implementar
                            servicios
                            de red en una infraestructura de redes de dominio; basada en Windows Server 2012. Asimismo, los
                            conocimientos necesarios para poder instalarlos, configurarlos y administrarlos.</p>-->
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/MS.-WINDOWS-2012-SERVER-SOPORTE-Y-CONFIGURACION.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/MS.-WINDOWS-2012-SERVER-SOPORTE-Y-CONFIGURACION.jfif"
                                        alt="Certificate of MS. WINDOWS 2012 SERVER -SUPPORT AND CONFIGURATION culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name: </span>MS.SQL SERVER 2012 - ADMINISTRATION</p>
                        <p class=""><span class="bold">Graduation Date: </span>2017 –
                            2017</p>
                        <!--<p>El curso me permiti&oacute; aprender a establecer los criterios para instalar un servidor de base
                            de
                            datos SQL SERVER, administrar archivos de una base de datos SQL Server, crear una base de datos,
                            dise&ntilde;ar e implementar la integridad de datos, establecer formas de respaldo a la Base de
                            datos,
                            realizar la distribuci&oacute;n de datos a trav&eacute;s de la replicaci&oacute;n e implementar
                            alertas y
                            programaci&oacute;n de tareas autom&aacute;ticas.</p>-->
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/MS.-WINDOWS-2021-SERVER-ADMINISTRACION.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/MS.-WINDOWS-2021-SERVER-ADMINISTRACION.jfif"
                                        alt="Certificate of MS.SQL SERVER 2012 - ADMINISTRATION culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name: </span>DEVELOPMENT OF ASP.NET AJAX MVC WEB SOLUTIONS WITH C#</p>
                        <p class=""><span class="bold">Graduation Date: </span>2017 –
                            2017</p>
                       <!-- <p>El curso me proporciono los conocimientos de implementaci&oacute;n de un sistema web en C#
                            aplicando el
                            patr&oacute;n MVC con WebForms y formularios de mantenimiento. As&iacute; como, realizar
                            consultas a base de
                            datos SQL SEVER.</p>-->
                        <br />
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/DESARROLLO-DE-SOLUCIONES-WEB-ASP.NET-AJAX-MVC-CON-C.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/DESARROLLO-DE-SOLUCIONES-WEB-ASP.NET-AJAX-MVC-CON-C.jfif"
                                        alt="Certificate of DEVELOPMENT OF ASP.NET AJAX MVC WEB SOLUTIONS WITH C# culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name:
                            </span>EXCEL SPECIALIST</p>
                        <p class=""><span class="bold">Graduation Date: </span>2017
                            –
                            2017</p>
                       <!-- <p>En esta &aacute;rea de especializaci&oacute;n obtuve los conocimientos y t&eacute;cnicas para
                            planear y controlar las
                            actividades de los procesos de obtenci&oacute;n de la informaci&oacute;n como recurso
                            fundamental para mejorar
                            la toma de decisiones.</p>
                        <p>
                            Comprende los M&oacute;dulos: MS.EXCEL B&Aacute;SICO 2016 | MS.EXCEL INTERMEDIO 2016 | MS.EXCEL
                            AVANZADO 2016
                            | TABLAS DIN&Aacute;MICAS Y GR&Aacute;FICOS CON EXCEL 2016
                        </p>-->
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/ESPECIALISTA-EN-EXCEL.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/ESPECIALISTA-EN-EXCEL.jfif"
                                        alt="Certificate of EXCEL SPECIALIST culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name: </span>WORSHOP: BASIC - ETHICAL HACKING</p>
                        <p class=""><span class="bold">Graduation Date: </span>2016
                            –
                            2016</p>
                        <!--<p>El curso me permiti&oacute; aprender a conocer LAS 05 fases de la metodolog&iacute;a de Ethical
                            Hacking: <BR />
                            1ER RECONOCIMIENTO | 2DO ESCANEO DE PUERTOS Y VULNERABILIDADES | 3ER GANANDO ACCESO | 4TO
                            MANTENIENDO ACCESO | 5TO BORRANDO HUELLAS
                        </p>-->
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/WORSHOP-ETHICAL-HACKING-BASICO.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/WORSHOP-ETHICAL-HACKING-BASICO.jfif"
                                        alt="Certificate of WORSHOP: BASIC - ETHICAL HACKING culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name: </span>MS.SQL SERVER 2012 - ADMINISTRATION</p>
                        <p class=""><span class="bold">Graduation Date: </span>2015
                            –
                            2016</p>
                       <!-- <p>El curso me permiti&oacute; establecer los criterios para instalar un servidor de base de datos
                            SQL
                            SERVER, administrar archivos de una base de datos SQL Server, crear una base de datos,
                            dise&ntilde;ar e
                            implementar la integridad de datos, establecer formas de respaldo a la Base de datos, realizar
                            la distribuci&oacute;n de datos a trav&eacute;s de la replicaci&oacute;n e implementar alertas y
                            programaci&oacute;n de
                            tareas autom&aacute;ticas.</p>-->
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/MS.SQL-SERVER-2012-ADMINISTRACION.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/MS.SQL-SERVER-2012-ADMINISTRACION.jfif"
                                        alt="Certificate of MS.SQL SERVER 2012 - ADMINISTRATION culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/universidad-complutence-de-madrid.png"
                            alt="Brand of Universidad Complutense de Madrid">
                    </picture>
                    <div>
                        <p class="h3 bold">Universidad Complutense de Madrid | Google Act&iacute;vate</p>
                        <p class=""><span class="bold">Degree name: </span>MOBILE APPS PROGRAMMING COURSE</p>
                        <p class=""><span class="bold">Graduation Date: </span>2015
                            –
                            2015</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/CURSO-DE-PROGRAMACION-DE-APPS-MOVILES.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/CURSO-DE-PROGRAMACION-DE-APPS-MOVILES.jfif"
                                        alt="Certificate of MOBILE APPS PROGRAMMING COURSE culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name: </span>MS.SQL SERVER 2021 - IMPLEMENTATION</p>
                        <p class=""><span class="bold">Graduation Date: </span>2015
                            –
                            2015</p>
                        <!--<p>El curso me permiti&oacute; aprender las herramientas integradas de SQL Server 2012 para la
                            implementaci&oacute;n de bases de datos relacionales, as&iacute; como su integraci&oacute;n con
                            la plataforma .NET
                            para el manejo de procedimientos almacenados. Adem&aacute;s, como reconocer las t&eacute;cnicas
                            recomendadas
                            para la implementaci&oacute;n de bases de datos, los objetos y las diferentes sentencias que
                            ofrece SQL
                            2012 con respecto a versiones anteriores.</p>-->
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/MS.SQL-SERVER-2021-IMPLEMENTACION.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/MS.SQL-SERVER-2021-IMPLEMENTACION.jfif"
                                        alt="Certificate of MS.SQL SERVER 2021 - IMPLEMENTATION culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name: </span>JAVA
                            DEVELOPER</p>
                        <p class=""><span class="bold">Graduation Date: </span>2015
                            – 2015</p>
                        <!--<p>El curso me permiti&oacute; comprender los fundamentos del lenguaje de programaci&oacute;n Java.
                            Desarrolle
                            aplicaciones de escritorio con conexi&oacute;n a base de dato ORACLE y creaci&oacute;n de
                            reportes.
                            Por ultimo implementar un sistema web utilizando tecnolog&iacute;as como JQuery, Ajax y
                            Servlets.</p>
                        <p>
                            Comprende los M&oacute;dulos: JAVA FUNDAMENTOS | JAVA CLIENTE/SERVIDOR | JAVA APLICACIONES WEB
                        </p>-->
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank" href="{{ Utils::getUrlBase() }}/img/educacion/JAVA-DEVELOPER.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/JAVA-DEVELOPER.jfif"
                                        alt="Certificate of JAVA DEVELOPER culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/eoi.jfif"
                            alt="Brand of School of Industrial Organization | Google Activate">
                    </picture>
                    <div>
                        <p class="h3 bold">School of Industrial Organization | Google Activate</p>
                        <p class=""><span class="bold">Degree name: </span>CLOUD COMPUTING COURSE</p>
                        <p class=""><span class="bold">Graduation Date: </span>2015
                            – 2015</p>
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/CURSO-DE-CLOUD-COMPUTING.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/CURSO-DE-CLOUD-COMPUTING.jfif"
                                        alt="Certificate of CLOUD COMPUTING COURSE culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name:
                            </span>DATA MODELING</p>
                        <p class=""><span class="bold">Graduation Date: </span>2015
                            – 2015</p>
                        <!--<p>El curso me permiti&oacute; estudiar los conceptos del Proceso Unificado de desarrollo de
                            Software
                            (UML), as&iacute; como su aplicaci&oacute;n mediante el uso de una herramienta case Rational
                            Rose o Visio
                            para el modelamiento de objetos.</p>--->
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/MODELAMIENTO-DE-DATOS.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/MODELAMIENTO-DE-DATOS.jfif"
                                        alt="Certificate of DATA MODELING culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name: </span>PHP LEVEL I - WEB PROGRAMMING</p>
                        <p class=""><span class="bold">Graduation Date: </span>2015
                            – 2015</p>
                        <!--<p>El curso me permiti&oacute; aprender el lenguaje de programaci&oacute;n PHP y elaborar una
                            p&aacute;gina web.</p>-->
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/PHP-NIVEL-I-PROGRAMACION-WEB.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/PHP-NIVEL-I-PROGRAMACION-WEB.jfif"
                                        alt="Certificate of PHP LEVEL I - WEB PROGRAMMING culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering | Faculty of Industrial and Systems
                            Engineering | UNI SYSTEMS</p>
                        <p class=""><span class="bold">Degree name:
                            </span>INSTALLATIONS AND OS CONFIGURATION</p>
                        <p class=""><span class="bold">Graduation Date: </span>2014
                            – 2014</p>
                        <!--<p>El curso me proporciono conocimientos para realizar la instalaci&oacute;n y configuraci&oacute;n
                            de un sistema
                            operativo seg&uacute;n las necesidades del cliente, as&iacute; como, diagn&oacute;stico y
                            soluci&oacute;n de problemas, a
                            nivel de software.</p>-->
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/INSTALACIONES-Y-CONFIGURACION-DE-SO.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/INSTALACIONES-Y-CONFIGURACION-DE-SO.jfif"
                                        alt="Certificate of INSTALLATIONS AND OS CONFIGURATION culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <picture>
                        <img src="{{ Utils::getUrlBase() }}/img/educacion/uni.png" alt="Brand of UNI">
                    </picture>
                    <div>
                        <p class="h3 bold">National University of Engineering Computing Center | Faculty of Mechanical
                            Engineering - FIM | INFOUNI</p>
                        <p class=""><span class="bold">Degree name:</span> SPECIALIST IN ASSEMBLY OF PC'S</p>
                        <p class=""><span class="bold">Graduation Date: </span>2011
                            – 2011</p>
                        <!--<p>El curso me proporciono conocimientos para realiza el ensamblaje, diagn&oacute;stico y
                            soluci&oacute;n de
                            problemas, a nivel de hardware y software, de un equipo de c&oacute;mputo. As&iacute; mismo, me
                            permite poder
                            dar asesor&iacute;a a empresas que requieren equipos de &oacute;ptimo rendimiento y de acuerdo
                            con sus
                            necesidades.</p>-->
                        <ul>
                            <!--<li class="bold">Click para ver: </li>-->
                            <li>
                                <a target="_blank"
                                    href="{{ Utils::getUrlBase() }}/img/educacion/ESPECIALISTA-EN-ENSAMBLAJE-DE-PCS.jfif">
                                    <img src="{{ Utils::getUrlBase() }}/img/educacion/ESPECIALISTA-EN-ENSAMBLAJE-DE-PCS.jfif"
                                        alt="Certificate of SPECIALIST IN ASSEMBLY OF PC'S culminated by {{ GlobalClass::getCreatorUser() }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </section>
        <section>
            <p>My Other Skills</p>
            <ul>
                <li>Analysis of data</li>
                <li>Business analysis</li>
                <li>Analysis capacity</li>
                <li>Web analytics</li>
                <li>Digital marketing</li>
                <li>System analysis</li>
                <li>Software development</li>
                <li>Information technology</li>
                <li>Computer engineering</li>
                <li>Web development</li>
                <li>Software</li>
                <li>Startup</li>
                <li>Marketing</li>
                <li>Scrum</li>
                <li>Android development</li>
                <li>Tools and technologies</li>
                <li>Swift (programming language)</li>
                <li>JavaScript</li>
                <li>jQuery</li>
                <li>HTML5</li>
                <li>Cascading Style Sheets (CSS)</li>
                <li>SCSS</li>
                <li>Laravel</li>
                <li>Kotlin</li>
                <li>React.js</li>
                <li>Bootstrap</li>
                <li>Angular</li>
                <li>Angular Material</li>
                <li>Node.js</li>
                <li>C#</li>
                <li>DotNetNuke</li>
                <li>MySQL</li>
                <li>PostgreSQL</li>
                <li>Ionic Framework</li>
                <li>Ionic</li>
                <li>API of Postman</li>
                <li>OAuth</li>
                <li>JIRA</li>
                <li>Unified modeling language (UML)</li>
                <li>Microsoft SQL Server</li>
                <li>Git</li>
                <li>Android Studio</li>
                <li>TypeScript</li>
                <li>Waterfall project management</li>
                <li>Haskell</li>
                <li>Express.js</li>
                <li>Rup</li>
                <li>API of Google Maps</li>
            </ul>
        </section>
    </main>
@stop
