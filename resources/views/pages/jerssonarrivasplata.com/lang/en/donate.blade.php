@extends(GlobalClass::getDomainFolder(0) . '::base.index')
@section('metatags')
    @parent
    <!-- some master css here -->
    <title>Donation to {{ GlobalClass::getCreatorUser() }} | Software Developer</title>


    <meta property="og:type" content="article">

    <meta name="description"
        content="Page donation by {{ GlobalClass::getCreatorUser() }} -
        Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">

    <meta property="og:description"
        content="Page donation by {{ GlobalClass::getCreatorUser() }} -
        Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">
    <meta property="og:title" content="Donar a {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta property="og:url" content="{{ Utils::getUrlBase() }}/donacion">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="{{ Utils::getUrlBase() }}/donacion">
    <meta name="twitter:title" content="Donar a {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta name="twitter:description"
        content="Page donation by {{ GlobalClass::getCreatorUser() }} -
        Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">
@stop
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase() }}/css/index-project.css?v=<?php echo rand(); ?>" rel="stylesheet">
@stop

@section('content')
    @parent
    <!-- Main content goes here -->
    <main>
        @include(GlobalClass::getDomainFolder(0) .
                '::lang.' .
                request()->route()->parameter('language') .
                '.common.header-menu')
        <section>
            <!--style="padding-top: 0px;"-->
                <p>DONATION</p>
        </section>
        <article>

            <p class="h3">If any of my projects helped you and you want to continue supporting me to continue with this
                personal project.<br />
                <label class="bold">I would appreciate leaving me a donation!</label>
            </p>
            <ul>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/bitcoin.png" />
                    <p>Bitcoin: {{ config('base.virtual_coins.bitcoin.address') }}</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/ethereum.png" />
                    <p>Ethereum: {{ config('base.virtual_coins.ethereum') }}</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/fio.png" />
                    <p>FIO: {{ config('base.virtual_coins.fio') }}</p>
                    <p>Titular: {{ GlobalClass::getCreatorUser() }}</p>
                </li>
            </ul>
        </article>
        <article>
            <ul>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/yape.png" />
                    <p>YAPE: {{ config('base.info.celphone.number') }}</p>
                    <p>Holder: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Type: PEN</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/plin.png" />
                    <p>PLIN: {{ config('base.info.celphone.number') }}</p>
                    <p>Holder: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Type: PEN</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/tunki.png" />
                    <p>TUNKI: {{ config('base.info.celphone.number') }}</p>
                    <p>Holder: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Type: PEN</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/lukita-bbva.png" />
                    <p>LUKITA BBVA: {{ config('base.info.celphone.number') }}</p>
                    <p>Holder: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Type: PEN</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/bcp.png" />
                    <p>N° de cuenta: 19104250410043</p>
                    <p>CCI: 00219110425041004350</p>
                    <p>Holder: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Type: PEN</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/interbank.png" />
                    <p>N° de cuenta: 898 3232032953</p>
                    <p>CCI: 00389801323203295346</p>
                    <p>Holder: {{ GlobalClass::getCreatorUser() }}</p>
                    <p>Type: Dollars</p>
                </li>
                <li>
                    <img src="{{ Utils::getUrlBase() }}/img/others/paypal.png" />
                    <p>jersson.arrivasplata@gmail.com</p>
                    <p>@jerssonArrivasplata</p>
                    <p>Holder: {{ GlobalClass::getCreatorUser() }}</p>
                </li>
            </ul>
        </article>
    </main>


@stop
