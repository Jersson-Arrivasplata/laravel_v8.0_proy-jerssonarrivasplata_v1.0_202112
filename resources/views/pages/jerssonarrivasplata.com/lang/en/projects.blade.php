@extends(GlobalClass::getDomainFolder(0).'::base.index')
@section('metatags')
    @parent
    <!-- some master css here -->
    <title>Projects of {{ GlobalClass::getCreatorUser() }} | Software Developer</title>


    <meta property="og:type" content="article">

    <meta name="description"
        content="Page of all projects by {{ GlobalClass::getCreatorUser() }} -
        Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">

    <meta property="og:description"
        content="Page of all projects by {{ GlobalClass::getCreatorUser() }} -
        Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">
    <meta property="og:title" content="Proyectos de {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta property="og:url" content="{{ Utils::getUrlBase() }}/educacion">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="{{ Utils::getUrlBase() }}/educacion">
    <meta name="twitter:title" content="Proyectos de {{ GlobalClass::getCreatorUser() }} | Software Developer">
    <meta name="twitter:description"
        content="Page of all projects by {{ GlobalClass::getCreatorUser() }} -
        Full Stack Developer and experience in development apps mobiles, systems business, analysis to process, analysis to business in
        transport sectos, bank, education, touristm and others.">

@stop
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase() }}/css/index-education.css?v=<?php echo rand(); ?>" rel="stylesheet">
    <style type="text/css">
        sami-grid div.sami-grid.sami-grid___card {
            grid-template-columns: repeat(3, 1fr);
        }
        sami-card-image {
            display: flex;
        }

        sami-card-image a.sami-card-image div.sami-card-image___content {
            margin-left: 2.5rem;
        }

        sami-card-image a.sami-card-image div.sami-card-image___content:nth-child(2) {
            width: 180px;
        }

        sami-grid .sami-grid div:nth-child(4) sami-card-image a.sami-card-image div.sami-card-image___content p {
            color: var(--sami-dark);
        }


        sami-grid .sami-grid div:nth-child(4) sami-card-image a.sami-card-image div.sami-card-image___content {
            margin-top: 1.2rem;
            margin-left: 3rem;
        }

        sami-grid .sami-grid div:nth-child(4) sami-card-image a.sami-card-image sami-img .sami-card-image___background {
            object-fit: contain;
        }

    </style>
@stop

@section('content')
    @parent
    <!-- Main content goes here -->
    <main>
        @include(GlobalClass::getDomainFolder(0) . '::lang.'.request()->route()->parameter('language').'.common.header-menu')
        <section>
            <p>My Projects</p>
            <sami-grid padding="28px 24px">
                <x-card-image title="Development Blog" subtitle="" link="{{ Utils::getUrlBase('dev') }}" counter=""
                    link-image="{{ Utils::getUrlBase() }}/img/dev.jerssonarrivasplata.com.png">
                </x-card-image>
                <x-card-image title="Documentation Project" subtitle="" link="{{ Utils::getUrlBase('doc') }}"
                    counter="" link-image="{{ Utils::getUrlBase() }}/img/doc.jerssonarrivasplata.com.png">
                </x-card-image>
                <x-card-image title="Sami project" subtitle="StencilJs" link="{{ Utils::getUrlBase('sami') }}"
                    counter="" link-image="{{ Utils::getUrlBase() }}/img/sami.jerssonarrivasplata.com.png">
                </x-card-image>
            </sami-grid>
        </section>
    </main>
@stop
