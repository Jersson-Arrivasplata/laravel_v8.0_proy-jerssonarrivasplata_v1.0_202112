@extends(GlobalClass::getDomainFolder(5) . '::base.index')
@section('metatags')
    @parent
    @php
        $description =
            'Contenido de ' . BlogContentType::where('type', $type)->value('name') . ' en el Blog de desarrollo de ';
        $description .= GlobalClass::getCreatorUser() . ' - ';
        $description .= 'Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles, ';
        $description .= 'con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,';
        $description .=
            'an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.';

        $title = 'Contenido de ' . BlogContentType::where('type', $type)->value('name') . ' | Blog de Desarrollo';

    @endphp

    <x-meta-tags description="{{ $description }}" title="{{ $title }}" og-type="article" twitter-content-card="sumary"
        link="{{ Utils::getUrlBase() }}/{{ Str::slug(BlogContentType::where('type', $type)->value('name'), '-') }}">
    </x-meta-tags>


    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            // Your inline scripts which uses methods from external-scripts.
            new window.exports.SamiUtils.Highlight().init()
            console.log(new window.exports.SamiUtils.Utils().hello('3'))

        });
    </script>
    <style type="text/css">
        sami-card-code div.sami-card-code div.sami-card-code___section-two div.sami-card-code___preview-html {
            position: relative;
            width: 95%;
            margin: auto;
            text-align: left;
        }

        .align-content-center {
            align-content: center !important;
        }

        .justify-content-center {
            justify-content: center !important;
        }

        .w-100 {
            width: 100% !important;
        }

        .d-flex {
            display: flex !important;
        }
    </style>
@stop
@section('script-header')
    @parent
    <link href="{{ Utils::getUrlBase() }}/library/bootstrap/5.1.3/bootstrap.min.css" rel="stylesheet">
    <!-- Swiper CSS -->
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{ Utils::getUrlBase() }}/library/highlight.js/11.4.0/default.min.css">
    <link rel="stylesheet" href="{{ Utils::getUrlBase() }}/library/highlight.js/theme.css">
    <link rel="stylesheet" href="{{ Utils::getUrlBase() }}/css/blog-content-highlight.css">
    <style type="text/css">
        :root {
            --bs-gutter-x: 1.5rem;
            --bs-gutter-y: 0;
            --size-2: 0.5rem;
        }

        .sami-grid___slider {
            display: flex !important;
            gap: var(--size-2);
            justify-content: center;
            flex-wrap: wrap;
            padding: var(--size-2);
            padding-bottom: 2em;
        }

        .slider-card-publish {
            flex: calc(33% - var(--size-2));
            max-width: calc(50% - var(--size-2));
        }

        @media (max-width: 1336px) {
            .slider-card-publish {
                flex: calc(100% - var(--size-2));
                max-width: calc(100% - var(--size-2));
            }
        }

        @media (max-width: 560px) {
            .slider-card-publish code {
                font-size: 12px !important;
            }

            .slider-card-publish .title,
            .slider-card-publish .subtitle {
                font-size: 14px;
            }
        }

        .sami-hyperlink-back:hover,
        .sami-hyperlink-only-desktop:hover {
            color: var(--sami-white-800) !important;
        }

        .sami-hyperlink-only-desktop {
            top: 20px;
            border-width: 2.5px;

        }

        .page-item .page-link {
            height: 40px;
            width: 40px;
            text-align: center;
            line-height: 2.2;
            border-radius: 0px !important;
        }
        sami-input .sami-input-inline{
            width: 100%;
            border-width: 2px;
        }
    </style>

@stop

@section('content')
    @parent
    <div class="content-images d-none" id="content-images"></div>
    <x-button-back link="{{ url()->previous() }}" />
    <section class="mt-8 mb-8">
        <x-title title="Mis Contenidos de {{ BlogContentType::where('type', $type)->value('name') }}" />
    </section>
    <section>
        <sami-grid align="center" type="slider">
            @foreach ($contents as $content)
                <div class="mb-2 slider-card-publish">
                    <x-slider-card-publish :content="$content" :status="true" :type="'dev'"></x-slider-card-publish>
                </div>
            @endforeach
        </sami-grid>
        <div class="mb-3 d-flex justify-content-center">
            @empty(!$contents)
                {!! $contents->links() !!}
            @endempty
        </div>
    </section>
    <div id="cv" style="display:none;"></div>
@stop
@section('script-bottom')
    @parent
    <!-- more javascript -->

    <script src="{{ Utils::getUrlBase() }}/library/bootstrap/5.1.3/bootstrap.min.js"></script>
    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="{{ Utils::getUrlBase() }}/library/highlight.js/11.4.0/highlight.min.js"></script>
    <script src="{{ Utils::getUrlBase() }}/library/html2canvas/html2canvas.js"></script>
    <script src="{{ Utils::getUrlBase() }}/js/blog-content-highlight.js"></script>
@stop
{{-- <sami-flex-code>
        <div slot="flex-code">
        @foreach (Utils::getArrayOfContentGenerates($content->toArray()) as $i => $response)

        @endforeach
    </div>
</sami-flex-code> --}}

{{-- @foreach ($content as $i => $response)
    <sami-slider class="sami-desk-max-width-520">
        @foreach ($response->contents as $index => $data)
            <li>
                @include(GlobalClass::getDomainFolder(1) . '::common.content')
                @include(GlobalClass::getDomainFolder(1) . '::common.content-social')
            </li>
        @endforeach
    </sami-slider>
@endforeach --}}
