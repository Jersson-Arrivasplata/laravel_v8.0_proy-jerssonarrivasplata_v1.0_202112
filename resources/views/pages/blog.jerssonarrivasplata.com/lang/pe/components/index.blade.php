@extends(GlobalClass::getDomainFolder(5).'::base.index')
@php
$requestType = Request::segment(1);
@endphp
@section('metatags')
    @parent
    <!-- some master css here -->
    @foreach ($response as $data)
        @php
            $description = 'Contenido de ' . $data->name . ' en Sitio de Proyectos de Desarrollo de ';
            $description .= GlobalClass::getCreatorUser() . ' - ';
            $description .= 'Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles, ';
            $description .= 'con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,';
            $description .= 'an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.';

            $title = 'Contenido de ' . $data->name . ' | Proyectos de Desarrollo';

        @endphp

        <x-meta-tags description="{{ $description }}" title="{{ $title }}" og-type="article"
            twitter-content-card="sumary"
            link="{{ Utils::getUrlBase() }}/project/{{ $requestType }}/{{ Str::slug(BlogContentType::where('type', $data->type)->value('name'), '-') }}">
        </x-meta-tags>
    @break
@endforeach
@stop
@section('script-header')
    @parent
    <style type="text/css">
        :root {
            --sami-screen-code: rgb(64 12 109);
            --sami-screen: rgb(64 12 109);
        }
    </style>
@stop
@section('content')
@parent

<x-button-back link="{{ url()->previous() }}" />
@foreach ($response as $data)
    <section>
        <x-title title="{{ $data->name }}" />
    </section>
    <section class="text-end mb-1">
        <x-for-link-social-media content="{!! $data->links !!}"></x-for-link-social-media>
    </section>
    <section class="sami-background-color-white " style="padding: 0px 15px 0px 15px;">
        <x-iframe link="{{ Utils::getUrlBase() }}/projects/{{ $data->path }}/files/index.html">
        </x-iframe>
    </section>
@break
@endforeach

@stop
