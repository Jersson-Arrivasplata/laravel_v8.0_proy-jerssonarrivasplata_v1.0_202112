@extends(GlobalClass::getDomainFolder(5).'::base.index')
@section('metatags')
    @parent
    <!-- some master here -->
    @php
    $description = 'Contenido de ' . BlogContentType::where('type', $type)->value('name') . ' en Sitio de Proyectos de Desarrollo de ';
    $description .= GlobalClass::getCreatorUser() . ' - ';
    $description .= 'Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles, ';
    $description .= 'con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,';
    $description .= 'an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.';

    $title = 'Contenido de ' . BlogContentType::where('type', $type)->value('name') . ' | Proyectos de Desarrollo';

    @endphp

    <x-meta-tags description="{{ $description }}" title="{{ $title }}" og-type="article"
        twitter-content-card="sumary"
        link="{{ Utils::getUrlBase() }}/{{ Str::slug(BlogContentType::where('type', $type)->value('name'), '-') }}">
    </x-meta-tags>

@stop
@section('script-header')
    @parent
    <style type="text/css">
        :root {
            --sami-screen-code: rgb(64 12 109);
            --sami-screen: rgb(64 12 109);
        }
    </style>
@stop
@section('content')
    @parent

    <x-button-back link="{{ Utils::getUrlBase() }}/{{request()->route()->parameter('language')}}/blog/project" />
    <section class="mt-8 mb-8">
        <x-title title="Mis Proyectos de {{ BlogContentType::where('type', $type)->value('name') }}" />
    </section>
    <section>
        <sami-grid>
            <x-for-card type="{{ $type }}" content="{!! json_encode($content->toArray(), true) !!}"></x-for-card>
        </sami-grid>
    </section>
@stop
