@extends(GlobalClass::getDomainFolder(5).'::base.index')

@section('metatags')
    @parent
    <!-- some master css here -->
    @php
        $description = 'Sitio de Proyectos de ' . GlobalClass::getCreatorUser() . ' - ';
        $description .= 'Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles, ';
        $description .= 'con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos, ';
        $description .= 'an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.';
        //'Proyectos de ' .
        $title = GlobalClass::getCreatorUser() . ' | Proyectos de Desarrollo';
    @endphp

    <x-meta-tags description="{{ $description }}" title="{{ $title }}" og-type="website"
        twitter-content-card="sumary" link="{{ Utils::getUrlBase() }}"></x-meta-tags>
@stop
@section('script-header')
    @parent
    <style type="text/css">
        :root {
            --sami-screen-code: rgb(64 12 109);
            --sami-screen: rgb(64 12 109);
        }
    </style>
@stop

@section('content')
    @parent

    <section>
        <x-title title="Mis Proyectos de Desarrollo" />
    </section>
    <section>
        <sami-grid padding="28px 24px">
            <x-for-card-image
            type="0"
            content="{!! json_encode($blogContentType->toArray(), true) !!}"
            link="{{ Utils::getUrlBase() }}"></x-for-card-image>
        </sami-grid>
    </section>
@stop
