@extends(GlobalClass::getDomainFolder(5).'::base.index')

@section('metatags')
    @parent
    <!-- some master css here -->
    @php
    $description = 'Blog de desarrollo de ' . GlobalClass::getCreatorUser() . ' - ';
    $description .= 'Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles, ';
    $description .= 'con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos, ';
    $description .= 'an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.';
    //'Proyectos de ' .
    $title = GlobalClass::getCreatorUser() . ' | Blog de Desarrollo';
    @endphp

    <x-meta-tags description="{{ $description }}" title="{{ $title }}" og-type="website"
        twitter-content-card="sumary" link="{{ Utils::getUrlBase() }}"></x-meta-tags>
@stop
@section('script-header')
    @parent

@stop

@section('content')
    @parent

    <section>
        <x-title title="Mi Blog de Desarrollo" />
    </section>
    <section >
        <sami-grid padding="28px 24px">
            <x-for-card-image
            type="1"
            link="{{ Utils::getUrlBase() }}"
            content="{!! json_encode($blogContentType->toArray(), true) !!}"></x-for-card-image>
        </sami-grid>
    </section>
@stop
