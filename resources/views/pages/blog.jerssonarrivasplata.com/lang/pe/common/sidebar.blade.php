<x-sidebar link="{{ Utils::getUrlBase() }}" link-image="{{ Utils::getUrlBase() }}/img/{{ request()->is('*project*') ? 'projects' : 'dev' }}.jerssonarrivasplata.com.png">
    <sami-hyperlink href="{{ Utils::getUrlBase() }}/{{request()->route()->parameter('language')}}/blog" onclick="loader()" target="_self">Inicio
    </sami-hyperlink>
    <sami-hyperlink href="{{ Utils::getUrlBase() }}#sobremi" onclick="loader()" target="_self">Sobre M&iacute;
    </sami-hyperlink>
    <sami-hyperlink href="{{ Utils::getUrlBase() }}/{{request()->route()->parameter('language')}}/educacion" onclick="loader()" target="_self">
        Mi Educaci&oacute;n
    </sami-hyperlink>
    <sami-hyperlink href="{{ Utils::getUrlBase() }}#miexperiencia" onclick="loader()" target="_self">Mi Experiencia</sami-hyperlink>
    <sami-hyperlink href="{{ Utils::getUrlBase() }}/{{request()->route()->parameter('language')}}/blog/project" onclick="loader()" target="_self">Proyectos del Blog</sami-hyperlink>
    <sami-hyperlink href="{{ Utils::getUrlBase() }}/docs/1.0/angular/angular.guideline" target="_blank">
        Documentaci&oacute;n</sami-hyperlink>
    <sami-hyperlink href="{{ Utils::getUrlBase('sami') }}" target="_blank">StencilJs - Sami</sami-hyperlink>
    <sami-hyperlink href="{{ Utils::getUrlBase() }}/contacto" target="_blank">Contactame</sami-hyperlink>
    <sami-hyperlink href="{{ Utils::getUrlBase() }}/donacion" target="_blank">Donar</sami-hyperlink>
</x-sidebar>
