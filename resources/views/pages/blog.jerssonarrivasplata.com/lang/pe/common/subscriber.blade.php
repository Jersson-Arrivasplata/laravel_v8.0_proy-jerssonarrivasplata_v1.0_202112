<x-subscriber
link="{{ Utils::getUrlBase() }}"
title="Suscríbete a mi Blog"
subtitle="¡Agrega tu correo y suscribete en mi blog para recibir un mensaje cada vez que se suba un contenido!"
></x-subscriber>
