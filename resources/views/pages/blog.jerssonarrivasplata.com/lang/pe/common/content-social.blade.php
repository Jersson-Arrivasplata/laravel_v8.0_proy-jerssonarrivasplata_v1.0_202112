<sami-list list-style="none" opacity="8" filter-invert="1" class="d-flex justify-content-evenly mb-4 mt-2">
    @if($response['url_linkedin'])
    <sami-list-item>
        <sami-hyperlink href="#" target="_blank"
            onclick="new window.exports.SamiUtils.SocialMediaShare().shareLinkedin('{{ $response['url_linkedin'] }}')">
            <sami-img width="25"
                src="{{ Utils::getUrlBase() }}/library/sami/assets/icon-social-media/linkedin.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    <sami-list-item>
        <sami-hyperlink href="#" target="_blank"
            onclick="new window.exports.SamiUtils.SocialMediaShare().shareFacebook('{{ $response['url_linkedin'] }}',`{{ $response['description'] }}`)">
            <sami-img width="25"
                src="{{ Utils::getUrlBase() }}/library/sami/assets/icon-social-media/facebook.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    <sami-list-item>
        <sami-hyperlink href="#" target="_blank"
            onclick="new window.exports.SamiUtils.SocialMediaShare().shareWhatsapp('{{ $response['url_linkedin'] }}')">
            <sami-img width="25"
                src="{{ Utils::getUrlBase() }}/library/sami/assets/icon-social-media/whatsapp.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    @endif
    <sami-list-item>
        <sami-hyperlink href="#" target="_self" onclick="new window.exports.SamiUtils.HtmlToCanvas().generateHtmlToCanvas(
                {
                    'id':'cv',
                    'measures': {
                        'width': ($(this.parentElement.parentElement.parentElement.parentElement.parentElement).find('sami-card-code').width()),
                        'height':($(this.parentElement.parentElement.parentElement.parentElement.parentElement).find('sami-card-code').height())
                    },
                    'element':$(this.parentElement.parentElement.parentElement.parentElement.parentElement).find('sami-card-code')[0]
                }
            )">
            <sami-img width="25" src="{{ Utils::getUrlBase() }}/library/sami/assets/icon/picture.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
</sami-list>

    {{--<sami-list-item>
    <sami-hyperlink href="#" target="_self"
        onclick="new window.exports.SamiUtils.Clipboard().copyToClipboard($(this.parentElement.parentElement.parentElement.parentElement.parentElement).find('.sami-card-code___content')[0], true)">
        <sami-img width="25" src="{{ Utils::getUrlBase() }}/library/sami/assets/icon/copy.svg">
        </sami-img>
    </sami-hyperlink>


    @if ($data['checkPreviewBox'] == 1)
        <sami-hyperlink href="#" target="_self"
            onclick="new window.exports.SamiUtils.Clipboard().copyToClipboard($(this.parentElement.parentElement.parentElement.parentElement.parentElement)
            .find('.sami-card-code___content div[slot=view-html]')[0], true)">
            <sami-img width="25" src="{{ Utils::getUrlBase() }}/library/sami/assets/icon/copy.svg">
            </sami-img>
        </sami-hyperlink>
    @else
        <sami-hyperlink href="#" target="_self"
            onclick="new window.exports.SamiUtils.Clipboard().copyToClipboard($(this.parentElement.parentElement.parentElement.parentElement.parentElement)
            .find('.sami-card-code___content')[0], true)">
            <sami-img width="25" src="{{ Utils::getUrlBase() }}/library/sami/assets/icon/copy.svg">
            </sami-img>
        </sami-hyperlink>
    @endif


    </sami-list-item>--}}
