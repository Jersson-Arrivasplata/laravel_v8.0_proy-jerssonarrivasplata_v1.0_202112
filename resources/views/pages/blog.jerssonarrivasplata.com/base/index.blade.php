<!DOCTYPE html>
<html lang="en">

<head>
    <x-html-head link="{{ Utils::getUrlBase() }}"
        link-image="{{ Utils::getUrlBase() }}/img/jerssonarrivasplata.com.png"
        color="#0652dd"
        google-tag-manager-id="G-5HSFR91RYS"></x-html-head>

    <link href="{{ Utils::getUrlBase() }}/css/base.dev.css?v=<?php echo rand(); ?>" rel="stylesheet">
    <style type="text/css">
        /* :root {
            --sami-screen-code: rgb(204, 30, 45);
            --sami-screen: rgb(204, 30, 45);
        }*/
    </style>
    @section('metatags')
        <!-- some master css here -->
    @show

    @section('script-header')
        <!-- some master css here -->
        <style type="text/css">
            sami-card-image a.sami-card-image sami-img .sami-card-image___background {
                object-fit: contain;
            }

        </style>
    @show
</head>

<body class="font-poppins">
    <x-loader></x-loader>
    @include(GlobalClass::getDomainFolder(5) . '::lang.'.request()->route()->parameter('language').'.common.header')
    @include(GlobalClass::getDomainFolder(5) . '::lang.'.request()->route()->parameter('language').'.common.sidebar')
    @include(GlobalClass::getDomainFolder(5) . '::lang.'.request()->route()->parameter('language').'.common.subscriber')

    <x-main width="">
        @if (Session::has('success'))
            <x-swal-fire link="{{ Utils::getUrlBase() }}"></x-swal-fire>
        @endif
        @yield('content')
        <x-footer type="1"></x-footer>
    </x-main>
    @section('script-bottom')
        <!-- some js here -->
    @show
</body>

</html>

