@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/message.css?v=<?php echo(rand()); ?>" rel="stylesheet">

@stop
@section('content')
    @parent
    <div class="container mt-5">
        <h3>
            Administrador de Mensajes Web
            <small class="text-"> - Crear</small>
        </h3>
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
            </div>
        @endif
        <form method="post" action="{{ url('message') }}">
            {{ csrf_field() }}
            <div class="mb-3">
                <label class="form-label">Nombre</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="mb-3">
                <label class="form-label">Correo electr&oacute;nico</label>
                <input type="text" name="email" class="form-control">
            </div>
            <div class="mb-3">
                <label class="form-label">Celular</label>
                <input type="text" name="celphone" class="form-control">
            </div>
            <div class="mb-3">
                <label class="form-label">Mensaje</label>
                <textarea style="height: 405px;" name="message" class="form-control"></textarea>
            </div>
            <a href="{{ url('message') }}" class="btn btn-danger">Regresar</a>
            <button type="submit" class="btn btn-primary">Agregar</button>
        </form>
    </div>
@stop
@section('script-bottom')
    @parent
    <!-- more css -->
    <script src="{{ Utils::getUrlBase('admin') }}/js/message.js?v=<?php echo(rand()); ?>" type="text/javascript"></script>

@stop
