<?php
    $type = Str::lower(BlogContentType::where('type', $response['id_blog_content_type'])->value('highlight'));
?>
<sami-card-code
    box="{{ $data['checkImageBox'] == '1'?'true':'false'}}"
    author-email="jerssonarrivasplata.com"
    identify="{{ $data['id'] }}"
    content-title="{{ $data['contentTitle'] }}"
    content-subtitle="{{ $data['contentSubTitle'] }}"
    background-color="{{$data['bgColor']}}"
    title-image="{{ $data['imgTitle'] }}"
    title-image-font-size="{{ $response['imgFontSizeRange'] }}px"
    subtitle-image="{{ $data['imgSubTitle'] }}"
    preview="{{ $data['checkPreviewBox'] == '1'?'true':'false'}}"
    background-image="{{ Utils::getUrlBase('admin') }}/img/blog/content-type/{{ $data['typeLanguage'] }}"
    padding-left="{{ $data['contentPaddingLeft'] }}px"
    margin-top="{{ $data['contentMargintop'] }}px"
    type="{{$type}}">
    @if ($data['position']>0)
        <sami-tag slot="counter" class="t-0 r-0" width="30px" position="absolute" align="center" font-weight="normal" line-height="2.3" color="var(--sami-white)" background="{{$data['bgColor']}}" width="30" height="30" border-radius="0px 0px 0px 15px">
            {{$data['position']}}
        </sami-tag>
    @endif
    @if ($data['checkPreviewBox'] == 1)
        <div slot="view-html" data-clipboard-text="{{$data['codes']}}">
            {!! $data['codes'] !!}
        </div>
    @else
    <pre slot="view-html" class="w-100 d-flex justify-content-center align-content-center">
        <code data-clipboard-text="{{$data['codes']}}" class="{{ $type }}" style="font-size:{{ $data['contentfontSizeRange'] }}px;" >{{$data['codes']}}</code>
    </pre>
    @endif
</sami-card-code>