<sami-list list-style="none" opacity="8" filter-invert="1" class="d-flex justify-content-evenly mb-4 mt-2">
    <sami-list-item>
        <sami-hyperlink href="#" target="_blank"
            onclick="new window.exports.SamiUtils.SocialMediaShare().shareLinkedin('{{ $response['url_linkedin'] }}')">
            <sami-img width="25"
                src="{{ Utils::getUrlBase('admin') }}/library/sami/assets/icon-social-media/linkedin.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    <sami-list-item>
        <sami-hyperlink href="#" target="_blank"
            onclick="new window.exports.SamiUtils.SocialMediaShare().shareFacebook('{{ $response['url_linkedin'] }}',`{{ $response['description'] }}`)">
            <sami-img width="25"
                src="{{ Utils::getUrlBase('admin') }}/library/sami/assets/icon-social-media/facebook.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    <sami-list-item>
        <sami-hyperlink href="#" target="_blank"
            onclick="new window.exports.SamiUtils.SocialMediaShare().shareWhatsapp('{{ $response['url_linkedin'] }}')">
            <sami-img width="25"
                src="{{ Utils::getUrlBase('admin') }}/library/sami/assets/icon-social-media/whatsapp.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    <sami-list-item>
        <sami-hyperlink href="#" target="_self" onclick="new window.exports.SamiUtils.HtmlToCanvas().generateHtmlToCanvas(
                {
                    'id':'cv',
                    'measures': {
                        'width': ($(this.parentElement.parentElement.parentElement.parentElement.parentElement).find('sami-card-code').width()),
                        'height':($(this.parentElement.parentElement.parentElement.parentElement.parentElement).find('sami-card-code').height())
                    },
                    'element':$(this.parentElement.parentElement.parentElement.parentElement.parentElement).find('sami-card-code')[0]
                }
            )">
            <sami-img width="25" src="{{ Utils::getUrlBase('admin') }}/library/sami/assets/icon/picture.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    <sami-list-item>
        <sami-hyperlink target="_blank" href="{{ url('/blog-content-generate'.'/'.$data['id'].'/edit') }}">
            <sami-img width="25"
                src="{{ Utils::getUrlBase('admin') }}/img/icons/edit.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    <sami-list-item>
        <button  class="blog-content-delete" onclick="blogContentDelete({{$data['id']}})">
            <sami-img width="20"
                src="{{ Utils::getUrlBase('admin') }}/img/icons/close.svg">
            </sami-img>
        </button>
    </sami-list-item>
    <sami-list-item>
        <button  class="blog-content-copy" onclick="blogContentCopy({{$data['id']}})">
            <sami-img width="20"
                src="{{ Utils::getUrlBase('admin') }}/img/icons/copy.svg">
            </sami-img>
        </button>
    </sami-list-item>
    <sami-list-item>
        <button  class="blog-content-replicate" onclick="blogContentReplicate({{$data['id']}})">
            <sami-img width="20"
                src="{{ Utils::getUrlBase('admin') }}/img/icons/replicate.svg">
            </sami-img>
        </button>
    </sami-list-item>
    <sami-list-item>
        <span>Id: {{$data['id']}}</span>
    </sami-list-item>
</sami-list>
