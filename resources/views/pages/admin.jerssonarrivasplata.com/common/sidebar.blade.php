<style type="text/css">
    sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav {
        background: var(--sami-white);
        padding-top: 2rem;
    }

</style>

<x-sidebar-dashboard>
    <sami-list-item>
        <sami-list list-style="none" class="m-0 p-0">
            <sami-list-item active="{{ Utils::getBooleanString(Request::segment(1) == '') }}">
                <sami-hyperlink href="{{ url('/') }}" target="_self">
                    <sami-paragraph>Inicio</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
        </sami-list>
    </sami-list-item>
    <sami-list-item>
        <sami-paragraph>Web: Jersson G.</sami-paragraph>
        <sami-list list-style="none" class="m-0 p-0">
            <sami-list-item active="{{ Utils::getBooleanString(Request::segment(1) == 'message') }}">
                <sami-hyperlink href="{{ url('/message') }}" target="_self">
                    <sami-paragraph>Mensajes</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
            <sami-list-item active="{{ Utils::getBooleanString(false) }}">
                <sami-hyperlink href="{{ Utils::getUrlBase() }}" target="_blank">
                    <sami-paragraph>Web - Jersson Arrivasplata</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
        </sami-list>
    </sami-list-item>
    <sami-list-item>
        <sami-paragraph>Web: Blog</sami-paragraph>
        <sami-list list-style="none" class="m-0 p-0">
            <sami-list-item active="{{ Utils::getBooleanString(Request::segment(1) == 'blog-content-type') }}">
                <sami-hyperlink href="{{ url('/blog-content-type') }}" target="_self">
                    <sami-paragraph>Tipo Contenido</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
            <sami-list-item active="{{ Utils::getBooleanString(Request::segment(1) == 'blog-content-generate') }}">
                <sami-hyperlink href="{{ url('/blog-content-highlight') }}" target="_self">
                    <sami-paragraph>Contenidos</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
            <sami-list-item active="{{ Utils::getBooleanString(false) }}">
                <sami-hyperlink href="{{ url('/blog-content-highlight') }}" target="_blank">
                    <sami-paragraph>Web - Blog</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
            <sami-list-item active="{{ Utils::getBooleanString(false) }}">
                <sami-hyperlink href="{{ Utils::getUrlBase('dev') }}/project" target="_blank">
                    <sami-paragraph>Web - Proyectos</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
        </sami-list>
    </sami-list-item>
    <sami-list-item>
        <sami-paragraph>Web: Administrador</sami-paragraph>
        <sami-list list-style="none" class="m-0 p-0">
            <sami-list-item active="{{ Utils::getBooleanString(Request::segment(1) == 'admin-sitemap') }}">
                <sami-hyperlink href="{{ url('/admin-sitemap') }}" target="_self">
                    <sami-paragraph>Sitemap</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
            <sami-list-item active="{{ Utils::getBooleanString(Request::segment(1) == 'blog-page') }}">
                <sami-hyperlink href="{{ url('/blog-page') }}" target="_self">
                    <sami-paragraph>Páginas</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
        </sami-list>
    </sami-list-item>
    <sami-list-item>
        <sami-paragraph>Web: Documentaci&oacute;n</sami-paragraph>
        <sami-list list-style="none" class="m-0 p-0">
           <!-- <sami-list-item active="{{ Utils::getBooleanString(Request::segment(1) == 'hyperlink') }}">
                <sami-hyperlink href="{{ url('/hyperlink') }}" target="_self">
                    <sami-paragraph>Hyperlinks</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>-->
            <sami-list-item active="{{ Utils::getBooleanString(false) }}">
                <sami-hyperlink href="{{ Utils::getUrlBase('doc') }}" target="_blank">
                    <sami-paragraph>Web - Documentaci&oacute;n</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
        </sami-list>
    </sami-list-item>
    <sami-list-item>
        <sami-paragraph>Web: Sami</sami-paragraph>
        <sami-list list-style="none" class="m-0 p-0">
            <sami-list-item active="{{ Utils::getBooleanString(Request::segment(1) == 'sami-sidebar') }}">
                <sami-hyperlink href="#" target="_self">
                    <sami-paragraph>Sidebar</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
            <sami-list-item active="{{ Utils::getBooleanString(false) }}">
                <sami-hyperlink href="{{ Utils::getUrlBase('sami') }}" target="_blank">
                    <sami-paragraph>Web - Sami</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
        </sami-list>
    </sami-list-item>
    <sami-list-item>
        <sami-paragraph>Web: Proyectos</sami-paragraph>
        <sami-list list-style="none" class="m-0 p-0">
            <sami-list-item active="{{ Utils::getBooleanString(Request::segment(1) == 'project-content') }}">
                <sami-hyperlink href="{{ url('/project-content') }}" target="_self">
                    <sami-paragraph>Contenidos</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
        </sami-list>
    </sami-list-item>
</x-sidebar-dashboard>
