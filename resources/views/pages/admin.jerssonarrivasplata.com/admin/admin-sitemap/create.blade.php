@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
@stop
@section('content')
    @parent
    <div class="container mt-5">
        <h3>
            Administrador de SiteMap
            <small class="text-"> - Crear</small>
        </h3>
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
            </div>
        @endif
        <form method="post" action="{{ url('admin-sitemap') }}">
            {{ csrf_field() }}
            <div class="mb-3">
                <label class="form-label">Descripci&oacute;n</label>
                <input type="text" name="description" class="form-control">
            </div>
            <div class="mb-3">
                <label class="form-label">Url</label>
                <input type="text" name="url" class="form-control">
            </div>
            <div class="mb-3">
                <label class="form-label">Prioridad</label>
                <select name="priority" class="form-control">
                    <option>0.1</option>
                    <option>0.2</option>
                    <option>0.3</option>
                    <option>0.4</option>
                    <option>0.5</option>
                    <option>0.6</option>
                    <option>0.7</option>
                    <option>0.8</option>
                    <option>0.9</option>
                    <option>1</option>
                </select>
            </div>
            <div class="mb-3">
                <label class="form-label">Tipo</label>
                <select name="type" class="form-control" >
                    @foreach (App\Models\Domain::get() as $response)
                        <option value="{{$response['type']}}">{{$response['name']}}</option>
                    @endforeach
                </select>
            </div>
            <a href="{{ url('admin-sitemap') }}" class="btn btn-danger">Regresar</a>
            <button type="submit" class="btn btn-primary">Agregar</button>
        </form>
    </div>
@stop
@section('script-bottom')
    @parent
    <!-- more css -->

@stop
