@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
@stop
@section('content')
    @parent
    <div class="admin-message-index container mt-5">
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
            </div>
        @endif
        <div class="row">
            <div class="col">
                <p class="h2">Administrador de SiteMap</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <a href="{{ url('admin-sitemap/create') }}" class="btn btn-primary mb-2">
                    + Agregar
                </a>
                <select name="type" class="form-control mb-2 ml-2" id="admin-sitemap___select-type">
                    <optgroup label="Seleccione Tipo de SiteMap">
                        @foreach (App\Models\Domain::get() as $response)
                            <option value="{{ $response['type'] }}">{{ $response['name'] }}</option>
                        @endforeach
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col table-responsive">
                <table class="table table-light table-hover">
                    <thead class="thead-light">
                        <tr>
                            <th>N°</th>
                            <th>Descripci&oacute;n</th>
                            <th>Url</th>
                            <th>Prioridad</th>
                            <th>Tipo</th>
                            <th>Alternates</th>
                            <th>Fecha de <br />creaci&oacute;n</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($content as $index => $data)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td style="width: 180px;word-break: break-word;">
                                    {{ $data->description }}
                                </td>
                                <td>{{ $data->url }}</td>
                                <td>{{ $data->priority }}</td>
                                <td>{{ App\Models\Domain::where('type', $data->type)->value('name') }}</td>
                                <td style="text-align: center">
                                    <p>{{ App\Models\SiteMapAlternates::where('id_sitemaps', $data->id)->count() }}</p>
                                    <a href="{{ url('admin-sitemap-alternate/show', $data->id) }}" class="btn btn-primary btn-sm mb-2">
                                        + Agregar
                                    </a>
                                </td>
                                <td>{{ $data->created_at }}</td>
                                <td class="inline">
                                    <div class=" d-grid gap-2" style="display:grid;">
                                        <form method="post" action="{{ url('admin-sitemap/' . $data->id) }}"
                                            style="display:inline">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-danger btn-block" type="submit"
                                                onclick="return confirm('¿Seguro en eliminar el archivo?');">Eliminar
                                                X</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <div class="col">
                @empty(!$content)
                    {!! $content->links() !!}
                @endempty
            </div>
        </div>
    </div>
@stop
@section('script-bottom')
    @parent
    <script type="text/javascript">
        $('#admin-sitemap___select-type').on('change', function(){
            let data = $(this).val();
            window.open("{{ url('admin-sitemap') }}/"+ data +"/sitemap.xml",'_blank');
        });
    </script>
@stop
