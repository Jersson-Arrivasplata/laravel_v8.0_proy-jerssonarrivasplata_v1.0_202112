<?php header('Content-type: text/xml'); ?>
<?xml version="1.0" encoding="UTF-8" ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd
    http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd"
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
    @foreach ($content as $data)
        <url>
            <loc>{{ $data->url }}</loc>
            @foreach ($data->alternates as $alternate)
            <xhtml:link rel="alternate" hreflang="{{ $alternate->lang }}" href="{{ $alternate->url }}" />
            @endforeach
            <lastmod>{{ $data->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>{{ $data->priority }}</priority>
        </url>
    @endforeach
</urlset>
