@extends(GlobalClass::getDomainFolder(2) . '::base.index')
@section('script-header')
    @parent
    <!-- more css -->
@stop
@section('content')
    @parent
    <div class="admin-message-index container mt-5">
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
            </div>
        @endif
        <div>
            <a href="{{ url('admin-sitemap') }}" class="btn btn-danger mt-3">Regresar</a>
        </div>
        <div class="row">
            <div class="col">
                <p class="h2">Administrador de SiteMapAlternate</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card" >
                    <div class="card-header">Agregar nuevo</div>
                    <div class="card-body">
                        <form  method="post" action="{{ url('admin-sitemap-alternate') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="url">Url</label>
                                <input type="text" class="form-control" id="url" name="url">
                            </div>
                            <div class="form-group">
                                <label for="lang">Lang</label>
                                <select class="form-control" id="lang" name="lang">
                                  <option>pe</option>
                                  <option>en</option>
                                </select>
                            </div>  
                            <input type="hidden" class="form-control" value="{{$id}}" id="url" name="id_sitemaps">

                            <button type="submit" class="btn btn-success mt-2">Agregar</button>
                        </form>
                    </div>
                </div>
            </div>
            

        </div>
        <div class="row mt-3">
            <div class="col table-responsive">
                <table class="table table-light table-hover">
                    <thead class="thead-light">
                        <tr>
                            <th>N°</th>
                            <th>Url</th>
                            <th>Lang</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($content as $index => $data)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $data->url }}</td>
                                <td>{{ $data->lang }}</td>
                                <td class="inline">
                                    <div class=" d-grid gap-2" style="display:grid;">
                                        <form method="post" action="{{ url('admin-sitemap-alternate/' . $data->id) }}"
                                            style="display:inline">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-danger btn-block" type="submit"
                                                onclick="return confirm('¿Seguro en eliminar ?');">Eliminar
                                                X</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <div class="col">
                @empty(!$content)
                    {!! $content->links() !!}
                @endempty
            </div>
        </div>
    </div>
@stop
