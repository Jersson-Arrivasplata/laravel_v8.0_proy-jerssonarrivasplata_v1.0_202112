@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-content.css?v=<?php echo rand(); ?>" rel="stylesheet">

    <style>
        .card-img-top {
            max-height: 180px;
            object-fit: contain;
        }

    </style>
@stop
@section('content')
    @parent
    <div class="admin-blog-content-index container mt-5">
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
            </div>
        @endif
        <div class="row">
            <div class="col">
                <p class="h2">Administrador del Blog de Paginas</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <a href="{{ url('blog-page/create') }}" class="btn btn-primary mb-2">
                    + Agregar
                </a>
            </div>
        </div>
        <div class="row mt-3">
            @foreach ($content as $item)
                <div class="card" style="width: 25rem; margin-right: 1rem;margin-bottom: 2rem;">
                    <img class="card-img-top"
                        src="{{ !isset($item['meta-property-og-image']) || !($item['meta-property-og-image'] == '')? $item['meta-property-og-image']: Utils::getUrlBase('admin') . '/img/background/400X400.png' }}"
                        alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{ $item['title'] }}</h5>
                        @if (!isset($item['meta-name-description']) || !($item['meta-name-description'] == ''))
                            <p class="card-text">
                                {{$item['meta-name-description']}}
                            </p>
                        @else
                            <p class="card-text">
                                {{substr($item['body'], 0, 200)}}
                            </p>
                        @endif
                        <a target="_blank" href="{{ $item['url'] }}" class="btn btn-primary d-block">Ir a</a>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- Modal -->
        <div class="modal fade" id="descriptionModal" aria-labelledby="descriptionModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="descriptionModalLabel">Descripci&oacute;n</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script-bottom')
    @parent
    <!-- more css -->
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-index.js?v=<?php echo rand(); ?>" type="text/javascript"></script>
@stop
