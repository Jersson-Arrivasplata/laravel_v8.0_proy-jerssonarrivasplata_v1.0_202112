@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-content.css?v=<?php echo rand(); ?>" rel="stylesheet">

@stop
@section('content')
    @parent
    <div class="container mt-5">
        <h3>
            Blog de Paginas
            <small class="text-"> - Crear</small>
        </h3>
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">X</button>

            </div>
        @endif
        <div id="alerts">

        </div>
        <form method="post" action="{{ url('blog-page') }}" >
            {{ csrf_field() }}
            <div class="mb-3" id="blog-page-class">
                <label class="form-label">Url</label>
                <input type="text" name="url" class="form-control">
            </div>
            <a href="{{ url('blog-page') }}" class="btn btn-danger">Regresar</a>
            <input type="submit" value="Agregar" class="btn btn-primary" />
        </form>
    </div>
@stop
@section('script-bottom')
    @parent
    {{--
        const meta = Array.from(document.querySelectorAll('meta[name]')).reduce((acc, meta) => (
  Object.assign(acc, { [meta.name]: meta.content })), {});

        --}}
    <!-- more css
        const meta = Array.from(document.querySelectorAll('meta[name]')).reduce((acc, meta) => (
  Object.assign(acc, { [meta.name]: meta.content })), {});
                    <script type="text/javascript" src="{ { env('APP_URL') }}library/ckeditor/ckeditor.js?v=<?php echo rand(); ?>"></script>

                <script type="text/javascript" src="{ { env('APP_URL') }}library/ckeditor/plugins/ckeditor01/ckeditor/ckeditor.js">
                </script>
         -->

@stop
