@extends(GlobalClass::getDomainFolder(2) . '::base.index')
@section('script-header')
    @parent
    <link rel="stylesheet" href="{{ Utils::getUrlBase('admin') }}/library/highlight.js/11.4.0/default.min.css">
    <link rel="stylesheet" href="{{ Utils::getUrlBase('admin') }}/library/highlight.js/theme.css">
    <link rel="stylesheet" href="{{ Utils::getUrlBase('admin') }}/css/blog-content-highlight.css">

    <script type="text/javascript">
        window.baseContentType = '{{ Utils::getUrlBase('admin') }}/img/blog/content-type/';

        document.addEventListener("DOMContentLoaded", function(event) {
            // Your inline scripts which uses methods from external-scripts.
            new window.exports.SamiUtils.Highlight().init()
            console.log(new window.exports.SamiUtils.Utils().hello('3'))

        });
    </script>
@stop
@section('content')
    @parent
    <a href="{{ url()->previous() }}" class="btn btn-danger btn-back mt-3"><span>Regresar</span></a>

    <main>
        <div class="container mb-4">
            <div class="h1 mt-4">Generar Contenido - Crear</div>
        </div>
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">X</button>

            </div>
        @endif
        <div id="alerts">

        </div>
        <input type="hidden" value="{{ url('blog-content-highlight') }}" id="url" />

        <div class="container">
            <div class="row">
                <div class="col-12 mb-2">
                    <div class="form-group">
                        <label>Grupo Asignado</label>
                        <select class="form-control" id="form-type-group">
                            <optgroup>Seleccione el tipo</optgroup>
                            @foreach (App\Models\BlogContentType::all() as $index => $data)
                                <option value="{{ $data->type }}">{{ $data->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-2">
                    <div class="form-group">
                        <label>Tipo</label>
                        <select class="form-control" id="form-type">
                            <optgroup>Seleccione el tipo</optgroup>
                            @foreach (App\Models\BlogContentType::all() as $index => $data)
                                <option value="{{ $data->type }}">{{ $data->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-2">
                    <div class="form-group">
                        <label>Titulo </label>
                        <input type="text" class="form-control" id="form-title" placeholder="Ingresa el titulo"
                            value="LENGUAJE DE PROGRAMACI&Oacute;N">
                    </div>
                </div>
                <div class="col-12 mb-2">
                    <div class="form-group">
                        <label>Sub Titulo </label>
                        <input type="text" id="form-subtitle" class="form-control" placeholder="Ingresa el subtitulo"
                            value="JavaScript">
                    </div>
                </div>
                <div class="col-12 mb-2">
                    <div class="form-group">
                        <label>Color</label>
                        <input type="color" class="form-control" id="form-bg-color" placeholder="Ingresa color"
                            value="#cab500">
                    </div>
                </div>
                <div class="col-12 mb-2">
                    <div class="form-group">
                        <label>Background Content</label>
                        <select class="form-control" id="form-background">
                            <optgroup>Seleccione el tipo</optgroup>
                            @foreach (App\Models\BlogContentType::all() as $index => $data)
                                <option attr-type-highlight="{{ $data->highlight }}" attr-type="{{ $data->type }}"
                                    attr-bg-color="{{ $data->background }}"
                                    attr-title="{{ strtoupper($data->description) }}" attr-subtitle="{{ $data->name }}"
                                    value="{{ $data->image }}" {{ $data->name == 'Javascript' ? 'selected' : '' }}>
                                    {{ $data->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 mb-2">
                    <div class="form-group">
                        <label>Altura</label>
                        <input type="number" class="form-control" id="form-height" placeholder="Ingresa la altura" value="320">
                    </div>
                </div>
                <div class="col-12 mb-2">
                    <div class="form-group">
                        <label>Posición</label>
                        <input type="text" class="form-control" id="form-position" placeholder="Ingresa posición" value="1">
                    </div>
                </div>
                <div class="col-12 mb-2">
                    <div class="form-group">
                        <label for="form-activated" class="mr-2">Estado</label>
                        <input type="checkbox"  id="form-activated" placeholder="Ingresa posición" >
                    </div>
                </div>
            </div>
            <div class="row mt-3 mb-3">
                <x-card-publish id=""  borderColor="" height="" lineHeight="" preview="" before="0" position="0" codes="" title="" subtitle="" image="" status="false"></x-card-publish>
            </div>
            <div class="row mt-3 mb-3">
                <div class="col-12">
                    <div class="content-images" id="content-images"></div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-12  ">
                    <button class="btn btn-primary mb-2 w-100" onclick="capture()">
                        Generar Imagen
                    </button>
                    <button class="btn btn-primary mb-2  w-100 d-none" id="download-canvas">
                        Descargar Canvas
                    </button>
                </div>
            </div>
            <div class="row mt-3 mb-3">
                <div class="col-12 col-md-6 position-relative">
                    <div class="form-group">
                        <label for="lang-selector" class="form-label">Language</label>
                        <select id="lang-selector" class="form-select ">
                            <option>(auto)</option>
                            <option>1c</option>
                            <option>abnf</option>
                            <option>accesslog</option>
                            <option>actionscript</option>
                            <option>ada</option>
                            <option>angelscript</option>
                            <option>apache</option>
                            <option>applescript</option>
                            <option>arcade</option>
                            <option>arduino</option>
                            <option>armasm</option>
                            <option>xml</option>
                            <option>asciidoc</option>
                            <option>aspectj</option>
                            <option>autohotkey</option>
                            <option>autoit</option>
                            <option>avrasm</option>
                            <option>awk</option>
                            <option>axapta</option>
                            <option>bash</option>
                            <option>basic</option>
                            <option>bnf</option>
                            <option>brainfuck</option>
                            <option>c</option>
                            <option>cal</option>
                            <option>capnproto</option>
                            <option>ceylon</option>
                            <option>clean</option>
                            <option>clojure</option>
                            <option>clojure-repl</option>
                            <option>cmake</option>
                            <option>coffeescript</option>
                            <option>coq</option>
                            <option>cos</option>
                            <option>cpp</option>
                            <option>crmsh</option>
                            <option>crystal</option>
                            <option>csharp</option>
                            <option>csp</option>
                            <option>css</option>
                            <option>d</option>
                            <option>markdown</option>
                            <option>dart</option>
                            <option>delphi</option>
                            <option>diff</option>
                            <option>django</option>
                            <option>dns</option>
                            <option>dockerfile</option>
                            <option>dos</option>
                            <option>dsconfig</option>
                            <option>dts</option>
                            <option>dust</option>
                            <option>ebnf</option>
                            <option>elixir</option>
                            <option>elm</option>
                            <option>ruby</option>
                            <option>erb</option>
                            <option>erlang-repl</option>
                            <option>erlang</option>
                            <option>excel</option>
                            <option>fix</option>
                            <option>flix</option>
                            <option>fortran</option>
                            <option>fsharp</option>
                            <option>gams</option>
                            <option>gauss</option>
                            <option>gcode</option>
                            <option>gherkin</option>
                            <option>glsl</option>
                            <option>gml</option>
                            <option>go</option>
                            <option>golo</option>
                            <option>gradle</option>
                            <option>graphql</option>
                            <option>groovy</option>
                            <option>haml</option>
                            <option>handlebars</option>
                            <option>haskell</option>
                            <option>haxe</option>
                            <option>hsp</option>
                            <option>http</option>
                            <option>hy</option>
                            <option>inform7</option>
                            <option>ini</option>
                            <option>irpf90</option>
                            <option>isbl</option>
                            <option>java</option>
                            <option>javascript</option>
                            <option>jboss-cli</option>
                            <option>json</option>
                            <option>julia</option>
                            <option>julia-repl</option>
                            <option>kotlin</option>
                            <option>lasso</option>
                            <option>latex</option>
                            <option>ldif</option>
                            <option>leaf</option>
                            <option>less</option>
                            <option>lisp</option>
                            <option>livecodeserver</option>
                            <option>livescript</option>
                            <option>llvm</option>
                            <option>lsl</option>
                            <option>lua</option>
                            <option>makefile</option>
                            <option>mathematica</option>
                            <option>matlab</option>
                            <option>maxima</option>
                            <option>mel</option>
                            <option>mercury</option>
                            <option>mipsasm</option>
                            <option>mizar</option>
                            <option>perl</option>
                            <option>mojolicious</option>
                            <option>monkey</option>
                            <option>moonscript</option>
                            <option>n1ql</option>
                            <option>nestedtext</option>
                            <option>nginx</option>
                            <option>nim</option>
                            <option>nix</option>
                            <option>node-repl</option>
                            <option>nsis</option>
                            <option>objectivec</option>
                            <option>ocaml</option>
                            <option>openscad</option>
                            <option>oxygene</option>
                            <option>parser3</option>
                            <option>pf</option>
                            <option>pgsql</option>
                            <option>php</option>
                            <option>php-template</option>
                            <option>plaintext</option>
                            <option>pony</option>
                            <option>powershell</option>
                            <option>processing</option>
                            <option>profile</option>
                            <option>prolog</option>
                            <option>properties</option>
                            <option>protobuf</option>
                            <option>puppet</option>
                            <option>purebasic</option>
                            <option>python</option>
                            <option>python-repl</option>
                            <option>q</option>
                            <option>qml</option>
                            <option>r</option>
                            <option>reasonml</option>
                            <option>rib</option>
                            <option>roboconf</option>
                            <option>routeros</option>
                            <option>rsl</option>
                            <option>ruleslanguage</option>
                            <option>rust</option>
                            <option>sas</option>
                            <option>scala</option>
                            <option>scheme</option>
                            <option>scilab</option>
                            <option>scss</option>
                            <option>shell</option>
                            <option>smali</option>
                            <option>smalltalk</option>
                            <option>sml</option>
                            <option>sqf</option>
                            <option>sql</option>
                            <option>stan</option>
                            <option>stata</option>
                            <option>step21</option>
                            <option>stylus</option>
                            <option>subunit</option>
                            <option>swift</option>
                            <option>taggerscript</option>
                            <option>yaml</option>
                            <option>tap</option>
                            <option>tcl</option>
                            <option>thrift</option>
                            <option>tp</option>
                            <option>twig</option>
                            <option>typescript</option>
                            <option>vala</option>
                            <option>vbnet</option>
                            <option>vbscript</option>
                            <option>vbscript-html</option>
                            <option>verilog</option>
                            <option>vhdl</option>
                            <option>vim</option>
                            <option>wasm</option>
                            <option>wren</option>
                            <option>x86asm</option>
                            <option>xl</option>
                            <option>xquery</option>
                            <option>zephir</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="theme-selector" class="mb-2">Theme</label>
                        <select id="theme-selector" class="form-control">
                            <option>a11y-dark</option>
                            <option>a11y-dark-min</option>
                            <option>a11y-light</option>
                            <option>a11y-light-min</option>
                            <option>agate</option>
                            <option>agate-min</option>
                            <option>an-old-hope</option>
                            <option>an-old-hope-min</option>
                            <option>androidstudio</option>
                            <option>androidstudio-min</option>
                            <option>arduino-light</option>
                            <option>arduino-light-min</option>
                            <option>arta</option>
                            <option>arta-min</option>
                            <option>ascetic</option>
                            <option>ascetic-min</option>
                            <option>atom-one-dark-reasonable</option>
                            <option>atom-one-dark-reasonable-min</option>
                            <option>atom-one-dark</option>
                            <option>atom-one-dark-min</option>
                            <option>atom-one-light</option>
                            <option>atom-one-light-min</option>
                            <option>base16-3024</option>
                            <option>base16-3024-min</option>
                            <option>base16-apathy</option>
                            <option>base16-apathy-min</option>
                            <option>base16-apprentice</option>
                            <option>base16-apprentice-min</option>
                            <option>base16-ashes</option>
                            <option>base16-ashes-min</option>
                            <option>base16-atelier-cave-light</option>
                            <option>base16-atelier-cave-light-min</option>
                            <option>base16-atelier-cave</option>
                            <option>base16-atelier-cave-min</option>
                            <option>base16-atelier-dune-light</option>
                            <option>base16-atelier-dune-light-min</option>
                            <option>base16-atelier-dune</option>
                            <option>base16-atelier-dune-min</option>
                            <option>base16-atelier-estuary-light</option>
                            <option>base16-atelier-estuary-light-min</option>
                            <option>base16-atelier-estuary</option>
                            <option>base16-atelier-estuary-min</option>
                            <option>base16-atelier-forest-light</option>
                            <option>base16-atelier-forest-light-min</option>
                            <option>base16-atelier-forest</option>
                            <option>base16-atelier-forest-min</option>
                            <option>base16-atelier-heath-light</option>
                            <option>base16-atelier-heath-light-min</option>
                            <option>base16-atelier-heath</option>
                            <option>base16-atelier-heath-min</option>
                            <option>base16-atelier-lakeside-light</option>
                            <option>base16-atelier-lakeside-light-min</option>
                            <option>base16-atelier-lakeside</option>
                            <option>base16-atelier-lakeside-min</option>
                            <option>base16-atelier-plateau-light</option>
                            <option>base16-atelier-plateau-light-min</option>
                            <option>base16-atelier-plateau</option>
                            <option>base16-atelier-plateau-min</option>
                            <option>base16-atelier-savanna-light</option>
                            <option>base16-atelier-savanna-light-min</option>
                            <option>base16-atelier-savanna</option>
                            <option>base16-atelier-savanna-min</option>
                            <option>base16-atelier-seaside-light</option>
                            <option>base16-atelier-seaside-light-min</option>
                            <option>base16-atelier-seaside</option>
                            <option>base16-atelier-seaside-min</option>
                            <option>base16-atelier-sulphurpool-light</option>
                            <option>base16-atelier-sulphurpool-light-min</option>
                            <option>base16-atelier-sulphurpool</option>
                            <option>base16-atelier-sulphurpool-min</option>
                            <option>base16-atlas</option>
                            <option>base16-atlas-min</option>
                            <option>base16-bespin</option>
                            <option>base16-bespin-min</option>
                            <option>base16-black-metal-bathory</option>
                            <option>base16-black-metal-bathory-min</option>
                            <option>base16-black-metal-burzum</option>
                            <option>base16-black-metal-burzum-min</option>
                            <option>base16-black-metal-dark-funeral</option>
                            <option>base16-black-metal-dark-funeral-min</option>
                            <option>base16-black-metal-gorgoroth</option>
                            <option>base16-black-metal-gorgoroth-min</option>
                            <option>base16-black-metal-immortal</option>
                            <option>base16-black-metal-immortal-min</option>
                            <option>base16-black-metal-khold</option>
                            <option>base16-black-metal-khold-min</option>
                            <option>base16-black-metal-marduk</option>
                            <option>base16-black-metal-marduk-min</option>
                            <option>base16-black-metal-mayhem</option>
                            <option>base16-black-metal-mayhem-min</option>
                            <option>base16-black-metal-nile</option>
                            <option>base16-black-metal-nile-min</option>
                            <option>base16-black-metal-venom</option>
                            <option>base16-black-metal-venom-min</option>
                            <option>base16-black-metal</option>
                            <option>base16-black-metal-min</option>
                            <option>base16-brewer</option>
                            <option>base16-brewer-min</option>
                            <option>base16-bright</option>
                            <option>base16-bright-min</option>
                            <option>base16-brogrammer</option>
                            <option>base16-brogrammer-min</option>
                            <option>base16-brush-trees-dark</option>
                            <option>base16-brush-trees-dark-min</option>
                            <option>base16-brush-trees</option>
                            <option>base16-brush-trees-min</option>
                            <option>base16-chalk</option>
                            <option>base16-chalk-min</option>
                            <option>base16-circus</option>
                            <option>base16-circus-min</option>
                            <option>base16-classic-dark</option>
                            <option>base16-classic-dark-min</option>
                            <option>base16-classic-light</option>
                            <option>base16-classic-light-min</option>
                            <option>base16-codeschool</option>
                            <option>base16-codeschool-min</option>
                            <option>base16-colors</option>
                            <option>base16-colors-min</option>
                            <option>base16-cupcake</option>
                            <option>base16-cupcake-min</option>
                            <option>base16-cupertino</option>
                            <option>base16-cupertino-min</option>
                            <option>base16-danqing</option>
                            <option>base16-danqing-min</option>
                            <option>base16-darcula</option>
                            <option>base16-darcula-min</option>
                            <option>base16-dark-violet</option>
                            <option>base16-dark-violet-min</option>
                            <option>base16-darkmoss</option>
                            <option>base16-darkmoss-min</option>
                            <option>base16-darktooth</option>
                            <option>base16-darktooth-min</option>
                            <option>base16-decaf</option>
                            <option>base16-decaf-min</option>
                            <option>base16-default-dark</option>
                            <option>base16-default-dark-min</option>
                            <option>base16-default-light</option>
                            <option>base16-default-light-min</option>
                            <option>base16-dirtysea</option>
                            <option>base16-dirtysea-min</option>
                            <option>base16-dracula</option>
                            <option>base16-dracula-min</option>
                            <option>base16-edge-dark</option>
                            <option>base16-edge-dark-min</option>
                            <option>base16-edge-light</option>
                            <option>base16-edge-light-min</option>
                            <option>base16-eighties</option>
                            <option>base16-eighties-min</option>
                            <option>base16-embers</option>
                            <option>base16-embers-min</option>
                            <option>base16-equilibrium-dark</option>
                            <option>base16-equilibrium-dark-min</option>
                            <option>base16-equilibrium-gray-dark</option>
                            <option>base16-equilibrium-gray-dark-min</option>
                            <option>base16-equilibrium-gray-light</option>
                            <option>base16-equilibrium-gray-light-min</option>
                            <option>base16-equilibrium-light</option>
                            <option>base16-equilibrium-light-min</option>
                            <option>base16-espresso</option>
                            <option>base16-espresso-min</option>
                            <option>base16-eva-dim</option>
                            <option>base16-eva-dim-min</option>
                            <option>base16-eva</option>
                            <option>base16-eva-min</option>
                            <option>base16-flat</option>
                            <option>base16-flat-min</option>
                            <option>base16-framer</option>
                            <option>base16-framer-min</option>
                            <option>base16-fruit-soda</option>
                            <option>base16-fruit-soda-min</option>
                            <option>base16-gigavolt</option>
                            <option>base16-gigavolt-min</option>
                            <option>base16-github</option>
                            <option>base16-github-min</option>
                            <option>base16-google-dark</option>
                            <option>base16-google-dark-min</option>
                            <option>base16-google-light</option>
                            <option>base16-google-light-min</option>
                            <option>base16-grayscale-dark</option>
                            <option>base16-grayscale-dark-min</option>
                            <option>base16-grayscale-light</option>
                            <option>base16-grayscale-light-min</option>
                            <option>base16-green-screen</option>
                            <option>base16-green-screen-min</option>
                            <option>base16-gruvbox-dark-hard</option>
                            <option>base16-gruvbox-dark-hard-min</option>
                            <option>base16-gruvbox-dark-medium</option>
                            <option>base16-gruvbox-dark-medium-min</option>
                            <option>base16-gruvbox-dark-pale</option>
                            <option>base16-gruvbox-dark-pale-min</option>
                            <option>base16-gruvbox-dark-soft</option>
                            <option>base16-gruvbox-dark-soft-min</option>
                            <option>base16-gruvbox-light-hard</option>
                            <option>base16-gruvbox-light-hard-min</option>
                            <option>base16-gruvbox-light-medium</option>
                            <option>base16-gruvbox-light-medium-min</option>
                            <option>base16-gruvbox-light-soft</option>
                            <option>base16-gruvbox-light-soft-min</option>
                            <option>base16-hardcore</option>
                            <option>base16-hardcore-min</option>
                            <option>base16-harmonic16-dark</option>
                            <option>base16-harmonic16-dark-min</option>
                            <option>base16-harmonic16-light</option>
                            <option>base16-harmonic16-light-min</option>
                            <option>base16-heetch-dark</option>
                            <option>base16-heetch-dark-min</option>
                            <option>base16-heetch-light</option>
                            <option>base16-heetch-light-min</option>
                            <option>base16-helios</option>
                            <option>base16-helios-min</option>
                            <option>base16-hopscotch</option>
                            <option>base16-hopscotch-min</option>
                            <option>base16-horizon-dark</option>
                            <option>base16-horizon-dark-min</option>
                            <option>base16-horizon-light</option>
                            <option>base16-horizon-light-min</option>
                            <option>base16-humanoid-dark</option>
                            <option>base16-humanoid-dark-min</option>
                            <option>base16-humanoid-light</option>
                            <option>base16-humanoid-light-min</option>
                            <option>base16-ia-dark</option>
                            <option>base16-ia-dark-min</option>
                            <option>base16-ia-light</option>
                            <option>base16-ia-light-min</option>
                            <option>base16-icy-dark</option>
                            <option>base16-icy-dark-min</option>
                            <option>base16-ir-black</option>
                            <option>base16-ir-black-min</option>
                            <option>base16-isotope</option>
                            <option>base16-isotope-min</option>
                            <option>base16-kimber</option>
                            <option>base16-kimber-min</option>
                            <option>base16-london-tube</option>
                            <option>base16-london-tube-min</option>
                            <option>base16-macintosh</option>
                            <option>base16-macintosh-min</option>
                            <option>base16-marrakesh</option>
                            <option>base16-marrakesh-min</option>
                            <option>base16-materia</option>
                            <option>base16-materia-min</option>
                            <option>base16-material-darker</option>
                            <option>base16-material-darker-min</option>
                            <option>base16-material-lighter</option>
                            <option>base16-material-lighter-min</option>
                            <option>base16-material-palenight</option>
                            <option>base16-material-palenight-min</option>
                            <option>base16-material-vivid</option>
                            <option>base16-material-vivid-min</option>
                            <option>base16-material</option>
                            <option>base16-material-min</option>
                            <option>base16-mellow-purple</option>
                            <option>base16-mellow-purple-min</option>
                            <option>base16-mexico-light</option>
                            <option>base16-mexico-light-min</option>
                            <option>base16-mocha</option>
                            <option>base16-mocha-min</option>
                            <option>base16-monokai</option>
                            <option>base16-monokai-min</option>
                            <option>base16-nebula</option>
                            <option>base16-nebula-min</option>
                            <option>base16-nord</option>
                            <option>base16-nord-min</option>
                            <option>base16-nova</option>
                            <option>base16-nova-min</option>
                            <option>base16-ocean</option>
                            <option>base16-ocean-min</option>
                            <option>base16-oceanicnext</option>
                            <option>base16-oceanicnext-min</option>
                            <option>base16-one-light</option>
                            <option>base16-one-light-min</option>
                            <option>base16-onedark</option>
                            <option>base16-onedark-min</option>
                            <option>base16-outrun-dark</option>
                            <option>base16-outrun-dark-min</option>
                            <option>base16-papercolor-dark</option>
                            <option>base16-papercolor-dark-min</option>
                            <option>base16-papercolor-light</option>
                            <option>base16-papercolor-light-min</option>
                            <option>base16-paraiso</option>
                            <option>base16-paraiso-min</option>
                            <option>base16-pasque</option>
                            <option>base16-pasque-min</option>
                            <option>base16-phd</option>
                            <option>base16-phd-min</option>
                            <option>base16-pico</option>
                            <option>base16-pico-min</option>
                            <option>base16-pop</option>
                            <option>base16-pop-min</option>
                            <option>base16-porple</option>
                            <option>base16-porple-min</option>
                            <option>base16-qualia</option>
                            <option>base16-qualia-min</option>
                            <option>base16-railscasts</option>
                            <option>base16-railscasts-min</option>
                            <option>base16-rebecca</option>
                            <option>base16-rebecca-min</option>
                            <option>base16-ros-pine-dawn</option>
                            <option>base16-ros-pine-dawn-min</option>
                            <option>base16-ros-pine-moon</option>
                            <option>base16-ros-pine-moon-min</option>
                            <option>base16-ros-pine</option>
                            <option>base16-ros-pine-min</option>
                            <option>base16-sagelight</option>
                            <option>base16-sagelight-min</option>
                            <option>base16-sandcastle</option>
                            <option>base16-sandcastle-min</option>
                            <option>base16-seti-ui</option>
                            <option>base16-seti-ui-min</option>
                            <option>base16-shapeshifter</option>
                            <option>base16-shapeshifter-min</option>
                            <option>base16-silk-dark</option>
                            <option>base16-silk-dark-min</option>
                            <option>base16-silk-light</option>
                            <option>base16-silk-light-min</option>
                            <option>base16-snazzy</option>
                            <option>base16-snazzy-min</option>
                            <option>base16-solar-flare-light</option>
                            <option>base16-solar-flare-light-min</option>
                            <option>base16-solar-flare</option>
                            <option>base16-solar-flare-min</option>
                            <option>base16-solarized-dark</option>
                            <option>base16-solarized-dark-min</option>
                            <option>base16-solarized-light</option>
                            <option>base16-solarized-light-min</option>
                            <option>base16-spacemacs</option>
                            <option>base16-spacemacs-min</option>
                            <option>base16-summercamp</option>
                            <option>base16-summercamp-min</option>
                            <option>base16-summerfruit-dark</option>
                            <option>base16-summerfruit-dark-min</option>
                            <option>base16-summerfruit-light</option>
                            <option>base16-summerfruit-light-min</option>
                            <option>base16-synth-midnight-terminal-dark</option>
                            <option>base16-synth-midnight-terminal-dark-min</option>
                            <option>base16-synth-midnight-terminal-light</option>
                            <option>base16-synth-midnight-terminal-light-min</option>
                            <option>base16-tango</option>
                            <option>base16-tango-min</option>
                            <option>base16-tender</option>
                            <option>base16-tender-min</option>
                            <option>base16-tomorrow-night</option>
                            <option>base16-tomorrow-night-min</option>
                            <option>base16-tomorrow</option>
                            <option>base16-tomorrow-min</option>
                            <option>base16-twilight</option>
                            <option>base16-twilight-min</option>
                            <option>base16-unikitty-dark</option>
                            <option>base16-unikitty-dark-min</option>
                            <option>base16-unikitty-light</option>
                            <option>base16-unikitty-light-min</option>
                            <option>base16-vulcan</option>
                            <option>base16-vulcan-min</option>
                            <option>base16-windows-10-light</option>
                            <option>base16-windows-10-light-min</option>
                            <option>base16-windows-10</option>
                            <option>base16-windows-10-min</option>
                            <option>base16-windows-95-light</option>
                            <option>base16-windows-95-light-min</option>
                            <option>base16-windows-95</option>
                            <option>base16-windows-95-min</option>
                            <option>base16-windows-high-contrast-light</option>
                            <option>base16-windows-high-contrast-light-min</option>
                            <option>base16-windows-high-contrast</option>
                            <option>base16-windows-high-contrast-min</option>
                            <option>base16-windows-nt-light</option>
                            <option>base16-windows-nt-light-min</option>
                            <option>base16-windows-nt</option>
                            <option>base16-windows-nt-min</option>
                            <option>base16-woodland</option>
                            <option>base16-woodland-min</option>
                            <option>base16-xcode-dusk</option>
                            <option>base16-xcode-dusk-min</option>
                            <option>base16-zenburn</option>
                            <option>base16-zenburn-min</option>
                            <option>brown-paper</option>
                            <option>brown-paper-min</option>
                            <option>codepen-embed</option>
                            <option>codepen-embed-min</option>
                            <option>color-brewer</option>
                            <option>color-brewer-min</option>
                            <option>dark</option>
                            <option>dark-min</option>
                            <option>default</option>
                            <option>default-min</option>
                            <option>devibeans</option>
                            <option>devibeans-min</option>
                            <option>docco</option>
                            <option>docco-min</option>
                            <option>far</option>
                            <option>far-min</option>
                            <option>felipec</option>
                            <option>felipec-min</option>
                            <option>foundation</option>
                            <option>foundation-min</option>
                            <option>github-dark-dimmed</option>
                            <option>github-dark-dimmed-min</option>
                            <option>github-dark</option>
                            <option>github-dark-min</option>
                            <option>github</option>
                            <option>github-min</option>
                            <option>gml</option>
                            <option>gml-min</option>
                            <option>googlecode</option>
                            <option>googlecode-min</option>
                            <option>gradient-dark</option>
                            <option>gradient-dark-min</option>
                            <option>gradient-light</option>
                            <option>gradient-light-min</option>
                            <option>grayscale</option>
                            <option>grayscale-min</option>
                            <option>hybrid</option>
                            <option>hybrid-min</option>
                            <option>idea</option>
                            <option>idea-min</option>
                            <option>intellij-light</option>
                            <option>intellij-light-min</option>
                            <option>ir-black</option>
                            <option>ir-black-min</option>
                            <option>isbl-editor-dark</option>
                            <option>isbl-editor-dark-min</option>
                            <option>isbl-editor-light</option>
                            <option>isbl-editor-light-min</option>
                            <option>kimbie-dark</option>
                            <option>kimbie-dark-min</option>
                            <option>kimbie-light</option>
                            <option>kimbie-light-min</option>
                            <option>lightfair</option>
                            <option>lightfair-min</option>
                            <option>lioshi</option>
                            <option>lioshi-min</option>
                            <option>magula</option>
                            <option>magula-min</option>
                            <option>mono-blue</option>
                            <option>mono-blue-min</option>
                            <option>monokai-sublime</option>
                            <option>monokai-sublime-min</option>
                            <option>monokai</option>
                            <option>monokai-min</option>
                            <option>night-owl</option>
                            <option>night-owl-min</option>
                            <option>nnfx-dark</option>
                            <option>nnfx-dark-min</option>
                            <option>nnfx-light</option>
                            <option>nnfx-light-min</option>
                            <option>nord</option>
                            <option>nord-min</option>
                            <option>obsidian</option>
                            <option>obsidian-min</option>
                            <option>panda-syntax-dark</option>
                            <option>panda-syntax-dark-min</option>
                            <option>panda-syntax-light</option>
                            <option>panda-syntax-light-min</option>
                            <option>paraiso-dark</option>
                            <option>paraiso-dark-min</option>
                            <option>paraiso-light</option>
                            <option>paraiso-light-min</option>
                            <option>pojoaque</option>
                            <option>pojoaque-min</option>
                            <option>purebasic</option>
                            <option>purebasic-min</option>
                            <option>qtcreator-dark</option>
                            <option>qtcreator-dark-min</option>
                            <option>qtcreator-light</option>
                            <option>qtcreator-light-min</option>
                            <option>rainbow</option>
                            <option>rainbow-min</option>
                            <option>routeros</option>
                            <option>routeros-min</option>
                            <option>school-book</option>
                            <option>school-book-min</option>
                            <option>shades-of-purple</option>
                            <option>shades-of-purple-min</option>
                            <option>srcery</option>
                            <option>srcery-min</option>
                            <option>stackoverflow-dark</option>
                            <option>stackoverflow-dark-min</option>
                            <option>stackoverflow-light</option>
                            <option>stackoverflow-light-min</option>
                            <option>sunburst</option>
                            <option>sunburst-min</option>
                            <option>tokyo-night-dark</option>
                            <option>tokyo-night-dark-min</option>
                            <option>tokyo-night-light</option>
                            <option>tokyo-night-light-min</option>
                            <option>tomorrow-night-blue</option>
                            <option>tomorrow-night-blue-min</option>
                            <option>tomorrow-night-bright</option>
                            <option>tomorrow-night-bright-min</option>
                            <option selected>vs</option>
                            <option>vs-min</option>
                            <option>vs2015</option>
                            <option>vs2015-min</option>
                            <option>xcode</option>
                            <option>xcode-min</option>
                            <option>xt256</option>
                            <option>xt256-min</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="form-editor" class="mb-2">Code</label>
                        <textarea id="form-editor" class="form-control" style="height: 500px; min-height: 500px; font-family: monospace;"></textarea>
                    </div>
                </div>

                <div class="col-12 col-md-6 " style="min-height: 6rem;">
                    <pre class="pre-hljs" class="mb-0 p-4 d-block"
                        style="min-height: 100%; overflow-auto; position:relative;    border-radius: 10px;">
                        <span class="hljs" style="background: transparent!important;">
                            <code></code>
                        </span>
                        <small class="text-white text-uppercase" style="position: absolute; top: 0px; right: 20px;">
                            <span class="visually">Language:</span><span id="language">JavaScript</span>
                        </small>
                    </pre>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-12  ">
                    <button class="btn btn-success mb-2 w-100"  id="save">
                        Guardar
                    </button>
                </div>
            </div>
        </div>
    </main>
@stop
@section('script-bottom')
    @parent
    <script src="{{ Utils::getUrlBase('admin') }}/library/highlight.js/11.4.0/highlight.min.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/library/html2canvas/html2canvas.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-highlight.js?v=<?php echo rand(); ?>"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-highlight-common.js?v=<?php echo rand(); ?>"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-highlight-create.js?v=<?php echo rand(); ?>"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/highlight.js?v=<?php echo rand(); ?>"></script>
@stop
<!--https://codepen.io/syedhuss/embed/WNxeaYL/?theme-id=modal-->
