@extends(GlobalClass::getDomainFolder(2) . '::base.index')
@section('metatags')
    @parent
    <!-- some master css here -->
    <title>Contenido | Administrador</title>
@stop
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog.css?v=<?php echo rand(); ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/styles/vs.min.css">
    <link rel="stylesheet" href="{{ Utils::getUrlBase('admin') }}/library/highlight.js/theme.css">
    <link rel="stylesheet" href="{{ Utils::getUrlBase('admin') }}/css/blog-content-highlight.css">
    <!-- Swiper CSS -->
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <style type="text/css">
        :root {
            --main-color-blue: #0652dd;
        }

        html,
        body {
            font-family: "Poppins", sans-serif;
        }

        body {
            background: var(--main-color-blue);
        }
    </style>

    <script type="text/javascript"></script>

    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-base.css?v=<?php echo rand(); ?>" rel="stylesheet">

    <script type="text/javascript">
        window.baseContentType = '{{ Utils::getUrlBase('admin') }}/img/blog/content-type/';
        window.base = '{{ Utils::getUrlBase('admin') }}';

        document.addEventListener("DOMContentLoaded", function(event) {
            // Your inline scripts which uses methods from external-scripts.
            new window.exports.SamiUtils.Highlight().init()
            console.log(new window.exports.SamiUtils.Utils().hello('3'))

        });



        function changeOnType(path, page) {

            // Obtén los parámetros de la URL
            let params = new URLSearchParams(window.location.search);
            // Paso 1: Parsear la URL actual
            const url = new URL(window.location.href);

            // Obtener el parámetro 'path' actual
            let currentPath = params.get('path');

            // Paso 2: Eliminar el parámetro 'path'
            url.searchParams.delete('path');

            // Verificar si el nuevo 'path' es diferente del actual o si no existe
            if (path && path !== currentPath) {
                // Si el nuevo 'path' es diferente, actualizarlo y reiniciar 'page' a 0
                url.searchParams.set('path', path);
                url.searchParams.set('page', '0'); // Reiniciar 'page' a 0
            } else if (!path) {
                // Si 'path' no se proporciona, simplemente elimina 'path' y no alteres 'page'
                url.searchParams.delete('path');
            } else {
                // Si 'path' no cambia, conserva el valor actual de 'page' o cualquier otra lógica necesaria
                let currentPage = params.get('page') || '0'; // Usa el valor actual de 'page' o establece a 0 si no existe
                url.searchParams.set('page', currentPage);
            }

            // Actualizar 'value' con la nueva URL
            let value = url.href;

            // Recorre los parámetros y muéstralos en la consola
            params.forEach((value, key) => {
                console.log(`${key}: ${value}`);
            });

            // Actualizar la ubicación con la nueva URL
            window.location.href = value;
        }
    </script>
@stop

@section('content')
    @parent
    <div class="content-images d-none" id="content-images"></div>
    <!-- Main content goes here -->


    <section class="blog-index__section-title-header mt-3">
        <div class="h1 text-center text-white font-weight-bolder">Administrador - Mis contenidos</div>
    </section>

    <div class="row mt-4 mb-2">
        <x-button-back link="{{ url('/') }}" />
    </div>
    <section class="mt-3 mb-3" style="margin-top:3rem!important;">
        <div class="col-4 pb-4 pt-4">
            <a href="{{ url('blog-content-highlight/create') }}" class="btn btn-primary mb-2">
                + Agregar
            </a>
            <a class="btn btn-success mb-2" href="{{ Utils::getUrlBase('admin') }}/api/getOpenAITip?language=es&tech={{str_replace('-', ' ', $path)}}" target="_blank">
                + Crear Tip
            </a>
        </div>
        <div class="col-4">
            <div class="form-group">
                <select class="form-control"
                    onchange="changeOnType(this.options[this.selectedIndex].value , '{{ $page }}')">
                    <!-- -->
                    <option value="">Seleccione el tipo</option>
                    @foreach (App\Models\BlogContentType::all() as $index => $data)
                        <option value="{{ $data->path }}" {{ $path == $data->path ? 'selected="selected"' : '' }}>
                            {{ $data->name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4">
        </div>
    </section>

    <section class="row col-12 mb-3">
        @foreach ($contents as $content)
            <div class="col-6 mb-2 slider-card-publish">
                <x-slider-card-publish :content="$content" :status="false" :type="'admin'"></x-slider-card-publish>
            </div>
        @endforeach

        @empty(!$contents)
            {!! $contents->links() !!}
        @endempty
    </section>
    <section class="d-none">
        <div class="content-images" id="content-images"></div>
    </section>
@stop
@section('script-bottom')
    @parent
    <!-- more javascript -->
    <script src="{{ Utils::getUrlBase('admin') }}/library/highlight.js/11.4.0/highlight.min.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/library/html2canvas/html2canvas.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-highlight.js?v=<?php echo rand(); ?>"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/highlight.js?v=<?php echo rand(); ?>"></script>

@stop
