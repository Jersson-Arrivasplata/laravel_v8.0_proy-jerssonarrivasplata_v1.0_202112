@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-content-type.css?v=<?php echo(rand()); ?>" rel="stylesheet">
@stop
@section('content')
    @parent
    <a href="{{ url('/') }}" class="btn btn-danger btn-back mt-3"><span>Regresar</span></a>

    <div class="admin-blog-content-type-index container mt-5">
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
            </div>
        @endif
        <div class="row">
            <div class="col">
                <p class="h2">Administrador del Blog de Tipo de Contenido</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <a href="{{ url('blog-content-type/create') }}" class="btn btn-primary mb-2">
                    + Agregar
                </a>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col table-responsive" >
                <table class="table table-light table-hover">
                    <thead class="thead-light">
                        <tr>
                            <th>N°</th>
                            <th>Identify</th>
                            <th>Nombres</th>
                            <th>Imagen</th>
                            <th>Tipos</th>
                            <th>Descripci&oacute;n</th>
                            <th>Fecha de <br />creaci&oacute;n</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($content as $index => $data)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $data->id  }}</td>
                                <td style="width: 180px;word-break: break-word;">
                                    {{ $data->name }}
                                </td>
                                <td><img style="max-width: 140px;" src="../../img/blog/content-type/{{ $data->image }}"/></td>
                                <td>{{ $data->type }}</td>
                                <td style="width: 300px;word-break: break-word;">
                                    {{ Str::limit($data->description, 100, $end = '...') }}
                                </td>
                                <td>{{ $data->created_at }}</td>
                                <td class="inline">
                                    <div class=" d-grid gap-2" style="display:grid;">
                                        <a href="{{ url('blog-content-type/' . $data->id) }}" class="btn btn-secondary mb-2">
                                            Ver
                                        </a>
                                        <a href="{{ url('blog-content-type/' . $data->id . '/edit') }}"
                                            class="btn btn-primary mb-2">
                                            Editar
                                        </a>
                                        <form method="post" action="{{ url('blog-content-type/' . $data->id) }}"
                                            style="display:inline">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-danger btn-block" type="submit"
                                                onclick="return confirm('¿Seguro en eliminar el archivo?');">Eliminar
                                                X</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <div class="col">
                @empty(!$content)
                    {!! $content->links() !!}
                @endempty
            </div>
        </div>

    </div>

@stop
@section('script-bottom')
    @parent
    <!-- more css -->
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-type.js?v=<?php echo(rand()); ?>" type="text/javascript"></script>

@stop
