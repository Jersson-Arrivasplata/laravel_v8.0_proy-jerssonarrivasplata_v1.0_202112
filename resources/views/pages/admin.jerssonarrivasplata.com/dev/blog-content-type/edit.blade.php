@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-content-type.css?v=<?php echo rand(); ?>" rel="stylesheet">




@stop
@section('content')
    @parent
    <a href="{{ url('/blog-content-type') }}" class="btn btn-danger btn-back mt-3"><span>Regresar</span></a>

    <div class="container mt-5">
        <h3>
            Blog de Tipo de Contenido
            <small class="text-"> - Editar</small>
        </h3>
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">X</button>
            </div>
        @endif
        <div id="alerts">

        </div>
        <form method="post" action="{{ url('blog-content-type.update/'.$content->id) }}" id="admin-blog-content-type"  class="dropzone"
            enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="mb-3">
                <label class="form-label">Nombre - (Ejemplo: Css)</label>
                <input type="text" name="name" class="form-control" value="{{ $content->name }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Tipo</label>
                <input type="number" name="type" class="form-control" value="{{ $content->type }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Highlight</label>
                <input type="text" name="highlight" class="form-control" value="{{ $content->highlight }}" >
            </div>
            <div class="mb-3">
                <label class="form-label">Background</label>
                <input type="text" name="background" class="form-control" value="{{ $content->background }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Imagen - (Ejemplo: Estilos - .png)  </label>
                <ul id="previewsContainer" class=" sortable dropzone-previews dropzone-border" >
                    <div class=" dropzone-container dz-message dz-message-dropzone dz-clickable" id="dropzone-click-target">
                        <p class="dropzone-container-paragraph">¡Suelta tus imagenes aqu&iacute;!</p>
                    </div>
                </ul>
            </div>
            <div class="mb-3">
                <label class="form-label">Descripci&oacute;n</label>
                <input type="text" name="description" class="form-control" value="{{ $content->description }}">

            </div>
            <a href="{{ url('blog-content-type') }}" class="btn btn-danger">Regresar</a>
            <input type="submit" id="submit" value="Editar" class="btn btn-primary"/>

        </form>
    </div>

@stop
@section('script-bottom')
    @parent
    <!-- more css -->
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-type.js?v=<?php echo rand(); ?>" type="text/javascript"></script>

@stop
