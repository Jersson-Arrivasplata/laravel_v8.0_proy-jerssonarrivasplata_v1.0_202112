@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-content-type.css?v=<?php echo(rand()); ?>" rel="stylesheet">

@stop
@section('content')
    @parent
    <a href="{{ url('/blog-content-type') }}" class="btn btn-danger btn-back mt-3"><span>Regresar</span></a>

    <div class="container mt-5">
        <h3>
            Blog de Tipo de Contenido
        </h3>
        <div class="form">
            <div class="mb-3">
                <label class="form-label">Nombre</label>
                <input type="text" name="name" class="form-control" value="{{ $content->name }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Tipo</label>
                <input type="number" name="type" class="form-control" value="{{ $content->type }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Imagen</label>
                <input type="text" name="image" class="form-control" value="{{ $content->image }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Descripci&oacute;n</label>
                <input type="text" name="description" class="form-control" value="{{ $content->description }}">

            </div>
            <div class="mb-3">
                <label class="form-label">Highlight</label>
                <input type="text" name="highlight" class="form-control" value="{{ $content->highlight }}" >
            </div>
            <div class="mb-3">
                <label class="form-label">Background</label>
                <input type="text" name="background" class="form-control" value="{{ $content->background }}">
            </div>
            <a href="{{ url('blog-content-type') }}" class="btn btn-danger">Regresar</a>
        </div>
    </div>
@stop
@section('script-bottom')
    @parent
    <!-- more css -->
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-type.js?v=<?php echo(rand()); ?>" type="text/javascript"></script>

@stop
