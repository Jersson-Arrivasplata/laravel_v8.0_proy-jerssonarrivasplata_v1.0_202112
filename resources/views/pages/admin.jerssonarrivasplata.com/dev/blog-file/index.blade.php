@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-content-type.css?v=<?php echo rand(); ?>" rel="stylesheet">

@stop
@section('content')
    @parent
    <!--<div class="my-dropzone"></div>-->

    <div class="container">
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Dropzone
                </div>
                <div class="panel-body">
                    <form action="{{ url('file') }}" method="POST" id="my-dropzone" class="dropzone"
                        enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="dz-message" style="height:200px;">
                            Drop your files here
                        </div>
                        <div class="dropzone-previews"></div>
                        <button type="submit" class="btn btn-success" id="submit">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
@section('script-bottom')
    @parent
    <!-- more css -->
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-type.js?v=<?php echo rand(); ?>" type="text/javascript"></script>

@stop
