@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link rel="stylesheet" href="{{ Utils::getUrlBase('admin') }}/library/jstree/3.2.1/style.min.css" />
    <style>
        .tree {
            position: relative;
            background: white;
            margin-top: 20px;
            padding: 30px;
            font-family: "Roboto Mono", monospace;
            font-size: 0.85rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
        }

        .tree span {
            font-size: 13px;
            font-style: italic;
            letter-spacing: 0.4px;
            color: #a8a8a8;
        }

        .tree .fa-folder-open,
        .tree .fa-folder {
            color: #007bff;
        }

        .tree .fa-html5 {
            color: #f21f10;
        }

        .tree ul {
            padding-left: 5px;
            list-style: none;
        }

        .tree ul li {
            position: relative;
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 15px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .tree ul li:before {
            position: absolute;
            top: 15px;
            left: 0;
            width: 10px;
            height: 1px;
            margin: auto;
            content: "";
            background-color: #666;
        }

        .tree ul li:after {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            width: 1px;
            height: 100%;
            content: "";
            background-color: #666;
        }

        .tree ul li:last-child:after {
            height: 15px;
        }

        .tree ul a {
            cursor: pointer;
            color: black!important;
        }

        .tree ul a:hover {
            text-decoration: none;
        }

    </style>
@stop
@section('content')
    @parent
    <div class="container mt-3">
        <div id="alerts">
            @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {!! session('success') !!}
                    <button type="button" class="btn-close" onclick="removeAlert()" aria-label="Close">X</button>
                </div>
            @endif
        </div>
        <div class="alert alert-dark  mt-3" role="alert">
            <h4 class="alert-heading">Administrador de Archivos</h4>
            <p>Se puede subir un archivo y descargar un archivos (Solo realizar click sobre un archivo.)</p>
          </div>
        <div class="tree">
            {!! $content !!}
        </div>
        <!-- Modal -->
        <div class="modal fade" id="fileModal" aria-labelledby="fileModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5 class="modal-title  mb-3" id="fileModalLabel">Descargar Archivo</h5>
                        <form method="get" action="{{ url('download') }}">
                            {{ csrf_field() }}
                            <div class="mb-3">
                                <label class="form-label">Ruta</label>
                                <input type="text" name="file_path" class="form-control">
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Descargar</button>
                        </form>

                        <h5 class="modal-title mb-3 mt-5" id="fileModalLabel">Subir Archivo</h5>

                        <form method="post" class="" action="{{ url('download') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="mb-3">
                                <label class="form-label">Ruta</label>
                                <input type="text" name="file_path" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Archivo</label>
                                <input type="file" name="file[]" class="form-control">
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
@section('script-bottom')
    @parent
    <script src="{{ Utils::getUrlBase('admin') }}/library/jstree/3.2.1/jstree.min.js"></script>
    <script type="text/javascript">

        $.fn.gparent = function( recursion ){
            console.log( 'recursion: ' + recursion );
            if( recursion > 1 ) return $(this).parent().gparent( recursion - 1 );
            return $(this).parent();
        };

        let filepath='';
        $('.tree a').on('click',function(){
           console.log($(this).text());

           filepath = $(this).data('path');
        });

        $('#fileModal').on('show.bs.modal', (event) => {

            $('input[name="file_path"]').val(filepath)
        });

        function removeAler(){
            $('#alerts').empty();
        }
    </script>

@stop
