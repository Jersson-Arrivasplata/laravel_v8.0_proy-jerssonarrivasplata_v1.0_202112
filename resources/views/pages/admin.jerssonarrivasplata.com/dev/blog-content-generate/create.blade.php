@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-create-content.css?v=<?php echo rand(); ?>" rel="stylesheet">
    <link rel="stylesheet" href="{{ Utils::getUrlBase('admin') }}/library/highlight.js/11.4.0/default.min.css">
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-content.css?v=<?php echo rand(); ?>" rel="stylesheet">
    <script>
        window.baseContentType = '{{ Utils::getUrlBase('admin') }}/img/blog/content-type/';

        /* window.blogContentGenerate = {
             create: '{{ env('APP_URL') }}'
         }*/
    </script>
       <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            // Your inline scripts which uses methods from external-scripts.
            new window.exports.SamiUtils.Highlight().init()
            console.log(new window.exports.SamiUtils.Utils().hello('3'))

        });
    </script>
    <style type="text/css">
        sami-main div.sami-main{
            background:transparent;
        }
        .card-grid .card-content-code #pre-codes{
            overflow-y: auto;
            height: 100%;
        }
    </style>
@stop
@section('content')
    @parent
    <a href="{{ url()->previous() }}" class="btn btn-danger btn-back mt-3"><span>Regresar</span></a>

    <div class="container mt-5">

        <h3>
            Generar Contenido
            <small class="text-"> - Crear</small>
        </h3>
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">X</button>

            </div>
        @endif
        <div id="alerts">

        </div>
        <input type="hidden" value="{{ url('blog-content-generate') }}" id="url" />
        <div class="row">
            <div class="col-12 ">
                <div class="form-group">
                    <label>Tipo del Grupo</label>
                    <select class="form-control" id="form-type-group">
                        <optgroup>Seleccione el tipo</optgroup>
                            @foreach (App\Models\BlogContentType::all() as $index => $data)
                                <option value="{{$data->type}}">{{$data->name}}</option>
                            @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Titulo Imagen</label>
                    <input type="text" class="form-control" id="form-img-title" placeholder="Ingresa el titulo"
                        value="LENGUAJE DE PROGRAMACI&Oacute;N">
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Sub Titulo Imagen</label>
                    <input type="text" id="form-img-subtitle" class="form-control" placeholder="Ingresa el subtitulo"
                        value="JavaScript">
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Titulo Content</label>
                    <input type="text" class="form-control" id="form-content-title"
                        placeholder="Ingresa el contenido del titulo" value="ES6 - Funci&oacute;n Flecha">
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Sub Titulo Content</label>
                    <input type="text" class="form-control" id="form-content-subtitle"
                        placeholder="Ingresa el contenido del subtitulo" value="Suma de 2 n&uacute;meros">
                </div>
            </div>
            <div class="col-12 col-sm-3">
                <div class="form-group">
                    <label>Redes Sociales </label><span id="form-select-socialmedia-measures">520x320</span>
                    <select class="form-control" id="form-select-socialmedia">
                        <option attr-width="520" attr-height="320" value="linkedIn">LinkedIn</option>
                        <option attr-width="1200" attr-height="1200" value="facebook">Facebook</option>
                        <option attr-width="1080" attr-height="1080" value="instagram" style="display: none;">Instagram</option>
                        <option attr-width="600" attr-height="335" value="twitter">Twitter</option>
                    </select>
                </div>
            </div>
            <div class="col-12 col-sm-3">
                <div class="form-group">
                    <label>Color</label>
                    <input type="color" class="form-control" id="form-bg-color" placeholder="Ingresa color"
                        value="#f7e018">
                </div>
            </div>
            <div class="col-12 col-sm-6 mt-4">
                <div class="form-group">
                    <label>Background Content</label>
                    <select class="form-control" id="form-background">
                        <optgroup>Seleccione el tipo</optgroup>
                           @foreach (App\Models\BlogContentType::all() as $index => $data)
                            <option attr-type-highlight="{{$data->highlight}}"
                                attr-type="{{$data->type}}"
                                attr-bg-color="{{$data->background}}"
                                attr-title="{{strtoupper($data->description)}}"
                                attr-subtitle="{{$data->name}}"
                                value="{{$data->image}}">{{$data->name}}</option>
                           @endforeach
                    </select>
                </div>
            </div>
            <div class="col-12">
                <div class="card-grid d-flex position-relative" id="contenido">
                    <a class="card" href="#">
                        <div class="card__background" id="card-background">
                            <img id="card-logo" src="{{ Utils::getUrlBase('admin') }}/img/blog/content-type/js-min.png">
                        </div>
                        <div class="card__content p-2">
                            <p class="card__category" id="card-img-title">Lenguaje de Programaci&oacute;n</p>
                            <h3 class="card__heading" id="card-img-subtitle">Javascript</h3>
                        </div>
                    </a>
                    <div class="card-content">
                        <h5 class="card-title  text-center mt-2" id="card-content-title">ES6 - Funci&oacute;n Flecha</h5>
                        <h6 class="card-subtitle mb-2 text-muted  text-center" id="card-content-subtitle">Suma de 2
                            n&uacute;meros</h6>
                        <a href="javascript:copy('const suma = (a,b) => a + b;')" class="text-center d-block ">
                            Copiar
                            <img src="{{ Utils::getUrlBase('admin') }}/img/icons/copy.png">
                        </a>
                        <div
                            class="card-content-code position-relative w-100 d-flex justify-content-center align-items-center">
                            <pre id="pre-codes"
                                class="w-100 d-flex justify-content-center align-content-center"><code class="xml" id="output" ></code></pre>
                            <iframe style="display: none" src="javascript:void(0);" id="output-iframe"></iframe>
                        </div>
                        <p class="blog-content-name m-0">
                            <span class="d-none">Jersson Arrivasplata Rojas</span><br />
                            <span>jerssonarrivasplata.com</span><br />
                            <span class="d-none">Celular: +51 987347691</span>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-3 mt-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="form-image-box">
                    <label class="form-check-label" for="form-image-box">
                        Activar Box
                    </label>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="form-preview-box">
                    <label class="form-check-label" for="form-preview-box">
                        Aplicar Preview
                    </label>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-4 d-none">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="form-content-border-left">
                    <label class="form-check-label" for="form-content-border-left">
                        Activar Border Left
                    </label>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-4 d-none">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="form-content-border-right" checked="true">
                    <label class="form-check-label" for="form-content-border-right">
                        Activar Border Right
                    </label>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-4 d-none">

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="form-content-direction-right">
                    <label class="form-check-label" for="form-content-direction-right">
                        Direcci&oacute;n Right
                    </label>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-4">

            </div>
            <div class="col-12 col-sm-3 mt-4">

            </div>
            <div class="col-12 col-sm-3 mt-3 d-none">
                <div class="form-group">
                    <label>Rango Imagen </label><span id="form-select-add-measures">520</span>

                    <input type="number" value="125" step="5" class="form-control" name="rangeInput" id="form-img-range"
                        min="0" max="1000">
                    <span id="form-img-range-text" class=" ml-2 d-none">125</span>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-3 d-none">
                <div class="form-group">
                    <label>Rango Content Ancho</label>
                    <input type="number" value="395" class="form-control" name="rangeContent-width"
                        id="form-content-range-width" min="0" step="5" max="1000">
                    <span id="form-content-range-width-text" class=" ml-2 d-none">395</span>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-3 d-none">
                <div class="form-group">
                    <label>Rango Content Alto</label>
                    <input type="number" value="320" class="form-control" name="rangeContent-height"
                        id="form-content-range-height" min="0" step="20" max="1000">
                    <span id="form-content-range-height-text" class=" ml-2 d-none">320</span>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-3 ">
                <div class="form-group">
                    <label>Posición</label>
                    <input type="number" value="1" class="form-control" name="position"
                        id="form-position" min="0" max="50">
                    <span id="form-position" class=" ml-2 d-none">1</span>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-3 ">
                <div class="form-group">
                    <label>Rango Image FontSize</label>
                    <input type="number" value="14" class="form-control" name="rangeImage-fontsize"
                        id="form-image-range-fontsize" min="0" max="50">
                    <span id="form-image-range-fontsize-text" class=" ml-2 d-none">14</span>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-3 d-none">
                <div class="form-group">
                    <label>Rango Content Title</label>
                    <input type="number" value="18" class="form-control" name="rangeContentTitle-fontsize"
                        id="form-content-title-range-fontsize" min="0" max="100">
                    <span id="form-content-title-range-fontsize-text" class=" ml-2 d-none">18</span>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-3 ">
                <div class="form-group">
                    <label>Rango Content FontSize</label>
                    <input type="number" value="22" class="form-control" name="rangeImage-fontsize"
                        id="form-content-range-fontsize" min="0" max="100">
                    <span id="form-content-range-fontsize-text" class=" ml-2 d-none">22</span>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-3">
                <div class="form-group">
                    <label>Rango Content Bottom FontSize</label>
                    <input type="number" value="10" class="form-control" name="rangeContentBottom-fontsize"
                        id="form-content-bottom-range-fontsize" min="0" max="30">
                    <span id="form-content-bottom-range-fontsize-text" class=" ml-2 d-none">10</span>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-3">
                <div class="form-group">
                    <label>Rango Content Margin Top</label>
                    <input type="number"  value="0" min="0" max="1000" class="form-control" name="rangeContentMarginTop"
                        id="form-content-margin-top" >
                    <span id="form-content-margin-top" class=" ml-2 d-none">*px</span>
                </div>
            </div>
            <div class="col-12 col-sm-3 mt-3">
                <div class="form-group">
                    <label>Rango Content Padding Left</label>
                    <input type="number"  value="0" min="0" max="1000" class="form-control" name="rangeContentPaddingLeft"
                        id="form-content-padding-left" >
                    <span id="form-content-padding-left" class=" ml-2 d-none">*px</span>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label>C&oacute;digo</label>
                    <textarea rows="5" class="form-control" id="form-code"
                        placeholder="Ingresa c&oacute;digo" style="    resize: auto;">const suma = (a,b) => a + b;</textarea>
                </div>
            </div>
            <div class="col-12 mt-4">
                <button class="btn btn-primary mb-2" onclick="capture()">
                    Generar Imagen
                </button>

                <button class="btn btn-primary mb-2  " id="download-canvas">
                    <!-- -->
                    Descargar Canvas
                </button>

                <button class="btn btn-success mb-2  " id="save">
                    Guardar
                </button>
                <button class="btn btn-info mb-2 text-white d-none" id="addTable">
                    Agregar a la tabla
                </button>
                <button class="btn btn-info mb-2 text-white d-none" id="cleanTable">
                    Limpiar la tabla
                </button>
            </div>
            <div class="col-12">
                <div class="content-images mt-4" id="content-images"></div>
            </div>
            <div class="col-12 d-none">
                <button class="btn btn-primary mr-2" id="addition-btn">Addition</button>
                <button class="btn btn-primary mr-2" id="overlay-btn">Overlay</button>
                <button class="btn btn-primary mr-2" id="softlight-btn">Soft Light</button>
            </div>
            <div class="col-3 d-none">
                <div class="form-group">
                    <button class="btn btn-primary mr-2" id="normal-btn">Normal</button>
                </div>
            </div>
            <div class="col-3 d-none">
                <div class="form-group">
                    <button class="btn btn-primary mr-2" id="multiply-btn">Multiply</button>
                </div>
            </div>
            <div class="col-3 d-none">
                <div class="form-group">
                    <button class="btn btn-primary mr-2" id="screen-btn">Screen</button>
                </div>
            </div>
            <div class="col-3 d-none">
                <div class="form-group">
                    <button class="btn btn-primary mr-2" id="difference-btn">Difference</button>
                </div>
            </div>
            <div class="col-3 d-none">
                <div class="form-group">
                    <button class="btn btn-primary mr-2" id="exclusion-btn">Exclusion</button>
                </div>
            </div>
            <div class="col-3 d-none">
                <div class="form-group">
                    <button class="btn btn-primary mr-2" id="lighten-btn">Lighten</button>
                </div>
            </div>
            <div class="col-3 d-none">
                <div class="form-group">
                    <button class="btn btn-primary mr-2" id="darken-btn">Darken</button>
                </div>
            </div>
            <div class="col-3 d-none">
                <div class="form-group d-none">
                    <label>Hex Color</label>
                    <input class="form-control m-auto" type="text" id="hex-color" placeholder="#ffffff (Overlay Color)" />
                </div>
            </div>
            <div class="col-12 d-none">
                <div class="form-group mt-2">
                    <label for="upload-file">Upload a Picture</label>
                    <input class="form-control" type="file" id="upload-file" placeholder="Upload a Picture" />
                </div>
            </div>
            <div class="col-12">

                <div class="create-content___filter-images text-center mt-2">
                    <canvas id="canvas"></canvas>
                </div>
            </div>

            <div class="col-12">
                <table id="table" class="table table-row">
                </table>
            </div>
            <div class="col-12 d-none">
                <button class="btn btn-success mb-2 text-white " id="saveAllTable">
                    Agregar toda la tabla
                </button>
            </div>
        </div>

        <div class="col-12 d-none">
            <div class="form row">
                <div class='social-share-btns-container'>
                    <div class='social-share-btns'>
                        <a class='share-btn share-btn-twitter'
                            href='https://twitter.com/intent/tweet?text=https://codepen.io/marko-zub/#' rel='nofollow'
                            target='_blank'>
                            <i class='ion-social-twitter'></i>
                            Tweet
                        </a>
                        <a class='share-btn share-btn-facebook'
                            href='https://www.facebook.com/sharer/sharer.php?u=https://codepen.io/marko-zub/#'
                            rel='nofollow' target='_blank'>
                            <i class='ion-social-facebook'></i>
                            Share
                        </a>
                        <a class='share-btn share-btn-linkedin'
                            href='https://www.linkedin.com/cws/share?url=https://codepen.io/marko-zub/#' rel='nofollow'
                            target='_blank'>
                            <i class='ion-social-linkedin'></i>
                            Share
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div id="editor"></div>
    <section class="try d-none">
        <div class="container">
            <h3>Editor</h3>
            <div class="editor row">
                <div class="span3">

                    Colorizers are implemented using <a href="monarch.html" target="_blank">Monarch</a>.


                </div>
                <div class="span9">
                    <div class="row">
                        <div class="span4">
                            <label class="control-label">Language</label>
                            <select class="language-picker"></select>
                        </div>
                        <div class="span4">
                            <label class="control-label">Theme</label>
                            <select class="theme-picker">
                                <option>Visual Studio</option>
                                <option>Visual Studio Dark</option>
                                <option>High Contrast Dark</option>
                            </select>
                        </div>
                    </div>
                    <div class="editor-frame">
                        <div class="loading editor d-none">
                            <div class="progress progress-striped active">
                                <div class="bar"></div>
                            </div>
                        </div>
                        <div id="editor"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <input type="hidden" id="blog-content-type-all" value="{{url('/blog-content-type/all')}}">
@stop
@section('script-bottom')
    @parent
    <script src="{{ Utils::getUrlBase('admin') }}/library/highlight.js/11.4.0/highlight.min.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/library/html2canvas/html2canvas.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/library/camanjs/4.1.2/caman.full.min.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/library/sweetalert2/sweetalert2.min.js"></script>
    <style>
        @font-face {
            font-family: 'oswald';
            src: url('/font/oswald.regular.ttf');
        }

        /*
                                                                                                                                                                                                                            https://codepen.io/shotastage/pen/KaKwya
                                                                                                                                                                                                                            */

    </style>
    <script>
        hljs.initHighlightingOnLoad();
    </script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-create-content.js?v=<?php echo rand(); ?>"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-create-content-filters-canvas.js?v=<?php echo rand(); ?>"></script>

    <script src='{{ Utils::getUrlBase('admin') }}/library/monaco-editor-0.32.1/package/min/vs/loader.js'></script>

    <script>
        window.require.config({
            paths: {
                'vs': '{{ Utils::getUrlBase('admin') }}/library/monaco-editor-0.32.1/package/min/vs'
            }
        });

        window.MonacoEnvironment = {
            getWorkerUrl: () => proxy
        };

        let proxy = URL.createObjectURL(new Blob([`
            self.MonacoEnvironment = {
                baseUrl: '{{ Utils::getUrlBase('admin') }}/library/monaco-editor-0.32.1/package/min/'
            };
            importScripts(
                '{{ Utils::getUrlBase('admin') }}/library/monaco-editor-0.32.1/package/min/vs/base/worker/workerMain.js'
            );

        `], {
            type: 'text/javascript'
        }));


    </script>
    <script src='{{ Utils::getUrlBase('admin') }}/library/monaco-editor-0.32.1/package/min/vs/editor/editor.main.js'></script>
    <script src='{{ Utils::getUrlBase('admin') }}/library/monaco-editor-0.32.1/package/min/vs/editor/editor.main.nls.js'></script>

    <script>
        /*require(['vs/editor/editor.main'],function() {
                                                                                                                                                                                                                                   var editorIns = monaco.editor.create(document.getElementById('editor'), {
                                                                                                                                                                                                                                        value: [
                                                                                                                                                                                                                                            'function x() {',
                                                                                                                                                                                                                                            '\tconsole.log("Hello world!");',
                                                                                                                                                                                                                                            '}'].
                                                                                                                                                                                                                                            join('\n'),
                                                                                                                                                                                                                                        language: 'javascript'
                                                                                                                                                                                                                                    });
                                                                                                                                                                                                                                });*/
    </script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-create-content-monaco2.js?v=<?php echo rand(); ?>"></script>
@stop
<!--https://codepen.io/syedhuss/embed/WNxeaYL/?theme-id=modal-->
