@extends(GlobalClass::getDomainFolder(2) . '::base.index')
@section('metatags')
    @parent
    <!-- some master css here -->
    <title>Contenido | Administrador</title>
@stop
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog.css?v=<?php echo rand(); ?>" rel="stylesheet">
    <style type="text/css">
        :root {
            --main-color-blue: #0652dd;
        }

        html,
        body {
            font-family: "Poppins", sans-serif;
        }

        body {
            background: var(--main-color-blue);
        }
    </style>

    <script type="text/javascript"></script>

    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-base.css?v=<?php echo rand(); ?>" rel="stylesheet">

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            // Your inline scripts which uses methods from external-scripts.
            new window.exports.SamiUtils.Highlight().init()
            console.log(new window.exports.SamiUtils.Utils().hello('3'))

        });
    </script>



    <style type="text/css">
        .blog-content-delete,
        .blog-content-copy,
        .blog-content-replicate {
            border: none;
            background: none;
            margin-top: 2px;
        }
    </style>
    <script type="text/javascript">
        function blogContentDelete(id) {
            if (confirm("¿Realmente desea eliminarlo?")) {

                let data = {
                    _method: 'DELETE',
                    _token: document.head.querySelector("[name=csrf-token]").content,
                    id: id
                };
                $.ajax({
                    url: "{{ url('/blog-content-generate') }}" + '/' + id,
                    data: JSON.stringify(data),
                    type: 'DELETE',
                    // el tipo de información que se espera de respuesta
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': document.head.querySelector("[name=csrf-token]").content,
                    },
                    success: function(data) {
                        alert("eliminado");
                        location.reload();
                    },

                    error: function(xhr, status) {

                    }
                });
            } else {
                return false;
            }
        }

        function blogContentCopy(id) {
            if (confirm("¿Realmente desea duplicar?")) {

                let data = {
                    id: id,
                    replicate: false
                };

                $.ajax({
                    url: "{{ url('/blog-content-generate') }}" + '/' + id + '/' + 'clone',
                    data: JSON.stringify(data),
                    type: 'POST',
                    // el tipo de información que se espera de respuesta
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': document.head.querySelector("[name=csrf-token]").content,
                    },
                    success: function(data) {
                        alert("duplicado");
                        location.reload();
                    },

                    error: function(xhr, status) {

                    }
                });
            } else {
                return false;
            }
        }

        function blogContentReplicate(id) {
            if (confirm("¿Realmente desea crear uno nuevo?")) {

                let data = {
                    id: id,
                    replicate:true
                };

                $.ajax({
                    url: "{{ url('/blog-content-generate') }}" + '/' + id + '/' + 'clone',
                    data: JSON.stringify(data),
                    type: 'POST',
                    // el tipo de información que se espera de respuesta
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': document.head.querySelector("[name=csrf-token]").content,
                    },
                    success: function(data) {
                        alert("duplicado");
                        location.reload();
                    },

                    error: function(xhr, status) {

                    }
                });
            } else {
                return false;
            }
        }
    </script>
    <style type="text/css">
        sami-card-code div.sami-card-code div.sami-card-code___section-two div.sami-card-code___preview-html {
            position: relative;
            width: 95%;
            margin: auto;
            text-align: left;
        }
    </style>
@stop

@section('content')
    @parent
    <div class="content-images d-none" id="content-images"></div>
    <!-- Main content goes here -->


    <section class="blog-index__section-title-header ">
        <h1 class="text-center text-white font-weight-bolder">Administrador - Mis contenidos</h1>
    </section>

    <div class="row mt-4 mb-2">
        <x-button-back link="{{ url('/') }}" />
    </div>
    <section style="margin-top: 3rem!important;">
        <div class="col-4 pb-4 pt-4">
            <a href="{{ url('blog-content-generate/create') }}" class="btn btn-primary mb-2">
                + Agregar
            </a>
            <a href="{{ url('blog-content-generate/show') }}" class="btn btn-primary mb-2">
                Editar
            </a>
            <div class="form-group">
                <select class="form-control" onchange="window.location = this.options[this.selectedIndex].value">
                    <!---->
                    <option value="">Seleccione el tipo</option>
                    @foreach (App\Models\BlogContentType::all() as $index => $data)
                        <option {{ $name == $data->name ? 'selected="selected"' : '' }}
                            value="{{ Utils::getUrlBase('admin') . '/' . 'blog-content-generate-type' . '/' . $data->name }}">
                            {{ $data->name }}</option>
                        <!---->
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-8">

        </div>
    </section>

    <section>
        <sami-grid align="center" type="slider">
            @foreach ($content as $i => $response)
                <sami-slider class="sami-desk-max-width-520">
                    @foreach ($response->contents as $index => $data)
                        <li>
                            @include(GlobalClass::getDomainFolder(2) . '::common.content')
                            @include(GlobalClass::getDomainFolder(2) . '::common.content-social')
                        </li>
                    @endforeach
                </sami-slider>
            @endforeach
        </sami-grid>

        @empty(!$content)
            {!! $content->links() !!}
        @endempty
    </section>

@stop
@section('script-bottom')
    @parent
    <!-- more javascript -->

@stop
