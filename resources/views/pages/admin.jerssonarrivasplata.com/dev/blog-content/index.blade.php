@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-content.css?v=<?php echo(rand()); ?>" rel="stylesheet">

@stop
@section('content')
    @parent
    <div class="admin-blog-content-index container mt-5">
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
            </div>
        @endif
        <div class="row">
            <div class="col">
                <p class="h2">Administrador del Blog de Contenido</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <a href="{{ url('blog-content/create') }}" class="btn btn-primary mb-2">
                    + Agregar
                </a>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col table-responsive">
                <table class="table table-light table-hover">
                    <thead class="thead-light">
                        <tr>
                            <th>N°</th>
                            <th>Nombres</th>
                            <th>Enlace de <br/> documentaci&oacute;n</th>
                            <th>Tipos</th>
                            <th>Resumen</th>
                            <th>Imagen</th>
                            <th>Fecha de <br />creaci&oacute;n</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($content as $index => $data)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td style="width: 180px;word-break: break-word;">
                                    {{ $data->name }}
                                </td>
                                <td>{{ $data->documention_link }}</td>
                                <td>{{ App\Models\BlogContentType::where('type',$data->type)->value('name') }}</td>
                                <td style="width: 300px;word-break: break-word;">
                                    {{ Str::limit($data->resume, 100, $end = '...') }}
                                    <button type="button" class="btn btn-primary mt-3
                                    admin-blog-content-index__btn-show-modal-description
                                    admin-blog-content-index__btn-show-modal-description_{{$data->id}}" style="display: block;width: 100%;"
                                        onclick="showModalDescription({{$data->id}})"  data-image="{{ $data->image }}"  data-name="{{ $data->name }}" data-content='<div>{{ $data->content }}</div>'>
                                        {{ $data->button_content }}
                                    </button>
                                </td>
                                <td>{{ $data->image }}</td>

                                <td>{{ $data->created_at }}</td>
                                <td class="inline">
                                    <div class=" d-grid gap-2" style="display:grid;">
                                        <a href="{{ url('blog-content/' . $data->id) }}" class="btn btn-secondary mb-2">
                                            Ver
                                        </a>
                                        <a href="{{ url('blog-content/' . $data->id . '/edit') }}"
                                            class="btn btn-primary mb-2">
                                            Editar
                                        </a>
                                        <form method="post" action="{{ url('blog-content/' . $data->id) }}"
                                            style="display:inline">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-danger btn-block" type="submit"
                                                onclick="return confirm('¿Seguro en eliminar el archivo?');">Eliminar
                                                X</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <div class="col">
                @empty(!$content)
                    {!! $content->links() !!}
                @endempty
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="descriptionModal" aria-labelledby="descriptionModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="descriptionModalLabel">Descripci&oacute;n</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script-bottom')
    @parent
    <!-- more css -->
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-index.js?v=<?php echo(rand()); ?>" type="text/javascript"></script>

@stop
