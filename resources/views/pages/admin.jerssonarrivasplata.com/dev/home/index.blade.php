@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('content')
    @parent
    <div class="container">
        <div class="row justify-content-center d-none">
            <div class="col-md-8 mt-5">
                <div class="card">
                    <div class="card-header">Administrador de Mensajes</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif You are logged in!
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Suscriptores - Blog</div>

                    <div class="card-body">
                        @foreach ($blogSubscribers as $key => $data)
                            <p> {{$key+1}} - {{$data->email}}</p>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"></div>

                    <div class="card-body">

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
