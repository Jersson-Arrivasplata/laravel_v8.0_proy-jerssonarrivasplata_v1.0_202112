@extends(GlobalClass::getDomainFolder(2) . '::base.index')
@section('script-header')
    @parent
    <style type="text/css">
        div[slot="main"] {
            background: var(--main-color-white)
        }
    </style>
    <script type="text/javascript">
        window.baseContentType = '{{ Utils::getUrlBase('admin') }}/img/blog/content-type/';
        window.base = '{{ Utils::getUrlBase('admin') }}';

        document.addEventListener("DOMContentLoaded", function(event) {
            // Your inline scripts which uses methods from external-scripts.
            new window.exports.SamiUtils.Highlight().init()
            console.log(new window.exports.SamiUtils.Utils().hello('3'))

        });
    </script>
@stop
@section('content')
    @parent
    <a href="{{ url()->previous() }}" class="btn btn-danger btn-back mt-3"><span>Regresar</span></a>

    <main>
        <div class="container mb-4">
            <div class="h1 mt-4">Actualizar Social Media - Editar</div>
        </div>
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">X</button>

            </div>
        @endif
        <div id="alerts">

        </div>
        <form id="form" class="form" action="{{ route('blog-social-media.store', $idRow) }}" method="POST">
            @csrf

            <input type="hidden" class="form-control" id="idRow" name="idRow" value="{{$idRow}}">

            <div class="container">
                <div class="row">
                    <div class="col-12 mb-2">
                        <div class="form-group">
                            <label>Tipo</label>
                            <select class="form-control" id="type" name="type">
                                <optgroup>Seleccione el tipo</optgroup>
                                <option value="0">LinkedIn </option>
                                <option value="1">Facebook </option>
                                <option value="2">Instagram </option>
                                <option value="3">TikTok </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mb-2">
                        <div class="form-group">
                            <label>Enlace </label>
                            <input type="text" class="form-control" id="url" name="url" placeholder="Ingresa el enlace" >
                        </div>
                    </div>
                    <div class="col-12 mb-2">
                        <div class="form-group">
                            <label>Descripción </label>
                            <textarea class="form-control" id="description" name="description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-12  ">
                        <button type="submit" class="btn btn-success mb-2 w-100"  id="save" >
                            Agregar
                        </button>
                    </div>
                </div>
            </div>
        </form>
        @php
            $types = [
                0 => 'LinkedIn',
                1 => 'Facebook',
                2 => 'Instagram',
                3 => 'WhatsApp',
            ];
        @endphp

        <table class="table table-row table-hover">
            <thead>
                <tr>
                    <th scope="col">Tipo</th>
                    <th scope="col">Enlace</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($contents as $content)
                    <tr>
                        <td>{{ $types[$content->type] ?? 'Desconocido' }}</td>
                        <td>{{ $content->url }}</td>
                        <td>{{ $content->description }}</td>
                        <td>
                            <form action="{{ route('blog-social-media.destroy', $content->idRow) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="id" value="{{ $content->id }}">
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </main>
@stop
@section('script-bottom')
    @parent
    <script src="{{ Utils::getUrlBase('admin') }}/library/highlight.js/11.4.0/highlight.min.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/library/html2canvas/html2canvas.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-highlight.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-highlight-common.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content-highlight-edit.js"></script>
    <script src="{{ Utils::getUrlBase('admin') }}/js/highlight.js"></script>
@stop
<!--https://codepen.io/syedhuss/embed/WNxeaYL/?theme-id=modal-->
