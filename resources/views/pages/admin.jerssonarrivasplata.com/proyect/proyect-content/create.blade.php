@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-content.css?v=<?php echo rand(); ?>" rel="stylesheet">
@stop
@section('content')
    @parent
    <div class="container mt-5">
        <h3>
            Blog de Contenido
            <small class="text-"> - Crear</small>
        </h3>
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">X</button>

            </div>
        @endif
        <div id="alerts">

        </div>
        <form action="{{ url('file/project-content') }}" method="POST" id="admin-blog-content" class="dropzone mt-5 mb-5"
            enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="mb-3">
                <label class="form-label">Imagen - (js/xxx)</label>
                <ul id="previewsContainer" class=" sortable dropzone-previews dropzone-border">
                    <div class=" dropzone-container dz-message dz-message-dropzone dz-clickable" id="dropzone-click-target">
                        <p class="dropzone-container-paragraph">¡Suelta tus imagenes aqu&iacute;!</p>
                    </div>
                </ul>
            </div>
            <div class="dropzone-previews"></div>
            <select name="type" class="form-control mb-3">
                <optgroup>Seleccione el tipo</optgroup>
                @foreach (App\Models\BlogContentType::get() as $data)
                    <option value="{{ $data->type }}">{{ $data->name }}</option>
                @endforeach
            </select>
            <button type="submit" id="submit" class="btn btn-success">Subir imagenes</button>
        </form>
        <form method="post" action="{{ url('project-content') }}"
            enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="mb-3">
                <label class="form-label">Tipo</label>
                <select name="type" class="form-control">
                    <optgroup>Seleccione el tipo</optgroup>
                    @foreach (App\Models\BlogContentType::get() as $data)
                        <option value="{{ $data->type }}">{{ $data->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label class="form-label">Imagen</label>
                <input type="file" name="image[]" class="form-control" >
            </div>
            <div class="mb-3">
                <label class="form-label">Nombre de Carpeta</label>
                <input type="text" name="folder" class="form-control">
            </div>

            <div class="mb-3">
                <label class="form-label">Files</label>
                <input type="file" name="file[]" class="form-control" multiple>
            </div>
            <div class="mb-3">
                <label class="form-label">Nombre</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="mb-3">
                <label class="form-label">Descripci&oacute;n</label>
                <textarea name="description" class="form-control"></textarea>
            </div>
            <div class="mb-3 ">
                <button type="button" name="add" id="dynamic-ar" class="btn btn-outline-primary mt-2">Agregar
                    enlace</button>
            </div>
            <div class="links ">
                <div class="mb-3 ">
                    <label class="form-label">Enlace</label>
                    <input type="text" name="links[]" class="form-control">
                </div>
            </div>
            <a href="{{ url('project-content') }}" class="btn btn-danger mb-3">Regresar</a>

            <input type="submit" value="Agregar" class="btn btn-primary mb-3" />
        </form>
    </div>
@stop
@section('script-bottom')
    @parent
    <script type="text/javascript">
        let globals = {
            url_blog_javascript: "{{ Utils::getUrlBase('admin') }}/img/project/content/javascript/",
        };

        $("#dynamic-ar").click(function() {
            $(".links").append(`
            <div class="mb-3 ">
                <label class="form-label">Enlace</label>
                <input type="text" name="links[]" class="form-control">
            </div>
            `);
        });
    </script>

    {{-- <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content.js?v=<?php echo rand(); ?>" type="text/javascript">
    </script> --}}
@stop
