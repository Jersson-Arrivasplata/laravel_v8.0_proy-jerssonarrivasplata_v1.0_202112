@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-content.css?v=<?php echo(rand()); ?>" rel="stylesheet">

@stop
@section('content')
    @parent
    <div class="container mt-5">
        <h3>
            Blog de Contenido
            <small class="text-"> - Editar</small>
        </h3>
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! session('success') !!}
            </div>
        @endif
        <div id="alerts">

        </div>
        <form action="{{ url('file/blog-content') }}" method="POST"  id="admin-blog-content" class="dropzone mt-5 mb-5"
        enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="mb-3">
                <label class="form-label">Imagen - (js/xxx)</label>
                <ul id="previewsContainer" class=" sortable dropzone-previews dropzone-border">
                    <div class=" dropzone-container dz-message dz-message-dropzone dz-clickable" id="dropzone-click-target">
                        <p class="dropzone-container-paragraph">¡Suelta tus imagenes aqu&iacute;!</p>
                    </div>
                </ul>
            </div>
            <div class="dropzone-previews"></div>
            <select name="type" class="form-control mb-3">
                <optgroup>Seleccione el tipo</optgroup>
                @foreach (App\Models\BlogContentType::get() as $data)
                    @if ($data->type  == $content->type)
                       <option value="{{ $data->type }}" selected>{{ $data->name }}</option>
                    @else
                       <option value="{{ $data->type }}">{{ $data->name }}</option>
                    @endif
                @endforeach
            </select>
            <button type="submit"   id="submit" class="btn btn-success" >Subir imagenes</button>
        </form>

        <form method="post" action="{{ url('blog-content/'.$content->id) }}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="mb-3">
                <label class="form-label">Nombre - ({{Str::slug($content->name , '-')}})</label>
                <input type="text" name="name" class="form-control" value="{{ $content->name }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Enlace de la documentaci&oacute;n - (docs/1.0/xxx)</label>
                <input type="text"  name="documention_link"  class="form-control" value="{{ $content->documention_link }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Resumen</label>
                <textarea  name="resume"  class="form-control" >{{ $content->resume }}</textarea>
            </div>
            <div class="mb-3">
                <label class="form-label">Tipo</label>
                <select name="type" class="form-control">
                    <optgroup>Seleccione el tipo</optgroup>
                    @foreach (App\Models\BlogContentType::get() as $data)
                        @if ($data->type  == $content->type)
                           <option value="{{ $data->type }}" selected>{{ $data->name }}</option>
                        @else
                           <option value="{{ $data->type }}">{{ $data->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label class="form-label">Texto del Bot&oacute;n - (Ver Imagen)</label>
                <input type="text" name="button_content" class="form-control" value="{{ $content->button_content }}">
            </div>
            <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                        aria-selected="true">Contenido con CKEditor - Texto</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                        aria-controls="profile" aria-selected="false">Contenido con CKEditor - Html</a>
                </li>
            </ul>
            <div>
                {{ Utils::getUrlBase('admin') }}/img/blog/content/xxx/
            </div>
            <div class="tab-content  mb-3" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <textarea id="editor" name="content">{{ $content->content }}</textarea>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <textarea id="editorHtml" name="content_html">
                    </textarea>
                </div>
            </div>
            <a href="{{ url('blog-content') }}" class="btn btn-danger">Regresar</a>

            <button type="submit" class="btn btn-primary">Editar</button>
        </form>
    </div>
@stop
@section('script-bottom')
    @parent
    <!-- more css -->

    <script src="{{ Utils::getUrlBase('admin') }}/library/ckeditor/4.13.0/full-all/ckeditor.js"></script>
    <script type="text/javascript" src="{{ Utils::getUrlBase('admin') }}/library/ckeditor/config.js?v=<?php echo rand(); ?>"></script>
    <script type="text/javascript" src="{{ Utils::getUrlBase('admin') }}/library/ckeditor/styles.js?v=<?php echo rand(); ?>"></script>


    <script src="{{ Utils::getUrlBase('admin') }}/js/blog-content.js?v=<?php echo(rand()); ?>" type="text/javascript"></script>

@stop
