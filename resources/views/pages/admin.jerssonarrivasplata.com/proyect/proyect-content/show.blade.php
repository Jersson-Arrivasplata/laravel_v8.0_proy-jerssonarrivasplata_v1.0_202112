@extends(GlobalClass::getDomainFolder(2).'::base.index')
@section('script-header')
    @parent
    <!-- more css -->
    <link href="{{ Utils::getUrlBase('admin') }}/css/blog-content.css?v=<?php echo rand(); ?>" rel="stylesheet">

@stop
@section('content')
    @parent
    <div class="container mt-5">
        <h3>
            Blog de Contenido
        </h3>
        <div class="form">
            <div class="mb-3">
                <label class="form-label">Nombre - (ECMAScript-ES vs JAVASCRIPT-JS)</label>
                <input type="text" name="name" class="form-control" value="{{ $content->name }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Enlace de la documentaci&oacute;n - (docs/1.0/xxx)</label>
                <input type="text" name="documention_link" class="form-control" value="{{ $content->documention_link }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Resumen</label>
                <textarea  name="resume"  class="form-control" >{{ $content->resume }}</textarea>
            </div>
            <div class="mb-3">
                <label class="form-label">Tipo</label>
                <select name="type" class="form-control">
                    <optgroup>Seleccione el tipo</optgroup>
                    @foreach (App\Models\BlogContentType::get() as $data)
                        @if ($data->type == $content->type)
                            <option value="{{ $data->type }}" selected>{{ $data->name }}</option>
                        @else
                            <option value="{{ $data->type }}">{{ $data->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label class="form-label">Imagen - (js/xxx)</label><br/>
                <img width="250px" src="../../../img/blog/{{ $content->image }}"/>
            </div>
            <div class="mb-3">
                <label class="form-label">Texto del Bot&oacute;n - (Ver Imagen)</label>
                <input type="text" name="button_content" class="form-control" value="{{ $content->button_content }}">
            </div>
            <div class="mb-3">
                {!! $content->content !!}
            </div>
            <a href="{{ url('blog-content') }}" class="btn btn-danger">Regresar</a>
        </div>
    </div>
@stop
@section('script-bottom')
    @parent

@stop
