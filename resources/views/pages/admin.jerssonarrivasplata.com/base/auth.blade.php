<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ GlobalClass::getCreatorUser() }} | Software Developer</title>
    <link rel="icon" type="image/png" href="./img/icon.png">

    <!--Inicio Fonts-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Assistant&family=Bebas+Neue&family=Poppins&display=swap"
        rel="stylesheet">
    <!--Fin Fonts-->

    <!--Inicio Seo-->
    <meta name="author" content="{{ GlobalClass::getCreatorUser() }}" />
    <meta name="copyright" content="{{ GlobalClass::getCreatorUser() }}" />
    <meta name="description" content="Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles, con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos, an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.
    ">
    <meta name="keywords"
        content="#software, #developer, #developments, #jerssonarrivasplata, #jerssonarrivasplataDEV, #jerssonarrivasplataPERU" />
    <!--Fin Seo-->

    <!--Inicio Limpiar Cache-->
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <!--Fin Limpiar Cache-->
    <link href="{{ Utils::getUrlBase('admin') }}/css/app.css?v=<?php echo(rand()); ?>" rel="stylesheet">
    @section('script-header')
        <!-- some master css here -->
    @show
</head>

<body>


    <div class="loader d-none">
        <span>{</span><span>}</span>
    </div>

    <div>
        @yield('content')
    </div>
    <script src="{{ Utils::getUrlBase('admin') }}/js/app.js?v=<?php echo(rand()); ?>" type="text/javascript"></script>
    @section('script-bottom')
        <!-- some js here -->
   @show

</body>

</html>
