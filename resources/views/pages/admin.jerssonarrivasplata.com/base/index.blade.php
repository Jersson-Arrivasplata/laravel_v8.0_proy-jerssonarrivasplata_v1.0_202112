<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @section('metatags')
        <!-- some master css here -->

    @show
    <link rel="icon" type="image/png" href="{{ Utils::getUrlBase('admin') }}/img/icon.png">

    <!--Inicio Fonts-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Assistant&family=Bebas+Neue&family=Poppins&display=swap"
        rel="stylesheet">
    <!--Fin Fonts-->

    <!--Inicio Seo-->
    <meta name="author" content="{{ GlobalClass::getCreatorUser() }}" />
    <meta name="copyright" content="{{ GlobalClass::getCreatorUser() }}" />
    <meta name="description" content="Blog de Jersson Arrivasplata Rojas | Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles, con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos, an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.
    ">
    <meta name="keywords"
        content="#software, #developer, #developments, #jerssonarrivasplata, #jerssonarrivasplataDEV, #jerssonarrivasplataPERU #blogjerssonarrivasplata #blogjerssonarrivasplataDEV" />
    <!--Fin Seo-->

    <!--Inicio Limpiar Cache-->
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <!--Fin Limpiar Cache -->
    <script src="{{ Utils::getUrlBase('admin') }}/library/jquery/3.4.1/jquery-3.4.1.min.js"></script>
    <link href="{{ Utils::getUrlBase('admin') }}/library/bootstrap/5.1.3/bootstrap.min.css" rel="stylesheet">
    <link rel='stylesheet' href='{{ Utils::getUrlBase('admin') }}/library/dropzone/4.3.0/dropzone.css'>
    <script src="{{ Utils::getUrlBase('admin') }}/library/sweetalert2/sweetalert2.min.js"></script>

     <link href="{{ Utils::getUrlBase('admin') }}/library/sami/icons/icons.css?v=<?php echo rand(); ?>" rel="stylesheet">
    <script defer="defer" src="{{ Utils::getUrlBase('admin') }}/library/sami-utils/core/dist/html2canvas.js"></script>
    <script defer="defer" src="{{ Utils::getUrlBase('admin') }}/library/sami-utils/core/dist/sweetalert2.js"></script>
    <script defer="defer" src="{{ Utils::getUrlBase('admin') }}/library/sami-utils/core/dist/@highlightjs.js"></script>
    <link href="{{ Utils::getUrlBase('admin') }}/library/sami-utils/core/dist/@highlightjs.css" rel="stylesheet">
    <script defer="defer" src="{{ Utils::getUrlBase('admin') }}/library/sami-utils/core/dist/app.js"></script>

    <script type="module" src="{{ Utils::getUrlBase('admin') }}/library/sami/build/sami.esm.js?v=<?php echo rand(); ?>"></script>
    <script nomodule src="{{ Utils::getUrlBase('admin') }}/library/sami/build/sami.js?v=<?php echo rand(); ?>"></script>
    <script>
        var exports = {
            "__esModule": true
        };
    </script>
    <style>
        :root {
            --main-color-white: #f8f9fa;
        }

        html,
        body {
            font-family: "Poppins", sans-serif;
        }

        body {
            background: var(--main-color-white);
        }

    </style>

    @section('script-header')
        <!-- some master css here -->
    @show
</head>

<body>

    <div id="cv" style="display:none;"></div>
    <x-loader></x-loader>
    @include(GlobalClass::getDomainFolder(2) . '::common.sidebar')
    <sami-main width="calc(100% - 350px)">
        <div slot="main" style="height: 100%;">
            <!--#Facebook End -  Your plugin del chat code -->
            {{-- @include('common.blog-menu-vertical') --}}
            @if (Session::has('success'))
                <script type="text/javascript">
                    (function() {
                        Swal.fire({
                            title: '{!! session('success') !!}',
                            html: 'Se cerrara en <b></b> milisegundos.',
                            width: 600,
                            padding: '3em',
                            color: '#716add',
                            background: "#fff url({{ Utils::getUrlBase('admin') }}/img/background/trees.png)",
                            backdrop: `
                                rgba(0,0,123,0.4)
                                url("{{ Utils::getUrlBase('admin') }}/img/background/nyan-cat.gif")
                                left top
                                no-repeat
                            `,
                            showConfirmButton: false,
                            timer: 4000,
                            timerProgressBar: true,
                            didOpen: () => {
                                Swal.showLoading()
                                const b = Swal.getHtmlContainer().querySelector('b')
                                timerInterval = setInterval(() => {
                                    b.textContent = Swal.getTimerLeft()
                                }, 100)
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        }).then((result) => {
                            /* Read more about handling dismissals below */
                            if (result.dismiss === Swal.DismissReason.timer) {
                                console.log('I was closed by the timer')
                            }
                        });
                    })();
                </script>
            @endif
            @yield('content')
        </div>
    </sami-main>
    <script src="{{ Utils::getUrlBase('admin') }}/library/bootstrap/5.1.3/bootstrap.min.js"></script>
    <script src='{{ Utils::getUrlBase('admin') }}/library/dropzone/4.3.0/dropzone.js'></script>
   
    @section('script-bottom')
        <!-- some js here -->
   @show
</body>

</html>
