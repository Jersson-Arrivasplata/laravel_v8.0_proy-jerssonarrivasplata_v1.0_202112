@section('metatags')
    @parent

    @php
    $description = 'Componente '.Request::segment(1).' desarrollado en StencilJs en el Proyecto Sami de ';
    $description .= GlobalClass::getCreatorUser() . ' - ';
    $description .= 'Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles, ';
    $description .= 'con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,';
    $description .= 'an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.';

    $title = Request::segment(1) .' de Sami | Proyecto StencilJs';
    @endphp

    <x-meta-tags description="{{ $description }}" title="{{ $title }}" og-type="article"
        twitter-content-card="sumary"
        link="{{ Utils::getUrlBase('sami') }}/{{Request::segment(1)}}">
    </x-meta-tags>

@stop