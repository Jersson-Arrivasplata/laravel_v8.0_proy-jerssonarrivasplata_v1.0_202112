<sami-header class="t-0  " position="sticky" align="center" style="margin-top: -20px;">
    <div class="sami-margin-right-auto ml-10 pl-4">
        <sami-hyperlink href="{{ Utils::getUrlBase('sami') }}" onclick="loader()" target="_self">
            <sami-img width="70" height="70" object-fit="contain"
                src="{{ Utils::getUrlBase('sami') }}/img/sami.jerssonarrivasplata.com.png">
            </sami-img>
        </sami-hyperlink>
    </div>
    <div class="sami-only-desktop">
        <sami-hyperlink href="{{ Utils::getUrlBase() }}" target="_blank" class="mr-2 ml-2">Jesson G. Arrivasplata</sami-hyperlink>
        <sami-hyperlink href="{{ Utils::getUrlBase('doc') }}/docs/1.0/angular/angular.guideline" target="_blank"  class="mr-2 ml-2">
            Documentaci&oacute;n</sami-hyperlink>
        <sami-hyperlink href="{{ Utils::getUrlBase('dev') }}" target="_blank"  class="mr-2 ml-2">Mi Blog</sami-hyperlink>
        <sami-hyperlink href="{{ Utils::getUrlBase('dev') }}/project" target="_blank"  class="mr-4 ml-2">Proyectos</sami-hyperlink>
    </div>
    <sami-list list-style="none" class="d-flex justify-content-evenly m-0 p-0 pr-4">
        <sami-list-item class="mr-2 ml-2">
            <sami-hyperlink href="{{ config('base.repositories.github.sami') }}" target="_blank">
                <sami-img width="25"
                    src="{{ Utils::getUrlBase('sami') }}/library/sami/assets/icon-social-media/github.svg">
                </sami-img>
            </sami-hyperlink>
        </sami-list-item>
        <sami-list-item class="mr-2 ml-2">
            <sami-hyperlink href="{{ config('base.projects.SAMI') }}" target="_blank">
                <sami-img width="95" height="30"
                    src="{{ Utils::getUrlBase('sami') }}/library/sami/assets/icon-social-media/npm.svg">
                </sami-img>
            </sami-hyperlink>
        </sami-list-item>
    </sami-list>
</sami-header>
