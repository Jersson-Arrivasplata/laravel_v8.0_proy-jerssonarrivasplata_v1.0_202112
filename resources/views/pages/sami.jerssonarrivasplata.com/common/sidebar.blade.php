<style type="text/css">
    sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav {
        padding-top: 115px;
    }

</style>

<x-sidebar-dashboard>
    <sami-list-item>
        <sami-paragraph>General</sami-paragraph>
        <sami-list list-style="none" class="m-0 p-0">
            <sami-list-item active="{{ Utils::getBooleanString(Request::segment(1) == 'icon') }}">
                <sami-hyperlink href="{{ url('/icon') }}" target="_self">
                    <sami-paragraph>Iconos</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
        </sami-list>
    </sami-list-item>
    <sami-list-item>
        <sami-paragraph>Data Display</sami-paragraph>
        <sami-list list-style="none" class="m-0 p-0">
            <sami-list-item active="{{ Utils::getBooleanString(Request::segment(1) == 'card') }}">
                <sami-hyperlink href="{{ url('/card') }}" target="_self">
                    <sami-paragraph>Cards</sami-paragraph>
                </sami-hyperlink>
            </sami-list-item>
        </sami-list>
    </sami-list-item>
</x-sidebar-dashboard>
