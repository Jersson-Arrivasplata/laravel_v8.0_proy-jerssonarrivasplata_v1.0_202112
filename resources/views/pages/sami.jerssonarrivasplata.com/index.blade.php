@extends(GlobalClass::getDomainFolder(4).'::base.index')
@section('metatags')
    @parent
    <!-- some master here -->
    @php
    $description = 'Componentes desarrollados en StencilJs en el Proyecto Sami de ';
    $description .= GlobalClass::getCreatorUser() . ' - ';
    $description .= 'Full Stack Developer y Desarrollador de Aplicaciones m&oacute;viles, ';
    $description .= 'con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,';
    $description .= 'an&aacute;lisis de negocio en los sectores de transporte, servicios, educaci&oacute;n y turismo.';

    $title = 'Inicio de Sami | Proyecto StencilJs';

    @endphp

    <x-meta-tags description="{{ $description }}" title="{{ $title }}" og-type="article"
        twitter-content-card="sumary"
        link="{{ Utils::getUrlBase('sami') }}">
    </x-meta-tags>

@stop

@section('content')
    @parent

    <section class="mt-8 mb-8">
        <x-title title="Inicio de Sami - Proyecto StencilJs" />
    </section>
    <section>
       
    </section>
@stop
