<!DOCTYPE html>
<html lang="en">

<head>
    <x-html-head link="{{ Utils::getUrlBase('sami') }}"
        link-image="{{ Utils::getUrlBase('sami') }}/img/sami.jerssonarrivasplata.com.png" color="#ffffff"
        google-tag-manager-id="G-SQVYDPNF9W"></x-html-head>
    <style type="text/css">
        :root {
            --sami-screen: rgb(255, 255, 255);
        }

    </style>
    @section('metatags')
        <!-- some master css here -->
    @show

    <x-sami-doc-links></x-sami-doc-links>
    @section('script-header')
        <!-- some master css here -->
    @show
</head>

<body class="font-poppins">
    <x-loader></x-loader>
    @include(GlobalClass::getDomainFolder(4) . '::common.header')
    @include(GlobalClass::getDomainFolder(4) . '::common.sidebar')
    <sami-main style="height: 100%;" width="calc(100% - 320px)" word-break="word">
        <div slot="main">
            @if (Session::has('success'))
                <x-swal-fire link="{{ Utils::getUrlBase('sami') }}"></x-swal-fire>
            @endif
            @yield('content')
            <x-footer type="0"></x-footer>
        </div>
    </sami-main>

    @section('script-bottom')
        <!-- some js here -->
    @show
</body>

</html>
