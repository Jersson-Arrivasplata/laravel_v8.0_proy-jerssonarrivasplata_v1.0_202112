@extends(GlobalClass::getDomainFolder(4).'::base.index')

@section('metatags')
    @parent
    <!-- some master css here -->
    <title>Cards de Sami | Proyecto StencilJs</title>
    @include(GlobalClass::getDomainFolder(4) . '::common.metatags')
@stop
@section('script-header')
    @parent
@stop

@section('content')
    @parent
    <!-- Main content goes here -->
    <div class="sami-skeleton mt-13 mb-4" style="display:flex;">
        <div class="container pt-4">
            <h1>sami-card</h1>
            <p>Es un componente que encabeza un título, subtítulo y contenido.</p>
            <h2>Uso</h2>
            <h3>Propiedades</h3>

            <div class="table-wrapper">
                <table class="table">
                    <tbody>
                        <tr>
                            <td><strong>Descripción</strong></td>
                            <td>If <code>true</code>, a button tag will be rendered and the card will be tappable.</td>
                        </tr>
                        <tr>
                            <td><strong>Atributo</strong></td>
                            <td><code>button</code></td>
                        </tr>
                        <tr>
                            <td><strong>Tipo</strong></td>
                            <td><code>boolean</code></td>
                        </tr>
                        <tr>
                            <td><strong>Default</strong></td>
                            <td><code>false</code></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <h3>CSS Custom Properties</h3>
             <div class="table-wrapper">
                <table class="table">
                    <tbody>
                        <tr>
                            <td><strong>Nombre</strong></td>
                            <td><strong>Descripción</strong></td>
                        </tr>
                        <tr>
                            <td><code>--background	</code></td>
                            <td>Background of the card</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- @include(GlobalClass::getDomainFolder(4) . '::common.markdown') --}}
@stop
@section('script-bottom')
    @parent
@stop