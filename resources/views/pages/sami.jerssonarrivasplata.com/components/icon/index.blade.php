@extends(GlobalClass::getDomainFolder(4).'::base.index')

@section('metatags')
    @parent
    <!-- some master css here -->
    <title>Iconos de Sami | Proyecto StencilJs</title>
    @include(GlobalClass::getDomainFolder(4) . '::common.metatags')
@stop
@section('content')
    @parent
    <div class="sami-skeleton mt-13" style="display:flex;">
        <div class="container pt-4">
            @include(GlobalClass::getDomainFolder(4) . '::common.markdown')
        </div>
    </div>
    <div class="sami-doc-icon-container">
        <sami-tab>
            <sami-list part="header" list-style="none" class="mt-4 mb-4 p-0">
                <x-sami-doc-tab-header-item href="#mdi" text="Mdi"></x-sami-doc-tab-header-item>
                <x-sami-doc-tab-header-item href="#fab" text="Fab"></x-sami-doc-tab-header-item>
                <x-sami-doc-tab-header-item href="#dripcons" text="Dripcons"></x-sami-doc-tab-header-item>
                <x-sami-doc-tab-header-item href="#bx" text="BX"></x-sami-doc-tab-header-item>
            </sami-list>
            <div part="search" class="mt-4 mb-4">
                <sami-input type="text"
                    onkeyup="new window.exports.SamiUtils.Filters().filterWordToHide('sami-tab div.sami-tab___active i',event.target.value)"
                    placeholder="Buscar por nombre ..." input-type="doc-search" title="Ingresa el nombre"></sami-input>
            </div>
            <div part="body" class="mt-4 mb-4">
                <x-sami-doc-icons id="mdi" content="{!! json_encode(json_decode(file_get_contents(Utils::getUrlBase('sami') . '/library/icons/mdi.json')), true) !!}"></x-sami-doc-icons>
                <x-sami-doc-icons id="fab" content="{!! json_encode(json_decode(file_get_contents(Utils::getUrlBase('sami') . '/library/icons/fab.json')), true) !!}"></x-sami-doc-icons>
                <x-sami-doc-icons id="dripcons" content="{!! json_encode(json_decode(file_get_contents(Utils::getUrlBase('sami') . '/library/icons/dripcons.json')), true) !!}"></x-sami-doc-icons>
                <x-sami-doc-icons id="bx" content="{!! json_encode(json_decode(file_get_contents(Utils::getUrlBase('sami') . '/library/icons/bx.json')), true) !!}"></x-sami-doc-icons>

        </sami-tab>
    </div>

@stop
{{--  --}}
