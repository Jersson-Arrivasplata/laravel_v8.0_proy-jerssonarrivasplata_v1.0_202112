 <div id="{{$id}}" >
     <div class="sami-doc-icon" class="mt-4 mb-4">
         <div class="sami-doc-icon-content">
             @foreach (json_decode($content) as $i => $data)
                 <div class="">
                     <i style="font-size: 2.5rem;" class="{{ $data->name }}"></i>
                     <span>{{ $data->name }}</span>
                 </div>
             @endforeach
         </div>
     </div>
 </div>