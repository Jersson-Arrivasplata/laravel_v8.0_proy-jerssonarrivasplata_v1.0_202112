
<sami-loader type="pulse">
    <sami-span>&lt;</sami-span>
    <sami-span>&gt;</sami-span>
</sami-loader>

<script type="text/javascript">
    const loader = () => {
        customElements.whenDefined('sami-loader').then(() => {
            const load = document.querySelector('sami-loader');
            load.componentOnReady().then(() => {
                load.show();
            });
        })
    }
</script>