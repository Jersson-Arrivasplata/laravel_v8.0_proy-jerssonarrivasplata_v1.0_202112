<sami-hyperlink color="white" target="_blank" decoration="none" class="mr-1" href="{{$link}}">
    <sami-img width="25" display="initial" src="{{$linkImage}}">
    </sami-img>
    <span>{{$text}}</span>
</sami-hyperlink>