<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id={{ $id }}"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', "{{ $id }}");
</script>
{{--

<!-- Global site tag (gtag.js) - Google Analytics -->
<script>
    (function(w, d, s, l, i) {
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s);
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtag/js?id=' + i;
        f.parentNode.insertBefore(j, f);
        
        w[l] = w[l] || [];

        function gtag() {
            w[l].push(arguments);
        }
        gtag('js', new Date());
        gtag('config', i);

    })(window, document, 'script', 'dataLayer', '{{ $id }}');
</script>

--}}