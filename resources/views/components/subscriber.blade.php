<sami-hyperlink only-desktop="true" color="white" position="absolute" border-style="solid" border-width="2" decoration="none" id="sami-subscriber-hyperlink" class="p-2 r-0 " href="#" target="_self" onclick="subscriberShow()">
    CLICK SUSCRÍBETE
</sami-hyperlink>
<sami-subscriber is-open="true">
    <sami-hyperlink part="close" decoration="none" color="dark" href="#">
        <sami-icon class="mdi mdi-close"></sami-icon>
    </sami-hyperlink>
    <div part="body" class="mb-13" >
        <sami-paragraph class="font-bebas-neue">{{$title}}</sami-paragraph>
        <sami-paragraph>{{$subtitle}}</sami-paragraph>
        <sami-form method="POST" action="{{ $link }}/subscribe">
            {{ csrf_field() }}
            <sami-input input-type="inline" type="email" name="email"
                placeholder="Ingrese su correo electr&oacute;nico"></sami-input>
            <sami-input align="center" input-type="subscribe" class="input-background-red" type="submit" value="Suscribirse" style="width: 100%;"></sami-input>
        </sami-form>
    </div>
</sami-subscriber>
<script type="text/javascript">
    function subscriberShow() {
        (async () => {
            await customElements.whenDefined('sami-subscriber');
            const samiSubscriber = document.querySelector('sami-subscriber');
            await samiSubscriber.show();
        })();
    }
</script>
