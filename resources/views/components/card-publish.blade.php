<div id="{{$id}}" class="container card-publish {{$preview == 1 ? 'preview':''}} {{$before==1?'index-card-publish':'after'}}"
    style="{{ $borderColor ? 'border-color:' . $borderColor . ';' : '' }} {{ $height ? 'height:' . $height . 'px;' : '' }}">
    <div class="position" style="{{$borderColor? 'background-color:'.$borderColor.';': ''}}">
        <span>{{$position}}</span>
    </div>
    <div class="title">{{ $title }}</div>
    <div class="subtitle">{{ $subtitle }}</div>
    @if ($preview!=1)
        @if ($before==1)
            <pre>
                <code style="{{$lineHeight? 'line-height:'.$lineHeight.';': ''}}{{$fontSize? 'font-size:'.$fontSize.'px;': ''}}">{{$codes}}</code>
            </pre>
        @else
            <pre>
                <code style="{{ $height ? 'height:' . ($height-60) . 'px;' : '' }}" attr-code="{{$codes}}"></code>
            </pre>
        @endif
    @else
        <div class="iframe-html">{!!$codes!!}</div>
        {{--<iframe style="width: 100%;height: 100%;" src="javascript:void(0);" srcdoc="{{{ $codes }}}"></iframe>--}}
    @endif
    <img src="{{ $image }}" alt="{{ $title }}" class="image">
    <div class="footer">JERSSONARRIVASPLATA.COM</div>
    @if (!$status)
        <span class="badge btn-primary" style="position: absolute; top:5px; left:5px;">{{$activated == 1 ? 'A': 'NA'}}</span>
    @endif
</div>
