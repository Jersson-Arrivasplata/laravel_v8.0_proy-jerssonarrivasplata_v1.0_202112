<div id="{{ $counter }}_sami-card" class="mb-1">
    <sami-card>
        <sami-img part="header" object-fit="cover" class="width-100" src="{{ $linkImage }}"></sami-img>
        <div part="body">
            <sami-paragraph type="subtitle" color="dark" transform="uppercase" weigth="bold">
                {{ $subtitle }}
            </sami-paragraph>
            <sami-paragraph type="title" color="dark" weigth="bold">{{ $title }}</sami-paragraph>
            <sami-paragraph>{{ $description }}</sami-paragraph>
        </div>
        <div part="footer">
            <sami-hyperlink hyperlink-type="outline-danger" href="{{ $link }}" onclick="loader()">Ir a
            </sami-hyperlink>
        </div>
    </sami-card>
</div>
