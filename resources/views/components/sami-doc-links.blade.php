<link href="{{ Utils::getUrlBase('sami') }}/library/skeleton/css/normalize.css?v=<?php echo rand(); ?>"
    rel="stylesheet">
<link href="{{ Utils::getUrlBase('sami') }}/library/skeleton/css/skeleton.css?v=<?php echo rand(); ?>" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@sukka/markdown.css">

<link href="{{ Utils::getUrlBase('sami') }}/css/sami.css?v=<?php echo rand(); ?>" rel="stylesheet">
<link href="{{ Utils::getUrlBase('sami') }}/library/sami/icons/icons.css?v=<?php echo rand(); ?>"
    rel="stylesheet">
