@if ($type == 0)
    <sami-footer>
        <sami-paragraph class="m-0">Desarrollado por <sami-hyperlink color="white"
                href="{{ Utils::getUrlBase() }}" target="_blank">{{ GlobalClass::getCreatorUser() }}
            </sami-hyperlink>
        </sami-paragraph>
    </sami-footer>
@endif
@if ($type == 1)
    <sami-footer>
        <sami-paragraph class="m-0">Implementaci&oacute;n de Sami <sami-hyperlink color="white"
                href="{{ Utils::getUrlBase('sami') }}" target="_blank">npm i @jersson-arrivasplata-rojas/sami
            </sami-hyperlink>
        </sami-paragraph>
    </sami-footer>
@endif
