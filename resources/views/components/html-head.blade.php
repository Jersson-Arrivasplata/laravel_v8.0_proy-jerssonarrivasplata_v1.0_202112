 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png" href="{{ $linkImage }}">

 <!--Inicio Fonts-->
 @include('common.fonts')
 <!--Fin Fonts-->

 <!--Inicio seo-->
 @include('common.metatags-seo')

 <!--Fin seo-->

 <!--Inicio Limpiar Cache-->
 @include('common.metatags-cache')
 <!--Fin Limpiar Cache-->

 <meta name="msapplication-TileColor" content="{{ $color }}">
 <meta name="msapplication-TileImage" content="{{ $linkImage }}">
 <meta name="theme-color" content="{{ $color }}">

 <!--Inicio Metatags Open Graph-->
 @include('common.metatags-open-graph')
 <meta property="og:image" content="{{ $linkImage }}">
 <meta property="og:image:secure_url" content="{{ $linkImage }}">
 <!--Fin Metatags Open Graph-->

 <!--Inicio Metatags Twitter-->
 @include('common.metatags-twitter')
 <meta name="twitter:image" content="{{ $linkImage }}">
 <!--Fin Metatags Twitter-->

 <link href="{{ $link }}/library/sami/icons/icons.css?v=<?php echo rand(); ?>" rel="stylesheet">
 <script defer="defer" src="{{ $link }}/library/sami-utils/core/dist/html2canvas.js"></script>
 <script defer="defer" src="{{ $link }}/library/sami-utils/core/dist/sweetalert2.js"></script>
 <script defer="defer" src="{{ $link }}/library/sami-utils/core/dist/@highlightjs.js"></script>
 <link href="{{ $link }}/library/sami-utils/core/dist/@highlightjs.css" rel="stylesheet">
 <script defer="defer" src="{{ $link }}/library/sami-utils/core/dist/app.js"></script>
 <!--
 
 -->

 <script type="module" src="{{ $link }}/library/sami/build/sami.esm.js?v=<?php echo rand(); ?>"></script>
 <script nomodule src="{{ $link }}/library/sami/build/sami.js?v=<?php echo rand(); ?>"></script>
 <script>
     var exports = {
         "__esModule": true
     };
 </script>



 <script src="{{ $link }}/library/jquery/3.4.1/jquery-3.4.1.min.js"></script>
 <script src="{{ $link }}/library/sweetalert2/sweetalert2.min.js"></script>

 <link href="{{ $link }}/css/base.css?v=<?php echo rand(); ?>" rel="stylesheet">

 <x-google-tag-manager id="{{ $googleTagManagerId }}"></x-google-tag-manager>
