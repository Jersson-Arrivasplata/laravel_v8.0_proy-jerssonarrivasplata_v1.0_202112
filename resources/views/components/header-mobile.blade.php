<sami-header-mobile class="t-0">
    <sami-hyperlink href="{{ $link }}" class="ml-10 pt-1" position="absolute" onclick="loader()" target="_self">
        <sami-img width="40" height="40" 
            src="{{ $linkImage }}">
        </sami-img>
    </sami-hyperlink>
    <sami-hyperlink color="white" position="absolute" border-style="solid" border-width="2" decoration="none" class="sami-subscriber-hyperlink t-0 p-2 r-0 mt-1" href="#" target="_self"
        onclick="subscriberShow()">
        CLICK SUSCRIBETE
    </sami-hyperlink>
</sami-header-mobile>
