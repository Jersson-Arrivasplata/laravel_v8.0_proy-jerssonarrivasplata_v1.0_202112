<!-- Swiper -->
<div class="swiper mySwiper">
    <div class="swiper-wrapper">
        @foreach ($content as $c)
            <div class="swiper-slide">
                <x-card-publish id="{{$c['id']}}" before="{{ $c['before'] ?? '' }}" position="{{ $c['position'] ?? '' }}"
                    borderColor="{{ $c['contents']['background'] ?? '' }}" lineHeight="{{ $c['lineHeight'] ?? '' }}"
                    fontSize="{{ $c['contentfontSizeRange'] ?? '' }}" title="{!! $c['contentTitle'] ?? '' !!}"
                    subtitle="{!! $c['contentSubTitle'] ?? '' !!}" codes="{!! $c['codes'] ?? '' !!}"
                    preview="{{ $c['checkPreviewBox'] ?? '' }}" height="{!! $c['height'] ?? '' !!}"
                    activated="{{ $c['activated'] ?? '' }}"
                    status="{{ $status ?? false }}"
                    image="{{ isset($c['contents']) ? Utils::getUrlBase($type) . '/img/blog/content-type/' . $c['contents']['image'] : '' }}">
                </x-card-publish>
                @if ($status)
                    <div class="d-flex justify-content-center mt-2 mb-2" style="z-index: 1; position:relative;">
                        @if(isset($content[0]['socials']))

                            @foreach ($content[0]['socials'] as $social)
                                <a href="{{$social['url']}}" target="_blank" class="mr-3 text-white d-flex">
                                    @php
                                        $icon = App\Classes\SocialIconHelper::getIconName($social['type']);
                                    @endphp
                                    <img style="width: 28px;" width="25" src="{{Utils::getUrlBase($type) }}/img/icons/{{$icon}}" />
                                </a>
                            @endforeach
                        @endif
                        <a href="javascript:void(0)" class="mr-3" onclick="generateImage(this)">
                            <img style="filter: invert(1);" width="25" src="{{Utils::getUrlBase($type) }}/library/sami/assets/icon/picture.svg" />
                        </a>
                    </div>
                @endif
                @if (!$status)
                    <div class="d-flex justify-content-center mt-2 mb-2">
                        <span class="mr-3 text-white ">Cantidad de Social Media: {{count($content[0]['socials'])}}</span>
                        <a href="{{ Utils::getUrlBase($type) }}/blog-social-media/{{$content[0]['idRow']}}" target="_self" class="mr-3 text-white d-flex" >
                            <img style="width: 28px;" width="25" src="{{Utils::getUrlBase($type) }}/img/icons/linkedin.svg" />
                        </a>
                        <a href="javascript:void(0)" class="mr-3" onclick="generateImage(this)">
                            <img style="filter: invert(1);" width="25" src="{{Utils::getUrlBase($type) }}/library/sami/assets/icon/picture.svg" />
                        </a>
                        <a href="javascript:void(0)" class="mr-3" onclick="show(this)">
                            <img style="filter: invert(1);" width="25" src="{{Utils::getUrlBase($type) }}/img/icons/edit.svg" />
                        </a>
                        <a href="javascript:void(0)" class="mr-3" onclick="addNewBlank(this)">
                            <img style="filter: invert(1);" width="25" src="{{Utils::getUrlBase($type) }}/img/icons/copy.svg" />
                        </a>
                        <a href="javascript:void(0)" class="mr-3" onclick="remove(this)">
                            <img style="filter: invert(1);" width="25" src="{{Utils::getUrlBase($type) }}/img/icons/close.svg" />
                        </a>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
    <!-- Add Navigation -->
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>

</div>
