<sami-list list-style="none" opacity="8" filter-invert="1" class="d-flex justify-content-evenly m-0 p-0">
    <sami-list-item>
        <sami-hyperlink href="{{ config('base.repositories.github.jersson-arrivasplata') }}" target="_blank">
            <sami-img width="25"
                src="{{ $link }}/library/sami/assets/icon-social-media/github.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    <sami-list-item>
        <sami-hyperlink href="{{ config('base.social_networks.linkedin') }}" target="_blank">
            <sami-img width="25"
                src="{{ $link }}/library/sami/assets/icon-social-media/linkedin.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    <sami-list-item>
        <sami-hyperlink href="{{ config('base.social_networks.instagram') }}" target="_blank">
            <sami-img width="25"
                src="{{ $link }}/library/sami/assets/icon-social-media/instagram.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    <sami-list-item>
        <sami-hyperlink href="{{ config('base.social_networks.facebook') }}" target="_blank">
            <sami-img width="25"
                src="{{ $link }}/library/sami/assets/icon-social-media/facebook.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
    <sami-list-item>
        <sami-hyperlink href="{{ config('base.social_networks.whatsapp') }}" target="_blank">
            <sami-img width="25"
                src="{{ $link }}/library/sami/assets/icon-social-media/whatsapp.svg">
            </sami-img>
        </sami-hyperlink>
    </sami-list-item>
</sami-list>
