
<style type="text/css">
    iframe{
        height: 100%;
        width: 100%;
    }
</style>
<iframe id="frame"  src="{{$link}}" frameborder="0" allowfullscreen></iframe>
<script type="text/javascript">
    // Selecting the iframe element
    var iframe = document.getElementById("frame");
    
    // Adjusting the iframe height onload event
    iframe.onload = function(){
        iframe.style.height = (iframe.contentWindow.document.body.scrollHeight + 40) + 'px';
    }
</script>