<title>{{ $title }}</title>
<meta property="og:type" content="{{$ogType}}">
<meta name="description" content="{{ $description }}">

<meta property="og:description" content="{{ $description }}">
<meta property="og:title" content="{{ $title }}">
<meta property="og:url" content="{{ $link }}">

<meta name="twitter:card" content="{{$twitterContentCard}}">
<meta name="twitter:url" content="{{ $link }}">
<meta name="twitter:title" content="{{ $title }}">
<meta name="twitter:description" content="{{ $description }}">
