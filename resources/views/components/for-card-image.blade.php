@foreach (json_decode($content) as $data)
    @if ($type == 0 && Utils::getCounterProjectContent($data->type) > 0)
        <x-card-image title="{{ $data->name }}" subtitle="{{ $data->description }}"
            link="{{ url(request()->route()->parameter('language').'/blog'.'/'.'project/'.Str::slug($data->name, '-')) }}"
            counter="{{ Utils::getCounterProjectContent($data->type) }}"
            link-image="{{ $link }}/img/blog/content-type/{{ $data->image }}">
        </x-card-image>
    @endif
    @if ($type == 1 && Utils::getCounterBlogContentGenerate($data->type) > 0)
        <x-card-image title="{{ $data->name }}" subtitle="{{ $data->description }}"
            link="{{ url(request()->route()->parameter('language').'/blog'.'/'.Str::slug($data->name, '-')) }}"
            counter="{{ Utils::getCounterBlogContentGenerate($data->type) }}"
            link-image="{{ $link }}/img/blog/content-type/{{ $data->image }}">
        </x-card-image>
    @endif
@endforeach
