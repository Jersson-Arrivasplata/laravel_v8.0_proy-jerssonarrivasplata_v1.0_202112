<div id="sami-sidebar">
    <sami-sidebar>
        <sami-hyperlink part="menu" decoration="none" color="white" href="#"  onclick="sidebarShow()">
            <sami-icon class="mdi mdi-menu"></sami-icon>
        </sami-hyperlink>
        <div part="header">
            <x-sidebar-header
            link="{{ $link }}"
            link-image="{{ $linkImage }}"
            ></x-sidebar-header>
        </div>
        <div part="body" class="mb-13">
            {{$slot}}
        </div>
        <div part="footer">
            <x-list-social-media
            link="{{ $link }}"
            ></x-list-social-media>
        </div>
    </sami-sidebar>
</div>
<script type="text/javascript">
    function sidebarShow() {
        (async () => {
            await customElements.whenDefined('sami-sidebar');
            const samiSidebar = document.querySelector('sami-sidebar');
            await samiSidebar.show();
        })();
    }
</script>

