<div style="height: 295px;">
    <sami-card-image url="{{ $link }}" onclick="loader()" style="height: 295px;width: 315px;">
        @if($counter!="")
            <sami-span>{{ $counter }}</sami-span>
        @endif
        <sami-img object-fit="cover" src="{{ $linkImage }}">
        </sami-img>
        <sami-paragraph>{{ $subtitle }}</sami-paragraph>
        <sami-paragraph>{{ $title }}</sami-paragraph>
    </sami-card-image>
</div>
