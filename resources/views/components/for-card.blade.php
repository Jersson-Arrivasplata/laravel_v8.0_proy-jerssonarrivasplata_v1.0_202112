@foreach (json_decode($content) as $i => $data)
    @if (Utils::getCounterProjectContent($data->type) > 0)
        <x-card counter="{{ $i }}"
            link="{{ Utils::getUrlBase() }}/{{request()->route()->parameter('language')}}/blog/project/{{ Str::slug(App\Models\BlogContentType::where('type', $type)->value('name'), '-') }}/{{ $data->url }}"
            link-image="{{ Utils::getUrlBase() }}/projects/{{ $data->path }}image/{{ $data->image }}"
            subtitle="{{ App\Models\BlogContentType::where('type', $type)->value('name') }}"
            title="{{ $data->name }}" description="{{ $data->description }}">
        </x-card>
    @endif
@endforeach
