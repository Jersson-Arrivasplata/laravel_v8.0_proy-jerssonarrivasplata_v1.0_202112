<sami-list-item>
    <sami-hyperlink href="{{$href}}" target="_self" >
        <sami-paragraph>{{$text}}</sami-paragraph>
    </sami-hyperlink>
</sami-list-item>
