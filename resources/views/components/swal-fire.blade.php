<script type="text/javascript">
    (function() {
        new window.exports.SamiUtils.SwalFireTwo().swalFireTimerToSuccess('{!! session('success') !!}', "#fff url({{ $link }}/img/background/trees.png)", `rgba(0,0,123,0.4) url("{{ $link }}/img/background/nyan-cat.gif") left topno-repeat`);
    })();
</script>
