
<div id="sami-sidebar-dashboard">
    <sami-sidebar-dashboard>
        <sami-hyperlink part="menu" decoration="none" color="white" href="#" onclick="sidebarDashboardShow()">
            <sami-icon class="mdi mdi-menu"></sami-icon>
        </sami-hyperlink>
        <div part="body" class="mb-13">
            <sami-list list-style="none" class="m-0 p-0">
                {{$slot}}
            </sami-list>
        </div>
    </sami-sidebar-dashboard>
</div>
<script type="text/javascript">
    function sidebarDashboardShow() {
        (async () => {
            await customElements.whenDefined('sami-sidebar-dashboard');
            const samiSidebarDashboard = document.querySelector('sami-sidebar-dashboard');
            await samiSidebarDashboard.show();
        })();
    }
</script>
