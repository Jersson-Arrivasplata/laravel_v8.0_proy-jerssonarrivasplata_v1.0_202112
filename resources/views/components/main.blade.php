@if ($width!=="")
    <sami-main style="height: 100%;" width="{{ $width }}">
        <div slot="main">
            {{ $slot }}
        </div>
    </sami-main>
@else
    <sami-main style="height: 100%;">
        <div slot="main">
            {{ $slot }}
        </div>
    </sami-main>
@endif
