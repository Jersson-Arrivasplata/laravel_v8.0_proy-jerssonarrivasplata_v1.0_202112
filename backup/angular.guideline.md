# Lineamientos de Angular

---

<a name="section-angular-guideline-1"></a>

## Lineamiento para nombrar proyecto en angular

<a href="#section-angular-guideline-1-step-1" name="section-angular-guideline-1-step-1">
  ### <span># </span>Paso 1. Tabla de Lineamiento 
</a>

1. Lineamientos de Nombre de Repositorios

| Framework | Versión    | Tipo | Nombre      | Versión | Año  | Mes | Nombre del Repositorio                         |
| --------- | ---------- | ---- | ----------- | ------- | ---- | --- | ---------------------------------------------- |
| ANGULAR   | VXX.XX.XX  | PROY | XXXXXXXXXX  | VX.X    | XXXX | XX  | ANGULAR_VXX.XX.XX_PROY-XXXXXXXXXX_VX.X_XXXXXX  |
| ANGULAR   | V11.2.14   | PROY | EJEMPLO     | V1.0    | 2022 | 01  | ANGULAR_V11.2.14_PROY-EJEMPLO_V1.0_202201      |

<a href="#section-angular-guideline-1-step-2" name="section-angular-guideline-1-step-2">
  ### <span># </span>Paso 2. Descripción de la Tabla de Lineamiento
</a>

1. Columna #01 (Framework): Nombre del Framework del desarrollo.
2. Columna #02 (Versión - **VXX.XX.XX**): Versión del Framework del desarrollo - (Ejemplo - **V11.2.14**).
3. Columna #03 (Tipo): Tipo de proyecto que hace referencia al proyecto - (Ejemplo: **PROY** -> Proyecto).
4. Columna #04 (Nombre): Nombre que se le asignará al proyecto.
5. Columna #05 (Versión - **VX.X**): Versión del proyecto - (Ejemplo: **V1.0**).
6. Columna #06 (Año - **YYYY**): Año en la que se creo el proyecto - (Ejemplo: **2022**).
7. Columna #07 (Mes - **MM**): Mesa en la que se creo el proyecto - (Ejemplo: **01**).
8. Columna #08 (Nombre del Repositorio): Nombre final que resulta al unir todos los campos anteriores - (Ejemplo: **ANGULAR_V11.2.14_PROY-EJEMPLO_V1.0_202201**).


<a name="section-angular-guideline-2"></a>

## Lineamientos del uso de Enum en Proyecto Angular


<a href="#section-angular-guideline-2-step-1" name="section-angular-guideline-2-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos del uso de Enum 
</a>

1. Tabla de Lineamiento de Archivos Enum

| Convención de Nombre             | Descripción                                                                          | Resultado                  |
| -------------------------------- | ------------------------------------------------------------------------------------ | -------------------------- |
| Lower Kebab Case                 | Cada palabra en minuscula se separa con un guión y se concatena con (**.enum.ts.**)  | lower-kebab-case.enum.ts   |

2. Tabla de Lineamiento de Nombre de Clases Enum

| Convención de Nombre             | Descripción                                                                                                  | Resultado                  |
| -------------------------------- | ------------------------------------------------------------------------------------------------------------ | -------------------------- |
| Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas y se concatena con (**Enum**) al ultimo.  | UpperCamelCaseEnum         |

3. Tabla de Lineamiento de Clave - Valor de un Enum

| Convención de Nombre             | Descripción                                                             | Resultado                  |
| -------------------------------- | ----------------------------------------------------------------------- | -------------------------- |
| Upper Snake Case                 | Cada letra en mayuscula separada por un guión bajo.    	               | UPPER_SNAKE_CASE           |

<a href="#section-angular-guideline-2-step-2" name="section-angular-guideline-2-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Archivos Enum
</a>

Ejemplo: Archivo Enum que contiene los errores de un registro

```js
document-type.enum.ts

export enum ErrorMessageRegisterEnum {
  ERROR_COMPLETE_FIELD = 'Debes completar este campo.'
}

```

1. **document-type.enum.ts**: Revisar la Tabla de Lineamiento de Archivos Enum
2. **ErrorMessageRegisterEnum**: Revisar la Tabla de Lineamiento de Nombre de Clases Enum
3. **ERROR_COMPLETE_FIELD**: Revisar la Tabla de Lineamiento de Clave - Valor de un Enum



<a name="section-angular-guideline-3"></a>

## Lineamientos del uso de Interfaces en Proyecto Angular


<a href="#section-angular-guideline-3-step-1" name="section-angular-guideline-3-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos del uso de Interfaces 
</a>

1. Tabla de Lineamiento de Archivos Interface

| Convención de Nombre             | Descripción                                                                               | Resultado                       |
| -------------------------------- | ----------------------------------------------------------------------------------------- | ------------------------------- |
| Lower Kebab Case                 | Cada palabra en minuscula se separa con un guión y se concatena con (**.interface.ts.**)  | lower-kebab-case.interface.ts   |

2. Tabla de Lineamiento de Nombre de Clase Interface

| Convención de Nombre             | Descripción                                                                                                 | Resultado                  |
| -------------------------------- | ----------------------------------------------------------------------------------------------------------- | -------------------------- |
| Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas y se concatena con (**I**) al inicio. 	 | IUpperCamelCase            |

3. Tabla de Lineamiento de métodos y propiedades de una Interface

| Tipo               | Convención de Nombre             | Descripción                                                                                        | Resultado                  |
| ------------------ | -------------------------------- | ------------------------------------------------------------------------------------------------   | -------------------------- |
| Propiedad o Método | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas e inicia con minúscula.         | upperCamelCase             |
| Interface | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas  y se concatena con (**I**) al inicio.	 | IUpperCamelCase            |
| Enum      | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas y se concatena con (**Enum**) al ultimo. | UpperCamelCaseEnum         |
| Type      | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas  y se concatena con (**T**) al inicio.	 | TUpperCamelCase            |


<a href="#section-angular-guideline-3-step-2" name="section-angular-guideline-3-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Archivos Interface
</a>

Ejemplo: Archivo Interface que contiene las propiedades de un perfil de usuario 

```js
user.interface.ts

export interface IUser {
  name: string;
  lastName: string;
  birthDate: Date;
  age: void;
}

```

1. **user.interface.ts**: Revisar la Tabla de Lineamiento de Archivos Interface
2. **IUser**: Revisar la Tabla de Lineamiento de Nombre de Clase Interface
3. **name || lastName || birthDate || age**: Revisar la Tabla de Lineamiento de métodos y propiedades de una Interface

<a name="section-angular-guideline-4"></a>

## Lineamientos del uso de Types en Proyecto Angular


<a href="#section-angular-guideline-4-step-1" name="section-angular-guideline-4-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos del uso de Types 
</a>

1. Tabla de Lineamiento de Archivos Types

| Convención de Nombre             | Descripción                                                                          | Resultado                  |
| -------------------------------- | ------------------------------------------------------------------------------------ | -------------------------- |
| Lower Kebab Case                 | Cada palabra en minuscula se separa con un guión y se concatena con (**.type.ts.**)  | lower-kebab-case.type.ts   |

2. Tabla de Lineamiento de Nombre de Clase Type

| Convención de Nombre             | Descripción                                                                                                 | Resultado                  |
| -------------------------------- | ----------------------------------------------------------------------------------------------------------- | -------------------------- |
| Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas  y se concatena con (**T**) al inicio.	 | TUpperCamelCase            |


3. Tabla de Lineamiento de métodos y propiedades de Clase Type

| Tipo               | Convención de Nombre             | Descripción                                                                                        | Resultado                  |
| ------------------ | -------------------------------- | ------------------------------------------------------------------------------------------------   | -------------------------- |
| Propiedad o Método | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas e inicia con minúscula.         | upperCamelCase             |
| Interface | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas  y se concatena con (**I**) al inicio.	 | IUpperCamelCase            |
| Enum      | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas y se concatena con (**Enum**) al ultimo. | UpperCamelCaseEnum         |
| Type      | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas  y se concatena con (**T**) al inicio.	 | TUpperCamelCase            |

<a href="#section-angular-guideline-4-step-2" name="section-angular-guideline-4-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Archivos Types
</a>

Ejemplo: Archivo Type que contiene las propiedades de un perfil de usuario 

```js
user.type.ts

export type TUser = {
  name: string;
  lastName: string;
  birthDate: Date;
  age: void;
}

```
Ejemplo: Archivo Type que contiene las propiedades de una interfaz de perfil de usuario
```js
user.type.ts

export type TUser = IUser;

```

1. **user.type.ts**: Revisar la Tabla de Lineamiento de Archivos Types
2. **TUser**: Revisar la Tabla de Lineamiento de Nombre de Clase Type
3. **name || lastName || birthDate || age || IUser**: Revisar la Tabla de Lineamiento de métodos y propiedades de Clase Type

<a name="section-angular-guideline-5"></a>

## Lineamientos del uso de Environments en Proyecto Angular


<a href="#section-angular-guideline-5-step-1" name="section-angular-guideline-5-step-1">
  ### <span># </span>Paso 1. Tablas de Lineamientos del uso de Environments 
</a>

1. Tabla de Lineamiento de Archivos Environment

| Convención de Nombre       | Descripción                                                        | Resultado        |
| -------------------------- | ------------------------------------------------------------------ | ---------------- |
| Lower Case                 | Cada palabra en minuscula y se concatena con un tipo (**.xx.ts**)  | environment.ts   |

2. Tabla de Lineamiento de Tipos de Environment

| Convención de Nombre             | Tipos                                                                       | Resultado                  |
| -------------------------------- | --------------------------------------------------------------------------- | -------------------------- |
| Lower Kebab Case                 | **.ts** o  **.local** o  **.dev.ts** o  **.cert.ts** o  **.prod.ts**        | environment.dev.ts         |


3. Tabla de Lineamiento de métodos y propiedades de la constante Environment

| Tipo               | Convención de Nombre             | Descripción                                                                                    | Resultado                  |
| ------------------ | -------------------------------- | --------------------------------------------------------------------------------------------   | -------------------------- |
| Propiedad o Método | Upper Camel Case o Camel Case    | La primera letra de cada palabra concatenada se pone en mayúsculas e inicia con minúscula.     | upperCamelCase             |

<a href="#section-angular-guideline-5-step-2" name="section-angular-guideline-5-step-2">
  ### <span># </span>Paso 2. Ejemplo de Lineamiento de Environment
</a>

Ejemplo: Archivo Environment que contiene las propiedades de un ambiente de desarrollo

```js
environment.dev.ts

export const environment = {
  production: false,
  apiUrl: 'https://www.xxx.com/api/',
  baseUrl: 'https://www.xxx.com/',
  type: 'dev'
};

```

1. **environment.dev.ts**: Revisar la Tabla de Lineamiento de Archivos Environment
2. **.dev.ts**: Revisar la Tabla de Lineamiento de Tipos de Environment
3. **production || apiUrl || baseUrl || type**: Revisar la Tabla de Lineamiento de métodos y propiedades de la constante Environment
