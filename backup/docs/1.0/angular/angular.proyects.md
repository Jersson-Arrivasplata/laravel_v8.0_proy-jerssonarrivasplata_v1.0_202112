# Proyectos Realizados

---

<a name="section-angular-lineamiento-1"></a>

## Tabla de Proyectos Realizados

| Nombre del Repositorio                                  | Ruta del Repositorio en Github                 | Descripción del Proyecto                       |
| ------------------------------------------------------- | ---------------------------------------------- | ---------------------------------------------- |
| ANGULAR_V11.2.14_PROY-FORMULARIO-REACTIVO_V1.0_202201   | [github.com/jersson-arrivasplata-rojas/ANGULAR_V11.2.14_PROY-FORMULARIO-REACTIVO_V1.0_202201](https://github.com/jersson-arrivasplata-rojas/ANGULAR_V11.2.14_PROY-FORMULARIO-REACTIVO_V1.0_202201) | Se realizá la implementación de un Formulario Reactivo - (Reactive Forms) utilizando la libreria de angular @angular/forms |
