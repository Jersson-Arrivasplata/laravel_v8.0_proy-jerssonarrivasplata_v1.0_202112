# Lineamientos de Angular

---

<a name="section-angular-lineamiento-1"></a>

## Lineamiento para nombrar proyecto en angular

### Paso 1. Tabla de Lineamiento
1. Lineamientos de Nombre de Repositorios

| Framework | Versión    | Tipo | Nombre      | Versión | Año  | Mes | Nombre del Repositorio                         |
| --------- | ---------- | ---- | ----------- | ------- | ---- | --- | ---------------------------------------------- |
| ANGULAR   | VXX.XX.XX  | PROY | XXXXXXXXXX  | VX.X    | XXXX | XX  | ANGULAR_VXX.XX.XX_PROY-XXXXXXXXXX_VX.X_XXXXXX  |
| ANGULAR   | V11.2.14   | PROY | EJEMPLO     | V1.0    | 2022 | 01  | ANGULAR_V11.2.14_PROY-EJEMPLO_V1.0_202201      |

### Paso 2. Descripción de la Tabla de Lineamiento
1. Columna #01 (Framework): Nombre del Framework del desarrollo.
2. Columna #02 (Versión - **VXX.XX.XX**): Versión del Framework del desarrollo - (Ejemplo - **V11.2.14**).
3. Columna #03 (Tipo): Tipo de proyecto que hace referencia al proyecto - (Ejemplo: **PROY** -> Proyecto).
4. Columna #04 (Nombre): Nombre que se le asignará al proyecto.
5. Columna #05 (Versión - **VX.X**): Versión del proyecto - (Ejemplo: **V1.0**).
6. Columna #06 (Año - **YYYY**): Año en la que se creo el proyecto - (Ejemplo: **2022**).
7. Columna #07 (Mes - **MM**): Mesa en la que se creo el proyecto - (Ejemplo: **01**).
8. Columna #08 (Nombre del Repositorio): Nombre final que resulta al unir todos los campos anteriores - (Ejemplo: **ANGULAR_V11.2.14_PROY-EJEMPLO_V1.0_202201**).
