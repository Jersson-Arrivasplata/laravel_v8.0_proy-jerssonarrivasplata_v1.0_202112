-- --------------------------------------------------------
-- Host:                         50.116.65.96
-- Versión del servidor:         5.7.23-23 - Percona Server (GPL), Release 23, Revision 500fcf5
-- SO del servidor:              Linux
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para jerssona_WPC1X
CREATE DATABASE IF NOT EXISTS `jerssona_WPC1X` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `jerssona_WPC1X`;

-- Volcando estructura para tabla jerssona_WPC1X.blog_content
CREATE TABLE IF NOT EXISTS `blog_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `documention_link` text COLLATE utf8_unicode_ci,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `resume` text COLLATE utf8_unicode_ci,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `button_content` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'Ver Imagen',
  `path` text COLLATE utf8_unicode_ci,
  `url` text COLLATE utf8_unicode_ci,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.blog_content: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `blog_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_content` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.blog_content_generates
CREATE TABLE IF NOT EXISTS `blog_content_generates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_blog_content_type_group` int(11) DEFAULT '0',
  `id_blog_content_type` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `idRow` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgTitle` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgSubTitle` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contentTitle` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contentSubTitle` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selectSocialMedia` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selectSocialMediaWidth` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selectSocialMediaHeight` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bgColor` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typeLanguage` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typeLanguageBgColor` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typeLanguageTitle` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typeLanguageSubTitle` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkImageBox` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkContentBorderLeft` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkContentBorderRight` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkContentDirectionRight` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkPreviewBox` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgRange` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contentWidthRange` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contentHeightRange` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgFontSizeRange` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contentTitleFontSizeRange` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contentfontSizeRange` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contentBottomFontSizeRange` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contentMargintop` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contentPaddingLeft` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contentPaddingRight` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkPosition` varchar(250) COLLATE utf8_unicode_ci DEFAULT '0',
  `codes` text COLLATE utf8_unicode_ci,
  `url_linkedin` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.blog_content_generates: ~16 rows (aproximadamente)
/*!40000 ALTER TABLE `blog_content_generates` DISABLE KEYS */;
INSERT INTO `blog_content_generates` (`id`, `id_blog_content_type_group`, `id_blog_content_type`, `position`, `idRow`, `imgTitle`, `imgSubTitle`, `contentTitle`, `contentSubTitle`, `selectSocialMedia`, `selectSocialMediaWidth`, `selectSocialMediaHeight`, `bgColor`, `typeLanguage`, `typeLanguageBgColor`, `typeLanguageTitle`, `typeLanguageSubTitle`, `checkImageBox`, `checkContentBorderLeft`, `checkContentBorderRight`, `checkContentDirectionRight`, `checkPreviewBox`, `imgRange`, `contentWidthRange`, `contentHeightRange`, `imgFontSizeRange`, `contentTitleFontSizeRange`, `contentfontSizeRange`, `contentBottomFontSizeRange`, `contentMargintop`, `contentPaddingLeft`, `contentPaddingRight`, `checkPosition`, `codes`, `url_linkedin`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(4, 1, 1, 0, 'M234', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', 'ES6 - Función Flecha', 'Suma de 2 números', 'linkedIn', '520', '320', '#cab500', 'js-min.png', '#f7e018', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', '0', '0', '1', '0', '0', '125', '395', '320', '14', '18', '18', '10', '60', '20', '20', '0', 'const suma = (a,b) => a + b;\r\nsuma(1, 2);\r\nResultado: 3', 'https://www.linkedin.com/feed/update/urn:li:activity:6904630857743564800', 'Ejemplo de una suma de 2 números utilizando la función flecha de javascript ES6.\r\n#jerssonarrivasplata #jerssonarrivasplataDEV #jerssonarrivasplataPERU #javascript #developer', '2022-02-28 01:05:10', '2022-03-24 06:27:28', NULL),
	(8, 1, 1, 0, 'M235', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', 'ES6 - Operador Condicional Ternario', 'Condicionales en JavaScript', 'linkedIn', '520', '320', '#cab500', 'js-min.png', '#f7e018', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', '0', '0', '1', '0', '0', '125', '395', '320', '14', '18', '14.5', '10', '30', '20', '20', '0', 'const validateLength = data => {\n  return data.length > 17 ? true: false;\n}\n\nvalidateLength("pruebapruebaprueba");\nRespuesta: true\n\nvalidateLength("prueba");\nRespuesta: false', 'https://www.linkedin.com/feed/update/urn:li:activity:6904690690203168768', 'Aplicación de un operador condicional ternario utilizando la función flecha de javascript ES6.\r\n#jerssonarrivasplata #jerssonarrivasplataDEV #jerssonarrivasplataPERU #javascript #developer', '2022-03-02 07:32:48', '2022-03-24 06:27:36', NULL),
	(11, 1, 1, 0, 'M236', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', 'ES5 - Función Reduce', 'Acumulador que retorna un único valor', 'linkedIn', '520', '320', '#cab500', 'js-min.png', '#f7e018', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', '1', '1', '1', '0', '0', '50', '520', '320', '14', '18', '14', '10', '15', '80', '50', '0', 'const reducer = (valorAnterior, valorActual) => {\n  return valorAnterior + valorActual;\n};\n\nfunction sumaTotal(array) {\n  return array.reduce(reducer);\n}\n\nsumaTotal([1,2,3]);\nResultado: 6\n\nsumaTotal([\'a\',\'b\',\'c\']);\nResultado:\'abc\'', 'https://www.linkedin.com/feed/update/urn:li:activity:6905184664156213249', 'Aplicación de una función reductora que recibe un arreglo y lo acumula en el retorno de un único valor - JavaScript ES5.\r\n#jerssonarrivasplata #jerssonarrivasplataDEV #jerssonarrivasplataPERU #javascript #developer', '2022-03-03 15:37:12', '2022-03-24 06:27:38', NULL),
	(14, 2, 3, 0, 'M237', 'LENGUAJE DE MARCADO DE HIPERTEXTO', 'Html5', 'HTML5 - Aplicación de Propiedad Flex', 'Elemento Flexible', 'linkedIn', '520', '320', '#bb5022', 'html.png', '#bb5022', 'LENGUAJE DE MARCADO DE HIPERTEXTO', 'Html5', '1', '1', '1', '0', '0', '50', '520', '320', '14', '18', '18', '10', '25', '80', '20', '0', '<style type="text/stylesheet">\n    #content{\n        display:flex;\n    }\n</style>\n<div id="content">\n    <div>1era Columna</div>\n    <div>2da Columna</div>\n</div>', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6906051614399184896-pDvY', 'Aplicación de la propiedad Flex que se incorporó en Css3 para mostrar elementos flexibles.\r\nNota: Se realiza una muestra en html y en css de "display:flex".\r\n#jerssonarrivasplata #jerssonarrivasplataDEV #jerssonarrivasplataPERU #javascript #developer', '2022-03-05 10:39:29', '2022-03-24 06:27:41', NULL),
	(15, 2, 2, 0, 'M237', 'ESTILOS', 'Css', 'CSS3 - Aplicación de propiedad Flex', 'Elemento Flexible', 'linkedIn', '520', '320', '#2645b7', 'css.png', '#2654bf', 'ESTILOS', 'Css', '1', '1', '1', '0', '1', '50', '520', '320', '14', '18', '18', '10', '30', '20', '20', '0', '<div class="grid-colum-autoflow  adervc34435dfdf">\n    <div>\n        <p class="mb-0 mt-0">Sin utilizar Flex</p>\n        <div>\n            <div class="column bg-red m-auto">1era Columna</div>\n            <div class="column bg-green m-auto">2da Columna</div>\n        </div>\n    </div>\n    <div>\n        <p class="mb-0 mt-0">Utilizando Flex</p>\n        <p class="mb-0 mt-1">"display:flex"</p>\n        <div class="mt-0" style="display: flex;justify-content: center;">\n            <div class="column bg-red">1era Columna</div>\n            <div class="column bg-green">2da Columna</div>\n        </div>\n    </div>\n</div>\n<style>\n    /*html{\n        margin-top: 0px;\n    }\n    *{\n        margin-top: 1rem;\n        font-family: sans-serif;\n    }*/\n.adervc34435dfdf {\n    width: 520px;\n    height: 235px;\n    position: absolute;\n    bottom: -32px;\n    left: 0;\n}\n   .adervc34435dfdf  p{\n        text-align: center;\n        font-weight: bolder;\n    }\n   .adervc34435dfdf.grid-colum-autoflow{\n    display: grid;\n    grid-auto-flow: column;\n    line-height: normal;\n    top: 0;\n    margin-top: 3rem;\n    }\n    .adervc34435dfdf  .column{\n        border-style: solid;\n        border-width: 1px;\n        width: 135px;\n        text-align: center;\n        padding-top: 0.8rem;\n        padding-bottom: 0.8rem;\n    }\n    .adervc34435dfdf  .bg-red{\n        background-color: rgb(228, 25, 25);\n    }\n    .adervc34435dfdf  .bg-green{\n        background-color: rgb(47, 212, 97);\n    }\n   .adervc34435dfdf   .m-auto{\n        margin: auto;\n    }\n    .adervc34435dfdf  .mb-0{\n        margin-bottom: 0px;\n    }\n    .adervc34435dfdf  .mt-0{\n        margin-top: 0px;\n    }\n   .adervc34435dfdf   .mt-1{\n        margin-top: 0.5em;\n    }\n  @media (max-width: 767px) {\n	.adervc34435dfdf{\n	  font-size: 12px;\n	  width: 100%;\n                   padding-top: 4rem;\n	}\n	\n	.adervc34435dfdf .column{\n	    width: 100px;\n   	  padding-top: 0.4rem;\n   	 padding-bottom: 0.4rem;\n	}\n }\n</style>', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6906051614399184896-pDvY', 'Aplicación de la propiedad Flex que se incorporó en Css3 para mostrar elementos flexibles.\r\nNota: Se realiza una muestra en html y en css de "display:flex".\r\n#jerssonarrivasplata #jerssonarrivasplataDEV #jerssonarrivasplataPERU #javascript #developer', '2022-03-05 10:39:29', '2022-03-24 06:27:48', NULL),
	(16, 1, 1, 1, 'VVSec9', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', 'ES6 - Función Generadora', 'Validación de LLamadas Máximas a una Función', 'linkedIn', '520', '320', '#cab500', 'js-min.png', '#cab500', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', '0', '0', '1', '0', '0', '125', '395', '320', '14', '18', '15', '10', '39', '20', '20', '1', 'function* clickToMax(max) {\n  let index = 0;\n  while (index < max) {\n    yield index;\n    index++;\n  }\n  return index;\n}', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6907004277324455937-8peh', 'Aplicación de una función generadora para contabilizar la cantidad máxima que se puede llamar a una función.', '2022-03-08 06:14:38', '2022-03-19 10:16:50', NULL),
	(17, 1, 1, 2, 'VVSec9', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', 'ES6 - Función Generadora', 'Función que valida a la función generadora', 'linkedIn', '520', '320', '#cab500', 'js-min.png', '#cab500', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', '0', '0', '1', '0', '0', '125', '395', '320', '14', '18', '16', '10', '36', '20', '20', '1', 'const max = 2;\nconst iterator = clickToMax(max);\n\nfunction validator(iterator) {\n    const data = iterator.next();\n    if(data.done) data.value = max;\n    return data;\n}', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6907004277324455937-8peh', 'Aplicación de una función generadora para contabilizar la cantidad máxima que se puede llamar a una función.', '2022-03-08 06:14:38', '2022-03-19 10:16:55', NULL),
	(18, 1, 1, 3, 'VVSec9', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', 'ES6 - Función Generadora', 'Validator(...) tiene como parametro la función generadora', 'linkedIn', '520', '320', '#cab500', 'js-min.png', '#cab500', 'LENGUAJE DE PROGRAMACIÓN', 'JavaScript', '0', '0', '1', '0', '0', '125', '395', '320', '14', '18', '15', '10', '0', '20', '20', '1', 'validator(iterator);\nResultado: {value: 0, done: false}\n\nvalidator(iterator);\nResultado: {value: 1, done: false}\n\nvalidator(iterator);\nResultado: {value: 2, done: true}\n\nvalidator(iterator);\nResultado: {value: 2, done: true}', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6907004277324455937-8peh', 'Aplicación de una función generadora para contabilizar la cantidad máxima que se puede llamar a una función.', '2022-03-08 06:14:38', '2022-03-19 10:16:58', NULL),
	(20, 5, 5, 0, 'VVSec2', 'SISTEMA DE CONTROL DE VERSIONES', 'Git', 'Git - Modificar Mensaje de Último Commit Realizado', 'git commit --amend', 'linkedIn', '520', '320', '#611300', 'git.png', '#611300', 'SISTEMA DE CONTROL DE VERSIONES', 'Git', '1', '1', '1', '0', '0', '50', '520', '320', '14', '18', '14', '10', '28', '70', '20', '0', 'Commit Actual:\nf2b9f00cf4 feat(JGAR-3881): Editar botones\n\n> git commit --amend\n> Crear botones (Comentario)\n> git push --force\n\nCommit Actualizado:\ng3c5t33sg5 feat(JGAR-3881): Crear botones', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6907515892608212992-xTrb', NULL, '2022-03-09 18:59:38', '2022-03-24 06:27:55', NULL),
	(21, 5, 5, 0, 'VVSec3', 'SISTEMA DE CONTROL DE VERSIONES', 'Git', 'Git - Eliminar Una Rama en Repositorio Remoto', 'git push origin --delete nombre-de-rama', 'linkedIn', '520', '320', '#611300', 'git.png', '#611300', 'SISTEMA DE CONTROL DE VERSIONES', 'Git', '1', '1', '1', '0', '0', '50', '520', '320', '14', '18', '15', '10', '40', '20', '20', '0', 'Rama actual: feature/JGAR-23456\n\n> git push origin --delete feature/JGAR-23456\n\nNota: El comando elimina la rama del ambiente remoto \nen el cual se encuentra (Github, Gitlab, Bitbucket,\notros).\n\n', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6907516978626764800-SM0J', NULL, '2022-03-09 19:20:37', '2022-03-24 06:27:57', NULL),
	(22, 0, 0, 1, 'jVXvx', 'OPTIMIZACIóN EN MOTORES DE BúSQUEDA', 'SEO', 'Seo - Etiqueta <title> para posicionamiento en los buscadores', 'Meta Etiqueta', 'linkedIn', '520', '320', '#1fb8d8', 'seo.png', '#1FB8D8', 'OPTIMIZACIóN EN MOTORES DE BúSQUEDA', 'SEO', '1', '1', '1', '0', '0', '50', '520', '320', '14', '18', '16', '10', '10', '60', '60', '1', '<!DOCTYPE html>\n<html lang="en">\n  <head>\n    <title>Título de la página</title>\n  </head>\n  <body>\n  </body>\n</html>', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6907700607143321600-bvZI', NULL, '2022-03-10 07:32:44', '2022-03-24 06:56:59', NULL),
	(23, 0, 0, 2, 'jVXvx', 'OPTIMIZACIóN EN MOTORES DE BúSQUEDA', 'SEO', 'Datos Importantes al ingresar un <title>', '<title>Título de la página</title>', 'linkedIn', '520', '320', '#1fb8d8', 'seo.png', '#1FB8D8', 'OPTIMIZACIóN EN MOTORES DE BúSQUEDA', 'SEO', '1', '1', '1', '0', '0', '50', '520', '320', '14', '18', '16', '10', '50', '50', '20', '1', '1- Título descriptivo, conciso y claro.\n2- La cantidad máxima de caracteres es 70\n3- Palabras clave al principio del título.\n4- Cada página debe tener su propio <title>\n5- No repetir el mismo <title> en cada página.', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6907700607143321600-bvZI', NULL, '2022-03-10 07:32:44', '2022-03-16 19:47:53', NULL),
	(24, 6, 6, 1, 'luoya', 'LENGUAJE DE PROGRAMACIóN', 'TypeScript', 'Aplicación de Modificador "readonly" y Excepción', 'Modificador de Acceso "public"', 'linkedIn', '520', '320', '#2d79c7', 'ts.png', '#27629d', 'LENGUAJE DE PROGRAMACIóN', 'TypeScript', '1', '1', '1', '0', '0', '60', '520', '320', '14', '18', '16', '10', '60', '20', NULL, '1', 'class Utils {\n    public readonly USER_PREFIX = "ABC___";\n    public static readonly ASSETS_URL = "/assets/";\n}\n', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6912788832857808896-AhB4?utm_source=linkedin_share&utm_medium=member_desktop_web', NULL, '2022-03-23 21:15:45', '2022-03-24 10:02:56', NULL),
	(25, 6, 6, 2, 'luoya', 'LENGUAJE DE PROGRAMACIóN', 'TypeScript', 'Consulta De Variable Estática ASSETS_URL', 'Palabra reservada "static" con  modificador de variable "readonly"', 'linkedIn', '520', '320', '#2d79c7', 'ts.png', '#27629d', 'LENGUAJE DE PROGRAMACIóN', 'TypeScript', '1', '1', '1', '0', '0', '60', '520', '320', '14', '18', '16', '10', '5', '30', NULL, '1', '\nconsole.log(Utils.ASSETS_URL);\nRESULTADO: "/assets/"\n\n/**\n * Palabra reservada - static\n * 1- Elementos no asociados a ninguna instancia.\n * 2- Son accedidos haciendo uso de la clase.\n */', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6912788832857808896-AhB4?utm_source=linkedin_share&utm_medium=member_desktop_web', NULL, '2022-03-23 21:15:45', '2022-03-24 10:03:02', NULL),
	(26, 6, 6, 3, 'luoya', 'LENGUAJE DE PROGRAMACIóN', 'TypeScript', 'Consulta De Variable USER_PREFIX', 'Variable no "static" con modificador de variable "readonly"', 'linkedIn', '520', '320', '#2d79c7', 'ts.png', '#27629d', 'LENGUAJE DE PROGRAMACIóN', 'TypeScript', '1', '1', '1', '0', '0', '60', '520', '320', '14', '18', '16', '10', '30', '40', NULL, '1', '//Se instancia la clase Utils\nconst utils = new Utils();\n\nconsole.log(utils.USER_PREFIX);\n\nRESULTADO: "ABC___"', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6912788832857808896-AhB4?utm_source=linkedin_share&utm_medium=member_desktop_web', NULL, '2022-03-23 21:15:45', '2022-03-24 10:03:08', NULL),
	(27, 6, 6, 4, 'luoya', 'LENGUAJE DE PROGRAMACIóN', 'TypeScript', 'Excepciones De Modificador "readonly"', 'Modificador de variable "readonly"', 'linkedIn', '520', '320', '#2d79c7', 'ts.png', '#27629d', 'LENGUAJE DE PROGRAMACIóN', 'TypeScript', '1', '1', '1', '0', '0', '60', '520', '320', '14', '18', '16', '10', '10', '60', NULL, '1', 'const utils = new Utils();\nutils.USER_PREFIX = "DEF___";\nResultado: Error\n\nUtils.ASSETS_URL = "/library/";\nResultado: Error\n\n/**\n * Modificador - readonly\n * 1- No cambia su valor en el tiempo\n * 2- Retorna error al asignarle un nuevo valor.\n */', 'https://www.linkedin.com/posts/jersson-arrivasplata-rojas_jerssonarrivasplata-jerssonarrivasplatadev-activity-6912788832857808896-AhB4?utm_source=linkedin_share&utm_medium=member_desktop_web', NULL, '2022-03-23 21:15:45', '2022-03-24 10:03:15', NULL);
/*!40000 ALTER TABLE `blog_content_generates` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.blog_content_type
CREATE TABLE IF NOT EXISTS `blog_content_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `highlight` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.blog_content_type: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `blog_content_type` DISABLE KEYS */;
INSERT INTO `blog_content_type` (`id`, `name`, `highlight`, `background`, `description`, `image`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Css3', 'css', '#2654bf', 'Estilos', 'css.png', 2, '2021-12-29', NULL, NULL),
	(2, 'Html5', 'language-xml', '#bb5022', 'Lenguajes de Marcado de Hipertexto', 'html.png', 3, '2021-12-29', NULL, NULL),
	(3, 'Angular', 'angular', '#a31331', 'Framework', 'angular-min.png', 4, '2021-12-29', NULL, NULL),
	(4, 'Git', 'git', '#611300', 'Sistema de Control de Versiones', 'git.png', 5, '2021-12-29', NULL, NULL),
	(5, 'TypeScript', 'typescript', '#2d79c7', 'Lenguaje de Programación', 'ts.png', 6, '2021-12-29', NULL, NULL),
	(6, 'Scss', 'scss', '#a14f79', 'Estilos  - Precompilador', 'scss.png', 7, '2021-12-29', NULL, NULL),
	(7, 'Javascript', 'javascript', '#cab500', 'Lenguaje de Programación', 'js-min.png', 1, '2021-12-29', NULL, NULL),
	(8, 'SEO', 'language-xml', '#1FB8D8', 'Optimización en motores de búsqueda', 'seo.png', 0, '2022-03-09', '2022-03-09', NULL);
/*!40000 ALTER TABLE `blog_content_type` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.blog_pages
CREATE TABLE IF NOT EXISTS `blog_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` text COLLATE utf8_unicode_ci,
  `title` text COLLATE utf8_unicode_ci,
  `meta-charset` text COLLATE utf8_unicode_ci,
  `meta-name-keywords` text COLLATE utf8_unicode_ci,
  `meta-name-viewport` text COLLATE utf8_unicode_ci,
  `meta-name-author` text COLLATE utf8_unicode_ci,
  `meta-name-description` text COLLATE utf8_unicode_ci,
  `meta-name-twitter-card` text COLLATE utf8_unicode_ci,
  `meta-name-twitter-title` text COLLATE utf8_unicode_ci,
  `meta-name-twitter-url` text COLLATE utf8_unicode_ci,
  `meta-name-msapplication-TileColor` text COLLATE utf8_unicode_ci,
  `meta-name-msapplication-TileImage` text COLLATE utf8_unicode_ci,
  `meta-name-msapplication-config` text COLLATE utf8_unicode_ci,
  `meta-name-twitter-description` text COLLATE utf8_unicode_ci,
  `meta-name-theme-color` text COLLATE utf8_unicode_ci,
  `meta-name-apple-mobile-web-app-capable` text COLLATE utf8_unicode_ci,
  `meta-property-og-site-name` text COLLATE utf8_unicode_ci,
  `meta-property-og-url` text COLLATE utf8_unicode_ci,
  `meta-property-og-title` text COLLATE utf8_unicode_ci,
  `meta-property-og-description` text COLLATE utf8_unicode_ci,
  `meta-property-og-image` text COLLATE utf8_unicode_ci,
  `meta-http-equiv-cleartype` text COLLATE utf8_unicode_ci,
  `meta-http-equiv-x-ua-compatible` text COLLATE utf8_unicode_ci,
  `meta-http-equiv-expires` text COLLATE utf8_unicode_ci,
  `meta-http-equiv-last-modified` text COLLATE utf8_unicode_ci,
  `meta-http-equiv-cache-control` text COLLATE utf8_unicode_ci,
  `meta-http-equiv-pragma` text COLLATE utf8_unicode_ci,
  `body` text COLLATE utf8_unicode_ci,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.blog_pages: ~15 rows (aproximadamente)
/*!40000 ALTER TABLE `blog_pages` DISABLE KEYS */;
INSERT INTO `blog_pages` (`id`, `url`, `title`, `meta-charset`, `meta-name-keywords`, `meta-name-viewport`, `meta-name-author`, `meta-name-description`, `meta-name-twitter-card`, `meta-name-twitter-title`, `meta-name-twitter-url`, `meta-name-msapplication-TileColor`, `meta-name-msapplication-TileImage`, `meta-name-msapplication-config`, `meta-name-twitter-description`, `meta-name-theme-color`, `meta-name-apple-mobile-web-app-capable`, `meta-property-og-site-name`, `meta-property-og-url`, `meta-property-og-title`, `meta-property-og-description`, `meta-property-og-image`, `meta-http-equiv-cleartype`, `meta-http-equiv-x-ua-compatible`, `meta-http-equiv-expires`, `meta-http-equiv-last-modified`, `meta-http-equiv-cache-control`, `meta-http-equiv-pragma`, `body`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(6, 'https://www.npmjs.com/package/@jersson-arrivasplata-rojas/sami', '@jersson-arrivasplata-rojas/sami - npm', 'utf-8', '', 'width=device-width,minimum-scale=1.0,initial-scale=1,user-scalable=yes', '', 'Componente de Stencil. Latest version: 0.0.4, last published: 9 days ago. Start using @jersson-arrivasplata-rojas/sami in your project by running `npm i @jersson-arrivasplata-rojas/sami`. There are no other projects in the npm registry using @jersson-arrivasplata-rojas/sami.', 'summary', 'npm: @jersson-arrivasplata-rojas/sami', 'https://www.npmjs.com/package/@jersson-arrivasplata-rojas/sami', '#cb3837', 'https://static.npmjs.com/7a7ffabbd910fc60161bc04f2cee4160.png', 'https://static.npmjs.com/668aac888e52ae13cac9cfd71fabd31f.xml', 'Componente de Stencil. Latest version: 0.0.4, last published: 9 days ago. Start using @jersson-arrivasplata-rojas/sami in your project by running `npm i @jersson-arrivasplata-rojas/sami`. There are no other projects in the npm registry using @jersson-arrivasplata-rojas/sami.', '#cb3837', 'yes', 'npm', 'https://www.npmjs.com/package/@jersson-arrivasplata-rojas/sami', '@jersson-arrivasplata-rojas/sami', 'Componente de Stencil. Latest version: 0.0.4, last published: 9 days ago. Start using @jersson-arrivasplata-rojas/sami in your project by running `npm i @jersson-arrivasplata-rojas/sami`. There are no other projects in the npm registry using @jersson-arrivasplata-rojas/sami.', 'https://static.npmjs.com/338e4905a2684ca96e08c7780fc68412.png', 'on', '', '', '', '', '', NULL, '2022-03-22', '2022-03-22', NULL),
	(9, 'https://jerssonarrivasplata.com/', 'Jersson G. Arrivasplata Rojas | Software Developer', 'UTF-8', '#software, #developer, #developments, #jerssonarrivasplata, #jerssonarrivasplataDEV, #jerssonarrivasplataPERU', 'width=device-width, initial-scale=1.0', 'Jersson Giomar Arrivasplata Rojas', 'Full Stack Developer y Desarrollador de Aplicaciones móviles,\n    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,\n    análisis de negocio en los sectores de transporte, servicios, educación y turismo.\n    Co-Fundador de startup #SnapStore', 'summary', 'Jersson G. Arrivasplata Rojas | Software Developer', 'https://jerssonarrivasplata.com', '#0652dd', 'https://jerssonarrivasplata.com/img/icon.png', '', 'Full Stack Developer y Desarrollador de Aplicaciones móviles,\n    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,\n    análisis de negocio en los sectores de transporte, servicios, educación y turismo.\n    Co-Fundador de startup #SnapStore', '#0652dd', '', 'jerssonarrivasplata', 'https://jerssonarrivasplata.com', 'Jersson G. Arrivasplata Rojas | Software Developer', 'Full Stack Developer y Desarrollador de Aplicaciones móviles,\n    con experiencia en desarrollo de sistemas empresariales, desarrollo de procesos,\n    análisis de negocio en los sectores de transporte, servicios, educación y turismo.\n    Co-Fundador de startup #SnapStore', 'https://jerssonarrivasplata.com/img/icon.png', '', 'IE=edge', '0', '0', 'no-cache, mustrevalidate', 'no-cache', NULL, '2022-03-22', '2022-03-22', NULL),
	(12, 'https://dev.w3.org/html5/html-author/charref', 'Character Entity Reference Chart', 'UTF-8', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '\n 	&Tab;&#x00009;&#9;CHARACTER TABULATION\n \n&NewLine;&#x0000A;&#10;LINE FEED (LF)\n !&excl;&#x00021;&#33;EXCLAMATION MARK\n "&quot; &QUOT;&#x00022;&#34;QUOTATION MARK\n #&num;&#x00023;&#35;NUMBER SIGN\n $&dollar;&#x00024;&#36;DOLLAR SIGN\n %&percnt;&#x00025;&#37;PERCENT SIGN\n &&amp; &AMP;&#x00026;&#38;AMPERSAND\n \'&apos;&#x00027;&#39;APOSTROPHE\n (&lpar;&#x00028;&#40;LEFT PARENTHESIS\n )&rpar;&#x00029;&#41;RIGHT PARENTHESIS\n *&ast; &midast;&#x0002A;&#42;ASTERISK\n +&plus;&#x0002B;&#43;PLUS SIGN\n ,&comma;&#x0002C;&#44;COMMA\n .&period;&#x0002E;&#46;FULL STOP\n /&sol;&#x0002F;&#47;SOLIDUS\n :&colon;&#x0003A;&#58;COLON\n ;&semi;&#x0003B;&#59;SEMICOLON\n <&lt; &LT;&#x0003C;&#60;LESS-THAN SIGN\n =&equals;&#x0003D;&#61;EQUALS SIGN\n >&gt; &GT;&#x0003E;&#62;GREATER-THAN SIGN\n ?&quest;&#x0003F;&#63;QUESTION MARK\n @&commat;&#x00040;&#64;COMMERCIAL AT\n [&lsqb; &lbrack;&#x0005B;&#91;LEFT SQUARE BRACKET\n \\&bsol;&#x0005C;&#92;REVERSE SOLIDUS\n ]&rsqb; &rbrack;&#x0005D;&#93;RIGHT SQUARE BRACKET\n ^&Hat;&#x0005E;&#94;', '2022-03-23', '2022-03-23', NULL),
	(13, 'https://www.digitalocean.com/community/tutorials/angular-custom-webpack-config', 'How To Use Custom webpack Configurations with Angular CLI Builders  | DigitalOcean', 'utf-8', '', 'minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no', '', 'Learn how to create a custom webpack config inside an Angular project using CLI Builders. This example will reduce the file size of a library. ', 'summary', 'How To Use Custom webpack Configurations with Angular CLI Builders  | DigitalOcean', '', '', '', '', 'Learn how to create a custom webpack config inside an Angular project using CLI Builders. This example will reduce the file size of a library. ', '', '', '', '', 'How To Use Custom webpack Configurations with Angular CLI Builders  | DigitalOcean', 'Learn how to create a custom webpack config inside an Angular project using CLI Builders. This example will reduce the file size of a library. ', 'https://www.digitalocean.com/_next/static/media/social-share-default.e8530e9e.jpeg', '', '', '', '', '', '', 'Learn how DigitalOcean delivers 200% ROI for your businessProductsPricingDocsSign inTutorialsQuestionsTech TalksGet Involvedsearch community/Sign UpCONTENTSIntroductionPrerequisitesStep 1 — Setting Up the ProjectStep 2 — Building a Localized Clock DisplayStep 3 — Modifying a Localized Clock Display with a Custom webpack ConfigurationConclusionRelatedHow To Upgrade from AngularJS to Angular with ngUpgradeTutorialHow To Upgrade Angular Sorting FiltersTutorial// Tutorial //How To Use Custom webpack Configurations with Angular CLI BuildersPublished on April 10, 2019 · Updated on March 18, 2021AngularBy PaulHallidayDeveloper and author at DigitalOcean.Introduction\nThe Angular CLI can create a new Angular project and it will handle the webpack configuration. However, there are situations where you will want to add custom webpack functionality.\nFor the purposes of this article, you will learn how to use the moment.js library and remove unused locales to create a smaller bundle size.\n\nN', '2022-03-23', '2022-03-23', NULL),
	(14, 'https://www.toptal.com/javascript/a-guide-to-managing-webpack-dependencies', 'The Beginner\'s Guide to Webpack Dependencies | Toptal', '', '', 'width=device-width, initial-scale=1.0', '', 'The Webpack module bundler processes JavaScript code and all static assets, such as stylesheets, images, and fonts. However, configuring Webpack and its dependencies can be cumbersome and not always a straightforward process, especially for beginners.\r\n\r\nIn this article, Toptal Software Engineer Andrej Gajdos provid...', 'summary_large_image', '', '', '', '', '', '', '', '', 'Toptal Engineering Blog', 'https://www.toptal.com/javascript/a-guide-to-managing-webpack-dependencies', 'A Guide to Managing Webpack Dependencies', 'The Webpack module bundler processes JavaScript code and all static assets, such as stylesheets, images, and fonts. However, configuring Webpack and its dependencies can be cumbersome and not always a straightforward process, especially for beginners.\r\n\r\nIn this article, Toptal Software Engineer Andrej Gajdos provid...', 'https://bs-uploads.toptal.io/blackfish-uploads/components/seo/content/og_image_file/og_image/777000/default-social-5-f95901aa20eb4661d6c68b58abcff464.png', '', '', '', '', '', '', 'DevelopersHiring? Toptal handpicks top JavaScript developers to suit your needs..expand_icon-path {\n  transform: rotate(0);\n  transform-origin: 50% 50%;\n  transition: transform .2s ease-in;\n}\n\n.expand_icon-path.is-bottom,\n.expand_icon-path.is-top {\n  transform: scale(1);\n}\n\n.expand_icon.is-expanded .expand_icon-path.is-bottom,\n.expand_icon.is-expanded .expand_icon-path.is-top {\n  transform: scale(0);\n}\n\n.expand_icon.is-expanded .expand_icon-path.is-left {\n  transform: rotate(45deg);\n}\n\n.expand_icon.is-expanded .expand_icon-path.is-right {\n  transform: rotate(-45deg);\n}Top 3%WhyClientsEnterpriseCommunityBlogAbout UsFollow us onLog InGo to Your ProfileEngineeringAll BlogsIcon ChevronIcon CloseSearchFilter byAllEngineeringDesignFinanceProjectsProductToptal InsightsView all results<a class="search-results_item" data-role="search-results-item" href="${url}"><span class="search-results_item_title">${title}<span class="search-results_item_label">${label}EngineeringDesignFinanceProjectsProduct', '2022-03-23', '2022-03-23', NULL),
	(15, 'https://www.digitalocean.com/community/tutorials/typescript-new-project', 'How To Set Up a New TypeScript Project  | DigitalOcean', 'utf-8', '', 'minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no', '', 'In this tutorial, you will learn how to set up a TypeScript project without a starter’s help. You will also learn how compiling works in TypeScript and how t… ', 'summary', 'How To Set Up a New TypeScript Project  | DigitalOcean', '', '', '', '', 'In this tutorial, you will learn how to set up a TypeScript project without a starter’s help. You will also learn how compiling works in TypeScript and how t… ', '', '', '', '', 'How To Set Up a New TypeScript Project  | DigitalOcean', 'In this tutorial, you will learn how to set up a TypeScript project without a starter’s help. You will also learn how compiling works in TypeScript and how t… ', 'https://www.digitalocean.com/_next/static/media/social-share-default.e8530e9e.jpeg', '', '', '', '', '', '', 'Learn how DigitalOcean delivers 200% ROI for your businessProductsPricingDocsSign inTutorialsQuestionsTech TalksGet Involvedsearch community/Sign UpCONTENTSIntroductionPrerequisitesStep 1 — Starting the TypeScript ProjectStep 2 — Compiling the TypeScript ProjectStep 3 — Configuring TSLint for the TypeScript ProjectStep 4 — Using gtsConclusionRelatedHow To Use Typescript with Create React AppTutorialBenefits of Using TypeScriptTutorial// Tutorial //How To Set Up a New TypeScript ProjectPublished on December 1, 2017 · Updated on October 19, 2020TypeScriptBy Alligator.ioDeveloper and author at DigitalOcean.Introduction\nYou may have worked with TypeScript before when using a starter project or a tool like the Angular CLI. In this tutorial, you will learn how to set up a TypeScript project without a starter’s help. You will also learn how compiling works in TypeScript and how to use a linter with your TypeScript project.\nPrerequisites\nTo complete this tutorial, you will need the ', '2022-03-24', '2022-03-24', NULL),
	(16, 'https://search.google.com/test/mobile-friendly?url=https%3A%2F%2Fjerssonarrivasplata.com%2F&url=https%3A%2F%2Fjerssonarrivasplata.com%2F&url=https%3A%2F%2Fjerssonarrivasplata.com%2F&hl=es', 'Prueba de optimizaciÃ³n para mÃ³viles - Google Search Console', '', '', 'initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no', '', '', '', '', '', '', '', '', '', '', 'yes', '', '', '', '', '', '', '', '', '', '', '', 'window.wiz_progress&&window.wiz_progress();Prueba de optimizaciÃ³n para mÃ³vilesAyudaComentariosIniciar sesiÃ³nthis.gbar_=this.gbar_||{};(function(_){var window=this;\ntry{\n_.ie=function(a,b,c){if(!a.o)if(c instanceof Array){c=_.Ua(c);for(var d=c.next();!d.done;d=c.next())_.ie(a,b,d.value)}else{d=(0,_.y)(a.F,a,b);var e=a.B+c;a.B++;b.setAttribute("data-eqid",e);a.D[e]=d;b&&b.addEventListener?b.addEventListener(c,d,!1):b&&b.attachEvent?b.attachEvent("on"+c,d):a.A.log(Error("C`"+b))}};\n\n}catch(e){_._DumpException(e)}\ntry{\n/*\n\n Copyright The Closure Library Authors.\n SPDX-License-Identifier: Apache-2.0\n*/\n_.je=function(){if(!_.n.addEventListener||!Object.defineProperty)return!1;var a=!1,b=Object.defineProperty({},"passive",{get:function(){a=!0}});try{_.n.addEventListener("test",_.gb,b),_.n.removeEventListener("test",_.gb,b)}catch(c){}return a}();\n_.ke=_.Db?"webkitTransitionEnd":"transitionend";\n\n}catch(e){_._DumpException(e)}\ntry{\nvar le=document.querySelector(".gb_z .gb_A"),me=doc', '2022-03-25', '2022-03-25', NULL),
	(17, 'https://developers.google.com/search?hl=es', 'Centro de la Búsqueda de Google (antes Webmasters) | Recursos sobre SEO en la Web  |  Google Developers', 'utf-8', '', 'width=device-width, initial-scale=1', '', 'El Centro de la Búsqueda de Google ofrece recursos sobre SEO para ayudar a que tu sitio web aparezca en la Búsqueda de Google. Consulta cómo aumentar la descubribilidad de tu sitio web.', 'summary_large_image', '', '', '', '', '', '', '#ffffff', '', 'Google Developers', 'https://developers.google.com/search?hl=es', 'Centro de la Búsqueda de Google (antes Webmasters) | Recursos sobre SEO en la Web  |  Google Developers', 'El Centro de la Búsqueda de Google ofrece recursos sobre SEO para ayudar a que tu sitio web aparezca en la Búsqueda de Google. Consulta cómo aumentar la descubribilidad de tu sitio web.', 'https://developers.google.com/search/images/home-social-share-lockup.png?hl=es', '', 'IE=Edge', '', '', '', '', '\n    \n  \n    \n  \n    \n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n  \n    \n      \n        \n        \n        \n\n  \n    \n  \n  \n    \n  \n  \n    \n      \n    \n  \n  \n  \n  \n  \n\n  \n\n\n\n  \n  \n  \n    \n  \n  \n    \n    \n    \n      \n      \n        \n  \n    \n          Centro de la Búsqueda de Google\n        \n  \n  \n      \n    \n  \n  \n\n  \n\n\n        \n          \n            \n              \n              \n  \n\n    \n      \n        \n          \n  \n  Documentación\n\n\n  \n\n  \n    \n    \n      \n        \n          \n            \n              \n                ¿No tienes mucho tiempo?\n              \n              \n              \n                \n                  \n                    \n                    \n                      Guía de inicio rápido\n                    \n                    \n                  \n                \n              \n                \n                  \n                    \n                    \n                      Cómo funciona la Búsqueda de Google\n                    \n                    \n                  \n   ', '2022-03-27', '2022-03-27', NULL),
	(18, 'https://ng.ant.design/', 'NG-ZORRO - Angular UI component library', 'utf-8', 'angular, ant design, ant, angular ant design, web, ui, components, ng, zorro, responsive, typescript, css, mobile web, open source, 组件库, 组件, UI 框架, Ant Design of Angular', 'width=device-width,initial-scale=1', 'NG-ZORRO Team', 'Angular Ant Design of Angular Component, An enterprise-class Angular UI component library based on Ant Design, all components are open source and free...', 'summary', 'NG-ZORRO - Angular UI component library', '', '', '', '', 'Angular Ant Design of Angular Component, An enterprise-class Angular UI component library based on Ant Design, all components are open source and free...', '#1890ff', 'yes', 'NG-ZORRO - Ant Design Of Angular', 'https://ng.ant.design/', 'NG-ZORRO - Angular UI component library', 'Angular Ant Design of Angular Component, An enterprise-class Angular UI component library based on Ant Design, all components are open source and free...', 'https://ng.ant.design/assets/img/site-preview.png', '', '', '', '', '', '', 'NG-ZORRODocsComponentsExperimental13.1.1中文RTLStar 0Ant Design of AngularAnt Design of AngularGetting Started快速上手Schematics脚手架Internationalization国际化Server-side Rendering服务端渲染Theme Customization定制主题Global Configuration全局配置项Animations Switch动画开关Direction文字方向Resources资源推荐FAQ常见问题How to Contribute贡献指南Change Log更新日志v13 Migration Guidev13 升级指南Join us加入我们Ant Design of AngularFeaturesEnvironment SupportVersionAngular SupportDesign SpecificationInstallationCompanies using ng-zorro-antdContributingNeed Help?An enterprise-class Angular UI component library based on Ant Design, all components are open source and free to use under MIT license.+Features#An enterprise-class UI design language for Angular applications.60+ high-quality Angular components out of the box.Written in TypeScript with complete defined types.Support OnPush mode, high performance.Powerful theme customization in ev', '2022-03-27', '2022-03-27', NULL),
	(19, 'https://gabrieltanner.org/blog/stencil-js-introduction', 'An Introduction into Stencil.js', 'utf-8', 'Golang DevOps Kubernetes Docker Ethical Hacking', 'width=device-width, initial-scale=1', 'Tanner Gabriel', 'Stencil is a compiler that generates Web Components developed by the Ionic team. Stencil combines the best concepts of the most popular frameworks into a simple build-time tool.', 'summary', 'An Introduction into Stencil.js', '', '', '', '', 'Stencil is a compiler that generates Web Components developed by the Ionic team. Stencil combines the best concepts of the most popular frameworks into a simple build-time tool.', '', '', 'Gabriel Tanner', '', 'An Introduction into Stencil.js', 'Stencil is a compiler that generates Web Components developed by the Ionic team. Stencil combines the best concepts of the most popular frameworks into a simple build-time tool.', 'https://content.gabrieltanner.org/content/images/2019/09/stencil-introduction.jpg', '', '', '', '', '', '', '\n     \n        Gabriel Tanner\n       \n          Home\n         \n          Articles\n         \n          Contact\n          \n            An Introduction into Stencil.js\n             Stencil is a compiler that generates Web Components developed by the Ionic team. Stencil combines the best concepts of the most popular frameworks into a simple build-time tool.Stencil takes popular features such as the Virtual DOM, Typescript and JSX to create standard-based Web components which can be used with every popular frontend framework out of the box (Angular, React, Vue).The main goal of this article is to give a quick overview of the Stencil framework and how to use it to build Web components. We will also look at how we can build our component so it can be use in the most popular frontend frameworks (Angular, React, Vue, Ember).So, without wasting any further time, let’s get started.Setup:Before we can start creating a component we first need to set up our stencil development environment. For tha', '2022-03-28', '2022-03-28', NULL),
	(20, 'https://github.com/mrmlnc/material-color/blob/master/material-color.scss', 'material-color/material-color.scss at master · mrmlnc/material-color · GitHub', 'utf-8', '', 'width=device-width', '', ':high_brightness: The colour palette, based on Google\'s Material Design, for use in your project. - material-color/material-color.scss at master · mrmlnc/material-color', 'summary_large_image', 'material-color/material-color.scss at master · mrmlnc/material-color', '', '', '', '', ':high_brightness: The colour palette, based on Google\'s Material Design, for use in your project. - material-color/material-color.scss at master · mrmlnc/material-color', '#1e2327', '', 'GitHub', 'https://github.com/mrmlnc/material-color', 'material-color/material-color.scss at master · mrmlnc/material-color', ':high_brightness: The colour palette, based on Google\'s Material Design, for use in your project. - material-color/material-color.scss at master · mrmlnc/material-color', 'https://opengraph.githubassets.com/6573c2ab7415078964d06503ca68f76142af2de26ec3e4aab6f581b49b504093/mrmlnc/material-color', '', '', '', '', '', '', '\n    \n\n    \n      Skip to content\n      \n    \n      \n      \n\n\n        \n\n            \n  \n    \n      \n        \n    \n\n      \n\n        \n          \n\n        \n\n      \n            \n              Sign up\n            \n\n          \n    \n\n      \n    \n\n    \n      \n          \n    \n\n      \n\n        \n          \n              \n    \n      \n        Product\n        \n      \n      \n        \n              \n    \n      Features\n  \n\n              \n    \n      Mobile\n  \n\n              \n    \n      Actions\n  \n\n              \n    \n      Codespaces\n  \n\n              \n    \n      Packages\n  \n\n              \n    \n      Security\n  \n\n              \n    \n      Code review\n  \n\n              \n    \n      Issues\n  \n\n              \n    \n      Integrations\n  \n\n              \n    \n      GitHub Sponsors\n  \n\n              \n    \n      Customer stories\n  \n\n        \n      \n    \n\n\n\n              \n    Team\n\n\n              \n    Enterprise\n\n\n\n            \n    \n      \n        Explore\n        \n      \n      \n        \n              \n    \n   ', '2022-04-01', '2022-04-01', NULL),
	(21, 'https://ogp.me/', 'The Open Graph protocol', 'utf-8', '', '', '', 'The Open Graph protocol enables any web page to become a rich object in a social graph.', '', '', '', '', '', '', '', '', '', '', 'https://ogp.me/', 'Open Graph protocol', 'The Open Graph protocol enables any web page to become a rich object in a social graph.', 'https://ogp.me/logo.png', '', '', '', '', '', '', '\n    \n    \n      The Open Graph protocol\n      \n    \n    \nIntroduction\nThe Open Graph protocol enables any web page to become a\nrich object in a social graph. For instance, this is used on Facebook to allow\nany web page to have the same functionality as any other object on Facebook.\nWhile many different technologies and schemas exist and could be combined\ntogether, there isn\'t a single technology which provides enough information to\nrichly represent any web page within the social graph. The Open Graph protocol\nbuilds on these existing technologies and gives developers one thing to\nimplement. Developer simplicity is a key goal of the Open Graph protocol which\nhas informed many of the technical design decisions.\n\n\nBasic Metadata\nTo turn your web pages into graph objects, you need to add basic metadata to\nyour page. We\'ve based the initial version of the protocol on\nRDFa which means that you\'ll place\nadditional <meta> tags in the <head> of your web page. The four required\nproperties for e', '2022-04-04', '2022-04-04', NULL),
	(22, 'https://uniwebsidad.com/libros/xhtml/capitulo-3/codificacion-de-caracteres', '3.6. Codificación de caracteres (Introducción a XHTML)', 'utf-8', '', 'width=device-width', '', '', '', '', '', '#003580', '', '', '', '#003580', '', '', '', '3.6. Codificación de caracteres (Introducción a XHTML)', '', '', '', '', '', '', '', '', '\n{"@context":"https:\\/\\/schema.org","@type":"WebSite","name":"uniwebsidad","url":"https:\\/\\/uniwebsidad.com","potentialAction":{"@type":"SearchAction","target":"https:\\/\\/uniwebsidad.com\\/buscar?q={query}","query-input":"required name=query"}}\n{"@context":"https:\\/\\/schema.org","@type":"Organization","url":"https:\\/\\/uniwebsidad.com","name":"uniwebsidad","logo":"https:\\/\\/uniwebsidad.com\\/logo.svg","contactPoint":{"@type":"ContactPoint","email":"contacto@uniwebsidad.com","url":"https:\\/\\/uniwebsidad.com\\/sitio\\/contacto","contactType":"customer service","availableLanguage":["Spanish"]}}\n{"@context":"https:\\/\\/schema.org","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":1,"item":{"@id":"\\/","name":"Inicio"}},{"@type":"ListItem","position":2,"item":{"@id":"\\/libros","name":"Libros"}},{"@type":"ListItem","position":3,"item":{"@id":"\\/libros\\/xhtml","name":"Introducci\\u00f3n a XHTML"}},{"@type":"ListItem","position":4,"item":{"@id":"\\/libros\\/xhtml\\/capitulo-3\\/co', '2022-04-05', '2022-04-05', NULL),
	(23, 'https://www.web2generators.com/html-based-tools/online-html-entities-encoder-and-decoder', ' Encode and decode a piece of text to its HTML equivalent -  HTML Entities Encoder / Decoder | Web 2.0 Generators', 'utf-8', '', 'width=device-width, initial-scale=1', 'IBE Software', 'Encode and decode a piece of text to its HTML equivalent', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'IE=edge', '', '', '', '', '\r\n\r\n\r\n    \r\n    \r\n        \r\n        \r\n            \r\n                \r\n            \r\n            \r\n        \r\n        \r\n        \r\n        \r\n        \r\n        \r\n        \r\n        \r\n            \r\n                \r\n                \r\n                    \r\n                    \r\n                \r\n            \r\n        \r\n        \r\n    \r\n    \r\n\r\n\r\n\r\n\r\n\r\n\r\n    \r\n    \r\n        \r\n            \r\n            \r\n                \r\n                    \r\n                        \r\n                        Home\r\n                                            \r\n                \r\n                \r\n                \r\n                    \r\n                        HTML-based tools\r\n                    \r\n                \r\n                \r\n                    \r\n                        \r\n                        HTML Encoder / Decoder\r\n                                                    \r\n                                            \r\n                \r\n                \r\n                \r\n                    \r\n            ', '2022-04-15', '2022-04-15', NULL),
	(24, 'https://eddoinfotips.wordpress.com/2010/03/23/acentos-unicode-para-el-javascript/', 'Unicode para el javascript (tildes y eñes) | EDDO INFO TIPS', '', '', '', '', '\\u00e1 = á \\u00e9 = é \\u00ed = í \\u00f3 = ó \\u00fa = ú \\u00c1 = Á \\u00c9 = É \\u00cd = Í \\u00d3 = Ó \\u00da = Ú \\u00f1 = ñ \\u00d1 = Ñ', 'summary', '', '', '', '', '', '', '', '', 'EDDO INFO TIPS', 'https://eddoinfotips.wordpress.com/2010/03/23/acentos-unicode-para-el-javascript/', 'Unicode para el javascript (tildes y eñes)', '\\u00e1 = á \\u00e9 = é \\u00ed = í \\u00f3 = ó \\u00fa = ú \\u00c1 = Á \\u00c9 = É \\u00cd = Í \\u00d3 = Ó \\u00da = Ú \\u00f1 = ñ \\u00d1 = Ñ', 'https://s0.wp.com/i/blank.jpg', '', '', '', '', '', '', '\n\n\n\n\n    EDDO INFO TIPS\n    Tips utiles\n\n\n	\n\n  \n		\n			« Calcular diferencia de dias entre 2 fechas con php\n			 Regiones de Chile »\n		\n\n		\n			Unicode para el javascript (tildes y eñes)\n            marzo 23, 2010 \n\n			\n				\\u00e1 = á\n\\u00e9 = é\n\\u00ed = í\n\\u00f3 = ó\n\\u00fa = ú\n\\u00c1 = Á\n\\u00c9 = É\n\\u00cd = Í\n\\u00d3 = Ó\n\\u00da = Ú\n\\u00f1 = ñ\n\\u00d1 = Ñ \nShare this:TwitterFacebookMe gusta esto:Me gusta Cargando...\n\n	Relacionado\n\n							\n			Publicado en UTILES | \n		\n\n	\n\n\n	\n		Deja una respuesta Cancelar la respuesta\n\n\n\n	Introduce aquí tu comentario...  \n	\n\n\n\n	\n		Introduce tus datos o haz clic en un icono para iniciar sesión:\n		\n			\n				\n									\n			\n			\n				\n									\n			\n			\n				\n									\n			\n			\n				\n									\n			\n		\n	\n\n	\n		\n			\n				\n			\n\n				\n				\n					Correo electrónico (obligatorio) (La dirección no se hará pública)\n					\n				\n				\n					Nombre (obligatorio)\n					\n				\n				\n					Web\n					\n				\n			\n			\n		\n	\n\n	\n		\n			\n				\n			\n\n				\n				\n				\n				\n						\n			\n			Es', '2022-04-18', '2022-04-18', NULL);
/*!40000 ALTER TABLE `blog_pages` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.blog_subscribe
CREATE TABLE IF NOT EXISTS `blog_subscribe` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8_unicode_ci,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.blog_subscribe: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `blog_subscribe` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_subscribe` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.domains
CREATE TABLE IF NOT EXISTS `domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` smallint(6) DEFAULT '0',
  `subdomain` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.domains: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `domains` DISABLE KEYS */;
INSERT INTO `domains` (`id`, `name`, `type`, `subdomain`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'jerssonarrivasplata.com', 0, '', NULL, NULL, NULL),
	(2, 'dev.jerssonarrivasplata.com', 1, 'dev', NULL, NULL, NULL),
	(3, 'admin.jerssonarrivasplata.com', 2, 'admin', NULL, NULL, NULL),
	(4, 'doc.jerssonarrivasplata.com', 3, 'doc', NULL, NULL, NULL),
	(5, 'sami.jerssonarrivasplata.com', 4, 'sami', NULL, NULL, NULL),
	(6, 'projects.jerssonarrivasplata.com', 5, 'projects', NULL, NULL, NULL);
/*!40000 ALTER TABLE `domains` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celphone` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.messages: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.migrations: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(5, '2014_10_12_000000_create_users_table', 1),
	(6, '2014_10_12_100000_create_password_resets_table', 1),
	(7, '2019_08_19_000000_create_failed_jobs_table', 1),
	(8, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(9, '2019_12_14_000002_create_messages_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.personal_access_tokens
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.personal_access_tokens: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.project_content
CREATE TABLE IF NOT EXISTS `project_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) DEFAULT '0',
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `links` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` text COLLATE utf8_unicode_ci,
  `url` text COLLATE utf8_unicode_ci,
  `folder` text COLLATE utf8_unicode_ci,
  `implementation` text COLLATE utf8_unicode_ci,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.project_content: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `project_content` DISABLE KEYS */;
INSERT INTO `project_content` (`id`, `type`, `name`, `links`, `description`, `image`, `path`, `url`, `folder`, `implementation`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 4, 'Tipos de Formularios en Angular', '[{"name":"Tipo de Formularios en Angular","url":"https:\\/\\/github.com\\/jersson-arrivasplata-rojas\\/ANGULAR-PROY-V1-IMPLEMENTATION-ANGULAR-FORMS-S2201","urlImage":"library/sami/0.0.1/assets/icon-social-media/github.svg","type":"github"}]', 'Se implementa formularios de angular aplicando Form Builder y FormGroup con @angular/forms aplicando las siguientes clases FormBuilder, FormGroup, FormArray.', 'angular-proy-v1-implementation-angular-forms-s2201.png', 'angular/ANGULAR-PROY-V1-IMPLEMENTATION-ANGULAR-FORMS-S2201/', 'tipos-de-formularios-en-angular', 'ANGULAR-PROY-V1-IMPLEMENTATION-ANGULAR-FORMS-S2201', '<angular-proy-v1-implementation-angular-forms-s2201></angular-proy-v1-implementation-angular-forms-s2201>', '2022-04-03', '2022-04-03', NULL);
/*!40000 ALTER TABLE `project_content` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.sami_sidebar
CREATE TABLE IF NOT EXISTS `sami_sidebar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_domain` int(10) unsigned DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `text` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.sami_sidebar: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `sami_sidebar` DISABLE KEYS */;
INSERT INTO `sami_sidebar` (`id`, `id_domain`, `url`, `text`, `target`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 5, 'button', 'Botones', '_self', '2022-04-06 15:18:08', NULL, NULL),
	(2, 5, 'icon', 'Iconos', '_self', '2022-04-06 15:18:08', NULL, NULL),
	(3, 5, 'icon', 'Iconos', '_self', '2022-04-06 15:18:08', NULL, NULL);
/*!40000 ALTER TABLE `sami_sidebar` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.sitemaps
CREATE TABLE IF NOT EXISTS `sitemaps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `priority` double DEFAULT NULL,
  `type` smallint(6) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.sitemaps: ~32 rows (aproximadamente)
/*!40000 ALTER TABLE `sitemaps` DISABLE KEYS */;
INSERT INTO `sitemaps` (`id`, `url`, `description`, `priority`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 'https://dev.jerssonarrivasplata.com/css3', 'CSS3 Blog', 0.8, 1, '2022-03-24 20:15:02', NULL, NULL),
	(3, 'https://dev.jerssonarrivasplata.com/git', 'Git Blog', 0.8, 1, '2022-03-24 20:15:02', NULL, NULL),
	(4, 'https://dev.jerssonarrivasplata.com/javascript', 'JavaScript Blog', 0.8, 1, '2022-03-24 20:15:02', NULL, NULL),
	(5, 'https://dev.jerssonarrivasplata.com/typescript', 'TypeScript Blog', 0.8, 1, '2022-03-24 20:15:02', NULL, NULL),
	(6, 'https://dev.jerssonarrivasplata.com/seo', 'Seo Blog', 0.8, 1, '2022-03-24 20:15:02', NULL, NULL),
	(7, 'https://dev.jerssonarrivasplata.com', 'Blog', 1, 1, '2022-03-24 20:15:02', NULL, NULL),
	(9, 'https://jerssonarrivasplata.com', 'Jersson Arrivasplata', 1, 0, '2022-03-25 04:53:22', '2022-03-25 04:53:22', NULL),
	(10, 'https://jerssonarrivasplata.com/educacion', 'Jersson Arrivasplata - Educación', 0.8, 0, '2022-03-25 04:53:52', '2022-03-25 04:53:52', NULL),
	(11, 'https://jerssonarrivasplata.com/donacion', 'Jersson Arrivasplata - Donación', 0.8, 0, '2022-03-25 04:54:09', '2022-03-25 04:54:09', NULL),
	(12, 'https://jerssonarrivasplata.com/contacto', 'Jersson Arrivasplata - Contacto', 0.8, 0, '2022-03-25 04:54:27', '2022-03-25 04:54:27', NULL),
	(13, 'https://doc.jerssonarrivasplata.com/docs/1.0/angular/angular.guideline', 'Documentación - Angular Guide', 0.8, 3, '2022-03-25 04:55:40', '2022-03-25 04:55:40', NULL),
	(14, 'https://doc.jerssonarrivasplata.com/docs/1.0/angular/angular.projects', 'Documentación - Angular Projects', 0.8, 3, '2022-03-25 04:55:40', '2022-03-25 04:55:40', NULL),
	(15, 'https://doc.jerssonarrivasplata.com/docs/1.0/angular/angular.step-1-instalation', 'Documentación - Angular Instalación', 0.8, 3, '2022-03-25 04:55:40', '2022-03-25 04:55:40', NULL),
	(16, 'https://doc.jerssonarrivasplata.com/docs/1.0/angular/angular.step-2-commands', 'Documentación - Angular Comandos', 0.8, 3, '2022-03-25 04:55:40', '2022-03-25 04:55:40', NULL),
	(17, 'https://doc.jerssonarrivasplata.com/docs/1.0/angular/angular.http-comunication', 'Documentación - Angular Lineamiento de Comunicación Http', 0.8, 3, '2022-03-25 04:55:40', '2022-03-25 04:55:40', NULL),
	(18, 'https://doc.jerssonarrivasplata.com/docs/1.0/angular/angular.forms', 'Documentación - Angular Forms', 0.8, 3, '2022-03-25 04:55:40', '2022-03-25 04:55:40', NULL),
	(19, 'https://projects.jerssonarrivasplata.com/angular', 'Enlace Angular | Proyectos', 0.8, 5, '2022-04-04 06:14:37', '2022-04-04 06:14:37', NULL),
	(20, 'https://projects.jerssonarrivasplata.com', 'Jersson G. Arrivasplata Rojas | Proyectos', 1, 5, '2022-04-04 06:15:11', '2022-04-04 06:15:11', NULL),
	(21, 'https://projects.jerssonarrivasplata.com/angular/tipos-de-formularios-en-angular', 'Contenido de Tipos de Formularios en Angular | Proyectos', 0.8, 5, '2022-04-04 06:16:23', '2022-04-04 06:16:23', NULL),
	(22, 'https://sami.jerssonarrivasplata.com', 'Jersson G. Arrivasplata Rojas | Proyecto StencilJs Sami', 1, 4, '2022-04-04 06:19:21', '2022-04-04 06:19:21', NULL),
	(23, 'https://doc.jerssonarrivasplata.com', 'Jersson G. Arrivasplata Rojas | Documentación', 1, 3, '2022-03-25 04:55:40', '2022-03-25 04:55:40', NULL),
	(24, 'https://sami.jerssonarrivasplata.com/button', 'Botones de Sami | Proyecto StencilJs', 0.8, 4, '2022-04-04 06:23:43', '2022-04-04 06:23:43', NULL),
	(25, 'https://sami.jerssonarrivasplata.com/icon', 'Iconos de Sami | Proyecto StencilJs', 0.8, 4, '2022-04-04 06:23:43', '2022-04-04 06:23:43', NULL),
	(26, 'https://sami.jerssonarrivasplata.com/hyperlink', 'Hyperlinks de Sami | Proyecto StencilJs', 0.8, 4, '2022-04-04 06:23:43', '2022-04-04 06:23:43', NULL),
	(27, 'https://sami.jerssonarrivasplata.com/subscribe', 'Subscribers de Sami | Proyecto StencilJs', 0.8, 4, '2022-04-04 06:23:43', '2022-04-04 06:23:43', NULL),
	(28, 'https://sami.jerssonarrivasplata.com/card', 'Cards de Sami | Proyecto StencilJs', 0.8, 4, '2022-04-04 06:23:43', '2022-04-04 06:23:43', NULL),
	(29, 'https://sami.jerssonarrivasplata.com/header', 'Headers de Sami | Proyecto StencilJs', 0.8, 4, '2022-04-04 06:23:43', '2022-04-04 06:23:43', NULL),
	(30, 'https://sami.jerssonarrivasplata.com/list', 'Listas de Sami | Proyecto StencilJs', 0.8, 4, '2022-04-04 06:23:43', '2022-04-04 06:23:43', NULL),
	(31, 'https://sami.jerssonarrivasplata.com/loader', 'Loaders de Sami | Proyecto StencilJs', 0.8, 4, '2022-04-04 06:23:43', '2022-04-04 06:23:43', NULL),
	(32, 'https://sami.jerssonarrivasplata.com/grid', 'Grids de Sami | Proyecto StencilJs', 0.8, 4, '2022-04-04 06:23:43', '2022-04-04 06:23:43', NULL),
	(33, 'https://sami.jerssonarrivasplata.com/main', 'Mains de Sami | Proyecto StencilJs', 0.8, 4, '2022-04-04 06:23:43', '2022-04-04 06:23:43', NULL),
	(34, 'https://sami.jerssonarrivasplata.com/sidebar', 'Sidebars de Sami | Proyecto StencilJs', 0.8, 4, '2022-04-04 06:23:43', '2022-04-04 06:23:43', NULL);
/*!40000 ALTER TABLE `sitemaps` ENABLE KEYS */;

-- Volcando estructura para tabla jerssona_WPC1X.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla jerssona_WPC1X.users: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Admin', 'developer@jerssonarrivasplata.com', NULL, '$2y$10$VliIClIYVHYCX6qE1sN1..dlIE/fsJnxRd9naEe9IkoYBMbOlYsRu', NULL, '2021-12-27 00:36:34', '2021-12-27 00:36:34', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
