<?php

return [
    'domains' => [
        'jerssonarrivasplata.com',
        'dev.jerssonarrivasplata.com',
        'admin.jerssonarrivasplata.com',
        'doc.jerssonarrivasplata.com',
        'sami.jerssonarrivasplata.com',
        'blog.jerssonarrivasplata.com'
    ],
    'others' => [
        'https' => 'https://'
    ],
    'social_networks' => [
        'linkedin' => 'https://linkedin.com/in/jerssonarrivasplata',
        'facebook' => 'https://facebook.com/jerssonarrivasplata',
        'instagram' =>'https://instagram.com/jerssonarrivasplata',
        'whatsapp' => 'https://api.whatsapp.com/send?phone=51987347691'
    ],
    'website' => 'https://jerssonarrivasplata.com',
    'repositories' => [
        'github' => [
            'jersson-arrivasplata' =>'https://github.com/jersson-arrivasplata-rojas',
            'sami' => 'https://github.com/jersson-arrivasplata-rojas/STENCIL-PROY-V1-SAMI-COMPONENTS-S2203.git'
        ]
    ],
    'info' => [
        'first_name' => 'Jersson',
        'last_name' => 'Arrivasplata',
        'mother_last_name' => 'Rojas',
        'full_name' => 'Jersson Giomar Arrivasplata Rojas',
        'celphone' => [
            'code' => '51',
            'number' => '987347691'
        ],
        'address' => 'Jirón Zorritos 1134 - Cercado de Lima',
        'email'=>'developer@jerssonarrivasplata.com'
    ],
    'projects' => [
        'LARAVEL_V80_PROY_JERSSONARRIVASPLATA_V10_202112' => 'https://github.com/jersson-arrivasplata-rojas/LARAVEL_V8.0_PROY-JERSSONARRIVASPLATA_V1.0_202112',
        'SAMI' => 'https://www.npmjs.com/package/@jersson-arrivasplata-rojas/sami'
    ],
    'blog' => [
        'content_types' => [
            'javascript' => 1
        ]
    ],
    'virtual_coins' => [
        'bitcoin'=>[
            'address_type' => 'SegWit compatible (P2SH)',
            'address'=>'361G6vKFpgfF2h5UtfBezqEcLiMoWuTVwd'
        ],
        'ethereum' => '0xcd44c2ff9678d2ecca0953e42deb2cdadc5571b9',
        'fio' => 'FIO5egkVCLVTWrv3zCGJjGDWTLq557YCwY9ZU8YXpcvf6xNK6t6dN'
    ]
];
