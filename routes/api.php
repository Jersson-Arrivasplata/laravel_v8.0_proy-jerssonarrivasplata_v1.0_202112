<?php

use App\Http\Controllers\Api\TipController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
///api/getOpenAITip?language=es&tech=JavaScript
Route::get('/getOpenAITip', [TipController::class, 'getOpenAITip']);
//Route::get('/getOpenAITurboTip', [TipController::class, 'getOpenAITurboTip']);
