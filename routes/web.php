<?php

use App\Classes\GlobalClass;
use App\Classes\Utils;
use App\Http\Controllers\Admin\AdminSitemapAlternateXmlController;
use App\Http\Controllers\Admin\AdminSitemapXmlController;
use App\Http\Controllers\Web\MessageController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;
use App\Http\Controllers\Admin\BlogContentGenerateController;
use App\Http\Controllers\Admin\BlogContentHighlightController;
use App\Http\Controllers\Admin\BlogContentTypeController;
use App\Http\Controllers\Web\BlogController;
use App\Http\Controllers\Web\DevController;
use App\Http\Controllers\Web\ProjectController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//https://runebook.dev/es/docs/laravel/docs/8.x/routing

Route::domain('sami.' . GlobalClass::getDomain(env('APP_URL')))->group(function () {

    Route::get('/sitemap.xml', function () {
        return AdminSitemapXmlController::get_site_map(4);
    });

    //Route::get('/', [SamiController::class, 'index']);
    Route::get('/', function () {
        return view(GlobalClass::getDomainFolder(4) . "::index");
    });

    /*Route::get('/', function () {
        return redirect(Utils::getUrlBase('sami').'/button');
    });*/

    Route::get('/button', function () {
        return view(GlobalClass::getDomainFolder(4) . "::components.button.index");
    });
    Route::get('/icon', function () {
        return view(GlobalClass::getDomainFolder(4) . "::components.icon.index");
    });
    Route::get('/hyperlink', function () {
        return view(GlobalClass::getDomainFolder(4) . "::components.hyperlink.index");
    });
    Route::get('/card', function () {
        return view(GlobalClass::getDomainFolder(4) . "::components.card.index");
    });
    Route::get('/list', function () {
        return view(GlobalClass::getDomainFolder(4) . "::components.list.index");
    });
    Route::get('/loader', function () {
        return view(GlobalClass::getDomainFolder(4) . "::components.loader.index");
    });
    Route::get('/main', function () {
        return view(GlobalClass::getDomainFolder(4) . "::components.main.index");
    });
    Route::get('/grid', function () {
        return view(GlobalClass::getDomainFolder(4) . "::components.grid.index");
    });
    Route::get('/header', function () {
        return view(GlobalClass::getDomainFolder(4) . "::components.header.index");
    });
    Route::get('/sidebar', function () {
        return view(GlobalClass::getDomainFolder(4) . "::components.sidebar.index");
    });
    Route::get('/subscribe', function () {
        return view(GlobalClass::getDomainFolder(4) . "::components.subscribe.index");
    });
    /*Route::get('/sitemap.xml', function () {
        return AdminSitemapXmlController::get_site_map(4);
    });*/
});

Route::domain('admin.' . GlobalClass::getDomain(env('APP_URL')))->group(
    function () {

        Auth::routes([
            'register' => false,
            'reset' => false,
            'verify' => false,
        ]);

        Route::group(
            [
                'middleware' => ['auth']
            ],
            function () {
                Route::get('blog-content-generate/filter', [BlogContentGenerateController::class, 'filter']);

                Route::get('blog-content-type/all', [BlogContentTypeController::class, 'allContentTypes']);


                Route::get('admin-sitemap/{type}/sitemap.xml', function ($type) {
                    return AdminSitemapXmlController::get_site_map($type);
                });
            }
        );

        Route::group([
            'middleware' => ['auth'],
            'namespace' => 'App\Http\Controllers\Admin',
        ], function () {


            Route::resource('/', 'HomeController');

            Route::resource('message', 'MessageController');


            Route::resource('docs', 'DocController');

            Route::resource('admin-sitemap', 'AdminSitemapXmlController');

            Route::resource('admin-sitemap-alternate', 'AdminSitemapAlternateXmlController');

            /** AdminSitemapAlternateXmlController inicio */
            Route::get('/admin-sitemap-alternate/show/{id}', [AdminSitemapAlternateXmlController::class, 'show']);

            Route::delete('/admin-sitemap-alternate/delete', [AdminSitemapAlternateXmlController::class, 'destroy']);

            Route::post('/admin-sitemap-alternate', [AdminSitemapAlternateXmlController::class, 'store']);
            /** AdminSitemapAlternateXmlController fin */

            Route::resource('blog-page', 'BlogPageController');

            Route::resource('blog-content-highlight', 'BlogContentHighlightController');

            Route::resource('blog-social-media', 'BlogSocialMediaController');

            Route::post('/blog-content-highlight/{id}/new-blank', [BlogContentHighlightController::class, 'addNewBlank']);

            Route::resource('blog-content', 'BlogContentController');

            Route::resource('blog-content-type', 'BlogContentTypeController');

            Route::resource('blog-content-generate', 'BlogContentGenerateController');

            Route::post('/blog-content-generate/{id}/clone', [BlogContentGenerateController::class, 'clone']);

            Route::get('/blog-content-generate-type/{name}', [BlogContentGenerateController::class, 'indexType']);

            Route::resource('sami-sidebar', 'SamiSidebarController');

            Route::resource('project-content', 'ProjectContentController');

            Route::resource('file', 'FileController');


            Route::get('download', 'DownloadController@download');

            Route::post('download', 'DownloadController@store');

            Route::post('file/blog-content', 'FileController@storeBlogContent');

            Route::post('file/project-content', 'FileController@storeProjectContent');
        });
    }
);

Route::domain('doc.' . GlobalClass::getDomain(env('APP_URL')))->group(
    function () {

        /*Route::get('/sitemap.xml', function () {
            return AdminSitemapXmlController::get_site_map(3);
        });*/
        Route::get('/sitemap.xml', function () {
            return AdminSitemapXmlController::get_site_map(3);
        });

        Route::get('/', function () {
            return redirect(GlobalClass::getProtocol(env('APP_URL')) . 'doc.' . GlobalClass::getDomain(env('APP_URL')) . '/docs/1.0/angular/angular.guideline');
        });
        /* Route::get('example', function(){
            return Artisan::call('route:list');//, ['--path' => 'web']
        });

        //Route::get('/', '\\BinaryTorch\\LaRecipe\\Http\\Controllers\\DocumentationController@index');
        Route::get('/', [DocumentationController::class, 'index']);

        Route::get('/scripts/{script} ', 'BinaryTorch\LaRecipe\Http\Controllers\ScriptController');

        Route::get('/search-index/{version}', 'BinaryTorch\LaRecipe\Http\Controllers\ScriptController');a

        Route::get('/styles/{style}', 'BinaryTorch\LaRecipe\Http\Controllers\ScriptController');

        Route::get('/{version}/{page?} ', 'BinaryTorch\LaRecipe\Http\Controllers\DocumentationController@show');*/
    }
);
//$domain = config('base.domains')[0];

Route::domain(GlobalClass::getDomain(env('APP_URL')))->group(function () {

    Route::get('/', function () {
        return redirect('/pe');
    });
    Route::get('/educacion', function () {
        return redirect('/pe/educacion');
    });
    Route::get('/donacion', function () {
        return redirect('/pe/donacion');
    });
    Route::get('/contacto', function () {
        return redirect('/pe/contacto');
    });
    Route::get('/proyectos', function () {
        return redirect('/pe/proyectos');
    });
    Route::get('/blog', function () {
        return redirect('/pe/blog');
    });
    Route::group(['prefix' => 'blog'], function () {
        Route::get('/project', function () {
            return redirect('/pe/blog/project');
        });
        Route::get('/doc', function () {
            return redirect('/pe/blog/doc');
        });
        Route::group(['prefix' => 'project'], function () {

            Route::get('/{name}', function () {
                return redirect('/pe/blog/project/' . request()->route()->parameter('name'));
            });

            Route::get('/{name}/{url}', function () {
                return redirect('/pe/blog/project/' . request()->route()->parameter('name') . '/' . request()->route()->parameter('url'));
            });
        });
        Route::get('/{name}', function ($name) {
            if ($name != 'project' && $name != 'doc') {
                return redirect('/pe/blog/' . $name);
            }
        });
    });

    Route::post('/{language}/message', [MessageController::class, 'store'])->name('contact.message');


    Route::get('/{language}', function () {
        //return view(GlobalClass::getDomainFolder(0) . "::index");
        return view(GlobalClass::getDomainFolder(0) . "::lang." . request()->route()->parameter('language') . ".index");
    })->where('language', 'pe|en|')->middleware('translate');

    Route::group(['prefix' => '{language}'], function () {
        Route::get("educacion", function () {
            //return view(GlobalClass::getDomainFolder(0) . "::education");
            return view(GlobalClass::getDomainFolder(0) . "::lang." . request()->route()->parameter('language') . ".education");
        })->where('language', 'pe')->middleware('translate');

        Route::get("education", function () {
            //return view(GlobalClass::getDomainFolder(0) . "::education");
            return view(GlobalClass::getDomainFolder(0) . "::lang." . request()->route()->parameter('language') . ".education");
        })->where('language', 'en')->middleware('translate');
    });

    Route::group(['prefix' => '{language}'], function () {
        Route::get("donacion", function () {
            //return view(GlobalClass::getDomainFolder(0) . "::donate");
            return view(GlobalClass::getDomainFolder(0) . "::lang." . request()->route()->parameter('language') . ".donate");
        })->where('language', 'pe')->middleware('translate');

        Route::get("donation", function () {
            //return view(GlobalClass::getDomainFolder(0) . "::donate");
            return view(GlobalClass::getDomainFolder(0) . "::lang." . request()->route()->parameter('language') . ".donate");
        })->where('language', 'en')->middleware('translate');
    });

    Route::group(['prefix' => '{language}'], function () {
        Route::get("contacto", function () {
            //return view(GlobalClass::getDomainFolder(0) . "::contact");
            return view(GlobalClass::getDomainFolder(0) . "::lang." . request()->route()->parameter('language') . ".contact");
        })->where('language', 'pe')->middleware('translate');

        Route::get("contact", function () {
            //return view(GlobalClass::getDomainFolder(0) . "::contact");
            return view(GlobalClass::getDomainFolder(0) . "::lang." . request()->route()->parameter('language') . ".contact");
        })->where('language', 'en')->middleware('translate');
    });

    Route::group(['prefix' => '{language}'], function () {

        Route::get('blog', [BlogController::class, 'index'])->where('language', 'pe')->middleware('translate');

        Route::group(['prefix' => 'blog'], function () {

            //Route::get('blog', [BlogController::class, 'index'])->where('language', 'en')->middleware('translate');

            Route::get('/doc', function () {
                return redirect(GlobalClass::getProtocol(env('APP_URL')) . GlobalClass::getDomain(env('APP_URL')) . '/docs/1.0/angular/angular.guideline');
            });

            Route::get('/project', [ProjectController::class, 'index'])->where('language', 'pe')->middleware('translate');

            //Route::get('blog', [BlogController::class, 'index'])->where('language', 'en')->middleware('translate');

            Route::group(['prefix' => 'project'], function () {

                Route::get('/{name}', [ProjectController::class, 'contentType'])->where('language', 'pe')->middleware('translate');

                //Route::get('blog', [BlogController::class, 'index'])->where('language', 'en')->middleware('translate');


                Route::get('/{name}/{url}', [ProjectController::class, 'show'])->where('language', 'pe')->middleware('translate');

                //Route::get('blog', [BlogController::class, 'index'])->where('language', 'en')->middleware('translate');
            });


            Route::get('/{name}', [BlogController::class, 'contentType'])
                ->where([
                    'language' => 'pe', // Esto asegura que el parámetro `language` sea exactamente 'pe'
                    'name' => '^(?!project$|doc$).*' // Esto excluye 'project' del parámetro `name`
                ])
                ->middleware('translate');
        });
        //Route::get('blog', [BlogController::class, 'index'])->where('language', 'en')->middleware('translate');
    });


    Route::get('/sitemap.xml', function () {
        return AdminSitemapXmlController::get_site_map(0);
    });
});
