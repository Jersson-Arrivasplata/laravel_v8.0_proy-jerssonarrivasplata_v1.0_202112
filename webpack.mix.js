const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
mix.js('resources/js/globals.js', 'public/js')
mix.js('resources/js/blog.js', 'public/js')
mix.js('resources/js/blog-content.js', 'public/js')
mix.js('resources/js/blog-content-index.js', 'public/js')
mix.js('resources/js/blog-content-type.js', 'public/js')
mix.js('resources/js/admin/message.js', 'public/js')

mix.css('resources/css/globals.css', 'public/css')
mix.css('resources/css/blog.css', 'public/css')
mix.css('resources/css/blog-content.css', 'public/css')
mix.css('resources/css/blog-content-type.css', 'public/css')


mix.css('resources/css/index.css', 'public/css')
mix.css('resources/css/index-contact.css', 'public/css')
mix.css('resources/css/index-education.css', 'public/css')
mix.css('resources/css/index-project.css', 'public/css')
mix.css('resources/css/admin/message.css', 'public/css')




.postCss('resources/css/app.css', 'public/css', [
    //
]);
