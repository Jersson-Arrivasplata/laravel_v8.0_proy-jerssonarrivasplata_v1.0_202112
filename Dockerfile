
# Usa la imagen base de PHP 7.4.1-FPM
FROM php:7.4.1-fpm

# Instala las extensiones necesarias para Laravel
RUN apt-get update && apt-get install -y \
    libzip-dev \
    unzip \
    && docker-php-ext-install zip pdo_mysql

# Instala Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
# Install Composer
#RUN curl --insecure -ksS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
#RUN curl -k https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#docker build -t laravel .
#mysql-client l
#apt-get purge ca-certificates
#apt-get install ca-certificates
#apt-get install openssl
#docker pull composer
#/home/Projects/laravel_v8.0_proy-jerssonarrivasplata_v1.0_202112
#php artisan serve --host 0.0.0.0 --port 8003 --env local
