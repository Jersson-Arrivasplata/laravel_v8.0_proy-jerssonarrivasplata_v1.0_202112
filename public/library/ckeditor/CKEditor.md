Setup a WYSIWYG editor with CKEditor. Includes instruction for Rails_Admin

- Add `gem 'ckeditor'` to your Gemfile and bundle
- Checkout https://github.com/galetahub/ckeditor for instruction to generate the correct model to generate for uploading of files (eg. ActiveRecord + Paperclip)
- Run `rake db:migrate`
- Add `//= require ckeditor/override` in your application.js (before require_tree .)

To configure the editor to be used in Rails_Admin, follow this https://github.com/sferik/rails_admin/wiki/CKEditor

## Sample Rails_Admin model config

```ruby
class SomeModel < ActiveRecord::Base
  rails_admin do
    list do
      include_all_fields
      field :description do
        formatted_value do
          HTML::FullSanitizer.new.sanitize(bindings[:object].description)
        end
      end
    end

    edit do
      include_all_fields
      field :description, :ck_editor
    end
  end
end
```

## Sample CKEditor Configuration
- Add this to the file: `app/assets/javascripts/ckeditor/config.js`
- Refer to: http://ckeditor.com/latest/samples/plugins/toolbar/toolbar.html

```javascript
CKEDITOR.editorConfig = function( config )
{
  // Define changes to default configuration here. For example:
  config.toolbar = [
    { name: 'document', items : [ 'Source','Preview', 'Maximize', 'ShowBlocks' ] },
    { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
    { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','Scayt' ] },
    { name: 'insert', items : [ 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
    { name: 'styles', items : [ 'Styles','Format' ] },
    { name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
    { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote' ] },
    { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
    { name: 'tools', items : [ 'Maximize','-','About' ] }
  ];

  /* Filebrowser routes */
  // The location of an external file browser, that should be launched when "Browse Server" button is pressed.
  config.filebrowserBrowseUrl = "/ckeditor/attachment_files";

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Flash dialog.
  config.filebrowserFlashBrowseUrl = "/ckeditor/attachment_files";

  // The location of a script that handles file uploads in the Flash dialog.
  config.filebrowserFlashUploadUrl = "/ckeditor/attachment_files";

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Link tab of Image dialog.
  config.filebrowserImageBrowseLinkUrl = "/ckeditor/pictures";

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Image dialog.
  config.filebrowserImageBrowseUrl = "/ckeditor/pictures";

  // The location of a script that handles file uploads in the Image dialog.
  config.filebrowserImageUploadUrl = "/ckeditor/pictures";

  // The location of a script that handles file uploads.
  config.filebrowserUploadUrl = "/ckeditor/attachment_files";

  // Rails CSRF token
  config.filebrowserParams = function(){
    var csrf_token, csrf_param, meta,
        metas = document.getElementsByTagName('meta'),
        params = new Object();

    for ( var i = 0 ; i < metas.length ; i++ ){
      meta = metas[i];

      switch(meta.name) {
        case "csrf-token":
          csrf_token = meta.content;
          break;
        case "csrf-param":
          csrf_param = meta.content;
          break;
        default:
          continue;
      }
    }

    if (csrf_param !== undefined && csrf_token !== undefined) {
      params[csrf_param] = csrf_token;
    }

    return params;
  };

  config.addQueryString = function( url, params ){
    var queryString = [];

    if ( !params ) {
      return url;
    } else {
      for ( var i in params )
        queryString.push( i + "=" + encodeURIComponent( params[ i ] ) );
    }

    return url + ( ( url.indexOf( "?" ) != -1 ) ? "&" : "?" ) + queryString.join( "&" );
  };

  // Integrate Rails CSRF token into file upload dialogs (link, image, attachment and flash)
  CKEDITOR.on( 'dialogDefinition', function( ev ){
    // Take the dialog name and its definition from the event data.
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;
    var content, upload;

    if (CKEDITOR.tools.indexOf(['link', 'image', 'attachment', 'flash'], dialogName) > -1) {
      content = (dialogDefinition.getContents('Upload') || dialogDefinition.getContents('upload'));
      upload = (content == null ? null : content.get('upload'));

      if (upload && upload.filebrowser && upload.filebrowser['params'] === undefined) {
        upload.filebrowser['params'] = config.filebrowserParams();
        upload.action = config.addQueryString(upload.action, upload.filebrowser['params']);
      }
    }
  });
};
```

## Deployment to Production
Due to Rails 4 asset pipeline, compilation of non-digest assets will cause CKEditor to not load in production.
Deploying CKEditor on production requires the following steps:
- Add to your gemfile: `gem 'non-stupid-digest-assets'`
- Add to your `config/application.rb`: `config.assets.precompile += Ckeditor.assets`

## Errors
- Browse server link to upload file may throw a undefined method limit. If so, run `bundle update orm_adapter`
- Browse server link to upload file may throw a 404 no routes found. If so, you need to monkey patch here: https://github.com/abdulsattar/ckeditor/blob/master/lib/ckeditor/utils.rb (Rails 4.1.0 and CKeditor 4.0.11)
