/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//config.extraPlugins = 'myplugin,anotherplugin';
	config.extraPlugins = 'token';
	config.allowedContent = true;
	config.fullPage = true;
	config.clipboard_defaultContentType = 'html';
	config.entities = false;
	config.availableTokens = [
		["", ""],
		["token1", "token1"],
		["token2", "token2"],
		["token3", "token3"],
	];
};
