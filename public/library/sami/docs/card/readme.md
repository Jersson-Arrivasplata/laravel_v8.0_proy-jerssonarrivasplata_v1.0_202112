
```html
<sami-card margin="auto"
card-title="ANGULAR"
card-subtitle="Tipos de Formularios en Angular"
card-resume="Se implementa formularios de angular aplicando Form Builder y FormGroup con
  @angular/forms aplicando las siguientes clases FormBuilder, FormGroup, FormArray."
image-src="angular-proy-v1-implementation-angular-forms-s2201.png"
list-social-media-data='[
  {
    "name":"Tipo de Formularios en Angular",
    "url":"https:\\/github.com\/jersson-arrivasplata-rojas\/ANGULAR-PROY-V1-IMPLEMENTATION-ANGULAR-FORMS-S2201",
    "urlImage":"library/sami/assets/icon-social-media/github.svg",
    "type":"github"
  }
]'
list-group-data='[
    {
        "url": "https://projects.jerssonarrivasplata.com/angular/tipos-de-formularios-en-angular",
        "text": "Ir a",
        "target": "_self",
        "fnClick": "function(){loader()}"
    }
]'></sami-card>
```
