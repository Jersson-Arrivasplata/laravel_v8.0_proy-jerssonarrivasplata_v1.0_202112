# Iconos



<!-- Auto Generated Below -->


## Uso

Agregar la siguiente ruta de iconos:

https://sami.jerssonarrivasplata.com/library/sami/icons/icons.css

Copiar e incluir en los estilos:
&lt;link href=&quot;[https://sami.jerssonarrivasplata.com/library/sami/icons/icons.css](https://sami.jerssonarrivasplata.com/library/sami/icons/icons.css)&quot; rel=&quot;stylesheet&quot;&gt;


