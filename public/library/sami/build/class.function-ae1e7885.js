var AlignItemsEnum;
(function (AlignItemsEnum) {
  AlignItemsEnum["CENTER"] = "center";
})(AlignItemsEnum || (AlignItemsEnum = {}));

var BackgroundColorEnum;
(function (BackgroundColorEnum) {
  BackgroundColorEnum["DARK"] = "dark";
})(BackgroundColorEnum || (BackgroundColorEnum = {}));

var BorderStyleEnum;
(function (BorderStyleEnum) {
  BorderStyleEnum["SOLID"] = "solid";
})(BorderStyleEnum || (BorderStyleEnum = {}));

var ColorEnum;
(function (ColorEnum) {
  ColorEnum["WHITE"] = "white";
  ColorEnum["DARK"] = "dark";
})(ColorEnum || (ColorEnum = {}));

var DisplayEnum;
(function (DisplayEnum) {
  DisplayEnum["BLOCK"] = "block";
  DisplayEnum["CONTENTS"] = "contents";
  DisplayEnum["FLEX"] = "flex";
  DisplayEnum["INITIAL"] = "initial";
})(DisplayEnum || (DisplayEnum = {}));

var FontWeigthEnum;
(function (FontWeigthEnum) {
  FontWeigthEnum["BOLD"] = "bold";
  FontWeigthEnum[FontWeigthEnum["NUMBER_400"] = 400] = "NUMBER_400";
})(FontWeigthEnum || (FontWeigthEnum = {}));

var JustifyItemsEnum;
(function (JustifyItemsEnum) {
  JustifyItemsEnum["CENTER"] = "center";
})(JustifyItemsEnum || (JustifyItemsEnum = {}));

var ListStyleEnum;
(function (ListStyleEnum) {
  ListStyleEnum["NONE"] = "none";
})(ListStyleEnum || (ListStyleEnum = {}));

var MarginEnum;
(function (MarginEnum) {
  MarginEnum["AUTO"] = "auto";
})(MarginEnum || (MarginEnum = {}));

var NumberEnum;
(function (NumberEnum) {
  NumberEnum[NumberEnum["NUMBER_0"] = 0] = "NUMBER_0";
  NumberEnum[NumberEnum["NUMBER_1"] = 1] = "NUMBER_1";
  NumberEnum[NumberEnum["NUMBER_2"] = 2] = "NUMBER_2";
  NumberEnum[NumberEnum["NUMBER_3"] = 3] = "NUMBER_3";
  NumberEnum[NumberEnum["NUMBER_4"] = 4] = "NUMBER_4";
})(NumberEnum || (NumberEnum = {}));

var ObjectFitEnum;
(function (ObjectFitEnum) {
  ObjectFitEnum["COVER"] = "cover";
  ObjectFitEnum["CONTAIN"] = "contain";
})(ObjectFitEnum || (ObjectFitEnum = {}));

var OpacityEnum;
(function (OpacityEnum) {
  OpacityEnum[OpacityEnum["NUMBER_8"] = 0.8] = "NUMBER_8";
})(OpacityEnum || (OpacityEnum = {}));

var PositionEnum;
(function (PositionEnum) {
  PositionEnum["ABSOLUTE"] = "absolute";
  PositionEnum["RELATIVE"] = "relative";
  PositionEnum["FIXED"] = "fixed";
  PositionEnum["STICKY"] = "sticky";
})(PositionEnum || (PositionEnum = {}));

var TextAlignEnum;
(function (TextAlignEnum) {
  TextAlignEnum["CENTER"] = "center";
})(TextAlignEnum || (TextAlignEnum = {}));

var TextDecorationEnum;
(function (TextDecorationEnum) {
  TextDecorationEnum["NONE"] = "none";
  TextDecorationEnum["UNDERLINE"] = "underline";
})(TextDecorationEnum || (TextDecorationEnum = {}));

var TextTransformEnum;
(function (TextTransformEnum) {
  TextTransformEnum["UPPERCASE"] = "uppercase";
})(TextTransformEnum || (TextTransformEnum = {}));

var WordBreakEnum;
(function (WordBreakEnum) {
  WordBreakEnum["WORD"] = "break-word";
})(WordBreakEnum || (WordBreakEnum = {}));

function textDecoration(text) {
  if (text && TextDecorationEnum[(text).toUpperCase()]) {
    return 'sami-text-decoration-' + TextDecorationEnum[text.toUpperCase()];
  }
  return '';
}
function textColor(text) {
  if (text && ColorEnum[(text).toUpperCase()]) {
    return 'sami-text-color-' + ColorEnum[text.toUpperCase()];
  }
  return '';
}
function position(text) {
  if (text && PositionEnum[(text).toUpperCase()]) {
    return 'sami-position-' + PositionEnum[text.toUpperCase()];
  }
  return '';
}
function borderStyle(text) {
  if (text && BorderStyleEnum[(text).toUpperCase()]) {
    return 'sami-border-style-' + BorderStyleEnum[text.toUpperCase()];
  }
  return '';
}
function borderWidth(text) {
  if (text && (NumberEnum['NUMBER_' + (text).toUpperCase()])) {
    return 'sami-border-width-' + (NumberEnum['NUMBER_' + (text).toUpperCase()]);
  }
  return '';
}
function backgroundColor(text) {
  if (text && BackgroundColorEnum[(text).toUpperCase()]) {
    return 'sami-background-color-' + BackgroundColorEnum[text.toUpperCase()];
  }
  return '';
}
function textAlign(text) {
  if (text && TextAlignEnum[(text).toUpperCase()]) {
    return 'sami-text-align-' + TextAlignEnum[text.toUpperCase()];
  }
  return '';
}
function justifyItems(text) {
  if (text && JustifyItemsEnum[(text).toUpperCase()]) {
    return 'sami-justify-items-' + JustifyItemsEnum[text.toUpperCase()];
  }
  return '';
}
function alignItems(text) {
  if (text && AlignItemsEnum[(text).toUpperCase()]) {
    return 'sami-align-items-' + AlignItemsEnum[text.toUpperCase()];
  }
  return '';
}
function textTransform(text) {
  if (text && TextTransformEnum[(text).toUpperCase()]) {
    return 'sami-text-transform-' + TextTransformEnum[text.toUpperCase()];
  }
  return '';
}
function fontWeigth(text) {
  if (text && (FontWeigthEnum[(text).toUpperCase()] || FontWeigthEnum['NUMBER_' + (text).toUpperCase()])) {
    return 'sami-font-weigth-' + (FontWeigthEnum[(text).toUpperCase()] || FontWeigthEnum['NUMBER_' + (text).toUpperCase()]);
  }
  return '';
}
function listStyle(text) {
  if (text && (ListStyleEnum[(text).toUpperCase()])) {
    return 'sami-list-style-' + (ListStyleEnum[(text).toUpperCase()]);
  }
  return '';
}
function opacity(text) {
  if (text && (OpacityEnum['NUMBER_' + (text).toUpperCase()])) {
    return 'sami-opacity-' + text;
  }
  return '';
}
function filterInvert(text) {
  if (text && (NumberEnum['NUMBER_' + (text).toUpperCase()])) {
    return 'sami-filter-invert-' + (NumberEnum['NUMBER_' + (text).toUpperCase()]);
  }
  return '';
}
function objectFit(text) {
  if (text && (ObjectFitEnum[(text).toUpperCase()])) {
    return 'sami-object-fit-' + (ObjectFitEnum[(text).toUpperCase()]);
  }
  return '';
}
function display(text) {
  if (text && (DisplayEnum[(text).toUpperCase()])) {
    return 'sami-display-' + (DisplayEnum[(text).toUpperCase()]);
  }
  return '';
}
function margin(text) {
  if (text && (MarginEnum[(text).toUpperCase()])) {
    return 'sami-margin-' + (MarginEnum[(text).toUpperCase()]);
  }
  return '';
}
function wordBreak(text) {
  if (text && (WordBreakEnum[(text).toUpperCase()])) {
    return 'sami-word-break-' + (WordBreakEnum[(text).toUpperCase()]);
  }
  return '';
}
/*export function zIndex(text: string) {
  if (text && (NumberEnum['NUMBER_' + (text).toUpperCase()])) {
    return 'sami-z-index-' + (NumberEnum['NUMBER_' + (text).toUpperCase()]);
  }
  return '';
}*/

export { alignItems as a, borderStyle as b, borderWidth as c, display as d, textDecoration as e, backgroundColor as f, filterInvert as g, objectFit as h, textAlign as i, justifyItems as j, textTransform as k, listStyle as l, margin as m, fontWeigth as n, opacity as o, position as p, textColor as t, wordBreak as w };
