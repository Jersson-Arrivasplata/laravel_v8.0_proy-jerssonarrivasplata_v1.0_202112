import { r as registerInstance, h, g as getElement } from './index-a31fb924.js';

const sliderCss = ":root{--sami-screen:rgb(6, 82, 221);--sami-screen-100:rgb(6, 82, 221);--sami-border:rgb(6, 82, 221);--sami-border-900:rgb(6, 82, 221);--sami-primary:rgb(13, 110, 253);--sami-secondary:rgb(102, 16, 242);--sami-success:rgb(32, 201, 151);--sami-danger:rgb(220, 53, 69);--sami-warning:rgb(255, 193, 7);--sami-info:rgb(13, 202, 240);--sami-light:rgb(173, 181, 189);--sami-dark:rgb(0, 0, 0);--sami-dark-200:rgba(0, 0, 0, 0.2);--sami-dark-500:rgba(0, 0, 0, 0.5);--sami-white:rgb(255, 255, 255);--sami-white-900:rgba(255, 255, 255, 0.9);--sami-white-800:rgba(255, 255, 255, 0.8);--sami-white-500:rgba(255, 255, 255, 0.5);--sami-spacing-s:8px;--sami-spacing-m:16px;--sami-spacing-l:24px;--sami-spacing-xl:32px;--sami-spacing-xxl:64px;--sami-width-container:1200px;--sami-muted:rgb(108, 117, 125)}.text-end{text-align:end}.mb-1{margin-bottom:1rem}sami-slider .sami-slider{position:relative;width:100%;margin:0 auto;overflow:hidden}sami-slider .sami-slider ul.sami-slider___ul{display:flex;margin:0;padding:0;list-style:none;transition:transform 0.5s ease-in-out}sami-slider .sami-slider ul.sami-slider___ul ::slotted(li){flex:1 0 auto;width:100%;margin:auto;height:300px;background-color:lightgreen}sami-slider .sami-slider ul.sami-slider___ul ::slotted(li:nth-child(even)){background-color:lightblue;height:400px}sami-slider .sami-slider___button{position:absolute;z-index:1;top:calc(50% - 32px);padding:5px;font-size:18px;line-height:20px;font-weight:bolder;opacity:0.5;color:var(--sami-white);border-style:none;outline:none;background:var(--sami-dark)}sami-slider .sami-slider___button:hover{opacity:1;transition:opacity 0.5s ease-in-out}sami-slider .sami-slider___button[disabled],sami-slider .sami-slider___button[disabled]:hover{opacity:0.25}sami-slider .sami-slider___button.sami-slider___button_next{right:0}sami-slider .sami-slider___button.sami-slider___button_prev{left:0}";

const Slider = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.showStatus = false;
    this.currentSlideNumber = 0;
    this.slidesCount = 0;
    this.slideWidth = 0;
    this.controls = {
      prev: null,
      next: null
    };
    this.class = [];
  }
  componentWillLoad() {
    const className = this.host.className;
    this.class = (className).split(' ');
    this.host.className = '';
    //this.slides = this.host.querySelectorAll('li');
    this.slides = Array.from(this.host.children);
    this.slidesCount = this.slides.length;
  }
  componentDidLoad() {
    //this.sliderList = this.el.shadowRoot.querySelector('ul');
    this.sliderList = this.host.querySelector('ul.sami-slider___ul');
    this.slideWidth = this.slides[0].offsetWidth;
    for (let type in this.controls) {
      //this.controls[type] = this.el.shadowRoot.querySelector('.btn_' + type);
      this.controls[type] = this.host.querySelector('.sami-slider___button_' + type);
    }
    this.updateControls();
  }
  componentDidUpdate() {
    this.sliderList.style.transform = `translateX(${this.currentSlideNumber * this.slideWidth * -1}px)`;
    this.updateControls();
  }
  slide(amount = 1) {
    let slideTo = this.currentSlideNumber + amount;
    if (slideTo < 0 || slideTo >= this.slidesCount)
      return;
    this.currentSlideNumber = slideTo;
  }
  updateControls() {
    this.switchControl('prev', (this.currentSlideNumber === 0) ? false : true);
    this.switchControl('next', (this.currentSlideNumber === this.slidesCount - 1) ? false : true);
  }
  switchControl(type, enabled) {
    if (this.controls[type])
      this.controls[type].disabled = !enabled;
  }
  getClass() {
    return this.class.join(' ');
  }
  validateSlideCount() {
    return !Boolean(this.slidesCount > 1);
  }
  render() {
    return (h("div", { class: `sami-slider ${this.getClass()}` }, h("button", { type: "button", class: "sami-slider___button sami-slider___button_next", onClick: this.slide.bind(this, 1), hidden: this.validateSlideCount() }, ">"), h("button", { type: "button", class: "sami-slider___button sami-slider___button_prev", onClick: this.slide.bind(this, -1), hidden: this.validateSlideCount() }, "<"), h("ul", { class: `sami-slider___ul` }, h("slot", null)), this.showStatus && h("div", null, "Slide ", this.currentSlideNumber + 1, "/", this.slidesCount)));
  }
  get host() { return getElement(this); }
};
Slider.style = sliderCss;

export { Slider as sami_slider };
