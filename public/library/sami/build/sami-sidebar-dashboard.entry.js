import { r as registerInstance, h, g as getElement } from './index-a31fb924.js';
import { b as build } from './index-34c8c83d.js';

const sidebarDashboardCss = ":root{--sami-screen:rgb(6, 82, 221);--sami-screen-100:rgb(6, 82, 221);--sami-border:rgb(6, 82, 221);--sami-border-900:rgb(6, 82, 221);--sami-primary:rgb(13, 110, 253);--sami-secondary:rgb(102, 16, 242);--sami-success:rgb(32, 201, 151);--sami-danger:rgb(220, 53, 69);--sami-warning:rgb(255, 193, 7);--sami-info:rgb(13, 202, 240);--sami-light:rgb(173, 181, 189);--sami-dark:rgb(0, 0, 0);--sami-dark-200:rgba(0, 0, 0, 0.2);--sami-dark-500:rgba(0, 0, 0, 0.5);--sami-white:rgb(255, 255, 255);--sami-white-900:rgba(255, 255, 255, 0.9);--sami-white-800:rgba(255, 255, 255, 0.8);--sami-white-500:rgba(255, 255, 255, 0.5);--sami-spacing-s:8px;--sami-spacing-m:16px;--sami-spacing-l:24px;--sami-spacing-xl:32px;--sami-spacing-xxl:64px;--sami-width-container:1200px;--sami-muted:rgb(108, 117, 125)}.text-end{text-align:end}.mb-1{margin-bottom:1rem}html{font-size:10pt}.sami-margin-auto{margin:auto}.sami-margin-right-auto{margin-right:auto}.sami-display-block{display:block}.sami-display-initial{display:initial}.sami-display-flex{display:flex}.sami-display-contents{display:contents}.sami-justify-items-center{justify-items:center}.sami-align-items-center{align-items:center}.sami-text-color-white{color:var(--sami-white)}.sami-text-color-dark{color:var(--sami-dark)}.sami-text-align-center{text-align:center}.sami-text-decoration-none{text-decoration:none}.sami-text-decoration-underline{text-decoration:underline}.sami-text-transform-uppercase{text-transform:uppercase}.sami-font-weigth-bold{font-weight:bold}.sami-font-weigth-400{font-weight:400}.sami-position-absolute{position:absolute}.sami-position-relative{position:relative}.sami-position-fixed{position:fixed}.sami-position-sticky{position:sticky}.sami-border-style-solid{border-style:solid}.sami-border-width-2{border-width:2px}.sami-background-color-dark{background-color:var(--sami-dark)}.sami-background-color-white{background-color:var(--sami-white)}.sami-list-style-none{list-style:none}.sami-opacity-8{opacity:0.8}.sami-filter-invert-1{filter:invert(1)}.sami-object-fit-cover{object-fit:cover}.sami-object-fit-contain{object-fit:contain}.sami-border-width-0{border-width:0px}.sami-border-width-1{border-width:1px}.sami-border-width-2{border-width:2px}.sami-border-width-3{border-width:3px}.d-flex{display:flex}.justify-content-evenly{justify-content:space-evenly}.width-100{width:100%}.r-0{right:0}.t-0{top:0}.m-0{margin:0}.mt-0{margin-top:0}.mr-0{margin-right:0}.mb-0{margin-bottom:0}.ml-0{margin-left:0}.m-1{margin:0.25rem}.mt-1{margin-top:0.25rem}.mr-1{margin-right:0.25rem}.mb-1{margin-bottom:0.25rem}.ml-1{margin-left:0.25rem}.m-2{margin:0.5rem}.mt-2{margin-top:0.5rem}.mr-2{margin-right:0.5rem}.mb-2{margin-bottom:0.5rem}.ml-2{margin-left:0.5rem}.m-3{margin:1rem}.mt-3{margin-top:1rem}.mr-3{margin-right:1rem}.mb-3{margin-bottom:1rem}.ml-3{margin-left:1rem}.m-4{margin:1.5rem}.mt-4{margin-top:1.5rem}.mr-4{margin-right:1.5rem}.mb-4{margin-bottom:1.5rem}.ml-4{margin-left:1.5rem}.m-5{margin:2rem}.mt-5{margin-top:2rem}.mr-5{margin-right:2rem}.mb-5{margin-bottom:2rem}.ml-5{margin-left:2rem}.m-6{margin:2.5rem}.mt-6{margin-top:2.5rem}.mr-6{margin-right:2.5rem}.mb-6{margin-bottom:2.5rem}.ml-6{margin-left:2.5rem}.m-7{margin:3rem}.mt-7{margin-top:3rem}.mr-7{margin-right:3rem}.mb-7{margin-bottom:3rem}.ml-7{margin-left:3rem}.m-8{margin:3.5rem}.mt-8{margin-top:3.5rem}.mr-8{margin-right:3.5rem}.mb-8{margin-bottom:3.5rem}.ml-8{margin-left:3.5rem}.m-9{margin:4rem}.mt-9{margin-top:4rem}.mr-9{margin-right:4rem}.mb-9{margin-bottom:4rem}.ml-9{margin-left:4rem}.m-10{margin:4.5rem}.mt-10{margin-top:4.5rem}.mr-10{margin-right:4.5rem}.mb-10{margin-bottom:4.5rem}.ml-10{margin-left:4.5rem}.m-11{margin:5rem}.mt-11{margin-top:5rem}.mr-11{margin-right:5rem}.mb-11{margin-bottom:5rem}.ml-11{margin-left:5rem}.m-12{margin:5.5rem}.mt-12{margin-top:5.5rem}.mr-12{margin-right:5.5rem}.mb-12{margin-bottom:5.5rem}.ml-12{margin-left:5.5rem}.m-13{margin:6rem}.mt-13{margin-top:6rem}.mr-13{margin-right:6rem}.mb-13{margin-bottom:6rem}.ml-13{margin-left:6rem}@media (max-width: 770px){.sm-m-10{margin:4.5rem}.sm-mt-10{margin-top:4.5rem}.sm-mr-10{margin-right:4.5rem}.sm-mb-10{margin-bottom:4.5rem}.sm-ml-10{margin-left:4.5rem}}.p-0{padding:0}.pt-0{padding-top:0}.pr-0{padding-right:0}.pb-0{padding-bottom:0}.pl-0{padding-left:0}.p-1{padding:0.25rem}.pt-1{padding-top:0.25rem}.pr-1{padding-right:0.25rem}.pb-1{padding-bottom:0.25rem}.pl-1{padding-left:0.25rem}.p-2{padding:0.5rem}.pt-2{padding-top:0.5rem}.pr-2{padding-right:0.5rem}.pb-2{padding-bottom:0.5rem}.pl-2{padding-left:0.5rem}.p-3{padding:1rem}.pt-3{padding-top:1rem}.pr-3{padding-right:1rem}.pb-3{padding-bottom:1rem}.pl-3{padding-left:1rem}.p-4{padding:1.5rem}.pt-4{padding-top:1.5rem}.pr-4{padding-right:1.5rem}.pb-4{padding-bottom:1.5rem}.pl-4{padding-left:1.5rem}.p-5{padding:3rem}.pt-5{padding-top:3rem}.pr-5{padding-right:3rem}.pb-5{padding-bottom:3rem}.pl-5{padding-left:3rem}.z-index-1{z-index:1}.z-index-2{z-index:2}.z-index-3{z-index:3}.z-index-4{z-index:4}.sami-word-break-break-word{word-break:break-word}.sami-only-desktop{display:none}@media (min-width: 820px){.sami-only-desktop{display:block !important}}@media (min-width: 520px){.sami-desk-max-width-520{max-width:520px !important}}sami-sidebar-dashboard div.sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav{left:0px;transition:all 1s}sami-sidebar-dashboard div.sami-sidebar-dashboard.sami-sidebar-dashboard___mobile:not(.sami-sidebar-dashboard___active) nav.sami-sidebar-dashboard___nav,sami-sidebar-dashboard div.sami-sidebar-dashboard.sami-sidebar-dashboard___desktop:not(.sami-sidebar-dashboard___active) nav.sami-sidebar-dashboard___nav{left:-320px;transition:all 1s}sami-sidebar-dashboard div.sami-sidebar-dashboard.sami-sidebar-dashboard___active::before{position:fixed;content:\"\";width:100%;height:100%;background:#00000042;left:0;top:0;z-index:1}sami-sidebar-dashboard div.sami-sidebar-dashboard.sami-sidebar-dashboard___mobile:not(.sami-sidebar-dashboard___active)::before,sami-sidebar-dashboard div.sami-sidebar-dashboard.sami-sidebar-dashboard___desktop:not(.sami-sidebar-dashboard___active)::before{display:none}sami-sidebar-dashboard div.sami-sidebar-dashboard.sami-sidebar-dashboard___active.sami-sidebar-dashboard___mobile::before,sami-sidebar-dashboard div.sami-sidebar-dashboard.sami-sidebar-dashboard___active.sami-sidebar-dashboard___desktop::before{display:block}@media (min-width: 770px){sami-sidebar-dashboard div.sami-sidebar-dashboard.sami-sidebar-dashboard___active.sami-sidebar-dashboard___mobile::before,sami-sidebar-dashboard div.sami-sidebar-dashboard.sami-sidebar-dashboard___active.sami-sidebar-dashboard___desktop::before{display:none}}sami-sidebar-dashboard div.sami-sidebar-dashboard.sami-sidebar-dashboard___active.sami-sidebar-dashboard___mobile nav.sami-sidebar-dashboard___nav .sami-sidebar-dashboard___menu,sami-sidebar-dashboard div.sami-sidebar-dashboard.sami-sidebar-dashboard___active.sami-sidebar-dashboard___desktop nav.sami-sidebar-dashboard___nav .sami-sidebar-dashboard___menu{box-shadow:8px 8px 8px -1px var(--sami-dark-200)}sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav{box-shadow:0px 0px 0px 0px var(--sami-dark-200), 0px 0px 3px 0px var(--sami-dark-200);background:var(--sami-screen);z-index:1;width:100%;height:100%;max-width:320px;top:0;position:fixed}sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav .sami-sidebar-dashboard___menu{display:none;padding:0px;position:absolute;top:18px;right:-45px;z-index:0;font-size:2.3rem;height:45px;width:45px;text-align:center;background:var(--sami-screen);color:var(--sami-dark)}sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav .sami-sidebar-dashboard___body{width:245px;margin:auto}sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav .sami-sidebar-dashboard___body sami-list ul sami-list-item>li:nth-child(1)>sami-paragraph:nth-child(1) p{color:var(--sami-dark-500);border-bottom-style:solid;border-bottom-width:1px;border-bottom-color:var(--sami-dark-200);display:block;padding-bottom:10px;font-size:12px}sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav .sami-sidebar-dashboard___body sami-list ul sami-list-item li a{padding:10px 34px 10px 35px;font-size:14px;text-decoration:none;color:var(--sami-dark);height:40px;display:flex;justify-content:left;align-items:center}sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav .sami-sidebar-dashboard___body sami-list ul sami-list-item li a:hover .sami-hyperlink___block{display:block;width:3px;background:var(--sami-border);height:100%;position:absolute;right:-37px}sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav .sami-sidebar-dashboard___footer{margin-bottom:10%}sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav .sami-sidebar-dashboard___footer ul{margin:0px 25px 0px 25px}@media (max-width: 770px){sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav .sami-sidebar-dashboard___menu{display:block}}@media (max-width: 770px){sami-sidebar-dashboard nav.sami-sidebar-dashboard___nav{padding-top:0px !important}}";

const SidebarDashboard = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.class = [];
    this.innerWidth = 0;
    this.isMenuOpen = false;
    this.isMobile = false;
  }
  handleScroll(e) {
    //const array = Array.from(e.srcElement['path']);
    const target = e.target;
    if (!(target.innerWidth === this.innerWidth)) {
      this.validateMenu(target); //target
    }
  }
  componentWillLoad() {
    const className = this.host.className;
    this.class = (className).split(' ');
    this.host.className = '';
    this.children = Array.from(this.host.children);
    (this.children).forEach(x => {
      const part = Array.from(x['part']);
      if (part && part[0] == 'menu')
        x.slot = 'menu';
      if (part && part[0] == 'body')
        x.slot = 'body';
      if (part && part[0] == 'footer')
        x.slot = 'footer';
      if (x.slot == 'menu') {
        x.classList.add('sami-sidebar-dashboard___menu');
      }
      else if (x.slot == 'body') {
        x.classList.add('sami-sidebar-dashboard___body');
      }
      else if (x.slot == 'footer') {
        x.classList.add('sami-sidebar-dashboard___footer');
      }
    });
  }
  componentDidLoad() {
    this.innerWidth = window.innerWidth;
    this.validateMenu(window); //window
  }
  componentWillRender() {
  }
  validateMenu(target) {
    //this.isMenuOpen = !this.isMenuOpen;
    this.isMobile = Boolean((target['innerWidth'] < 770) ? new build.Devices().isMobile() : false);
    //this.isMenuOpen = Boolean(target['innerWidth'] < 770);
    this.isMenuOpen = true;
  }
  async show() {
    this.isMenuOpen = !this.isMenuOpen;
    this.isMobile = Boolean(new build.Devices().isMobile());
  }
  getClass() {
    this.class = [];
    if (this.isMenuOpen) {
      this.class.push('sami-sidebar-dashboard___active');
    }
    if (this.isMobile) {
      this.class.push('sami-sidebar-dashboard___mobile');
    }
    if (!this.isMobile) {
      this.class.push('sami-sidebar-dashboard___desktop');
    }
    return this.class.join(' ');
  }
  render() {
    return (h("div", { class: `sami-sidebar-dashboard ${this.getClass()}` }, h("nav", { class: 'sami-sidebar-dashboard___nav' }, h("slot", { name: "menu" }), h("slot", { name: "body" }), h("slot", { name: "footer" }), h("div", { class: "sami-sidebar-dashboard___border" }))));
  }
  get host() { return getElement(this); }
};
SidebarDashboard.style = sidebarDashboardCss;

export { SidebarDashboard as sami_sidebar_dashboard };
