import { B as BUILD, c as consoleDevInfo, p as plt, w as win, H, d as doc, N as NAMESPACE, a as promiseResolve, b as bootstrapLazy } from './index-a31fb924.js';
import { g as globalScripts } from './app-globals-15fb550d.js';

/*
 Stencil Client Patch Browser v2.15.2 | MIT Licensed | https://stenciljs.com
 */
const getDynamicImportFunction = (namespace) => `__sc_import_${namespace.replace(/\s|-/g, '_')}`;
const patchBrowser = () => {
    // NOTE!! This fn cannot use async/await!
    if (BUILD.isDev && !BUILD.isTesting) {
        consoleDevInfo('Running in development mode.');
    }
    if (BUILD.cssVarShim) {
        // shim css vars
        plt.$cssShim$ = win.__cssshim;
    }
    if (BUILD.cloneNodeFix) {
        // opted-in to polyfill cloneNode() for slot polyfilled components
        patchCloneNodeFix(H.prototype);
    }
    if (BUILD.profile && !performance.mark) {
        // not all browsers support performance.mark/measure (Safari 10)
        // because the mark/measure APIs are designed to write entries to a buffer in the browser that does not exist,
        // simply stub the implementations out.
        // TODO(STENCIL-323): Remove this patch when support for older browsers is removed (breaking)
        // @ts-ignore
        performance.mark = performance.measure = () => {
            /*noop*/
        };
        performance.getEntriesByName = () => [];
    }
    // @ts-ignore
    const scriptElm = BUILD.scriptDataOpts || BUILD.safari10 || BUILD.dynamicImportShim
        ? Array.from(doc.querySelectorAll('script')).find((s) => new RegExp(`\/${NAMESPACE}(\\.esm)?\\.js($|\\?|#)`).test(s.src) ||
            s.getAttribute('data-stencil-namespace') === NAMESPACE)
        : null;
    const importMeta = import.meta.url;
    const opts = BUILD.scriptDataOpts ? scriptElm['data-opts'] || {} : {};
    if (BUILD.safari10 && 'onbeforeload' in scriptElm && !history.scrollRestoration /* IS_ESM_BUILD */) {
        // Safari < v11 support: This IF is true if it's Safari below v11.
        // This fn cannot use async/await since Safari didn't support it until v11,
        // however, Safari 10 did support modules. Safari 10 also didn't support "nomodule",
        // so both the ESM file and nomodule file would get downloaded. Only Safari
        // has 'onbeforeload' in the script, and "history.scrollRestoration" was added
        // to Safari in v11. Return a noop then() so the async/await ESM code doesn't continue.
        // IS_ESM_BUILD is replaced at build time so this check doesn't happen in systemjs builds.
        return {
            then() {
                /* promise noop */
            },
        };
    }
    if (!BUILD.safari10 && importMeta !== '') {
        opts.resourcesUrl = new URL('.', importMeta).href;
    }
    else if (BUILD.dynamicImportShim || BUILD.safari10) {
        opts.resourcesUrl = new URL('.', new URL(scriptElm.getAttribute('data-resources-url') || scriptElm.src, win.location.href)).href;
        if (BUILD.dynamicImportShim) {
            patchDynamicImport(opts.resourcesUrl, scriptElm);
        }
        if (BUILD.dynamicImportShim && !win.customElements) {
            // module support, but no custom elements support (Old Edge)
            // @ts-ignore
            return import(/* webpackChunkName: "polyfills-dom" */ './dom-7cd9cf71.js').then(() => opts);
        }
    }
    return promiseResolve(opts);
};
const patchDynamicImport = (base, orgScriptElm) => {
    const importFunctionName = getDynamicImportFunction(NAMESPACE);
    try {
        // test if this browser supports dynamic imports
        // There is a caching issue in V8, that breaks using import() in Function
        // By generating a random string, we can workaround it
        // Check https://bugs.chromium.org/p/chromium/issues/detail?id=990810 for more info
        win[importFunctionName] = new Function('w', `return import(w);//${Math.random()}`);
    }
    catch (e) {
        // this shim is specifically for browsers that do support "esm" imports
        // however, they do NOT support "dynamic" imports
        // basically this code is for old Edge, v18 and below
        const moduleMap = new Map();
        win[importFunctionName] = (src) => {
            const url = new URL(src, base).href;
            let mod = moduleMap.get(url);
            if (!mod) {
                const script = doc.createElement('script');
                script.type = 'module';
                script.crossOrigin = orgScriptElm.crossOrigin;
                script.src = URL.createObjectURL(new Blob([`import * as m from '${url}'; window.${importFunctionName}.m = m;`], {
                    type: 'application/javascript',
                }));
                mod = new Promise((resolve) => {
                    script.onload = () => {
                        resolve(win[importFunctionName].m);
                        script.remove();
                    };
                });
                moduleMap.set(url, mod);
                doc.head.appendChild(script);
            }
            return mod;
        };
    }
};
const patchCloneNodeFix = (HTMLElementPrototype) => {
    const nativeCloneNodeFn = HTMLElementPrototype.cloneNode;
    HTMLElementPrototype.cloneNode = function (deep) {
        if (this.nodeName === 'TEMPLATE') {
            return nativeCloneNodeFn.call(this, deep);
        }
        const clonedNode = nativeCloneNodeFn.call(this, false);
        const srcChildNodes = this.childNodes;
        if (deep) {
            for (let i = 0; i < srcChildNodes.length; i++) {
                // Node.ATTRIBUTE_NODE === 2, and checking because IE11
                if (srcChildNodes[i].nodeType !== 2) {
                    clonedNode.appendChild(srcChildNodes[i].cloneNode(true));
                }
            }
        }
        return clonedNode;
    };
};

patchBrowser().then(options => {
  globalScripts();
  return bootstrapLazy([["sami-card",[[4,"sami-card"]]],["sami-card-code",[[4,"sami-card-code",{"box":[4],"preview":[4],"directionRight":[4,"direction-right"],"flexDirection":[1,"flex-direction"],"identify":[1],"author":[1],"authorEmail":[1,"author-email"],"backgroundColor":[1,"background-color"],"authorFontSize":[1,"author-font-size"],"contentTitleFontSize":[1,"content-title-font-size"],"contentTitle":[1,"content-title"],"contentSubtitleFontSize":[1,"content-subtitle-font-size"],"contentSubtitle":[1,"content-subtitle"],"titleImage":[1,"title-image"],"titleImageFontSize":[1,"title-image-font-size"],"subtitleImage":[1,"subtitle-image"],"subtitleImageFontSize":[1,"subtitle-image-font-size"],"backgroundImage":[1,"background-image"],"paddingRight":[1,"padding-right"],"paddingLeft":[1,"padding-left"],"marginTop":[1,"margin-top"],"type":[1],"width":[32]},[[9,"resize","handleScroll"]]]]],["sami-card-image",[[4,"sami-card-image",{"url":[1]}]]],["sami-dropdown",[[0,"sami-dropdown",{"border":[4],"text":[1],"width":[1],"right":[1],"data":[1025]}]]],["sami-footer",[[4,"sami-footer"]]],["sami-form",[[4,"sami-form"]]],["sami-grid",[[4,"sami-grid",{"align":[1],"type":[1],"padding":[1]}]]],["sami-header",[[4,"sami-header",{"position":[1],"desktop":[4],"display":[1],"align":[1]}]]],["sami-header-mobile",[[4,"sami-header-mobile",{"position":[1]}]]],["sami-hyperlink",[[4,"sami-hyperlink",{"color":[1],"position":[1],"borderStyle":[1,"border-style"],"borderWidth":[1,"border-width"],"decoration":[1],"background":[1],"hyperlinkType":[1,"hyperlink-type"],"onlyDesktop":[4,"only-desktop"]}]]],["sami-icon",[[0,"sami-icon"]]],["sami-img",[[0,"sami-img",{"src":[1],"alt":[1],"height":[1],"width":[1],"opacity":[1],"filterInvert":[1,"filter-invert"],"objectFit":[1,"object-fit"],"display":[1],"margin":[1]}]]],["sami-input",[[0,"sami-input",{"align":[1],"type":[1],"inputType":[1,"input-type"]}]]],["sami-list",[[4,"sami-list",{"type":[1],"listStyle":[1,"list-style"],"opacity":[1],"filterInvert":[1,"filter-invert"]}]]],["sami-list-item",[[4,"sami-list-item",{"type":[1]}]]],["sami-loader",[[4,"sami-loader",{"type":[1],"isOpen":[32],"show":[64],"close":[64]}]]],["sami-main",[[4,"sami-main",{"width":[1],"wordBreak":[1,"word-break"]}]]],["sami-paragraph",[[4,"sami-paragraph",{"type":[1],"decoration":[1],"color":[1],"align":[1],"transform":[1],"weigth":[1],"heading":[1]}]]],["sami-sidebar",[[4,"sami-sidebar",{"innerWidth":[32],"isMenuOpen":[32],"isMobile":[32],"show":[64]},[[9,"resize","handleScroll"]]]]],["sami-sidebar-dashboard",[[4,"sami-sidebar-dashboard",{"innerWidth":[32],"isMenuOpen":[32],"isMobile":[32],"show":[64]},[[9,"resize","handleScroll"]]]]],["sami-slider",[[4,"sami-slider",{"showStatus":[4,"show-status"],"currentSlideNumber":[32]}]]],["sami-span",[[4,"sami-span"]]],["sami-subscriber",[[4,"sami-subscriber",{"toggle":[16],"isOpen":[32],"show":[64]}]]],["sami-tab",[[4,"sami-tab",{"selectedIndex":[32]}]]],["sami-tag",[[4,"sami-tag",{"position":[1],"align":[1],"color":[1],"background":[1],"top":[1],"bottom":[1],"width":[1],"height":[1],"zIndex":[1,"z-index"],"lineHeight":[1,"line-height"],"fontWeight":[1,"font-weight"],"right":[1],"left":[1],"borderRadius":[1,"border-radius"],"transition":[1]}]]]], options);
});
