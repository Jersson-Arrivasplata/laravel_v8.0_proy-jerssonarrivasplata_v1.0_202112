import { r as registerInstance, h, g as getElement } from './index-a31fb924.js';

const tabCss = ":root{--sami-screen:rgb(6, 82, 221);--sami-screen-100:rgb(6, 82, 221);--sami-border:rgb(6, 82, 221);--sami-border-900:rgb(6, 82, 221);--sami-primary:rgb(13, 110, 253);--sami-secondary:rgb(102, 16, 242);--sami-success:rgb(32, 201, 151);--sami-danger:rgb(220, 53, 69);--sami-warning:rgb(255, 193, 7);--sami-info:rgb(13, 202, 240);--sami-light:rgb(173, 181, 189);--sami-dark:rgb(0, 0, 0);--sami-dark-200:rgba(0, 0, 0, 0.2);--sami-dark-500:rgba(0, 0, 0, 0.5);--sami-white:rgb(255, 255, 255);--sami-white-900:rgba(255, 255, 255, 0.9);--sami-white-800:rgba(255, 255, 255, 0.8);--sami-white-500:rgba(255, 255, 255, 0.5);--sami-spacing-s:8px;--sami-spacing-m:16px;--sami-spacing-l:24px;--sami-spacing-xl:32px;--sami-spacing-xxl:64px;--sami-width-container:1200px;--sami-muted:rgb(108, 117, 125)}.text-end{text-align:end}.mb-1{margin-bottom:1rem}sami-tab .sami-tab{overflow:hidden;}sami-tab .sami-tab ul.sami-tab___header{display:flex;flex-direction:row;margin:0;padding:0}sami-tab .sami-tab ul.sami-tab___header li{list-style:none}sami-tab .sami-tab ul.sami-tab___header li a{background-color:inherit;float:left;border:none;outline:none;cursor:pointer;padding:14px 16px;transition:0.3s;font-size:17px;text-decoration:none;color:black}sami-tab .sami-tab ul.sami-tab___header li a:hover{background-color:#ddd}sami-tab .sami-tab ul.sami-tab___header .sami-tab___active a{background-color:#ddd}sami-tab .sami-tab .sami-tab___body :not(.sami-tab___active),sami-tab .sami-tab .sami-tab___body :not(.sami-tab___active) *{display:none}sami-tab .sami-tab .sami-tab___body .sami-tab___active,sami-tab .sami-tab .sami-tab___body .sami-tab___active *{display:block}";

const Tab = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.selectedIndex = 0;
    this.class = [];
  }
  componentWillLoad() {
    const className = this.host.className;
    this.class = (className).split(' ');
    this.host.className = '';
    this.children = Array.from(this.host.children);
    (this.children).forEach(x => {
      const part = Array.from(x['part']);
      if (part && part[0] == 'header') {
        x.slot = 'header';
        this.header = Array.from(x.children);
        this.updateHeader();
      }
      if (part && part[0] == 'body') {
        x.slot = 'body';
        this.body = Array.from(x.children);
        this.updateBody();
      }
      if (part && part[0] == 'search')
        x.slot = 'search';
      if (x.slot == 'header') {
        x.classList.add('sami-tab___header');
      }
      else if (x.slot == 'body') {
        x.classList.add('sami-tab___body');
      }
      else if (x.slot == 'search') {
        x.classList.add('sami-tab___search');
      }
    });
  }
  updateHeader() {
    (this.header).forEach((x, pos) => {
      if (pos === 0) {
        x.classList.add('sami-tab___active');
      }
      x.addEventListener('click', () => this.show(pos));
    });
  }
  updateBody() {
    (this.body).forEach((x, pos) => {
      if (pos === 0) {
        x.classList.add('sami-tab___active');
      }
    });
  }
  getClass() {
    return this.class.join(' ');
  }
  show(position) {
    (this.header).forEach((x, pos) => {
      x.firstElementChild.classList.remove('sami-tab___active');
      if (pos === position) {
        x.firstElementChild.classList.add('sami-tab___active');
      }
    });
    (this.body).forEach((x, pos) => {
      x.classList.remove('sami-tab___active');
      if (pos === position) {
        x.classList.add('sami-tab___active');
      }
    });
  }
  render() {
    return (h("div", { class: `sami-tab ${this.getClass()}` }, h("div", null, h("slot", { name: `header` })), h("div", null, h("slot", { name: `search` })), h("div", null, h("slot", { name: `body` }))));
  }
  get host() { return getElement(this); }
};
Tab.style = tabCss;

export { Tab as sami_tab };
