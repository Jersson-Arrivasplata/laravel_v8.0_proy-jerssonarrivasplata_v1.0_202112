import { r as registerInstance, h, g as getElement } from './index-a31fb924.js';
import { j as justifyItems } from './class.function-ae1e7885.js';

const gridCss = ":root{--sami-screen:rgb(6, 82, 221);--sami-screen-100:rgb(6, 82, 221);--sami-border:rgb(6, 82, 221);--sami-border-900:rgb(6, 82, 221);--sami-primary:rgb(13, 110, 253);--sami-secondary:rgb(102, 16, 242);--sami-success:rgb(32, 201, 151);--sami-danger:rgb(220, 53, 69);--sami-warning:rgb(255, 193, 7);--sami-info:rgb(13, 202, 240);--sami-light:rgb(173, 181, 189);--sami-dark:rgb(0, 0, 0);--sami-dark-200:rgba(0, 0, 0, 0.2);--sami-dark-500:rgba(0, 0, 0, 0.5);--sami-white:rgb(255, 255, 255);--sami-white-900:rgba(255, 255, 255, 0.9);--sami-white-800:rgba(255, 255, 255, 0.8);--sami-white-500:rgba(255, 255, 255, 0.5);--sami-spacing-s:8px;--sami-spacing-m:16px;--sami-spacing-l:24px;--sami-spacing-xl:32px;--sami-spacing-xxl:64px;--sami-width-container:1200px;--sami-muted:rgb(108, 117, 125)}.text-end{text-align:end}.mb-1{margin-bottom:1rem}sami-grid{display:contents;}sami-grid div.sami-grid{background:transparent;display:grid}sami-grid div.sami-grid.sami-grid___card{grid-column-gap:var(--sami-spacing-l);grid-row-gap:var(--sami-spacing-l);grid-template-columns:repeat(4, 1fr)}@media (max-width: 1490px){sami-grid div.sami-grid.sami-grid___card{grid-template-columns:repeat(3, 1fr)}}@media (max-width: 1330px){sami-grid div.sami-grid.sami-grid___card{grid-template-columns:repeat(2, 1fr)}}@media (max-width: 950px){sami-grid div.sami-grid.sami-grid___card{grid-template-columns:repeat(1, 1fr)}}sami-grid div.sami-grid.sami-grid___slider{grid-template-columns:repeat(2, 1fr)}@media (max-width: 1390px){sami-grid div.sami-grid.sami-grid___slider{grid-template-columns:repeat(1, 1fr)}}";

const Grid = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.align = 'center';
    this.type = 'card';
    this.class = [];
  }
  componentWillLoad() {
    const className = this.host.className;
    this.class = (className).split(' ');
    this.host.className = '';
  }
  getStyles() {
    const styles = Object.assign({});
    (this.padding) ? styles.padding = this.padding : delete styles.padding;
    return styles;
  }
  getClass() {
    if (this.type === "card") {
      this.class.push('sami-grid___card');
    }
    if (this.type === "slider") {
      this.class.push('sami-grid___slider');
    }
    this.class.push(justifyItems(this.align));
    return this.class.join(' ');
  }
  render() {
    return (h("div", { class: `sami-grid ${this.getClass()}`, style: this.getStyles() }, h("slot", null)));
  }
  get host() { return getElement(this); }
};
Grid.style = gridCss;

export { Grid as sami_grid };
