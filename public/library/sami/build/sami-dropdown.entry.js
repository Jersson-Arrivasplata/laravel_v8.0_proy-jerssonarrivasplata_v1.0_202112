import { r as registerInstance, h } from './index-a31fb924.js';

const dropdownCss = ":root{--sami-screen:rgb(6, 82, 221);--sami-screen-100:rgb(6, 82, 221);--sami-border:rgb(6, 82, 221);--sami-border-900:rgb(6, 82, 221);--sami-primary:rgb(13, 110, 253);--sami-secondary:rgb(102, 16, 242);--sami-success:rgb(32, 201, 151);--sami-danger:rgb(220, 53, 69);--sami-warning:rgb(255, 193, 7);--sami-info:rgb(13, 202, 240);--sami-light:rgb(173, 181, 189);--sami-dark:rgb(0, 0, 0);--sami-dark-200:rgba(0, 0, 0, 0.2);--sami-dark-500:rgba(0, 0, 0, 0.5);--sami-white:rgb(255, 255, 255);--sami-white-900:rgba(255, 255, 255, 0.9);--sami-white-800:rgba(255, 255, 255, 0.8);--sami-white-500:rgba(255, 255, 255, 0.5);--sami-spacing-s:8px;--sami-spacing-m:16px;--sami-spacing-l:24px;--sami-spacing-xl:32px;--sami-spacing-xxl:64px;--sami-width-container:1200px;--sami-muted:rgb(108, 117, 125)}.text-end{text-align:end}.mb-1{margin-bottom:1rem}sami-dropdown div.sami-dropdown{display:inline-block}sami-dropdown div.sami-dropdown div.sami-dropdown___border{display:block;border-top-style:solid;border-top-width:2px;border-color:rgb(var(--sami-border))}sami-dropdown div.sami-dropdown div.sami-dropdown___content{display:none;position:absolute;background-color:rgba(var(--sami-white), 3%);min-width:160px;box-shadow:0px 8px 16px 0px rgba(var(--sami-dark), 0.2);z-index:1}sami-dropdown div.sami-dropdown div.sami-dropdown___content a{color:rgb(var(--sami-dark));padding:12px 16px;text-decoration:none;display:block;text-align:left;height:45px;line-height:2.5}sami-dropdown div.sami-dropdown div.sami-dropdown___content a:hover{background-color:rgba(var(--sami-white), 5%);}sami-dropdown div.sami-dropdown div.sami-dropdown___content:hover{display:block}sami-dropdown div.sami-dropdown:hover{}sami-dropdown div.sami-dropdown:hover div.sami-dropdown___content{display:block}sami-dropdown div.sami-dropdown:hover div.sami-dropdown___border{border-color:rgb(var(--sami-border))}";

const Dropdown = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.border = false;
    this.text = "";
    this.width = "";
    this.right = "";
  }
  parseListGroupData(newValue) {
    if (newValue && Array.isArray(newValue)) {
      this.data = newValue;
    }
    else if (newValue && typeof newValue === "string") {
      this.data = JSON.parse(newValue);
    }
  }
  getListGroup() {
    if (Array.isArray(this.data)) {
      return this.data;
    }
    return JSON.parse(this.data);
  }
  getContentStyles() {
    const styles = Object.assign({});
    (this.right) ? styles.right = this.right : delete styles.right;
    return styles;
  }
  /*private getAttrHyperLink() {
    const styles = Object.assign({});
    (this.width) ? styles.width = this.width : delete styles.width;

    return styles;
  }*/
  render() {
    //{this.cardTag ? <sami-card-tag text={this.text}></sami-card-tag>: (this.cardTag as HTMLElement)}
    // display="grid" 
    //width={this.width} 
    // <sami-hyperlink class="sami-dropdown___header" text={this.text} url="javascript:void(0)" target="_self"></sami-hyperlink>
    return (h("div", { class: 'sami-dropdown' }, this.border ? h("div", { class: 'sami-dropdown___border' }) : [], (this.data) ? (h("div", { class: "sami-dropdown___content", style: this.getContentStyles() }, h("sami-list-group", { data: this.getListGroup() }))) : []));
  }
  static get watchers() { return {
    "data": ["parseListGroupData"]
  }; }
};
Dropdown.style = dropdownCss;

export { Dropdown as sami_dropdown };
