import { r as registerInstance, h, g as getElement } from './index-a31fb924.js';
import { w as wordBreak } from './class.function-ae1e7885.js';

const mainCss = ":root{--sami-screen:rgb(6, 82, 221);--sami-screen-100:rgb(6, 82, 221);--sami-border:rgb(6, 82, 221);--sami-border-900:rgb(6, 82, 221);--sami-primary:rgb(13, 110, 253);--sami-secondary:rgb(102, 16, 242);--sami-success:rgb(32, 201, 151);--sami-danger:rgb(220, 53, 69);--sami-warning:rgb(255, 193, 7);--sami-info:rgb(13, 202, 240);--sami-light:rgb(173, 181, 189);--sami-dark:rgb(0, 0, 0);--sami-dark-200:rgba(0, 0, 0, 0.2);--sami-dark-500:rgba(0, 0, 0, 0.5);--sami-white:rgb(255, 255, 255);--sami-white-900:rgba(255, 255, 255, 0.9);--sami-white-800:rgba(255, 255, 255, 0.8);--sami-white-500:rgba(255, 255, 255, 0.5);--sami-spacing-s:8px;--sami-spacing-m:16px;--sami-spacing-l:24px;--sami-spacing-xl:32px;--sami-spacing-xxl:64px;--sami-width-container:1200px;--sami-muted:rgb(108, 117, 125)}.text-end{text-align:end}.mb-1{margin-bottom:1rem}sami-main div.sami-main{height:100%;background:var(--sami-screen);margin-top:0;width:calc(100% - 250px);float:right;margin-left:0px}@media (max-width: 769px){sami-main div.sami-main{width:100% !important;}}";

const Main = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.class = [];
  }
  componentWillLoad() {
    const className = this.host.className;
    this.class = (className).split(' ');
    this.host.className = '';
  }
  getStyles() {
    const styles = Object.assign({});
    (this.width) ? styles.width = this.width : delete styles.width;
    return styles;
  }
  getClass() {
    this.class.push(wordBreak(this.wordBreak));
    return this.class.join(' ');
  }
  render() {
    //    <slot></slot>
    return (h("div", { class: `sami-main ${this.getClass()}`, style: this.getStyles() }, h("slot", { name: "main" })));
  }
  get host() { return getElement(this); }
};
Main.style = mainCss;

export { Main as sami_main };
