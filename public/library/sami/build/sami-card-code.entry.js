import { r as registerInstance, h, g as getElement } from './index-a31fb924.js';
import { b as build } from './index-34c8c83d.js';

const cardCodeCss = ":root{--sami-screen:rgb(6, 82, 221);--sami-screen-100:rgb(6, 82, 221);--sami-border:rgb(6, 82, 221);--sami-border-900:rgb(6, 82, 221);--sami-primary:rgb(13, 110, 253);--sami-secondary:rgb(102, 16, 242);--sami-success:rgb(32, 201, 151);--sami-danger:rgb(220, 53, 69);--sami-warning:rgb(255, 193, 7);--sami-info:rgb(13, 202, 240);--sami-light:rgb(173, 181, 189);--sami-dark:rgb(0, 0, 0);--sami-dark-200:rgba(0, 0, 0, 0.2);--sami-dark-500:rgba(0, 0, 0, 0.5);--sami-white:rgb(255, 255, 255);--sami-white-900:rgba(255, 255, 255, 0.9);--sami-white-800:rgba(255, 255, 255, 0.8);--sami-white-500:rgba(255, 255, 255, 0.5);--sami-spacing-s:8px;--sami-spacing-m:16px;--sami-spacing-l:24px;--sami-spacing-xl:32px;--sami-spacing-xxl:64px;--sami-width-container:1200px;--sami-muted:rgb(108, 117, 125)}.text-end{text-align:end}.mb-1{margin-bottom:1rem}sami-card-code div.sami-card-code.active div.sami-card-code___section-one{width:50px;height:50px;bottom:0px;display:flex;justify-content:center;align-items:center}sami-card-code div.sami-card-code.active div.sami-card-code___section-one div.sami-card-code___header{display:none}sami-card-code div.sami-card-code.active div.sami-card-code___section-one div.sami-card-code___img img{width:50px}sami-card-code div.sami-card-code.active div.sami-card-code___section-one div.sami-card-code___img img.javascript{}sami-card-code div.sami-card-code.active div.sami-card-code___section-two{padding-left:0px}@media (max-width: 550px){sami-card-code div.sami-card-code:not(.active),sami-card-code div.sami-card-code.active{margin:auto}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-two p.sami-card-code___title,sami-card-code div.sami-card-code.active div.sami-card-code___section-two p.sami-card-code___title{font-size:14px}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-two p.sami-card-code___subtitle,sami-card-code div.sami-card-code.active div.sami-card-code___section-two p.sami-card-code___subtitle{font-size:12px}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-two div.sami-card-code___content pre code,sami-card-code div.sami-card-code.active div.sami-card-code___section-two div.sami-card-code___content pre code{font-size:13px !important}}@media (max-width: 385px){sami-card-code div.sami-card-code:not(.active),sami-card-code div.sami-card-code.active{}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-two p.sami-card-code___title,sami-card-code div.sami-card-code.active div.sami-card-code___section-two p.sami-card-code___title{font-size:13px}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-two p.sami-card-code___subtitle,sami-card-code div.sami-card-code.active div.sami-card-code___section-two p.sami-card-code___subtitle{font-size:11px}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-two div.sami-card-code___content pre code,sami-card-code div.sami-card-code.active div.sami-card-code___section-two div.sami-card-code___content pre code{font-size:12px !important}}@media (max-width: 550px){sami-card-code div.sami-card-code:not(.active){}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-one{width:55px;flex-direction:column-reverse}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-one div.sami-card-code___header{display:none}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-one div.sami-card-code___img img{width:55px}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-two{padding-left:60px}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-two div.sami-card-code___content{padding-left:5px !important}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-two div.sami-card-code___content pre code{overflow:hidden}}@media (max-width: 385px){sami-card-code div.sami-card-code:not(.active){}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-one{width:50px}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-one div.sami-card-code___img img{width:50px}sami-card-code div.sami-card-code:not(.active) div.sami-card-code___section-two{padding-left:55px}}@media (max-width: 550px){sami-card-code div.sami-card-code.active{}sami-card-code div.sami-card-code.active div.sami-card-code___section-two{padding-left:0px}sami-card-code div.sami-card-code.active div.sami-card-code___section-two div.sami-card-code___content{padding-left:5px !important}sami-card-code div.sami-card-code.active div.sami-card-code___section-two div.sami-card-code___content pre code{overflow:hidden}}@media (max-width: 385px){sami-card-code div.sami-card-code.active{}}sami-card-code div.sami-card-code{height:320px;width:516px;position:relative;border-style:solid;border-width:2px;background:var(--sami-white)}sami-card-code div.sami-card-code div.sami-card-code___section-one{position:absolute;left:0;display:flex;flex-direction:column;justify-content:space-between;width:125px;height:100%}sami-card-code div.sami-card-code div.sami-card-code___section-one div.sami-card-code___header p.sami-card-code___title,sami-card-code div.sami-card-code div.sami-card-code___section-one div.sami-card-code___header p.sami-card-code___subtitle{text-align:left;margin:0;padding-left:0.4rem}sami-card-code div.sami-card-code div.sami-card-code___section-one div.sami-card-code___header p.sami-card-code___title{padding-top:0.4rem;padding-bottom:0.4rem;font-size:14px;color:var(--sami-white-800);text-transform:uppercase}sami-card-code div.sami-card-code div.sami-card-code___section-one div.sami-card-code___header p.sami-card-code___subtitle{font-size:13px;color:var(--sami-white-900)}sami-card-code div.sami-card-code div.sami-card-code___section-one div.sami-card-code___img{display:flex;justify-content:end}sami-card-code div.sami-card-code div.sami-card-code___section-one div.sami-card-code___img img{width:80px}sami-card-code div.sami-card-code div.sami-card-code___section-one div.sami-card-code___img img.javascript{}sami-card-code div.sami-card-code div.sami-card-code___section-two{padding-left:125px}sami-card-code div.sami-card-code div.sami-card-code___section-two p.sami-card-code___title,sami-card-code div.sami-card-code div.sami-card-code___section-two p.sami-card-code___subtitle{text-align:center;margin:0 auto;width:95%}sami-card-code div.sami-card-code div.sami-card-code___section-two p.sami-card-code___title{font-size:18px;padding-top:0.6rem;padding-bottom:0.2rem}sami-card-code div.sami-card-code div.sami-card-code___section-two p.sami-card-code___subtitle{font-size:14px;color:var(--sami-muted)}sami-card-code div.sami-card-code div.sami-card-code___section-two p.sami-card-code___author{position:absolute;bottom:10px;font-size:10px;right:10px;margin:0}sami-card-code div.sami-card-code div.sami-card-code___section-two div.sami-card-code___preview-html{position:absolute;width:100%}sami-card-code div.sami-card-code div.sami-card-code___section-two pre{line-height:1.2}sami-card-code div.sami-card-code div.sami-card-code___section-two pre code{background:transparent;text-align:left;padding:0}";

const CardCode = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.box = false;
    this.preview = false;
    this.directionRight = false;
    this.identify = "";
    this.author = "";
    this.authorEmail = "";
    this.backgroundColor = "#ffffff";
    this.authorFontSize = "";
    this.contentTitleFontSize = "";
    this.contentTitle = "";
    this.contentSubtitleFontSize = "";
    this.contentSubtitle = "";
    this.titleImage = "";
    this.titleImageFontSize = "";
    this.subtitleImage = "";
    this.subtitleImageFontSize = "";
    this.backgroundImage = "";
    this.type = "";
    this.width = "";
    this.class = [];
  }
  handleScroll(e) {
    //const array = Array.from(e.srcElement['path']);
    const target = e.target;
    this.validate(target); //target
  }
  componentWillLoad() {
    const className = this.host.className;
    this.class = (className).split(' ');
    this.host.className = '';
  }
  componentDidLoad() {
    this.validate(window); //window
  }
  validate(target) {
    let measures = {
      'width': (new build.Devices().isMobile()) ? target['screen']['width'] : target['innerWidth'],
    };
    if (measures.width <= 550) {
      this.host.parentElement.parentElement.parentElement.style.maxWidth = (measures.width - 20) + 'px';
      this.width = (measures.width - 20) + 'px';
    }
    else {
      this.width = '';
    }
  }
  getStyles() {
    const styles = Object.assign({});
    (this.width) ? styles.width = this.width : delete styles.width;
    (this.flexDirection) ? styles.flexDirection = `url(${this.flexDirection})` : delete styles.flexDirection;
    (this.backgroundColor) ? styles.borderColor = `${this.backgroundColor}` : delete styles.borderColor;
    return styles;
  }
  getAuthorStyles() {
    const styles = Object.assign({});
    (this.authorFontSize) ? styles.authorFontSize = `url(${this.authorFontSize})` : delete styles.authorFontSize;
    return styles;
  }
  getSectionTwoStyles() {
    const styles = Object.assign({});
    (this.backgroundColor) ? styles.borderColor = `url(${this.backgroundColor})` : delete styles.borderColor;
    return styles;
  }
  getSectionOneStyles() {
    const styles = Object.assign({});
    (this.backgroundColor) ? styles.backgroundColor = `${this.backgroundColor}` : delete styles.backgroundColor;
    return styles;
  }
  getContentStyles() {
    const styles = Object.assign({});
    (this.paddingRight) ? styles.paddingRight = `${this.paddingRight}` : delete styles.paddingRight;
    (this.paddingLeft) ? styles.paddingLeft = `${this.paddingLeft}` : delete styles.paddingLeft;
    (this.marginTop) ? styles.marginTop = `${this.marginTop}` : delete styles.marginTop;
    return styles;
  }
  getClass() {
    return this.class.join(' ');
  }
  render() {
    //{{ 'sami-card-code': true, 'active': this.box }} 
    //{this.cardTag ? <sami-card-tag text={this.text}></sami-card-tag>: (this.cardTag as HTMLElement)}
    this.validate(window);
    return (h("div", { class: `sami-card-code ${(this.box) ? 'active' : ''} ${this.getClass()}`, style: this.getStyles(), id: `sami-card-code___` + this.identify }, h("div", { class: { 'sami-card-code___section-one': true }, style: this.getSectionOneStyles() }, h("div", { class: "sami-card-code___header " }, h("p", { class: { 'sami-card-code___title': true }, style: { 'fontSize': this.titleImageFontSize } }, this.titleImage), h("p", { class: { 'sami-card-code___subtitle': true }, style: { 'fontSize': this.subtitleImageFontSize } }, this.subtitleImage)), h("div", { class: "sami-card-code___img" }, h("img", { class: this.type, src: this.backgroundImage }))), h("div", { class: { 'sami-card-code___section-two': true, 'right': this.directionRight }, style: this.getSectionTwoStyles() }, h("slot", { name: "counter" }), h("p", { class: "sami-card-code___title ", style: { 'fontSize': this.contentTitleFontSize } }, this.contentTitle), h("p", { class: "sami-card-code___subtitle", style: { 'fontSize': this.contentSubtitleFontSize } }, this.contentSubtitle), h("div", { class: "sami-card-code___content", style: !this.preview ? this.getContentStyles() : '' }, this.preview ? h("div", { class: 'sami-card-code___preview-html' }, h("slot", { name: "view-html" })) : h("slot", { name: "view-html" })), h("p", { class: 'sami-card-code___author', style: this.getAuthorStyles() }, h("span", null, this.authorEmail)))));
  }
  get host() { return getElement(this); }
};
CardCode.style = cardCodeCss;

export { CardCode as sami_card_code };
