export interface IUser {
    name: string;
    lastName: string;
    birthDate: Date;
    age: void;
}
