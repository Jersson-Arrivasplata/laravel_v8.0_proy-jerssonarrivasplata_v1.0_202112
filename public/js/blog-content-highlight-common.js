$(document).ready(function () {
    $("#download-canvas").on("click", function () {
        let canvas = document.getElementById("canvas");
        let ctx = canvas.getContext("2d");

        var a = document.createElement("a");
        a.download = "filename.png";
        a.href = canvas.toDataURL();
        a.click();
        document.body.removeChild(a);
    });

    $("#form-title").on("input", function () {
        const data = $(this).val();
        $(".card-publish .title").text(data);
    });

    $("#form-subtitle").on("input", function () {
        const data = $(this).val();
        $(".card-publish .subtitle").text(data);
    });

    $("#form-position").on("input", function () {
        const data = $(this).val();
        $(".card-publish div.position span").text(data);
    });

    $("#form-height").on("input", function () {
        const data = $(this).val();
        $(".card-publish").css("height", data + "px");
        $(".card-publish pre code").css("height", (data - 60) + "px");
    });
});

function capture() {
    let contentImg = document.getElementById("content-images");
    let element = document.querySelector(".card-publish");
    $(element).find("span.badge").hide();
    html2canvas(element, {
        scale: 3,
        useCORS: true // This tells html2canvas to attempt to load images with CORS enabled
    }).then((canvas) => {
        canvas.id = "canvas";
        contentImg.innerHTML = "";
        contentImg.appendChild(canvas);

        // Move the downloading logic inside the then callback
        let a = document.createElement("a");
        a.download = "filename.png";
        a.href = canvas.toDataURL("image/png");
        document.body.appendChild(a); // Append the element to the body before clicking it
        a.click();
        document.body.removeChild(a); // Remove the element after clicking

        $(element).find("span.badge").show();
    });
}
