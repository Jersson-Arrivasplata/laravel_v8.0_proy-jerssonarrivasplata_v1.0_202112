document.addEventListener("DOMContentLoaded", () => {
    const langSelector = "(auto)";
    const themeSelector = "vs";
    const themeLink = document.createElement("link");
    themeLink.rel = "stylesheet";
    document.head.appendChild(themeLink); // Asumiendo que siempre se va a usar, lo agregamos desde el inicio

    function loadTheme(theme) {
        themeLink.href = `https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/styles/${theme}.min.css`;
    }

    document
        .querySelectorAll(".card-publish.after pre code")
        .forEach((block) => {
            const codeAttr = block.attributes.getNamedItem("attr-code");
            const codeContent = codeAttr
                ? codeAttr.nodeValue
                : "No code available.";
            block.textContent = codeContent;
            block.className = langSelector;
            hljs.highlightElement(block);
        });

    loadTheme(themeSelector);

    generateImage = (self) => {

        const canvasOnLoad = (element) => {
            $(element).find("span.badge").hide();


            html2canvas(element, {
                scale: 3,
                useCORS: true // This tells html2canvas to attempt to load images with CORS enabled
            }).then((canvas) => {
                canvas.id = "canvas";
                let contentImg = document.getElementById("content-images");
                contentImg.innerHTML = ""; // Clear previous content
                contentImg.appendChild(canvas); // Append the new canvas

                // Move the downloading logic inside the then callback
                let a = document.createElement("a");
                a.download = "filename.png";
                a.href = canvas.toDataURL("image/png");
                document.body.appendChild(a); // Append the element to the body before clicking it
                a.click();
                document.body.removeChild(a); // Remove the element after clicking
                $(element).find("span.badge").show();
            });
        };

        // Select the .card-publish element inside the currently active .swiper-slide
        let activeCardPublish = $(self)
            .closest(".slider-card-publish")
            .find(".swiper-slide-active .card-publish");
        canvasOnLoad($(activeCardPublish).get(0));
    };

    show = (self) => {
        let id = $(self)
            .closest(".slider-card-publish")
            .find(".swiper-slide-active .card-publish")
            .attr("id");
        redirect = base + "/blog-content-highlight/" + id;
        window.open(redirect, "_self");
    };

    remove = (self) => {
        let id = $(self)
            .closest(".slider-card-publish")
            .find(".swiper-slide-active .card-publish")
            .attr("id");
        if (confirm("¿Realmente desea eliminarlo?")) {
            $.ajax({
                url: base + "/blog-content-highlight" + "/" + id,
                type: "DELETE",
                // el tipo de información que se espera de respuesta
                dataType: "json",
                headers: {
                    "X-CSRF-TOKEN":
                        document.head.querySelector("[name=csrf-token]")
                            .content,
                },
                success: function (data) {
                    alert("eliminado");
                    location.reload();
                },

                error: function (xhr, status) {},
            });
        } else {
            return false;
        }
    };

    addNewBlank = (self) => {
        let id = $(self)
            .closest(".slider-card-publish")
            .find(".swiper-slide-active .card-publish")
            .attr("id");
        if (confirm("¿Realmente desea crear uno nuevo?")) {
            $.ajax({
                url: base + "/blog-content-highlight" + "/" + id + "/new-blank",
                type: "POST",
                dataType: "json",
                headers: {
                    "X-CSRF-TOKEN":
                        document.head.querySelector("[name=csrf-token]")
                            .content,
                },
                success: function (data) {
                    alert("agregado");
                    redirect = base + "/blog-content-highlight/" + data.id;
                    window.open(redirect, "_self");
                },

                error: function (xhr, status) {},
            });
        } else {
            return false;
        }
    };
});

$(document).ready(function () {
    var mySwiper = new Swiper(".mySwiper", {
        // Swiper options here
        loop: true,
        pagination: {
            el: ".swiper-pagination",
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
});
