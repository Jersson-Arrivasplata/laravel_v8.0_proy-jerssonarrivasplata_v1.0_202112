/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!***************************************!*\
  !*** ./resources/js/admin/message.js ***!
  \***************************************/
var init = {
  modalDescription: $('#descriptionModal'),
  modalDescriptionContent: ''
};

showModalDescription = function showModalDescription(id) {
  init.modalDescriptionContent = $('.admin-message-index__btn-show-modal-description_' + id).data('content');
  init.modalDescription.modal('show');
};

init.modalDescription.on('show.bs.modal', function (event) {
  init.modalDescription.find('.modal-body').append(init.modalDescriptionContent);
});
/******/ })()
;