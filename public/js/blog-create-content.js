

$('#download-canvas').on('click', function () {

    let canvas = document.getElementById('canvas');
    let ctx = canvas.getContext('2d');

    var a = document.createElement('a');
    a.download = 'filename.png';
    a.href = canvas.toDataURL()
    a.click();
    document.body.removeChild(a)
});
function copy(data) {
    var aux = document.createElement('input');
    aux.setAttribute('style', "display:none");
    aux.setAttribute('value', data);
    document.body.appendChild(aux);
    aux.select();
    document.execCommand('copy');
}

function capture() {
    let contentImg = document.getElementById('content-images');
    let element = document.querySelector("#contenido");
    $(element).find("span.badge").hide();
    html2canvas(element).then(canvas => {
        canvas.id = "canvas";
        contentImg.innerHTML = ""
        contentImg.appendChild(canvas);

        $(element).find("span.badge").show();
    });
}


var inputarea = document.getElementById("form-code");
//https://codepen.io/shotastage/pen/KaKwya
//https://codepen.io/shotastage/pen/KaKwya
var outputarea = document.getElementById("output");
//'&lt;h1&gt;hello>&lt;/h1&gt;'
outputCode(($('#form-code').val()).replace('</', '&lt;/').replace('>', '&gt;'))
function outputCode(data) {
    if ($('#form-preview-box').is(':checked')) {
        outputarea.innerHTML = data;
        refreshHighlighting();
    }
}
inputarea.addEventListener("input", function (event) {
    if ($('#form-preview-box').is(':checked')) {
        //  $('#output-iframe').contents().find('html').html(inputarea.value);
        //$('#form-preview-box').dblclick();
    } else {

        outputarea.innerHTML = (inputarea.value).replace(/</g, "&lt;").replace(/>/g, "&gt;");
        //inputarea.style.zIndex = 4;
        //
        refreshHighlighting();
    }


}, false);

inputarea.addEventListener("blur", function (event) {
    refreshHighlighting();
}, false)


function refreshHighlighting() {
    hljs.highlightBlock(outputarea);
    //setTimeout("refreshHighlighting()", 1000);
    //inputarea.style.zIndex = 0;
    //  inputarea.style.color = "transparent";
}

$(document).ready(function () {
    //$('#form-image-box').click();
});

$('#form-img-title').on('input', function () {
    let data = $('#form-img-title').val();
    document.getElementById('card-img-title').innerText = data;
});

$('#form-img-subtitle').on('input', function () {
    let data = $('#form-img-subtitle').val();
    document.getElementById('card-img-subtitle').innerText = data;
});

$('#form-background').on('change', function () {
    let bg = $('#form-background').val();
    var optionBackgroundColor = $('#form-background').find('option:selected').attr('attr-bg-color');
    $('#form-bg-color').val(optionBackgroundColor);

    var optionTitle = $('#form-background').find('option:selected').attr('attr-title');
    $('#form-img-title').val(optionTitle);

    var optionSubTitle = $('#form-background').find('option:selected').attr('attr-subtitle');
    $('#form-img-subtitle').val(optionSubTitle);

    var optionTypeHighlight = $('#form-background').find('option:selected').attr('attr-type-highlight');

    document.getElementById('card-img-title').innerText = optionTitle;
    document.getElementById('card-img-subtitle').innerText = optionSubTitle;
    document.querySelector('#pre-codes code').removeAttribute('class');
    document.querySelector('#pre-codes code').setAttribute('class', optionTypeHighlight);




    formBackgroundColor();
    let img = window.baseContentType + bg;
    document.getElementById('card-background').style.backgroundColor = optionBackgroundColor;
    //  $('#card-background').css('background', window.baseContentType + bg);
    document.getElementById('card-logo').src = img;




});
$('#form-bg-color').on('input', function () {
    formBackgroundColor();
});
function formBackgroundColor() {
    let data = $('#form-bg-color').val();

    document.querySelector('.card-grid .card-content').style.borderColor = data;

    document.querySelector('.card-grid .card').style.background = data;

    document.querySelector('.card-grid .card__background').style.background = data;
}


$('#form-content-title').on('input', function () {
    let data = $('#form-content-title').val();
    document.getElementById('card-content-title').innerText = data;
});

$('#form-content-subtitle').on('input', function () {
    let data = $('#form-content-subtitle').val();
    document.getElementById('card-content-subtitle').innerText = data;
});
$('#form-img-range').on('change', function () {
    let data = $('#form-img-range').val();
    $('#form-img-range-text').text(data);

    document.querySelector('.card-grid .card').style.width = data + 'px';
    document.getElementById('card-logo').style.maxWidth = data + 'px';

    getSocialMediaMeasures();

});

$('#form-image-box').on('change', function () {
    let data = $('#form-img-range').val();
    if ($('#form-image-box').is(':checked')) {
        document.querySelector('.card-grid .card').style.height = data + 'px';
        document.querySelector('.card-grid .card__content').style.display = 'none';
        document.querySelector('.card-grid .card').style.position = 'absolute';
        document.querySelector('.card-grid .card').style.bottom = '0';

        $('#form-img-range').val(40);
        $('#form-content-range-width').val(520);
        $('#form-content-range-height').val(320);
        $('#form-content-range-fontsize').val(14);
        $('#form-content-title-range-fontsize').val(18);
        $('#form-content-range-fontsize').val(14);
        $('#form-content-bottom-range-fontsize').val(10);

        document.getElementById('output').style.fontSize = 14 + 'px';

        document.querySelector('.card-grid .card').style.height = 40 + 'px';
        document.querySelector('.card-grid .card').style.width = 40 + 'px';
        document.getElementById('card-logo').style.maxWidth = 40 + 'px';
        document.querySelector('.card-grid .card-content').style.width = '520px';


        let iframeElement = $('#output-iframe')[0];
        iframeElement.src = "about:blank";

        // Set the iframe's new HTML
        iframeElement.contentWindow.document.open();
        iframeElement.contentWindow.document.write(inputarea.innerHTML);
        iframeElement.contentWindow.document.close();

    } else {
        document.querySelector('.card-grid .card').style.height = 'auto';
        document.querySelector('.card-grid .card__content').style.display = 'block';
        document.querySelector('.card-grid .card').style.position = 'relative';
        //document.querySelector('.card-grid .card').style.bottom = '0';

        document.querySelector('.card-grid .card').style.height = 'auto';
        document.querySelector('.card-grid .card').style.width = '125px';
        document.querySelector('.card-grid .card-content').style.width = '395px';
        document.getElementById('output').style.fontSize = 14 + 'px';
        document.querySelector('.card-grid .card-content').style.borderStyle = 'auto';
        document.querySelector('.card-grid .card-content').style.borderLeft = '0px';

        $('#form-img-range').val(125);
        $('#form-content-range-width').val(395);
        $('#form-content-range-height').val(320);
        $('#form-content-range-fontsize').val(14);
        $('#form-content-title-range-fontsize').val(18);
        $('#form-content-range-fontsize').val(22);
        $('#form-content-bottom-range-fontsize').val(10);
    }

    $('#form-content-border-left').click();
});

$('#form-content-range-width').on('change', function () {
    let data = $('#form-content-range-width').val();
    $('#form-content-range-width-text').text(data);

    document.querySelector('.card-grid .card-content').style.width = data + 'px';
    getSocialMediaMeasures();
});

function getSocialMediaMeasures() {
    document.getElementById('form-select-add-measures').innerText = parseInt($('#form-img-range').val()) + parseInt($('#form-content-range-width').val());
}

$('#form-content-range-height').on('change', function () {
    let data = $('#form-content-range-height').val();
    $('#form-content-range-height-text').text(data)

    document.querySelector('.card-grid .card-content').style.height = data + 'px';
    document.querySelector('.card-grid .card-content-code').style.height = (data - 80) + 'px';
});

$('#form-content-border-left').on('change', function () {
    let data = $('#form-bg-color').val();
    if ($('#form-content-border-left').is(':checked')) {
        document.querySelector('.card-grid .card-content').style.borderLeft = 'solid ' + data;
    } else {
        document.querySelector('.card-grid .card-content').style.borderLeft = 'none';
    }
});

$('#form-content-border-right').on('change', function () {
    let data = $('#form-bg-color').val();
    if ($('#form-content-border-right').is(':checked')) {
        document.querySelector('.card-grid .card-content').style.borderRight = 'solid ' + data;
    } else {
        document.querySelector('.card-grid .card-content').style.borderRight = 'none';
    }
});

$('#form-preview-box').on('change', function () {
    if ($('#form-preview-box').is(':checked')) {
        document.querySelector('#pre-codes').classList.remove('d-flex');
        document.querySelector('#pre-codes').classList.add('d-none');

        //document.querySelector('#form-code').style.display = 'none';
        document.querySelector('#output-iframe').style.display = 'block';

        $('#output-iframe').contents().find('html').html($(`<div>${$('#form-code').val()}</div>`).html());
        /*  var iframevar = document.getElementById('output-iframe');
          var elmnt = iframevar.contentWindow.document;
          elmnt.innerHTML = $(`<div>${$('#form-code').val()}</div>`).html();*/
    } else {
        document.querySelector('#pre-codes').classList.remove('d-none');
        document.querySelector('#pre-codes').classList.add('d-flex');

        //document.querySelector('#form-code').style.display = 'block';
        document.querySelector('#output-iframe').style.display = 'none';

    }
});

$('#form-content-direction-right').on('change', function () {
    if ($('#form-content-direction-right').is(':checked')) {
        document.querySelector('.card-grid .blog-content-name').style.textAlign = "left";
        document.querySelector('.card-grid .blog-content-name').style.left = "10px";
        document.querySelector('#contenido').style.flexDirection = "row-reverse";
    } else {
        document.querySelector('.card-grid .blog-content-name').style.textAlign = "right";
        document.querySelector('.card-grid .blog-content-name').style.right = "10px";
        document.querySelector('#contenido').style.flexDirection = "row";
    }

    $('#form-content-border-left').click();
    $('#form-content-border-right').click();
});

$('#form-select-socialmedia').on('change', function () {


    var socialMedia = $('#form-select-socialmedia').val()
    var socialMediaWidth = $('#form-select-socialmedia').find('option:selected').attr('attr-width');
    var socialMediaHeight = $('#form-select-socialmedia').find('option:selected').attr('attr-height');




    //form-select-socialmedia
    /*if ($('#form-content-direction-right').is(':checked')) {
        document.querySelector('#contenido').setAttribute('style', 'flex-direction: row-reverse; left: 10px;');
    } else {
        document.querySelector('#contenido').setAttribute('style', 'flex-direction: row;');
    }
    $('#form-content-border-left').click();
    $('#form-content-border-right').click();*/
});

//

$('#form-content-range-fontsize').on('change', function () {
    let data = $('#form-content-range-fontsize').val();
    document.querySelector('.card-grid .card-content-code #pre-codes code').style.fontSize = data + 'px';
    document.querySelector('#form-content-range-fontsize-text').textContent = data;

});

$('#form-content-title-range-fontsize').on('change', function () {
    let data = $('#form-content-title-range-fontsize').val();
    document.querySelector('.card-grid .card-title').style.fontSize = data + 'px';
    document.querySelector('.card-grid .card-subtitle').style.fontSize = (data - 4.5) + 'px';
    document.querySelector('#form-content-title-range-fontsize-text').textContent = data;

});
$('#form-image-range-fontsize').on('change', function () {
    let data = $('#form-image-range-fontsize').val();
    document.querySelector('.card-grid .card__heading').style.fontSize = (data - 1) + 'px';
    document.querySelector('.card-grid .card__category').style.fontSize = data + 'px';
    document.querySelector('#form-image-range-fontsize-text').textContent = data;
});

$('#form-content-bottom-range-fontsize').on('change', function () {
    let data = $('#form-content-bottom-range-fontsize').val();
    document.querySelector('.card-grid .blog-content-name').style.fontSize = data + 'px';
});

$('#form-content-margin-top').on('change', function () {
    let data = $('#form-content-margin-top').val();
    document.querySelector('.card-grid #pre-codes').style.marginTop = data + 'px';
});

let tableData = new Array();

$('#cleanTable').on('click', function () {
    tableData = [];
});

$('#addTable').on('click', function () {
    let imgTitle = $('#form-img-title').val();
    let imgSubTitle = $('#form-img-subtitle').val();
    let contentTitle = $('#form-content-title').val();
    let contentSubTitle = $('#form-content-subtitle').val();
    let selectSocialMedia = $('#form-select-socialmedia').val();
    let selectSocialMediaWidth = $('#form-select-socialmedia').find('option:selected').attr('attr-width');
    let selectSocialMediaHeight = $('#form-select-socialmedia').find('option:selected').attr('attr-height');
    let bgColor = $('#form-bg-color').val();
    let typeLanguage = $('#form-background').val();
    let typeLanguageBgColor = $('#form-background').find('option:selected').attr('attr-bg-color');
    let typeLanguageTitle = $('#form-background').find('option:selected').attr('attr-title');
    let typeLanguageSubTitle = $('#form-background').find('option:selected').attr('attr-subtitle');
    let id_blog_content_type = $('#form-background').find('option:selected').attr('attr-type');
    let checkImageBox = $('#form-image-box').is(':checked');
    //let checkContentBorderLeft = $('#form-content-border-left').is(':checked');
    //let checkContentBorderRight = $('#form-content-border-right').is(':checked');
    //let checkContentDirectionRight = $('#form-content-direction-right').is(':checked');
    let checkPreviewBox = $('#form-preview-box').is(':checked');
    let imgRange = $('#form-img-range').val();
    let contentWidthRange = $('#form-content-range-width').val();
    let contentHeightRange = $('#form-content-range-height').val();
    let imgFontSizeRange = $('#form-image-range-fontsize').val();
    let contentTitleFontSizeRange = $('#form-content-title-range-fontsize').val();
    let contentfontSizeRange = $('#form-content-range-fontsize').val();
    let contentBottomFontSizeRange = $('#form-content-bottom-range-fontsize').val();
    let contentMargintop = $('#form-content-margin-top').val();
    let codes = $('#form-code').val();
    let created_at = Date.now();
    let updated_at = Date.now();

    let idRow = makeid(5);
    let data = {
        imgTitle,
        imgSubTitle,
        contentTitle,
        contentSubTitle,
        selectSocialMedia,
        selectSocialMediaWidth,
        selectSocialMediaHeight,
        bgColor,
        typeLanguage,
        typeLanguageBgColor,
        typeLanguageTitle,
        typeLanguageSubTitle,
        id_blog_content_type,
        checkImageBox,
        //checkContentBorderLeft,
        //checkContentBorderRight,
        //checkContentDirectionRight,
        checkPreviewBox,
        imgRange,
        contentWidthRange,
        contentHeightRange,
        imgFontSizeRange,
        contentTitleFontSizeRange,
        contentfontSizeRange,
        contentBottomFontSizeRange,
        contentMargintop,
        codes,
        idRow
    };

    tableData.push(data);

    $('#table').append(`
        <tr id="${idRow}">
            <td>#</td>
            <td>${typeLanguage}</td>
            <td>${typeLanguageBgColor}</td>
            <td>${contentTitle}</td>
            <td>${contentSubTitle}</td>
            <td>${selectSocialMedia}</td>
            <td><button class="btn btn-sm btn-danger" onclick="removeRowFromTable(this,'${idRow}')">Eliminar</button></td>
        </tr>
    `);
});


$('#saveAllTable').on('click', function () {
    fetch($('#blog-content-type-all').val()).then(data => data.json())
        .then(data => {
            console.log(data);
            let inputOptions = [];
            for (var i = 0; i < data.length; i++) {
                inputOptions[data[i]['type']] = data[i]['name']
            }
            inputOptions.pop();

            return Swal.fire({
                title: 'Seleccione el tipo',
                input: 'select',
                inputOptions: inputOptions,
                inputPlaceholder: 'Seleccione un tipo',
                showCancelButton: true,
                inputValidator: function (value) {
                    return new Promise(function (resolve, reject) {
                        if (value !== '') {
                            resolve();
                        } else {
                            resolve('Debe escoger alguno');
                        }
                    });
                }
            });
        }).then(function (result) {
            if (result.isConfirmed) {
                var url = $("#url").val();
                tableData.forEach(x => x.id_blog_content_type_group = result.value);
                tableData.forEach(x => x.idRow = tableData[0].idRow);
                //tableData.forEach(x => x.checkPosition = 1);
                tableData.forEach((x, index) => x.position = (index + 1));



                //id_blog_content_type_group
                $.ajax({
                    // la URL para la petición
                    url: url,// + '/blog-content-generate'
                    data: JSON.stringify(tableData),
                    type: 'POST',
                    // el tipo de información que se espera de respuesta
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': document.head.querySelector("[name=csrf-token]").content,
                    },
                    success: function (data) {
                        //tableData = [];
                        Swal.fire({
                            icon: 'success',
                            html: 'Se realizo con exito'
                        });
                    },

                    error: function (xhr, status) {
                        Swal.fire({
                            icon: 'danger',
                            html: '¡Ocurrio un error!'
                        });
                    }
                });

            }
        });
});


function removeRowFromTable(self, id) {
    $('#' + id).remove();
}

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

//

$('#edit').on('click', function () {
    let id = $('#id').val();
    let imgTitle = $('#form-img-title').val() ? $('#form-img-title').val() : null;
    let imgSubTitle = $('#form-img-subtitle').val() ? $('#form-img-subtitle').val() : null;
    let contentTitle = $('#form-content-title').val() ? $('#form-content-title').val() : null;
    let contentSubTitle = $('#form-content-subtitle').val() ? $('#form-content-subtitle').val() : null;
    let selectSocialMedia = $('#form-select-socialmedia').val() ? $('#form-select-socialmedia').val() : null;
    let selectSocialMediaWidth = $('#form-select-socialmedia').find('option:selected').attr('attr-width') ? $('#form-select-socialmedia').find('option:selected').attr('attr-width') : null;
    let selectSocialMediaHeight = $('#form-select-socialmedia').find('option:selected').attr('attr-height') ? $('#form-select-socialmedia').find('option:selected').attr('attr-height') : null;
    let bgColor = $('#form-bg-color').val() ? $('#form-bg-color').val() : null;
    let typeLanguage = $('#form-background').val() ? $('#form-background').val() : null;
    let typeLanguageBgColor = $('#form-background').find('option:selected').attr('attr-bg-color') ? $('#form-background').find('option:selected').attr('attr-bg-color') : null;
    let typeLanguageTitle = $('#form-background').find('option:selected').attr('attr-title') ? $('#form-background').find('option:selected').attr('attr-title') : null;
    let typeLanguageSubTitle = $('#form-background').find('option:selected').attr('attr-subtitle') ? $('#form-background').find('option:selected').attr('attr-subtitle') : null;
    let id_blog_content_type = $('#form-background').find('option:selected').attr('attr-type') ? $('#form-background').find('option:selected').attr('attr-type') : null;
    let checkImageBox = $('#form-image-box').is(':checked') ? $('#form-image-box').is(':checked') : null;
    //let checkContentBorderLeft = $('#form-content-border-left').is(':checked');
    //let checkContentBorderRight = $('#form-content-border-right').is(':checked');
    //let checkContentDirectionRight = $('#form-content-direction-right').is(':checked');
    let checkPreviewBox = $('#form-preview-box').is(':checked') ? $('#form-preview-box').is(':checked') : null;
    let imgRange = $('#form-img-range').val() ? $('#form-img-range').val() : null;
    let contentWidthRange = $('#form-content-range-width').val() ? $('#form-content-range-width').val() : null;
    let contentHeightRange = $('#form-content-range-height').val() ? $('#form-content-range-height').val() : null;
    let imgFontSizeRange = $('#form-image-range-fontsize').val() ? $('#form-image-range-fontsize').val() : null;
    let contentTitleFontSizeRange = $('#form-content-title-range-fontsize').val() ? $('#form-content-title-range-fontsize').val() : null;
    let contentfontSizeRange = $('#form-content-range-fontsize').val() ? $('#form-content-range-fontsize').val() : null;
    let contentBottomFontSizeRange = $('#form-content-bottom-range-fontsize').val() ? $('#form-content-bottom-range-fontsize').val() : null;
    let contentMargintop = $('#form-content-margin-top').val() ? $('#form-content-margin-top').val() : null;
    let contentPaddingLeft = $('#form-content-padding-left').val() ? $('#form-content-padding-left').val() : null;
    let url_linkedin = $('#form-url-description').val() ?$('#form-url-description').val() : null;
    let description = $('#form-share-description').val() ? $('#form-share-description').val() : null;
    let position = $('#form-position').val() ? $('#form-position').val() : 1;
    let id_blog_content_type_group = $('#form-type-group').val() ? $('#form-type-group').val() : id_blog_content_type;

    let codes = $('#form-code').val() ? $('#form-code').val() : null;
    let data = {
        _method: 'PUT',
        _token: document.head.querySelector("[name=csrf-token]").content,
        id,
        imgTitle,
        imgSubTitle,
        contentTitle,
        contentSubTitle,
        selectSocialMedia,
        selectSocialMediaWidth,
        selectSocialMediaHeight,
        bgColor,
        typeLanguage,
        typeLanguageBgColor,
        typeLanguageTitle,
        typeLanguageSubTitle,
        id_blog_content_type_group,
        id_blog_content_type,
        checkImageBox,
        //checkContentBorderLeft,
        //checkContentBorderRight,
        //checkContentDirectionRight,
        checkPreviewBox,
        imgRange,
        contentWidthRange,
        contentHeightRange,
        imgFontSizeRange,
        contentTitleFontSizeRange,
        contentfontSizeRange,
        contentBottomFontSizeRange,
        contentMargintop,
        contentPaddingLeft,
        codes,
        url_linkedin,
        description,
        position
    };
    var url = $("#url").val();

    $.ajax({
        // la URL para la petición
        url: url,// + '/blog-content-generate'
        data: JSON.stringify(data),
        type: 'PUT',
        // el tipo de información que se espera de respuesta
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': document.head.querySelector("[name=csrf-token]").content,
        },
        success: function (data) {
            alert("editado");
            console.log(data)
        },

        error: function (xhr, status) {

        }
    });

    //https://codeanddeploy.com/blog/laravel/laravel-8-ajax-post-request-example
});

$('#save').on('click', function () {
    let imgTitle = $('#form-img-title').val() ? $('#form-img-title').val() : null;
    let imgSubTitle = $('#form-img-subtitle').val() ? $('#form-img-subtitle').val() : null;
    let contentTitle = $('#form-content-title').val() ? $('#form-content-title').val() : null;
    let contentSubTitle = $('#form-content-subtitle').val() ? $('#form-content-subtitle').val() : null;
    let selectSocialMedia = $('#form-select-socialmedia').val() ? $('#form-select-socialmedia').val() : null;
    let selectSocialMediaWidth = $('#form-select-socialmedia').find('option:selected').attr('attr-width') ? $('#form-select-socialmedia').find('option:selected').attr('attr-width') : null;
    let selectSocialMediaHeight = $('#form-select-socialmedia').find('option:selected').attr('attr-height') ? $('#form-select-socialmedia').find('option:selected').attr('attr-height') : null;
    let bgColor = $('#form-bg-color').val() ? $('#form-bg-color').val() : null;
    let typeLanguage = $('#form-background').val() ? $('#form-background').val() : null;
    let typeLanguageBgColor = $('#form-background').find('option:selected').attr('attr-bg-color') ? $('#form-background').find('option:selected').attr('attr-bg-color') : null;
    let typeLanguageTitle = $('#form-background').find('option:selected').attr('attr-title') ? $('#form-background').find('option:selected').attr('attr-title') : null;
    let typeLanguageSubTitle = $('#form-background').find('option:selected').attr('attr-subtitle') ? $('#form-background').find('option:selected').attr('attr-subtitle') : null;
    let id_blog_content_type = $('#form-background').find('option:selected').attr('attr-type') ? $('#form-background').find('option:selected').attr('attr-type') : null;
    let id_blog_content_type_group = $('#form-type-group').val() ? $('#form-type-group').val() : id_blog_content_type;
    let checkImageBox = $('#form-image-box').is(':checked') ? $('#form-image-box').is(':checked') : null;
    //let checkContentBorderLeft = $('#form-content-border-left').is(':checked');
    //let checkContentBorderRight = $('#form-content-border-right').is(':checked');
    //let checkContentDirectionRight = $('#form-content-direction-right').is(':checked');
    let checkPreviewBox = $('#form-preview-box').is(':checked') ? $('#form-preview-box').is(':checked') : null;
    let imgRange = $('#form-img-range').val() ? $('#form-img-range').val() : null;
    let contentWidthRange = $('#form-content-range-width').val() ? $('#form-content-range-width').val() : null;
    let contentHeightRange = $('#form-content-range-height').val() ? $('#form-content-range-height').val() : null;
    let imgFontSizeRange = $('#form-image-range-fontsize').val() ? $('#form-image-range-fontsize').val() : null;
    let contentTitleFontSizeRange = $('#form-content-title-range-fontsize').val() ? $('#form-content-title-range-fontsize').val() : null;
    let contentfontSizeRange = $('#form-content-range-fontsize').val() ? $('#form-content-range-fontsize').val() : null;
    let contentBottomFontSizeRange = $('#form-content-bottom-range-fontsize').val() ? $('#form-content-bottom-range-fontsize').val() : null;
    let contentMargintop = $('#form-content-margin-top').val() ? $('#form-content-margin-top').val() : null;
    let contentPaddingLeft = $('#form-content-padding-left').val() ? $('#form-content-padding-left').val() : null;
    let codes = $('#form-code').val() ? $('#form-code').val() : null;
    //let checkPosition = 0;
    let position = $('#form-position').val() ? $('#form-position').val() : 1;
    let created_at = Date.now();
    let updated_at = Date.now();


    let idRow = makeid(5);
    let data = {
        imgTitle,
        imgSubTitle,
        contentTitle,
        contentSubTitle,
        selectSocialMedia,
        selectSocialMediaWidth,
        selectSocialMediaHeight,
        bgColor,
        typeLanguage,
        typeLanguageBgColor,
        typeLanguageTitle,
        typeLanguageSubTitle,
        id_blog_content_type_group,
        id_blog_content_type,
        checkImageBox,
        //checkContentBorderLeft,
        //checkContentBorderRight,
        //checkContentDirectionRight,
        checkPreviewBox,
        imgRange,
        contentWidthRange,
        contentHeightRange,
        imgFontSizeRange,
        contentPaddingLeft,
        contentTitleFontSizeRange,
        contentfontSizeRange,
        contentBottomFontSizeRange,
        contentMargintop,
        codes,
        idRow,
        //checkPosition,
        position
    };
    var url = $("#url").val();

    $.ajax({
        // la URL para la petición
        url: url,// + '/blog-content-generate'
        data: JSON.stringify(data),
        type: 'POST',
        // el tipo de información que se espera de respuesta
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': document.head.querySelector("[name=csrf-token]").content,
        },
        success: function (data) {
            alert("creado");
            console.log(data)
        },

        error: function (xhr, status) {

        }
    });

    //https://codeanddeploy.com/blog/laravel/laravel-8-ajax-post-request-example
});
