document.addEventListener('DOMContentLoaded', () => {
    const langSelector = document.getElementById('lang-selector');
    const themeSelector = document.getElementById('theme-selector');
    const codeEditor = document.getElementById('form-editor');
    const codeBlocks = document.querySelectorAll('pre.pre-hljs code, .card-publish pre code');
    const themeLink = document.createElement('link');
    themeLink.rel = 'stylesheet';
    document.head.appendChild(
    themeLink); // Asumiendo que siempre se va a usar, lo agregamos desde el inicio

    function updateLanguageDisplay() {
        const selectedLanguage = langSelector.options[langSelector.selectedIndex].text;
        $('#language').text(selectedLanguage);
    }

    function loadTheme(theme) {
        themeLink.href =
            `https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/styles/${theme}.min.css`;
        const codeTheme = document.querySelector('pre.pre-hljs');
        const backgroundColor = window.getComputedStyle(codeTheme.querySelector('code')).backgroundColor;

        const cardPublishTheme = document.querySelector('.card-publish');
        cardPublishTheme.style.backgroundColor = backgroundColor;
        codeTheme.style.backgroundColor = backgroundColor;
    }

    function updateCode() {
        codeBlocks.forEach(block => {
            block.textContent = codeEditor.value;
            block.className = langSelector.value;
            hljs.highlightElement(block);
        });
        loadTheme(themeSelector.value);
    }

    langSelector.addEventListener('change', () => {
        updateLanguageDisplay();
        updateCode();
    });
    themeSelector.addEventListener('change', updateCode);
    codeEditor.addEventListener('input', updateCode);

    updateLanguageDisplay();
    loadTheme(themeSelector.value);
});
