$(document).ready(function () {
    const selectedOption = $('#form-subtitle');
    const optionTitle = $('#form-title').val();
    const optionSubTitle = $('#form-subtitle').val();
    const color = $("#form-bg-color").val();
    const src = $('#form-background').val();
    const img = window.baseContentType + src;
    let width = '4px';
    $('.card-publish').css({
        'border-color': color,
        'border-width': width
    });
    $('.card-publish div.position').css({
        'background-color': color,
    });

    $('.card-publish img').attr('src', img);
    $('.card-publish .title').text(optionTitle);
    $('.card-publish .subtitle').text(optionSubTitle);


    $('#form-background').on('change', function() {
        const selectedOption = $('#form-background option:selected');
        const optionBackgroundColor = selectedOption.attr('attr-bg-color');
        const optionTitle = $('#form-title').val();
        const optionSubTitle = $('#form-subtitle').val();
        const optionTypeHighlight = selectedOption.attr('attr-type-highlight');
        const src = $('#form-background').val();
        const img = window.baseContentType + src;

        $('#form-bg-color').val(optionBackgroundColor);
        $('#form-img-title').val(optionTitle);
        $('#form-img-subtitle').val(optionSubTitle);

        $('.card-publish').css({
            'border-color': optionBackgroundColor,
        });
        $('.card-publish div.position').css({
            'background-color': optionBackgroundColor,
        });

        $('.card-publish .title').text(optionTitle);
        $('.card-publish .subtitle').text(optionSubTitle);

        $('.card-publish pre code').removeClass().addClass(optionTypeHighlight);

        $('.card-publish img').attr('src', img);
    });

    $("#save").on("click", function () {
        const fields = [
            { id: "#form-id", attr: null },
            { id: '#form-type-group', attr: null},
            { id: "#form-title", attr: null },
            { id: "#form-subtitle", attr: null },
            { id: "#form-bg-color", attr: null },
            { id: "#form-background", attr: null },
            {
                id: "#form-background",
                attr: "attr-bg-color",
                isOptionAttr: true,
            },
            { id: "#form-background", attr: "attr-title", isOptionAttr: true },
            {
                id: "#form-background",
                attr: "attr-subtitle",
                isOptionAttr: true,
            },
            {
                id: "#form-background",
                attr: "attr-type",
                isOptionAttr: true,
                fallbackId: "#form-type",
            },
            { id: "#form-editor", attr: null },
            { id: '#form-height', attr: null },
            { id: "#form-position", attr: null, defaultValue: 1 },
            { id: "#form-activated", attr: null, defaultValue: 1 },
        ];

        let data = fields.reduce((acc, field) => {
            let value, attrValue;
            // Inicializa el objeto para el ID si aún no existe
            if (!acc[field.id]) {
                acc[field.id] = { value: null, attr: {} };
            }
            if (field.isOptionAttr) {
                // Captura el valor del atributo especificado
                attrValue = $(field.id)
                    .find("option:selected")
                    .attr(field.attr);
                // Asume que también quieres el valor del campo si no se ha capturado antes
                if (acc[field.id].value === null) {
                    acc[field.id].value = $(field.id).val();
                }
            } else {
                // Especialmente para checkboxes
                if ($(field.id).attr('type') === 'checkbox') {
                    acc[field.id].value = $(field.id).is(':checked');
                } else {
                    // Captura el valor del campo si aún no se ha capturado
                    if (acc[field.id].value === null) {
                        acc[field.id].value = $(field.id).val() || field.defaultValue;
                    }
                }
            }
            // Guarda el valor del atributo si está especificado
            if (field.attr) {
                acc[field.id].attr[field.attr] = attrValue;
            }
            return acc;
        }, {});

        // Convierte el objeto en un objeto plano
        data = Object.entries(data).reduce((acc, [key, value]) => {
            if (value.attr) {
                acc[key] = { ...value.attr, value: value.value };
            } else {
                acc[key] = value.value;
            }
            return acc;
        }, {});

        data = {
            id: data["#form-id"]["value"],
            idBlogContentTypeGroup: data['#form-type-group']['value'],
            idBlogContentType: data["#form-background"]["attr-type"],
            contentTitle: data["#form-title"]["value"],
            contentSubTitle: data["#form-subtitle"]["value"],
            codes: data["#form-editor"]["value"],
            height: data["#form-height"]["value"],
            position: data["#form-position"]["value"],
            activated: data["#form-activated"]["value"],
        };

        $.ajax({
            url: base + "/blog-content-highlight" + "/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            dataType: "json",
            headers: {
                'X-CSRF-TOKEN': document.head.querySelector("[name=csrf-token]")
                    .content,
            },
            success: function (data) {
                alert("actualizado");
                console.log(data);
            },
            error: function (xhr, status) {
                // Handle error
            },
        });
    });
});
