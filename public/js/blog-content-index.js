/******/
(() => { // webpackBootstrap
    var __webpack_exports__ = {};
    /*!********************************************!*\
      !*** ./resources/js/blog-content-index.js ***!
      \********************************************/
    var init = {
        modalDescription: $('#descriptionModal'),
        modalDescriptionContent: '',
        modalImage: '',
        modalName: ''
    };

    showModalDescription = function showModalDescription(id) {
        init.modalDescriptionContent = $('.admin-blog-content-index__btn-show-modal-description_' + id).data('content');
        init.modalImage = $('.admin-blog-content-index__btn-show-modal-description_' + id).data('image');
        init.modalName = $('.admin-blog-content-index__btn-show-modal-description_' + id).data('name');
        init.modalDescription.modal('show');
    };

    closeModalDescription = function closeModalDescription() {
        init.modalDescription.modal('hide');
    };

    init.modalDescription.on('show.bs.modal', function(event) {
        init.modalDescription.find('.modal-body').empty();
        //init.modalDescription.find('.modal-body').append('<h1 class="text-center">' + init.modalName + '</h1>');
        init.modalDescription.find('.modal-body').html(init.modalDescriptionContent);
        //init.modalDescription.find('.modal-body').append('<img src="../img/blog/' + init.modalImage + '" style="  margin-top:1em;   width: 100%;"/>');
    });
    /******/
})();