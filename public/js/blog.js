/******/
(() => { // webpackBootstrap
    var __webpack_exports__ = {};
    /*!******************************!*\
      !*** ./resources/js/blog.js ***!
      \******************************/
    var init = {
        modalDescription: $('#descriptionModal'),
        modalDescriptionContent: '',
        modalImage: '',
        modalName: ''
    };

    showModalDescription = function showModalDescription(id) {
        init.modalDescriptionContent = $('.blog-index__btn-content_' + id).data('content');
        init.modalImage = $('.blog-index__btn-content_' + id).data('image');
        init.modalName = $('.blog-index__btn-content_' + id).data('name');
        init.modalDescription.modal('show');
    };

    closeModalDescription = function closeModalDescription() {
        init.modalDescription.modal('hide');
    };

    init.modalDescription.on('show.bs.modal', function(event) {
        init.modalDescription.find('.modal-body').empty();
        //init.modalDescription.find('.modal-body').append('<h1 class="text-center">' + init.modalName + '</h1>');
        init.modalDescription.find('.modal-body').html(init.modalDescriptionContent);
        // init.modalDescription.find('.modal-body').append('<img src="../img/blog/' + init.modalImage + '" style="  margin-top:1em;   width: 100%;"/>');
    });


    /******/
})();
function downloadCanvas() {
    var canvas = $('#content-images').find('canvas')[0].getContext('2d');
    var img = new Image();
    img.onload = function () {
        canvas.width = this.width
        canvas.height = this.height
        canvas.getContext('2d').drawImage(this, 0, 0)
        canvas.toDataURL()
        canvas.getContext('2d').getImageData(0, 0, 100, 100)
    };
    img.src = 'http://jerssonarrivasplata2.com/img/icon.png';

  /*  link.download = "my-image.png";
    link.href = imageUrl;
    link.click();*/
}
