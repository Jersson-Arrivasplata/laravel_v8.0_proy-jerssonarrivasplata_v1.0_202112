/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*********************************!*\
  !*** ./resources/js/globals.js ***!
  \*********************************/
(function () {
  menu = function menu() {
    var headerClass = document.getElementsByTagName('header')[0].classList;
    var classVerify = 'header-close';
    headerClass.contains(classVerify) ? headerClass.remove(classVerify) : headerClass.add(classVerify);
  };

  loaderWithHash = function loaderWithHash() {
    if (location.pathname !== '/') {
      loader();
    }
  };

  loaderWithOutHash = function loaderWithOutHash() {
    loader();
  };

  loader = function loader() {
    var loaderClass = document.getElementsByClassName('loader')[0].classList;
    var classVerify = 'd-none';
    if (loaderClass.contains(classVerify)) loaderClass.remove(classVerify);
  };
})();
/******/ })()
;